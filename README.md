# Residual Distribution high order code

Authors: Rémi Abgrall, Paola Bacigaluppi, Lorenzo Micalizzi, Philipp Öffner, Svetlana Tokareva, Davide Torlo and Fatemeh Mojarrad

Corresponding email: remi.abgrall@math.uzh.ch

## Features

* Residual distribution as discretization technique
* Deferred correction for high order accurate time integration
* Models: linear transport, Burgers, wave equation, shallow water, Euler
* 2D and 1D
* Order of accuracy from 2 to 4 (2D)  and order 5 (1D)
* 2D meshes can be triangular, quadrilateral and hybrid
* Mood a posteriori limiter to guarantee physical constraints
* Extra conservation constraints possible (entropy/kinetic energy/momentum)


**Warning**: works for Euler, the other models need to be checked.

### 1D code
To use the 1D code, all the information is in the [README](/Test1D/README.md) in folder [Test1D](/Test1D), where all the executions should be made (compilation and running of the tests.

### Compilation
Please find the Makefile in Test1D and Test2D accordingly.
In 1D the file is called `Makefile_1D.gfortran`, while in 2D it is `Makefile 2D.gfortran`. To compile go in the Test1D or Test2D folder and type:
```
make -f Make/Makefile_2D.gfortran dec

make -f Make/Makefile_2D.gfortran clean
```
Note #1: The Makefile in Test# D uses gfortran. To compile with different settings, check the folder Make
on the main folder.
Note #2: `LIBS=-L/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib` for mac.
Note #3: According to the desired system set-up, uncomment/comment the required modules in the Makefile. The only modules to be changed are the `variable_def_*` and `init_bc *`.

### Meshes
How to generate compatible meshes with gmsh:
```
gmsh format msh2: gmsh -format msh2 geofile.geo
```

### How to run a test
In the Makefile the tests should match (variable_def, boundary, init_bc), example:
test case: see Boundary_euler and init_bc_euler (they should be consistant). 

### Reference:
1. R. Abgrall, J. Nordström, P. Öffner and S. Tokareva, Analysis of the SBP-SAT stabilisation for finite element methods, part II: the non-linear case, Communications in Applied Mathematics and Computation, 2021, DOI: \url{10.1007/s42967-020-00086-2}
1. R. Abgrall, J. Nordström, P. Öffner and S. Tokareva, Analysis of the SBP-SAT stabilisation for finite element methods, part I: the linear case,  J. Sci. Comput. 85 (2020), no. 2, Paper No. 43.
1. R. Abgrall and D. Torlo, High Order Asymptotic Preserving Deferred Correction Implicit-Explicit Schemes for Kinetic Models,  SIAM SISC, 2020,  v42(3), pp B816-845, https://arxiv.org/abs/1811.09284

1. R. Abgrall, A general framework to construct schemes satisfying additional conservation relations, application to entropy conservative  and entropy dissipative schemes, J. Comput. Phys, vol 372(1), 2018

1. R. Abgrall, P. Bacigaluppi and S. Tokareva, High-order residual distribution scheme for the time-dependent Euler equations of fluid dynamics, Computer \& Mathematics with Applications, 2019, vol 78 (2), pages  274-297
1. R. Abgrall, Some remarks about conservation for residual distribution schemes, Computational Methods in Applied Mathematics, v18(3), pp 327-350, 2018, doi: https://doi.org/10.1515/cmam-2017-0056
1. R. Abgrall, P. Bacigaluppi and S. Tokareva, A high-order nonconservative approach for hyperbolic equations in fluid dynamics, Computers and Fluids, vol 169, pages 10-22, 2018
 doi: https://doi.org/10.1016/j.compfluid.2017.08.019
1. R. Abgrall, High order schemes for hyperbolic problems using globally continuous approximation and avoiding mass matrices., Journal of Scientific Computing, 73(2-3), pp 461-494, 2017
