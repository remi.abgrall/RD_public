!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE variable_def
  !------------------------------------------------------------------------------------------------
  ! MODULE SPECIFICALLY DESIGNED FOR 1D SYSTEM OF EULER EQUATIONS
  !------------------------------------------------------------------------------------------------
  ! This module collects all the information related to the following items:
  ! - equations of state: pressure, internal specific energy, speed of sound
  ! - conversion from conservative to primitive and viceversa
  ! - definition of the Jacobian of the considered system
  ! - definition of the Fluxes of the considered system
  ! - definition of the Jacobian in absolute values as: |J|=R* |lambda| *L (cf. AbsJacobian_eul)
  ! - definition of the Eigenvalues (cf. evalues_eul)
  ! - defintion of the Spectral Radius = max ( |lambda|)
  ! - definition of the Right eigenvectors
  ! - definition of the Left eigenvectors
  ! - definition of the positive and negative Jacobian

  
  USE algebra
  use precision

  IMPLICIT NONE

  INTEGER, PUBLIC, PARAMETER:: n_vars = 3   ! number of primitive variables
  INTEGER, PUBLIC, PARAMETER:: n_dim  = 1   ! number of physical dimensions
  REAL(dp),         PARAMETER:: pi=ACOS(-1._dp) ! pi
  REAL(dp), PUBLIC           :: gmm=1.4_dp      ! EOS parameters


  ! Type for the vector of primitive variables
  TYPE, PUBLIC :: PVar
     INTEGER                              :: NVars = n_vars
     REAL(dp), DIMENSION(n_vars)           :: U

   CONTAINS 
     PROCEDURE, PUBLIC:: flux            => flux_eul
     PROCEDURE, PUBLIC:: source          => source_euler
     !     PROCEDURE, PUBLIC:: evalues         => evalues_eul
     PROCEDURE, PUBLIC:: spectral_radius => spectral_radius_eul
     PROCEDURE, PUBLIC:: rvectors        => rvectors_eul
     PROCEDURE, PUBLIC:: lvectors        => lvectors_eul
     PROCEDURE, PUBLIC:: Jacobian        => Jacobian_eul
     PROCEDURE, PUBLIC:: AbsJacobian     => AbsJacobian_eul
     PROCEDURE, PUBLIC:: Nmat            => Nmat_eul
  END TYPE PVar

  PRIVATE
  PUBLIC:: pEOS, epsEOS, convert_cons2prim, convert_prim2cons

CONTAINS

  !--------------------------
  ! p = EOS(rho,eps)
  !--------------------------
  FUNCTION pEOS(rho,eps) RESULT(p)
    REAL(dp), INTENT(in) :: rho, eps
    REAL(dp)             :: p
        p = (gmm-1.0_dp)*rho*eps

  END FUNCTION pEOS

  !--------------------------
  ! eps = EOS(rho,p)
  !--------------------------
  FUNCTION epsEOS(rho,p) RESULT(eps)
    REAL(dp), INTENT(in) :: rho, p
    REAL(dp)             :: eps
    eps = p/((gmm-1.0_dp)*rho)
  END FUNCTION epsEOS

  !--------------------------
  ! Speed of sound
  !--------------------------
  FUNCTION SoundSpeed(rho,p) RESULT(a)
    REAL(dp), INTENT(in) :: rho, p
    REAL(dp)             :: a
    a = SQRT(gmm*p/rho)
  END FUNCTION SoundSpeed

  !-----------------------------------------------
  ! Calulate pressure from conservative variables
  !-----------------------------------------------
  FUNCTION getPressureFromCons(Q) RESULT(p)
    TYPE(Pvar), INTENT(in) :: Q
    REAL(dp)                :: rho, eps, p
    rho = Q%U(1)
    eps = ( Q%U(3) - 0.5*Q%U(2)**2/Q%U(1) )/Q%U(1)
    p = pEOS(rho,eps)
  END FUNCTION getPressureFromCons

  !---------------------------------------------
  ! Convert conservative variables to primitive
  !---------------------------------------------
  FUNCTION convert_cons2prim(Q) RESULT(W)
    TYPE(Pvar), INTENT(in) :: Q
    TYPE(Pvar)             :: W
    REAL(dp)                :: eps
    ! rho
    W%U(1) = Q%U(1)
    ! u
    W%U(2) = Q%U(2)/Q%U(1)
    ! p
    eps    = Q%U(3)/Q%U(1) - 0.5_dp*( Q%U(2)**2.0 )/Q%U(1)**2.0_dp
    W%U(3) = pEOS(W%U(1),eps)
  END FUNCTION convert_cons2prim

  !---------------------------------------------
  ! Convert primitive variables to conservative
  !---------------------------------------------
  FUNCTION convert_prim2cons(W) RESULT(Q)
    TYPE(Pvar), INTENT(in) :: W
    TYPE(Pvar)             :: Q
    REAL(dp)                :: eps
    ! q1 = rho
    Q%U(1) = W%U(1)
    ! q2 = rho*u
    Q%U(2) = W%U(1)*W%U(2)
    ! q3 = E = rho*eps + 0.5*rho*u^2.0
    eps    = epsEOS(W%U(1),W%U(3))
    Q%U(3) = W%U(1)*eps + 0.5_dp*W%U(1)*W%U(2)**2.0_dp
  END FUNCTION convert_prim2cons


!!!!-----------------------


  FUNCTION roe_eul(e,u,n) RESULT (J)
    ! evaluate a roe average to estimate a rough speed for 
    ! Burman jump operator
    CLASS(Pvar), INTENT(in):: e
    REAL(dp),DIMENSION(N_Vars), INTENT(in):: u
    REAL(dp), DIMENSION(n_dim), INTENT(in):: n ! here it will be a normal of norme 1
    REAL(dp), DIMENSION(n_vars,n_vars):: J
    REAL(dp), DIMENSION(n_vars,n_vars,n_dim):: JJ
    REAL(dp),DIMENSION(n_dim):: v=0._dp
    JJ=Jacobian_eul(e,v)
    J(:,:) = JJ(:,:,1)*n(1)

  END FUNCTION roe_eul

  FUNCTION flux_eul(Var,x) RESULT(f)
    CLASS(PVar),                  INTENT(in) :: Var    ! vector of conservative variables
    REAL(dp),       DIMENSION(n_dim), INTENT(in) :: x
    TYPE(PVar), DIMENSION(n_dim)             :: f
    REAL(dp) :: eps, p

    eps = Var%u(3)/Var%u(1) - 0.5_dp*( Var%u(2)/Var%u(1))**2_dp
    !    print*, 'p in flux'
    !    print*, 'Var = ', Var%u
    p = pEOS(Var%u(1),eps)
    !    print*, 'after p in flux'

    F(1)%u(1) = Var%u(2)
    F(1)%u(2) = Var%u(2)**2_dp/Var%u(1) + p
    F(1)%u(3) = Var%u(2)*( Var%u(3) + p )/Var%u(1)

  END FUNCTION flux_eul

  FUNCTION source_euler(e,x,test) RESULT(s)
    CLASS(PVar),                INTENT(IN) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(IN) :: x
    INTEGER ,                   INTENT(IN) :: test
    TYPE(PVar)                             :: s
    s%u = 0._dp
  END FUNCTION source_euler

  FUNCTION Jacobian_eul(Var,x) RESULT(J)
    CLASS(Pvar),              INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim),   INTENT(in) :: x
    REAL(dp), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    REAL(dp) :: Vi2, u, H, eps, p

    eps = Var%u(3)/Var%u(1) - 0.5_dp*( Var%u(2)**2_dp )/Var%u(1)**2._dp
    p = pEOS(Var%u(1),eps)

    u = Var%u(2)/Var%u(1)

    Vi2 = u**2._dp

    H = (Var%u(3) + p)/Var%u(1)

    J=0.
    J(1,:,1) = (/ 0.0_dp, 1.0_dp, 0.0_dp /)
    J(2,:,1) = (/ -u**2._dp + 0.5_dp*(gmm-1._dp)*Vi2, (3._dp-gmm)*u, gmm-1._dp /)
    J(3,:,1) = (/ u*( 0.5_dp*(gmm-1.)*Vi2 - H ), H - (gmm-1._dp)*u**2_dp, gmm*u /)

  END FUNCTION Jacobian_eul

  FUNCTION AbsJacobian_eul(Var,x) RESULT(J)
    !compute abs value of jacobian matrix
    REAL(dp), DIMENSION(n_dim), PARAMETER:: nn=(/1.0_dp/),xx=(/1.0_dp/)
    CLASS(Pvar),              INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim),   INTENT(in) :: x
    REAL(dp), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    REAL(dp) :: Vi2, u, H, eps, p
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: L
    REAL(dp), DIMENSION(n_Vars,n_Vars) :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    INTEGER:: i

    R=rvectors_eul(Var,nn)
    L=lvectors_eul(Var,nn)
    lambda=evalues_eul(var,xx,nn)
    lambda=ABS(lambda)

    J(:,:,1)=MATMUL(R,MATMUL(lambda,L))

  END FUNCTION AbsJacobian_eul

  FUNCTION evalues_eul(Var,x,n) RESULT(lambda)
    ! eigenvalues: diagonal matrix. It is written as a matrix for ease of calculations
    CLASS(PVar),            INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim)             :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda

    REAL(dp), DIMENSION(n_vars, n_vars, n_dim) :: J
    REAL(dp)    :: un, c, p, eps

    eps = Var%u(3)/Var%u(1) - 0.5_dp*( Var%u(2)**2._dp)/Var%u(1)**2._dp
    p   = pEOS(Var%u(1),eps)
    c   = SoundSpeed(Var%u(1),p)

    lambda = 0._dp
    un = ( Var%u(2)*n(1) )/Var%u(1)
    lambda(1,1) = un-c
    lambda(2,2) = un
    lambda(3,3) = un+c

  END FUNCTION evalues_eul

  REAL(dp) FUNCTION spectral_radius_eul(Var,x,n)
    ! compute the maximum value of eigenvalues:
    ! max_i {lambda_ii}
    CLASS(PVar),            INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda1, lambda2
    REAL(dp):: vi, c, p, eps
    REAL(dp),DIMENSION(n_Vars,n_Vars)      :: lambda

    lambda = evalues_eul(Var,x,n)
    spectral_radius_eul = MAXVAL(ABS(lambda))

  END  FUNCTION spectral_radius_eul

  FUNCTION rvectors_eul(Q,n) RESULT(R)
    ! right e-vectors
    ! assume ||n||=1
    CLASS(PVar),           INTENT(in) :: Q
    REAL(dp), DIMENSION(n_dim)         :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars) :: R
    REAL(dp) :: rho, u, p, a, E, H, eps

    rho = Q%U(1)
    u   = Q%U(2)/Q%U(1)
    eps = Q%u(3)/Q%u(1) - 0.5_dp*( Q%u(2)**2._dp )/Q%u(1)**2._dp
    p   = pEOS(Q%u(1),eps)
    a   = SoundSpeed(rho,p)
    E   = Q%U(3)
    H   = (E+p)/rho

    R(:,1) = (/ 1.0_dp, u-a, H-u*a /)
    R(:,2) = (/ 1.0_dp, u, 0.5_dp*u**2._dp /)
    R(:,3) = (/ 1.0_dp, u+a, H+u*a /)


  END FUNCTION rvectors_eul

  FUNCTION lvectors_eul(Q,n) RESULT(L)
    ! left e-vectors
    ! assumes ||n||=1
    CLASS(PVar),            INTENT(in) :: Q
    REAL(dp), DIMENSION(n_dim)          :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: L
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: R
    REAL(dp) :: rho, u, p, a, E, H, gmm1, eps
        R=rvectors_eul(Q,n)
        L=inverse(R)

  END FUNCTION lvectors_eul



  FUNCTION Nmat_eul(e, n_ord, grad, x) RESULT (Nmat)
    CLASS(Pvar),                  INTENT(in) :: e
    INTEGER,                      INTENT(in) :: n_ord
    REAL(dp), DIMENSION(n_dim),       INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim,n_ord), INTENT(in) :: grad
    REAL(dp), DIMENSION(n_vars,n_vars)           :: Nmat
    REAL(dp), DIMENSION(n_vars, n_vars, n_dim)   :: J
    INTEGER:: l

    J= Jacobian_eul(e,x)
    Nmat=0_dp
    DO l=1, n_ord
       Nmat = Nmat + ABS( grad(1,l)*J(1,1,1) )
    ENDDO
    Nmat =0._dp!Inverse(Nmat)

  END FUNCTION Nmat_eul
  FUNCTION min_mat_eul(e, x, n, alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp),                   INTENT(in) :: alpha
    REAL(dp), DIMENSION(n_dim), INTENT(IN) :: n
    REAL(dp), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_eul(e,x,n)
    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( MIN(lambda(:,:),alpha), L) )

  END FUNCTION min_mat_eul


  FUNCTION max_mat_eul(e,x,n,alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp),                   INTENT(in) :: alpha
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: n
    REAL(dp), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_eul(e,x,n)
    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( MAX(lambda(:,:),alpha), L) )

  END FUNCTION max_mat_eul
  !--------------------------




END MODULE variable_def
