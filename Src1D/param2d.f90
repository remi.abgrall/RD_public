!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE param2d
  USE element_class
  USE variable_def
  USE arete_class
  use precision
  IMPLICIT NONE
  REAL(8), PARAMETER:: Pi=ACOS(-1.0)
  
  
  TYPE maillage
     INTEGER:: ndofs
     INTEGER:: ns, nt
     TYPE(element), DIMENSION(:),ALLOCATABLE:: e
     REAL(dp),DIMENSION(:),ALLOCATABLE:: aires
     INTEGER:: nsegmt
     TYPE(arete), DIMENSION(:), ALLOCATABLE:: edge
  END TYPE maillage

  TYPE variables
     REAL(dp):: dt
     INTEGER:: Ncells
     TYPE(PVar), DIMENSION(:,:),ALLOCATABLE:: ua, up
     TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: un
  END TYPE variables

  TYPE donnees
     INTEGER:: iordret ! also defines the number of levels in the Dec
     REAL(dp):: cfl
     INTEGER:: ktmax
     REAL(dp):: tmax
     INTEGER:: ifre
     INTEGER:: ischema
     INTEGER:: iter
     INTEGER:: nt, itype
     REAL(dp):: Length, domain_left
     REAL(dp):: temps
     LOGICAL:: restart=.FALSE.
     REAL(dp):: alpha_jump
     REAL(dp):: alpha_jump2
     INTEGER:: test
     LOGICAL:: mood
     LOGICAL:: periodicBC

  END TYPE donnees
END MODULE param2d
