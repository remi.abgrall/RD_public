!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE postprocessing

  ! This module is intended to output the postprocessing files with the data to generate plots.
  ! The data can in general easily read via gnuplot or xmgrace (as example)
  ! In the data files are printed ALL the DOFs of the domain

  USE param2d
  USE init_bc
  USE Model
  USE variable_def
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  PUBLIC:: visu, descriptor!, visudeb !,errors

CONTAINS

  SUBROUTINE visu(DATA, kt, mesh, var,folder)
    TYPE(Donnees), INTENT(inout):: DATA
    INTEGER, INTENT(in):: kt
    TYPE(maillage), INTENT(in):: mesh
    TYPE(variables), INTENT(in):: var

    CHARACTER(len=100), INTENT(in)::  folder
    INTEGER::jt, l, k, ios, lp, is
    TYPE(element):: e
    TYPE(Pvar),DIMENSION(:),ALLOCATABLE::v,u,w
    REAL(dp), DIMENSION(:), ALLOCATABLE:: base
    REAL(dp), DIMENSION(:,:), ALLOCATABLE:: grad, x
    TYPE(PVar), DIMENSION(:), ALLOCATABLE:: sol
    CHARACTER(LEN = 75) :: nombre, nt_str

    WRITE(nombre, *) kt
    WRITE(nt_str, *) Mesh%nt
    call system('mkdir -p ' //folder )

    OPEN( UNIT = 1, FILE = trim(FOLDER)//"/"//"/sol_cons_last.dat", IOSTAT = ios )
    OPEN( UNIT = 2, FILE = trim(FOLDER)//"/"//"/ex_prim_last.dat", IOSTAT = ios )
    OPEN( UNIT = 3, FILE = trim(FOLDER)//"/"//"/error_last.dat", IOSTAT = ios )
    OPEN( UNIT = 4, FILE = trim(FOLDER)//"/"//"/sol_prim_last.dat", IOSTAT = ios )
    OPEN( UNIT = 80, FILE = trim(FOLDER)//"/"//"/sol_prim_nodal_last.dat", IOSTAT = ios )
    OPEN( UNIT = 5, FILE = trim(FOLDER)//"/"//"/sol_prim_"//TRIM(ADJUSTL(nombre))//".dat", IOSTAT = ios )
    OPEN( UNIT = 50, FILE = trim(FOLDER)//"/"//"/ex_prim_"//TRIM(ADJUSTL(nombre))//".dat", IOSTAT = ios )
    OPEN( UNIT = 7, FILE = trim(FOLDER)//"/"//"/FluxIndicator_"//TRIM(ADJUSTL(nt_str))//"_"//TRIM(ADJUSTL(nombre))//".dat", IOSTAT = ios )
    OPEN( UNIT = 9, FILE = trim(FOLDER)//"/"//"/DiagIndicator_"//TRIM(ADJUSTL(nt_str))//"_"//TRIM(ADJUSTL(nombre))//".dat", IOSTAT = ios )
    OPEN( UNIT = 60, FILE = trim(FOLDER)//"/"//"/DiagIndicator2_"//TRIM(ADJUSTL(nt_str))//"_"//TRIM(ADJUSTL(nombre))//".dat", IOSTAT = ios )


    OPEN( UNIT = 70, FILE = trim(FOLDER)//"/"//"/FluxIndicator_last.dat", IOSTAT = ios )
    OPEN( UNIT = 90, FILE = trim(FOLDER)//"/"//"/DiagIndicator_last.dat", IOSTAT = ios )




    OPEN( UNIT = 8, FILE = trim(FOLDER)//"/"//"/Entropy_"//TRIM(ADJUSTL(nt_str))//".dat", IOSTAT = ios )
!!$
!!$    
!!$
    REWIND(1); REWIND(2); REWIND(3); REWIND(4); REWIND(5);REWIND(7); REWIND(8); REWIND(9);REWIND(70);REWIND(90);REWIND(80);REWIND(50);REWIND(60)
!!$
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(sol(e%nsommets),u(e%nsommets),v(e%nsommets),w(e%nsommets))
       ALLOCATE(base(e%nsommets),grad(n_dim,e%nsommets))
       DO l=1, e%nsommets
          sol(l)=IC(e%coor(l)-DATA%temps,DATA)
          u(l)=Var%ua(e%nu(l),0)
          u(l)=Var%ua(e%nu(l),0)
       ENDDO
       DO l=1, e%nsommets
          DO k=1, e%nsommets
             base(k)=e%base(k,e%x(:,l))
             grad(:,k)=e%gradient(k,e%x(:,l))
          ENDDO
          v(l)=e%eval_func(u,e%x(:,l))
          w(l)=convert_cons2prim(v(l))
       ENDDO
!!$
    WRITE(7,*) SUM(e%coor)/REAL(e%nsommets), Mesh%e(jt)%type_flux
    WRITE(9,*) SUM(e%coor)/REAL(e%nsommets), Mesh%e(jt)%diag
    WRITE(60,*) SUM(e%coor)/REAL(e%nsommets), Mesh%e(jt)%diag2
  
     WRITE(70,*) SUM(e%coor)/REAL(e%nsommets), Mesh%e(jt)%type_flux
    WRITE(90,*) SUM(e%coor)/REAL(e%nsommets), Mesh%e(jt)%diag
!!$
       SELECT CASE(e%itype)
       CASE(1,11)
          WRITE(1,*) e%coor(1),v(1)%u, u(1)%u
          WRITE(1,*) e%coor(2),v(2)%u, u(2)%u
          WRITE(2,*) e%coor(1),sol(1)%u
          WRITE(2,*) e%coor(2),sol(2)%u
          WRITE(3,*) e%coor(1),sol(1)%u-v(1)%u
          WRITE(3,*) e%coor(2),sol(2)%u-v(2)%u
          WRITE(4,*) e%coor(1),w(1)%u
          WRITE(4,*) e%coor(2),w(2)%u
          WRITE(5,*) e%coor(1),w(1)%u
          WRITE(5,*) e%coor(2),w(2)%u
          WRITE(50,*) e%coor(1),sol(1)%u
          WRITE(50,*) e%coor(2),sol(2)%u
          WRITE(80,*) e%coor(1),w(1)%u
          WRITE(80,*) e%coor(2),w(2)%u
       CASE(2,3,12)
          WRITE(1,*) e%coor(1),v(1)%u, u(1)%u
          WRITE(1,*) e%coor(3),v(3)%u, u(3)%u
          WRITE(1,*) e%coor(2),v(2)%u, u(2)%u
          WRITE(2,*) e%coor(1),sol(1)%u
          WRITE(2,*) e%coor(3),sol(3)%u
          WRITE(2,*) e%coor(2),sol(2)%u
          WRITE(3,*) e%coor(1),sol(1)%u-v(1)%u
          WRITE(3,*) e%coor(3),sol(3)%u-v(3)%u
          WRITE(3,*) e%coor(2),sol(2)%u-v(2)%u
          WRITE(4,*) e%coor(1),w(1)%u
          WRITE(4,*) e%coor(3),w(3)%u
          WRITE(4,*) e%coor(2),w(2)%u
          WRITE(5,*) e%coor(1),w(1)%u
          WRITE(5,*) e%coor(3),w(3)%u
          WRITE(5,*) e%coor(2),w(2)%u
          WRITE(50,*) e%coor(1),sol(1)%u
          WRITE(50,*) e%coor(3),sol(3)%u
          WRITE(50,*) e%coor(2),sol(2)%u
          WRITE(80,*) e%coor(1),w(1)%u
          WRITE(80,*) e%coor(2),w(2)%u

       CASE(4,5,13)
          WRITE(1,*) e%coor(1),v(1)%u, u(1)%u
          WRITE(1,*) e%coor(3),v(3)%u, u(3)%u
          WRITE(1,*) e%coor(4),v(4)%u, u(4)%u
          WRITE(1,*) e%coor(2),v(2)%u, u(2)%u
          WRITE(2,*) e%coor(1),sol(1)%u
          WRITE(2,*) e%coor(3),sol(3)%u
          WRITE(2,*) e%coor(4),sol(4)%u
          WRITE(2,*) e%coor(2),sol(2)%u
          WRITE(3,*) e%coor(1),sol(1)%u-v(1)%u
          WRITE(3,*) e%coor(3),sol(3)%u-v(3)%u
          WRITE(3,*) e%coor(4),sol(4)%u-v(4)%u
          WRITE(3,*) e%coor(2),sol(2)%u-v(2)%u
          WRITE(4,*) e%coor(1),w(1)%u
          WRITE(4,*) e%coor(3),w(3)%u
          WRITE(4,*) e%coor(4),w(4)%u
          WRITE(4,*) e%coor(2),w(2)%u
          WRITE(5,*) e%coor(1),w(1)%u
          WRITE(5,*) e%coor(3),w(3)%u
          WRITE(5,*) e%coor(4),w(4)%u
          WRITE(5,*) e%coor(2),w(2)%u

          WRITE(50,*) e%coor(1),sol(1)%u
          WRITE(50,*) e%coor(3),sol(3)%u
          WRITE(50,*) e%coor(4),sol(4)%u
          WRITE(50,*) e%coor(2),sol(2)%u
          WRITE(80,*) e%coor(1),w(1)%u
          WRITE(80,*) e%coor(2),w(2)%u

       CASE(6,7,14)
          WRITE(1,*) e%coor(1),v(1)%u, u(1)%u
          WRITE(1,*) e%coor(3),v(3)%u, u(3)%u
          WRITE(1,*) e%coor(4),v(4)%u, u(4)%u
          WRITE(1,*) e%coor(5),v(5)%u, u(5)%u
          WRITE(1,*) e%coor(2),v(2)%u, u(2)%u
          WRITE(2,*) e%coor(1),sol(1)%u
          WRITE(2,*) e%coor(3),sol(3)%u
          WRITE(2,*) e%coor(4),sol(4)%u
          WRITE(2,*) e%coor(5),sol(5)%u
          WRITE(2,*) e%coor(2),sol(2)%u
          WRITE(3,*) e%coor(1),sol(1)%u-v(1)%u
          WRITE(3,*) e%coor(3),sol(3)%u-v(3)%u
          WRITE(3,*) e%coor(4),sol(4)%u-v(4)%u
          WRITE(3,*) e%coor(5),sol(5)%u-v(5)%u
          WRITE(3,*) e%coor(2),sol(2)%u-v(2)%u
          WRITE(4,*) e%coor(1),w(1)%u
          WRITE(4,*) e%coor(3),w(3)%u
          WRITE(4,*) e%coor(4),w(4)%u
          WRITE(4,*) e%coor(5),w(5)%u
          WRITE(4,*) e%coor(2),w(2)%u
          WRITE(5,*) e%coor(1),w(1)%u
          WRITE(5,*) e%coor(3),w(3)%u
          WRITE(5,*) e%coor(4),w(4)%u
          WRITE(5,*) e%coor(5),w(5)%u
          WRITE(5,*) e%coor(2),w(2)%u

          WRITE(50,*) e%coor(1),sol(1)%u
          WRITE(50,*) e%coor(3),sol(3)%u
          WRITE(50,*) e%coor(4),sol(4)%u
          WRITE(50,*) e%coor(5),sol(5)%u
          WRITE(50,*) e%coor(2),sol(2)%u
          WRITE(80,*) e%coor(1),w(1)%u
          WRITE(80,*) e%coor(2),w(2)%u
       END SELECT
       DEALLOCATE(sol,v,u,w,base,grad)
    ENDDO
    CLOSE(1);CLOSE(2);CLOSE(3); CLOSE(4);CLOSE(5);CLOSE(7);CLOSE(8);CLOSE(9);CLOSE(70); CLOSE(90);CLOSE(80); CLOSE(50); close(60);

  END SUBROUTINE visu

  SUBROUTINE descriptor(folder,data)
      character(len=100)::folder
      character(len=100)::filename
      TYPE(donnees):: DATA
      call system('mkdir -p ' //folder )
      filename = '/Descriptor'
      open(unit=10,status="REPLACE",file= trim(folder)//trim(filename)//'.dat')
      write(10,*)  data%nt, data%itype,data%ischema,data%test

      !write(10,*) 'itype = ', data%itype
      !write(10,*) 'ischema = ', data%ischema
      !write(10,*) 'test = ', data%test
      close(10)
   END SUBROUTINE descriptor
END MODULE postprocessing
