!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE init_bc

  USE param2d
  USE overloading
use precision
  IMPLICIT NONE

CONTAINS

!---------------------------------------
! Setup domain
!---------------------------------------

  SUBROUTINE init_geom(DATA)
    TYPE(donnees), INTENT(inout):: DATA

    SELECT CASE(DATA%test)
    CASE(1)
       ! wave packet
       DATA%domain_left = -1.0_dp
       DATA%Length      = 3.0_dp

    CASE default
       PRINT*, "Wrong test number for geom(), test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE init_geom

!---------------------------------------
! Set initial and boundary conditions
!---------------------------------------

  FUNCTION IC(x,DATA) RESULT(Var)
    REAL(dp),       INTENT(in)   :: x
    TYPE(donnees), INTENT(inout):: DATA
    TYPE(PVar) :: Var

    Var%u(1) = 0.0_dp
    Var%u(2) = EXP( -bb*(x-0.5_dp)**2_dp ) * ( aa*COS(aa*x) - 2._dp*bb*(x-0.5_dp)*SIN(aa*x) )

  END FUNCTION IC

  SUBROUTINE BC(Var,i1,iN,DATA,k)
    TYPE(variables), INTENT(inout):: Var
    INTEGER,         INTENT(in)   :: i1, iN, k
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(Pvar):: a0,a1
    INTEGER:: p1, pN

    SELECT CASE(DATA%test)
    CASE(1)
        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0.0_dp!a0+a1
        Var%un(pN)=0.0_dp!a0+a1
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

    CASE(-1,-2)
        ! outflow
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0.0_dp
        Var%un(pN)=0.0_dp

    CASE(-3)
        ! reflective
        Var%un(i1)%u(2) = 2.0_dp*Var%ua(k-1,i1)%u(2)
        Var%un(iN)%u(2) = 2.0_dp*Var%ua(k-1,iN)%u(2)

    CASE default
       PRINT*, "Wrong test number for wave_1D, test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE BC




  SUBROUTINE isPeriodic(DATA)
    TYPE(donnees),   INTENT(inout)   :: DATA

    SELECT CASE(DATA%test)
    CASE(1)
      DATA%periodicBC = .TRUE.
    CASE default
      DATA%periodicBC = .FALSE.
    END SELECT
  END SUBROUTINE isPeriodic

!---------------------------------------
! Exact solution
!---------------------------------------

  FUNCTION exact_wave(DATA,x,t) RESULT (U_ex)
    TYPE(donnees), INTENT(inout)          :: DATA
    REAL(dp),                   INTENT(in) :: x
    REAL(dp)                               :: t
    TYPE(PVar)                            :: U_ex, U0, U01, U02, W
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: L
    REAL(dp), DIMENSION(n_dim)             :: n = (/1.0/)

    ! U0 only needed to evaluate eigenvectors (constant)
    U0 = IC(x,DATA)
    lambda = U0%evalues((/x/),n)
    R      = U0%rvectors(n)
    L      = U0%lvectors(n)

    ! calculate ICs shifted in time
    U01 = IC(x-lambda(1,1)*t,DATA)  ! evaluated at lambda(1,1)
    U02 = IC(x-lambda(2,2)*t,DATA)  ! evaluated at lambda(2,2)

    ! calculate solution in characteristic variables
    W%U(1) = L(1,1)*U01%U(1) + L(1,2)*U01%U(2)
    W%U(2) = L(2,1)*U02%U(1) + L(2,2)*U02%U(2)

    ! transter back to original variables
    U_ex%U = MATMUL(R,W%U)

!    print*, 'lambda = ', lambda
!    print*, 'R = ', R
!    print*, 'L = ', L
!    print*, 'U01  = ', U01%U
!    print*, 'U02  = ', U02%U
!    print*, 'W    = ', W%U
!    print*, 'U_ex = ', U_ex%U

  END FUNCTION exact_wave

END MODULE init_bc
