MODULE init_bc

  USE param2d
  USE overloading
  USE precision

  IMPLICIT NONE

CONTAINS

!---------------------------------------
! Setup domain
!---------------------------------------

  SUBROUTINE init_geom(DATA)
    TYPE(donnees), INTENT(inout):: DATA

    SELECT CASE(DATA%test)
    CASE(0,10)
       ! wave packet
       DATA%domain_left = 0.0_dp
       DATA%Length      = 2.0_dp
    CASE(1)
       ! shock moving
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE default
       PRINT*, "Wrong test number for geom(), test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE init_geom

!---------------------------------------
! Set initial and boundary conditions
!---------------------------------------

  FUNCTION IC(x,DATA) RESULT(Var)
    REAL(dp),       INTENT(in)   :: x
    TYPE(donnees), INTENT(inout):: DATA
    TYPE(PVar) :: Var

    SELECT CASE(DATA%test)
		CASE(0,10)
		  DATA%tmax = 0.5_dp
	    Var%u(1) = sin(ACOS(-1._dp)*x)*0.1_dp
    CASE(1)
      IF (x<0.5) THEN
        Var%u(1) = 1._dp
      ELSE
        Var%u(1) = 0._dp
      ENDIF
    CASE default
       PRINT*, "Wrong test number for geom(), test = ", DATA%test
       STOP
    END SELECT

  END FUNCTION IC

  SUBROUTINE BC(Var,i1,iN,DATA,k)
    TYPE(variables), INTENT(inout):: Var
    INTEGER,         INTENT(in)   :: i1, iN, k
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(Pvar):: a0,a1
    INTEGER:: p1, pN

    SELECT CASE(DATA%test)
    CASE(0)
        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0._dp!a0+a1
        Var%un(pN)=0._dp!a0+a1
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

    CASE(1,2)
        ! outflow
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0._dp
        Var%un(pN)=0._dp
        Var%ua(p1,k-1)%u(1) = 1._dp

    CASE(10)
        ! Dirichlet
        Var%ua(i1,k-1)%u(1) = sin(ACOS(-1.)*(-a*DATA%temps))*0.1_dp 
        Var%ua(iN,k-1)%u(1) = sin(ACOS(-1.)*(2.0-a*DATA%temps))*0.1_dp 

    CASE(100)
       ! weak periodic
       p1=i1
       pN=iN
       a0=Var%un(p1)
       a1=Var%un(pN)
       Var%un(p1)%u(1)=Var%un(p1)%u(1)+Var%dt*0.5*( Var%ua(k-1,p1)%u(1)-Var%ua(k-1,pN)%u(1) )
       Var%un(pN)%u(1)=Var%un(pN)%u(1)+Var%dt*0.5*( Var%ua(k-1,p1)%u(1)-Var%ua(k-1,pN)%u(1) )


    CASE default
       PRINT*, "Wrong test number for wave_1D, test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE BC

!---------------------------------------
! Exact solution
!---------------------------------------

  FUNCTION exact_scalar(DATA,x,t) RESULT (U_ex)
    TYPE(donnees), INTENT(inout)          :: DATA
    REAL(dp),                   INTENT(in) :: x
    REAL(dp), INTENT(in)                   :: t
    TYPE(Pvar)                        :: U_ex
    U_ex = IC(x-a*t,DATA)

!    print*, 'lambda = ', lambda
!    print*, 'R = ', R
!    print*, 'L = ', L
!    print*, 'U01  = ', U01%U
!    print*, 'U02  = ', U02%U
!    print*, 'W    = ', W%U
!    print*, 'U_ex = ', U_ex%U

  END FUNCTION exact_scalar

  SUBROUTINE isPeriodic(DATA)
    TYPE(donnees),   INTENT(inout)   :: DATA

    SELECT CASE(DATA%test)
    CASE(0,100)
      DATA%periodicBC = .TRUE.
    CASE default
      DATA%periodicBC = .FALSE.
    END SELECT
  END SUBROUTINE isPeriodic


END MODULE init_bc
