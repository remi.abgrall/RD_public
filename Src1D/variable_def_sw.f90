MODULE variable_def
  !------------------------------------------------------------------------------------------------
  ! MODULE SPECIFICALLY DESIGNED FOR 1D SYSTEM OF SW EQUATIONS
  !------------------------------------------------------------------------------------------------
  ! This module collects all the information related to the following items:
  ! - equations of state: pressure, internal specific energy, speed of sound
  ! - conversion from conservative to primitive and viceversa
  ! - definition of the Jacobian of the considered system
  ! - definition of the Fluxes of the considered system
  ! - definition of the Jacobian in absolute values as: |J|=R* |lambda| *L (cf. AbsJacobian_sw)
  ! - definition of the Eigenvalues (cf. evalues_sw)
  ! - defintion of the Spectral Radius = max ( |lambda|)
  ! - definition of the Right eigenvectors
  ! - definition of the Left eigenvectors
  ! - definition of the positive and negative Jacobian

  
    USE algebra
    USE precision
    USE utils
  IMPLICIT NONE

  INTEGER, PUBLIC, PARAMETER:: n_vars = 2   ! number of primitive variables
  INTEGER, PUBLIC, PARAMETER:: n_dim  = 1   ! number of physical dimensions
  REAL(dp),         PARAMETER:: pi=ACOS(-1._dp) ! pi
  REAL(dp), PUBLIC           :: grav=9.81_dp      ! EOS parameters


  ! Type for the vector of primitive variables
  TYPE, PUBLIC :: PVar
     INTEGER                              :: NVars = n_vars
     REAL(dp), DIMENSION(n_vars)           :: U

   CONTAINS 
     PROCEDURE, PUBLIC:: flux            => flux_sw
     PROCEDURE, PUBLIC:: source          => source_sw
     !     PROCEDURE, PUBLIC:: evalues         => evalues_eul
     PROCEDURE, PUBLIC:: spectral_radius => spectral_radius_sw
     PROCEDURE, PUBLIC:: rvectors        => rvectors_sw
     PROCEDURE, PUBLIC:: lvectors        => lvectors_sw
     PROCEDURE, PUBLIC:: Jacobian        => Jacobian_sw
     PROCEDURE, PUBLIC:: AbsJacobian     => AbsJacobian_sw
     PROCEDURE, PUBLIC:: Nmat            => Nmat_sw
  END TYPE PVar

  PRIVATE
  PUBLIC:: convert_cons2prim, convert_prim2cons

CONTAINS


  !--------------------------
  ! Speed of sound
  !--------------------------
  FUNCTION SoundSpeed(rho) RESULT(a)
    REAL(dp), INTENT(in) :: rho
    REAL(dp)             :: a
    a = SQRT(grav*rho)
  END FUNCTION SoundSpeed


  !---------------------------------------------
  ! Convert conservative variables to primitive
  !---------------------------------------------
  FUNCTION convert_cons2prim(Q) RESULT(W)
    TYPE(Pvar), INTENT(in) :: Q
    TYPE(Pvar)             :: W
    REAL(dp)                :: eps
    ! rho
    W%U(1) = Q%U(1)
    ! u
    W%U(2) = Q%U(2)/Q%U(1)
  END FUNCTION convert_cons2prim

  !---------------------------------------------
  ! Convert primitive variables to conservative
  !---------------------------------------------
  FUNCTION convert_prim2cons(W) RESULT(Q)
    TYPE(Pvar), INTENT(in) :: W
    TYPE(Pvar)             :: Q
    REAL(dp)                :: eps
    ! q1 = rho
    Q%U(1) = W%U(1)
    ! q2 = rho*u
    Q%U(2) = W%U(1)*W%U(2)
  END FUNCTION convert_prim2cons


!!!!-----------------------


  FUNCTION roe_sw(e,u,n) RESULT (J)
    ! evaluate a roe average to estimate a rough speed for 
    ! Burman jump operator
    CLASS(Pvar), INTENT(in):: e
    REAL(dp),DIMENSION(N_Vars), INTENT(in):: u
    REAL(dp), DIMENSION(n_dim), INTENT(in):: n ! here it will be a normal of norme 1
    REAL(dp), DIMENSION(n_vars,n_vars):: J
    REAL(dp), DIMENSION(n_vars,n_vars,n_dim):: JJ
    REAL(dp),DIMENSION(n_dim):: v=0.
    JJ=Jacobian_sw(e,v)
    J(:,:) = JJ(:,:,1)*n(1)

  END FUNCTION roe_sw

  FUNCTION flux_sw(Var,x) RESULT(f)
    CLASS(PVar),                  INTENT(in) :: Var    ! vector of conservative variables
    REAL(dp),       DIMENSION(n_dim), INTENT(in) :: x
    TYPE(PVar), DIMENSION(n_dim)             :: f

    F(1)%u(1) = Var%u(2)
    F(1)%u(2) = Var%u(2)**2._dp/Var%u(1) + grav*0.5_dp*Var%u(1)**2._dp

  END FUNCTION flux_sw



  FUNCTION source_sw(e,x,test) RESULT(s)
    CLASS(PVar),                INTENT(IN) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(IN) :: x
    INTEGER ,                   INTENT(IN) :: test
    TYPE(PVar)                             :: s
    s%u(1) = 0._dp
    s%u(2) = -grav*e%u(1)*deriv_bathymetry(test,x(1))
  END FUNCTION source_sw

  FUNCTION Jacobian_sw(Var,x) RESULT(J)
    CLASS(Pvar),              INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim),   INTENT(in) :: x
    REAL(dp), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    REAL(dp) :: Vi2, u

    u = Var%u(2)/Var%u(1)

    Vi2 = u**2._dp

    J=0.
    J(1,:,1) = (/ 0.0_dp, 1.0_dp /)
    J(2,:,1) = (/ -u**2._dp + grav*Var%u(1), 2.0_dp*u /)
   
  END FUNCTION Jacobian_sw

  FUNCTION AbsJacobian_sw(Var,x) RESULT(J)
    !compute abs value of jacobian matrix
    REAL(dp), DIMENSION(n_dim), PARAMETER:: nn=(/1.0_dp/),xx=(/1.0_dp/)
    CLASS(Pvar),              INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim),   INTENT(in) :: x
    REAL(dp), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    REAL(dp) :: Vi2, u, H, eps, p
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: L
    REAL(dp), DIMENSION(n_Vars,n_Vars) :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    INTEGER:: i

    R=rvectors_sw(Var,nn)
    L=lvectors_sw(Var,nn)
    lambda=evalues_sw(var,xx,nn)
    lambda=ABS(lambda)

    J(:,:,1)=MATMUL(R,MATMUL(lambda,L))

  END FUNCTION AbsJacobian_sw

  FUNCTION evalues_sw(Var,x,n) RESULT(lambda)
    ! eigenvalues: diagonal matrix. It is written as a matrix for ease of calculations
    CLASS(PVar),            INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim)             :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp)    :: un, c

    c   = SoundSpeed(Var%u(1))

    lambda = 0._dp
    un = ( Var%u(2)*n(1) )/Var%u(1)
    lambda(1,1) = un-c
    lambda(2,2) = un+c

  END FUNCTION evalues_sw

  REAL(dp) FUNCTION spectral_radius_sw(Var,x,n)
    ! compute the maximum value of eigenvalues:
    ! max_i {lambda_ii}
    CLASS(PVar),            INTENT(in) :: Var
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: n
    REAL(dp),DIMENSION(n_Vars,n_Vars)      :: lambda

    lambda = evalues_sw(Var,x,n)
    spectral_radius_sw = MAXVAL(ABS(lambda))

  END  FUNCTION spectral_radius_sw

  FUNCTION rvectors_sw(Q,n) RESULT(R)
    ! right e-vectors
    ! assume ||n||=1
    CLASS(PVar),           INTENT(in) :: Q
    REAL(dp), DIMENSION(n_dim)         :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars) :: R
    REAL(dp) :: rho, u,  a

    rho = Q%U(1)
    u   = Q%U(2)/Q%U(1)
    a   = SoundSpeed(rho)


    R(:,1) = (/ 1.0_dp, u-a /)
    R(:,2) = (/ 1.0_dp, u+a /)


  END FUNCTION rvectors_sw

  FUNCTION lvectors_sw(Q,n) RESULT(L)
    ! left e-vectors
    ! assumes ||n||=1
    CLASS(PVar),            INTENT(in) :: Q
    REAL(dp), DIMENSION(n_dim)          :: n
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: L
    REAL(dp), DIMENSION(n_Vars,n_Vars)  :: R
    REAL(dp) :: rho, u, p, a, E, H, gmm1, eps
        R=rvectors_sw(Q,n)
        L=inverse(R)

  END FUNCTION lvectors_sw



  FUNCTION Nmat_sw(e, n_ord, grad, x) RESULT (Nmat)
    CLASS(Pvar),                  INTENT(in) :: e
    INTEGER,                      INTENT(in) :: n_ord
    REAL(dp), DIMENSION(n_dim),       INTENT(in) :: x
    REAL(dp), DIMENSION(n_dim,n_ord), INTENT(in) :: grad
    REAL(dp), DIMENSION(n_vars,n_vars)           :: Nmat
    REAL(dp), DIMENSION(n_vars, n_vars, n_dim)   :: J
    INTEGER:: l

    J= Jacobian_sw(e,x)
    Nmat=0_dp
    DO l=1, n_ord
       Nmat = Nmat + ABS( grad(1,l)*J(1,1,1) )
    ENDDO
    Nmat =0._dp!Inverse(Nmat)

  END FUNCTION Nmat_sw
  FUNCTION min_mat_sw(e, x, n, alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp),                   INTENT(in) :: alpha
    REAL(dp), DIMENSION(n_dim), INTENT(IN) :: n
    REAL(dp), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_sw(e,x,n)
    R      = rvectors_sw(e,n)
    L      = lvectors_sw(e,n)
    Ap     = MATMUL( R, MATMUL( MIN(lambda(:,:),alpha), L) )

  END FUNCTION min_mat_sw


  FUNCTION max_mat_sw(e,x,n,alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: x
    REAL(dp),                   INTENT(in) :: alpha
    REAL(dp), DIMENSION(n_dim), INTENT(in) :: n
    REAL(dp), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(dp), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_sw(e,x,n)
    R      = rvectors_sw(e,n)
    L      = lvectors_sw(e,n)
    Ap     = MATMUL( R, MATMUL( MAX(lambda(:,:),alpha), L) )

  END FUNCTION max_mat_sw
  !--------------------------




END MODULE variable_def
