!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
PROGRAM main
  USE param2d
  USE scheme
  USE overloading
  USE Model
  USE geometry
  USE utils
  USE postprocessing
  USE init_bc
  USE timestepping
  use precision
  IMPLICIT NONE


  INTEGER, DIMENSION(5)::loc_ndofs=(/2,3,3,4,4/)

  REAL(dp),DIMENSION(4,2:5):: alpha
  REAL(dp),DIMENSION(4,4,3:5):: beta, gamma

  INTEGER, DIMENSION(5):: n_theta
  REAL(dp),DIMENSION(0:4,1:4,1:5):: theta
  REAL(dp), PARAMETER:: s=SQRT(3.0_dp)
  TYPE(maillage):: mesh
  TYPE(variables):: var, debug
  TYPE(donnees):: DATA
  TYPE(element):: e, e1, e2
  TYPE(arete):: ed
  TYPE(PVar):: error_L1, error_L2, error_Linf

  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: temp_debug, temp_var, temp
  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: res,residu, uu, difference
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
  TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
  REAL(dp), DIMENSION(3):: x=0._dp
  INTEGER:: nt,itype, jt, i, kt, is, l, k_inter, p1, p2, k, iseg, jt1, jt2, ll, kt0
  REAL(dp):: dx, dt0, dt, temps
  CHARACTER(LEN = 1024) :: maille
  INTEGER:: nb_args, Impre, n, lp, liter
  TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: u_b, u_c
  TYPE(Pvar),DIMENSION(1):: FL,FR
  INTEGER :: iarg, nargs
  REAL(dp)    :: num, tn
  CHARACTER(len=32) :: arg
  CHARACTER(len=100) :: folder
INTEGER, DIMENSION(:), ALLOCATABLE :: ShIndArray


  INTEGER, DIMENSION(:), ALLOCATABLE:: fluxes_mood 
  INTEGER :: nflux

  folder='TEST'

!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------

  CALL theta_alpha_beta()
  
  !--------------------------- READ INITIAL DATA --------------------------------------------------
  OPEN(1, file="Data/don1d")
  READ(1,*) DATA%nt
  ! pass mesh size from command line
  nargs = command_argument_COUNT()
  IF (nargs > 0) THEN
     CALL get_command_ARGUMENT(1, arg)
     READ(arg, *) iarg
     DATA%nt = iarg
  END IF
  PRINT*, 'N = ', DATA%nt
  READ(1,*) DATA%itype
  !  DATA%itype_b=DATA%itype
  READ(1,*)DATA%iordret, DATA%iter
  IF (DATA%iter.LT.DATA%iordret) THEN
     PRINT*, "wrong Data%iter,Data%iordret"
     PRINT*,"Data%iter should be >= than Data%iordret"
     PRINT*, "Data%iordret", DATA%iordret
     PRINT*, "Data%iter", DATA%iter
  ENDIF
  READ(1,*) DATA%ischema
  READ(1,*) DATA%alpha_jump
  READ(1,*) DATA%alpha_jump2
  READ(1,*) DATA%cfl
  READ(1,*) DATA%ktmax
  READ(1,*) DATA%tmax
  READ(1,*) DATA%ifre
  READ(1,*) DATA%test
  READ(1,*) DATA%mood
  IF(DATA%mood) THEN
     READ(1,*)jt
     ALLOCATE(fluxes_mood(jt))
     DO k=1, jt
        READ(1,*) fluxes_mood(k)
     ENDDO
  ELSE
    ALLOCATE(fluxes_mood(1))
    fluxes_mood(1) = DATA%ischema
  ENDIF
  CLOSE(1)

  !----------- SET geometry of the problem test --------------------------
  CALL isPeriodic(DATA)
  CALL geom(Mesh,DATA)

  DO jt=1,Mesh%nt
     mesh%e(jt)%type_flux=DATA%ischema
  ENDDO
  Impre=6 
  jt=Mesh%nt

  call descriptor(folder,data)

  ! geometrie
  !initialisation
  Var%Ncells=Mesh%ns

  kt0=0
  DATA%temps=0._dp

  ! initialisation
  ALLOCATE(Var%ua(Mesh%ndofs,0:DATA%iordret-1), &
       & Var%up(Mesh%ndofs,0:DATA%iordret-1)&
       & , var%un(Mesh%ndofs),&
      & debug%ua(Mesh%ndofs,0:DATA%iordret-1) &
       &,debug%up(Mesh%ndofs,0:DATA%iordret-1)&
       & , debug%un(Mesh%ndofs), &
       & temp_var(Mesh%ndofs,0:DATA%iordret-1), &
       & temp(Mesh%ndofs,0:DATA%iordret-1), &
       & temp_debug(Mesh%ndofs,0:DATA%iordret-1))

  !-------------------------------------------------------------------------------------------------------------------------------
  ! ----------Aquire old data if test has been interrupted and does not need to restart from scratch --------
  IF (.NOT.DATA%restart) THEN ! we start with a new solution
     kt=0
     DO jt=1, Mesh%nt
        e=Mesh%e(jt)
        DO l=1, e%nsommets

           Var%ua(e%nu(l),0)=IC(e%coor(l),DATA)
           Var%un(e%nu(l))=Var%ua(e%nu(l),0)

        ENDDO
        SELECT CASE(e%itype)
        CASE(1,3,4,7,11,12,13,14) !Lagrange-> nothing to do

        CASE(2,5,6) ! B3 ! cubic Bezier: modif all dof except vertices
           ALLOCATE(u_b(e%nsommets), u_c(e%nsommets))
           DO k=1, n_vars
              DO l=1, e%nsommets
                 Var%un(e%nu(l))%u(k)=SUM(e%base1(l,:)*Var%ua(e%nu(:),0)%u(k))
              ENDDO
           ENDDO
           DEALLOCATE(u_b,u_c)

        CASE default
           PRINT*, "correct initialisation not yet implemented for these elements,e%itype=",e%itype
           STOP
        END SELECT

     ENDDO

     Var%ua(:,0)=Var%un(:) 
     DO l=1, n_vars
        PRINT*, "min variable ",l,  MINVAL(Var%ua(:,0)%u(l))
        PRINT*, "max variable ",l,  MAXVAL(Var%ua(:,0)%u(l))
        REWIND(10)

        PRINT*
     ENDDO

  ELSE

     kt=kt0
  ENDIF
  !-------------------------------------------------------------------------------------------------------------------------------

  PRINT*, "Initialisation"
  DO l=1, n_vars
     PRINT*, "min variable ",l,  MINVAL(Var%ua(:,0)%u(l))
     PRINT*, "max variable ",l,  MAXVAL(Var%ua(:,0)%u(l))
     PRINT*
  ENDDO


  CALL visu(DATA, kt, mesh, Var,folder)

  !-------------------------------------------------------------------------------------------------------------------------------
  ! -------------------- Time- stepping loop -------------------------

  DO kt=kt0+1, DATA%ktmax  ! loop over time steps


    
     ! switch
     DO is=1, Mesh%ndofs
        temp(is,:)=Var%ua(is,0)
        Var%ua(is,1:)=Var%ua(is,0)
        Var%un(is)=0._dp
        debug%ua(is,:)=Var%ua(is,0)
        temp_debug(is,:)=Debug%ua(is,:)
        temp_var(is,:)=Var%ua(is,:)
     ENDDO

     dt0=  Pas_de_temps(Mesh,Var, DATA)
     dt=MIN(dt0,DATA%tmax-DATA%temps)
     Var%dt=dt
     IF (dt.LE.1.e-13_dp) EXIT
     tn=DATA%temps


     ! Euler time stepping
     ! initilalise the time step with the right scheme

     ! loop over the cascade of schemes
     IF (DATA%mood ) then 
         Mesh%e(:)%type_flux=DATA%ischema
         Mesh%e(:)%diag2=0

         DO nflux = 1,size(fluxes_mood)


            if (nflux.gt.1) then
               DO is=1, Mesh%ndofs
                  Debug%ua(is,:)= temp_debug(is,:)
                  Var%ua(is,:)=temp(is,:)
               END DO
            endif

            DO k_inter=1,DATA%iter
               DO is=1, Mesh%ndofs
                  debug%up(is,:)=debug%ua(is,:)
               ENDDO
               DO k=2, DATA%iordret
                  CALL debut()
               ENDDO ! k
            END DO

            ! do the test at the end of timestep, not at every iteration!!!!
            ! maybe positivity checks inside every iteration

            CALL test(Data%iordret-1,Debug,Var, mesh, DATA,fluxes_mood(nflux))

       
         END DO ! nflux

    !!$     
         DO is=1, Mesh%ndofs
            Var%ua(is,:)=temp(is,:)
         END DO
     END IF


     DO k_inter=1,DATA%iter
        DO is=1, Mesh%ndofs
           Var%up(is,:)=Var%ua(is,:)
        ENDDO

        DO k=2, DATA%iordret
             DATA%temps = tn + alpha(k-1,DATA%iordret)*dt
             CALL fin()
        ENDDO ! k
     ENDDO ! n_inter
     DATA%temps= tn +dt
     Var%ua(:,0)=Var%ua(:,DATA%iordret-1)


     IF (MOD(kt,DATA%ifre)==0) THEN
        PRINT*
        PRINT*, kt, DATA%temps, DATA%tmax, dt
        DO l=1, n_vars

           PRINT*, "min ua, variable ",l, MINVAL(Var%ua(:,0)%u(l))
           PRINT*, "max ua, variable ",l, MAXVAL(Var%ua(:,0)%u(l))
           PRINT*
        ENDDO

        !100     FORMAT(1x,i5,3(1x,e10.4))
        CALL visu(DATA, kt, mesh, Var,folder)

        PRINT*
     ENDIF
  ENDDO ! end  time stepping
  PRINT*, "final time=", DATA%temps, DATA%tmax
  !post processing
  CALL visu(DATA, kt, mesh, Var,folder)
 
  DEALLOCATE(Var%ua, Var%up , var%un, Mesh%e, Mesh%edge, Mesh%aires )
  DEALLOCATE( temp_var, temp_debug,temp, fluxes_mood)

CONTAINS

  REAL(dp) FUNCTION pas_de_temps(Mesh,Var,DATA)
    IMPLICIT NONE

    TYPE(variables), INTENT(in):: Var
    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    INTEGER:: jt, l
    TYPE(element):: e
    TYPE(Pvar), ALLOCATABLE, DIMENSION(:):: u
    REAL(dp):: dt, dx, umax
    REAL(dp), DIMENSION(n_dim):: x=0.0_dp, n
    dt=HUGE(1.00_dp)
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u(e%nsommets))
        DO l=1, e%nsommets
          dx=e%volume
          n=1.0
          x=e%coor(l)
          u(l) = e%eval_func(Var%ua(e%nu,0),e%x(:,l))
          umax=u(l)%spectral_radius(x,n)
          dt=MIN(dt,e%volume/( ABS(umax)+1.e-6_dp))
       ENDDO
       DEALLOCATE(u)
    ENDDO
    pas_de_temps=dt*DATA%cfl
  END FUNCTION pas_de_temps


SUBROUTINE debut ()

    DO is=1, Mesh%Ndofs
       debug%un(is)=0.0_dp
    ENDDO

    !!    CALL shock_indicator(Mesh,Var%ua(k-1,:),ShIndArray)
    DO jt=1, Mesh%nt
       CALL main_update(k,dt,Mesh%e(jt),debug,DATA,alpha,beta,gamma,n_theta,theta,jt,Mesh)
    ENDDO
   !--------------------------------------------------------------------------------------
    ! EDGE UPDATE DONE ONLY IN CASE WE DO NOT HAVE LXF scheme
    IF (DATA%ischema==5 .OR.DATA%ischema==4) THEN !(with jumps--> Burman'stuff)
        CALL edge_main_update(k,DATA,Mesh,debug,dt,alpha,beta,gamma,n_theta,theta)
    END IF  
    CALL BC(debug, Mesh%e(1)%nu(1), Mesh%e(Mesh%nt)%nu(2), DATA, k)

    ! Compute the 'trial' solution to make MOOD tests
    DO is=1, Mesh%ndofs
       debug%ua(is,k-1)=debug%up(is,k-1)-debug%un(is)*Mesh%aires(is)
    ENDDO

  END SUBROUTINE debut


  SUBROUTINE fin()
    DO is=1, Mesh%Ndofs
       var%un(is)=0.0_dp
    ENDDO

    DO jt=1, Mesh%nt
       CALL main_update(k,dt,Mesh%e(jt),Var,DATA,alpha,beta,gamma,n_theta,theta,jt,Mesh)
    ENDDO

    !--------------------------------------------------------------------------------------
    ! EDGE UPDATE DONE ONLY IN CASE WE DO NOT HAVE LXF scheme
    IF (DATA%ischema==5 .OR.DATA%ischema==4) THEN !(with jumps--> Burman'stuff)
       CALL edge_main_update(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta	)

    ENDIF
    !--------------------------------------------------------------------------------------

    CALL BC(Var, Mesh%e(1)%nu(1), Mesh%e(Mesh%nt)%nu(2), DATA, k)

    !--------- Update of the solution -----------------------------------------------
    DO is=1, Mesh%ndofs
       var%ua(is,k-1)=Var%up(is,k-1)-Var%un(is)*Mesh%aires(is)
    ENDDO


  END SUBROUTINE fin

  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------

  SUBROUTINE theta_alpha_beta()

    ! loading the weights

    ! alpha: this defines the fractions of Delta t for the Euler method (operator L1)
    ! alpha( subtimestep, order in time)
    ! theta: \int_0^alpha(L,k) f = \sum_{p=0}^{order in time) theta(p,L, order in time)* f(u[p])
    ! beta contains the (time) weight for the Euler step of Version 1
    alpha=0._dp
    theta=0._dp
    beta=0._dp
    gamma=0._dp
    n_theta=0._dp

    ! for order 1
    n_theta(1)=1
    theta(:,:,1)=0._dp
    theta(0,1,1)=1._dp
!!$
    ! for order 2
    alpha(1,2)=1._dp
    n_theta(2)=2
    theta(:,:,2)=0._dp
    theta(0:1,1,2)=0.5_dp

    ! quadratic interpolation over [0,1]
    ! alpha, beta gamma coeffcients for order 3
    !
    alpha(1,3)=0.5_dp
    alpha(2,3)=1._dp
    beta(1,2,3)=alpha(1,3)
    beta(2,2,3)=alpha(2,3)-alpha(1,3)
    GAMMA(2,2,3)=beta(2,2,3)
    n_theta(3)=3
    !
    ! integration coefficients for order 3
    theta(0:2,1:2,3)=RESHAPE( (/5.0_dp/24._dp,1._dp/3._dp,-1._dp/24._dp&
         &,1._dp/6._dp,2._dp/3._dp,1._dp/6._dp/),(/3,2/))

!!$  ! cubic interpolation
!!$#if (-2==0)
!!$  ! Here I choose the points 0.5*(1+cos(k*pi/N)), N=3, k=3,2,1,0
!!$!!!!!!!!!!!!!!
!!$  ! these are
!!$
!!$  alpha(1,4)=1./4.
!!$  alpha(2,4)=3./4.
!!$  alpha(3,4)=1.
!!$  n_theta(4)=4
!!$
!!$  theta(0:3,1:3,4)=RESHAPE( (/59./576., & !(0,1)
!!$       & 47./288.,  &                      !(1,1)
!!$       & -7./288., &                      !(2,1)
!!$       & 5./576.,  &                      !(3,1)
!!$       & 3./64.,   &                      !(0,2)
!!$       & 15./32.,  &                      !(1,2)
!!$       & 9./32.,   &                      !(2,2)
!!$       & -3./64.,  &                      !(3,2)
!!$       &1./18.,    &                      !(0,3)
!!$       &4./9.,     &                      !(1,3)
!!$       &4./9.,     &                      !(2,3)
!!$       &1./18./)   &                      !(3,3)
!!$       &,(/4,3/))
!!$#endif
#if (-2==-2) 
    ! Here I choose the points k/3, k=3,2,1,0
!!!!!!!!!!!!!!
    ! these are

    alpha(1,4)=1._dp/3._dp
    alpha(2,4)=2._dp/3._dp
    alpha(3,4)=1._dp
    n_theta(4)=4

    theta(0:3,1:3,4)=RESHAPE( (/1._dp/8._dp, & !(0,1)
         & 19._dp/72._dp,  &                      !(1,1)
         & -5._dp/72._dp, &                      !(2,1)
         & 1._dp/72._dp,  &                      !(3,1)
         & 1._dp/9._dp,   &                      !(0,2)
         & 4._dp/9._dp,  &                      !(1,2)
         & 1._dp/9._dp,   &                      !(2,2)
         & 0._dp,      &                      !(3,2)
         &1._dp/8._dp,    &                      !(0,3)
         &3._dp/8._dp,     &                      !(1,3)
         &3._dp/8._dp,     &                      !(2,3)
         &1._dp/8._dp/)   &                      !(3,3)
         &,(/4,3/))
#endif
    !! The following lines are good only for the version 1
    beta(1,3,4)=alpha(1,4) ! this gives the fraction of timestep for the Euler method
    beta(2,3,4)=alpha(2,4)-alpha(1,4)
    beta(3,3,4)=alpha(3,4)-alpha(2,4)
    GAMMA(2:3,3,4)=beta(2:3,3,4)

!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------

!!$ 4th degree interpolation, 5th order

#if (-1==-1)
    !EQUISPACED POINTS
    alpha(1,5)=1._dp/4._dp
    alpha(2,5)=2._dp/4._dp
    alpha(3,5)=3._dp/4._dp
    alpha(4,5)=1._dp
    n_theta(5)=5

    theta(0:4,1:4,5)=RESHAPE( (/ 251._dp/2880._dp, & !(0,1)
         & 323._dp/1440._dp,  &                      !(1,1)
         & -11._dp/120._dp, &                      !(2,1)
         & 53._dp/1440._dp,  &                      !(3,1)
         & -19._dp/2880._dp,  &                      !(4,1)
         & 29._dp/360._dp,   &                      !(0,2)
         & 31._dp/90._dp,  &                      !(1,2)
         & 1._dp/15._dp,   &                      !(2,2)
         & 1._dp/90._dp,      &                      !(3,2)
         & -1._dp/360._dp,      &                    !(4,2)
         &27._dp/320._dp,    &                      !(0,3)
         &51._dp/160._dp,     &                      !(1,3)
         &9._dp/40._dp,     &                      !(2,3)
         &21._dp/160._dp,   &                      !(3,3)
         &-3._dp/320._dp,   &                      !(4,3)
         &7._dp/90._dp,    &                      !(0,4)
         &16._dp/45._dp,     &                      !(1,4)
         &2._dp/15._dp,     &                      !(2,4)
         &16._dp/45._dp,   &                      !(3,4)
         &7._dp/90._dp /)   &                      !(4,4)
         &,(/5,4/))
#else
    !Gauss Lobatto Chebishev POINTS 
    alpha(1,5)=1._dp/2._dp-1._dp/SQRT(2._dp)/2._dp
    alpha(2,5)=1._dp/2._dp
    alpha(3,5)=1._dp/2._dp+1._dp/SQRT(2._dp)/2._dp
    alpha(4,5)=1._dp
    n_theta(5)=5

    theta(0:4,1:4,5)=RESHAPE( (/SQRT(2._dp)/120._dp+23._dp/480._dp, & !(0,1)
         & -13._dp/480._dp*SQRT(2._dp)+2._dp/15._dp,  & !(1,1)
         & -3._dp/20._dp*SQRT(2._dp)+1._dp/5._dp, &     !(2,1)
         & -43._dp/480._dp*SQRT(2._dp)+2._dp/15._dp,  & !(3,1)
         & -1._dp/120._dp*SQRT(2._dp)-7._dp/480._dp,  &  !(4,1)
         & 1._dp/60._dp,   &                      !(0,2)
         &  1._dp/8._dp*SQRT(2._dp)+2._dp/15._dp,  &    !(1,2)
         & 1._dp/5._dp,   &                       !(2,2)
         & -1._dp/8._dp*SQRT(2._dp)+2._dp/15._dp,  &    !(3,2)
         & 1._dp/60._dp,     &                    !(4,2)
         & -SQRT(2._dp)/120._dp+23._dp/480._dp, &       !(0,3)
         &  43._dp/480._dp*SQRT(2._dp)+2._dp/15._dp,  & !(1,3)
         &  3._dp/20._dp*SQRT(2._dp)+1._dp/5._dp, &     !(2,3)
         &  13._dp/480._dp*SQRT(2._dp)+2._dp/15._dp,  & !(3,3)
         & -1._dp/120._dp*SQRT(2._dp)-7._dp/480._dp,  &  !(4,3)
         &1._dp/30._dp,    &                      !(0,4)
         &4._dp/15._dp,     &                      !(1,4)
         &2._dp/5._dp,     &                      !(2,4)
         &4._dp/15._dp,   &                      !(3,4)
         &1._dp/30._dp/)   &                      !(4,4)
         &,(/5,4/))
#endif

    !! The following lines are good only for the version 1
    beta(1,4,5)=alpha(1,5) ! this gives the fraction of timestep for the Euler method
    beta(2,4,5)=alpha(2,5)-alpha(1,5)
    beta(3,4,5)=alpha(3,5)-alpha(2,5)
    beta(4,4,5)=alpha(4,5)-alpha(3,5)
    GAMMA(2:4,4,5)=beta(2:4,4,5)


  END SUBROUTINE theta_alpha_beta

END PROGRAM
