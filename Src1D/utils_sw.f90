!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE utils
  USE PRECISION
!  USE param2d
  IMPLICIT NONE
  PRIVATE
  PUBLIC:: bathymetry, deriv_bathymetry
CONTAINS

  REAL(dp) FUNCTION bathymetry(test,x)
    INTEGER, INTENT(in):: test
    REAL(dp), INTENT(in):: x
    REAL(dp) ::x0,r,b

   SELECT CASE(test) 
        CASE(21, 20,22,23,24,25,19,14,26)  !immersed smooth bumb
            x0=10._dp
            r=5._dp
            IF (x>x0-r .AND. x<x0+r ) THEN
                b = 0.2_dp*EXP(1._dp-1._dp/(1.-((x-x0)/r)**2._dp))
            ELSE
                b= 0._dp
            END IF

        CASE(0,2,3,4,5,6,10,11,12,13,40) !immersed 2 smooth bumbs
            b=0.1_dp*exp(-50._dp*(x-0.4_dp)**2_dp) +0.04_dp*exp(-300._dp*(x-0.55_dp)**2_dp) 
        CASE(301)
            b=0.1_dp*exp(-100._dp*(x-0.2_dp)**2_dp) 
        CASE(30,29,34) !parabola
            b=0.5_dp*((x-2._dp)**2._dp -1._dp)

        CASE(31,33)
            b=0.17_dp*(15._dp-x)/10._dp+0.48_dp*(x-5._dp)/10._dp

        CASE(1,32,41,42,70,71,76,77)
            b=0._dp

        CASE(50,51)
            b = 0.5_dp+0.35_dp*SIN(6._dp*x*ACOS(-1._dp))
        CASE(62,63,67,68,97,98)
            b = 3._dp-x*0.002_dp !0.0026750049164762152
        CASE(60,61,65,66,95,96)
            b = 3._dp -x*0.01_dp
        CASE(27)
            IF (x>10_dp .AND. x<15_dp) THEN
              b=0.02_dp*(x-10._dp)
            ELSE
              IF (x>20_dp .AND. x<21) THEN
                b=-0.1*(x-21_dp)
              ELSE
                IF (x .GE. 15_dp .AND. x .LE. 20_dp) THEN
                  b= 0.1_dp
                ELSE 
                  b=0_dp
                END IF
              END IF
            END IF
        CASE default
            PRINT*, "ERROR CASE NOT DEFINED IN bathymetry"
            STOP

    END SELECT
    bathymetry=b
  END FUNCTION bathymetry

  REAL(dp) FUNCTION deriv_bathymetry(test,x)
    INTEGER, INTENT(in):: test
    REAL(dp), INTENT(in):: x
    REAL(dp) ::x0,r,b

   SELECT CASE(test) 
        CASE(21, 20,22,23,24,25,19,14,26)  !immersed smooth bumb
            x0=10._dp
            r=5._dp
            IF (x>x0-r .AND. x<x0+r ) THEN
                b = -0.2_dp*EXP(1._dp-1._dp/(1.-((x-x0)/r)**2._dp))*&
                  &2._dp*(x-x0)/r**2._dp/((1.-((x-x0)/r)**2._dp)**2._dp)
            ELSE
                b= 0._dp
            END IF

        CASE(0,2,3,4,5,6,10,11,12,13,40) !immersed 2 smooth bumbs
            b=-50._dp*(x-0.4_dp)*2_dp*0.1_dp*exp(-50._dp*(x-0.4_dp)**2_dp) &
              &-300._dp*(x-0.55_dp)*2_dp*0.04_dp*exp(-300._dp*(x-0.55_dp)**2_dp) 
        CASE(301)
            b=-100._dp*(x-0.2_dp)*2_dp*0.1_dp*exp(-100._dp*(x-0.2_dp)**2_dp) 
        CASE(30,29,34) !parabola
            b=(x-2._dp)

        CASE(31,33)
            b=-0.17_dp/10._dp+0.48_dp/10._dp

        CASE(1,32,41,42,70,71,76,77)
            b=0._dp

        CASE(50,51)
            b = 6._dp*ACOS(-1._dp)*0.35_dp*COS(6._dp*x*ACOS(-1._dp))
        CASE(62,63,67,68,97,98)
            b = -0.002_dp !0.0026750049164762152
        CASE(60,61,65,66,95,96)
            b = -0.01_dp
        CASE(27)
            IF (x>10_dp .AND. x<15_dp) THEN
              b=0.02_dp
            ELSE
              IF (x>20_dp .AND. x<21) THEN
                b=-0.1
              ELSE
                IF (x .GE. 15_dp .AND. x .LE. 20_dp) THEN
                  b= 0._dp
                ELSE 
                  b=0_dp
                END IF
              END IF
            END IF
        CASE default
            PRINT*, "ERROR CASE NOT DEFINED IN bathymetry"
            STOP

    END SELECT
    deriv_bathymetry=b
  END FUNCTION deriv_bathymetry



  REAL(dp) FUNCTION fbon(x)
    REAL(dp), INTENT(in)::x
    REAL(dp):: s, r, pi=ACOS(-1._dp)
    fbon=SIN(2.d0*pi*x)
  END FUNCTION fbon

  REAL(dp) FUNCTION fonc_d(x)
    REAL(dp), INTENT(in):: x
    !      integer, intent(in):: k
    REAL(dp):: y, pi=ACOS(-1._dp)
    INTEGER:: p
    p=1.d0
    fonc_d=2*pi*p*COS(2*pi*x*p)
  END FUNCTION fonc_d

END MODULE  utils
