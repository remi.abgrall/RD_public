!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE timestepping

  ! In this module are collected the main_update, corresponding to the update of the Flux of the scheme
  ! and the edge_main_update which accounts for the stabilization of the scheme 


  USE overloading
  USE element_class
  USE variable_def
  USE arete_class
  USE param2d
  USE scheme
  USE Model
  USE PRECISION
  IMPLICIT NONE
CONTAINS


  SUBROUTINE main_update(k, dt, e,Var, DATA,alpha,beta,gamma,n_theta,theta,jt,mesh)
    IMPLICIT NONE
    REAL(dp), INTENT(in):: dt
    REAL(dp),DIMENSION(4,2:5):: alpha
    REAL(dp),DIMENSION(4,4,3:5):: beta, gamma
    INTEGER, INTENT(IN):: jt
    INTEGER, DIMENSION(5):: n_theta
    REAL(dp),DIMENSION(0:4,1:4,1:5):: theta
    TYPE(maillage),INTENT(IN):: mesh
    INTEGER, INTENT(in):: k
    TYPE(element), INTENT(in):: e
    TYPE(variables), INTENT(inout):: Var
    TYPE(donnees), INTENT(in):: DATA
    TYPE(Pvar),DIMENSION(e%nsommets,n_dim):: flux, flux_c
    TYPE(Pvar),DIMENSION(e%nsommets,0:DATA%iordret-1):: u, up, u_p, up_p
    TYPE(Pvar),DIMENSION(e%nsommets):: res, difference, uu,uu_c, source, source_c
    INTEGER:: l, lp

    DO l=1,e%nsommets
       up(l,:) =Var%up(e%nu(l),:)
    ENDDO
    u=up

    DO l=1,e%nsommets
       difference(l)=up(l,k-1)-up(l,0)
    ENDDO


    DO l=0,DATA%iordret-1
       u_P(:,l) =Control_to_Cons(u(:,l),e)
    ENDDO
    up_P=u_P

    flux_c=0._dp; uu=0._dp; source_c=0._dp
    DO l=1, e%nsommets


       DO lp=0, n_theta(DATA%iordret)-1
          flux_c(l,:)= flux_c(l,:)+ theta(lp,k-1,DATA%iordret)* up_P(l,lp)%flux((/e%coor(l)/))
          uu(l)      = uu(l)      + theta(lp,k-1,DATA%iordret)* up  (l,lp)
          source_c(l)= source_c(l)+ theta(lp,k-1,DATA%iordret)* up_P(l,lp)%source((/e%coor(l)/),DATA%test)
       ENDDO


#if (1==0)
       ! the pure Jacobi-like does not need this, only the 
       ! the Gauss-Seidel like
       ! version 1
       DO lp=1,k-1
          flux_c(l,:)= flux_c(l,:)-&
               &GAMMA(lp, DATA%iordret-1,DATA%iordret)*( up_P(l,lp)%flux( (/e%coor(l)/) )&
               & -u_p(l,lp)%flux( (/e%coor(l)/) )   )
          source_c(l)= source_c(l)-&
               &GAMMA(lp, DATA%iordret-1,DATA%iordret)*( up_P(l,lp)%source( (/e%coor(l)/) ,DATA%test)&
               & -u_p(l,lp)%source( (/e%coor(l)/),DATA%test )   )
          uu(l)=uu(l) -GAMMA(lp,DATA%iordret-1,DATA%iordret)*( up(l,lp)-u(l,lp))
       ENDDO
#endif 

    ENDDO
    DO l=1, n_dim
       flux(:,l)=Cons_to_control(flux_c(:,l),e)
    ENDDO
    source = Cons_to_control(source_c,e)

    res=schema( DATA%ischema, e, uu, difference, flux, source, dt, jt,mesh)
    DO l=1, e%nsommets
       Var%un(e%nu(l))=Var%un(e%nu(l))+res(l)
    ENDDO


  END SUBROUTINE main_update

  SUBROUTINE edge_main_update(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    REAL(dp),DIMENSION(4,2:5):: alpha
    REAL(dp),DIMENSION(4,4,3:5):: beta, gamma

    INTEGER, DIMENSION(5):: n_theta
    REAL(dp),DIMENSION(0:4,1:4,1:5):: theta
    INTEGER, INTENT(IN):: k

    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    TYPE(variables), INTENT(inout):: Var
    REAL(dp), INTENT(in):: dt
    TYPE(element):: e, e1, e2
    TYPE(arete):: ed
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
    TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
    TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
    INTEGER:: jt1, jt2, iseg, l, lp
    ! question : nothing on Jump for Version 1 ?
    DO iseg=1, Mesh%nsegmt

       ed=Mesh%edge(iseg)

       IF (ed%bord)  CYCLE ! we are on the boundary

       jt1 = ed%jt1

       e1=Mesh%e(jt1)
       jt2 = ed%jt2
       e2=Mesh%e(jt2)

      IF (e1%type_flux==2&!4&
           & .OR.e1%type_flux==1&!5 &
           & .OR.e1%type_flux==4&!5 &
           & .OR.e1%type_flux==5&!5 &
           & .OR.e1%type_flux==6 &
           & .OR.e1%type_flux==7 &
           & .OR.e1%type_flux==9) THEN !(with jumps--> Burman'stuff on cell e1 )
!!$     

         IF  (e2%type_flux==2&!4&
            & .OR. e2%type_flux==1&!5 &
            & .OR. e2%type_flux==4&!5 &
            & .OR. e2%type_flux==5&!5 &
               & .OR.e2%type_flux==6 &
              & .OR.e2%type_flux==7 &
               & .OR.e2%type_flux==9) THEN !(with jumps--> Burman'stuff on cell e2 )

       ALLOCATE(u1(e1%nsommets), u2(e2%nsommets), resJ(e1%nsommets,2) )
       ALLOCATE(ua1(e1%nsommets,0:DATA%iordret-1),&
            & ua2(e2%nsommets,0:DATA%iordret-1),&
            & up1(e1%nsommets,0:DATA%iordret-1),&
            & up2(e2%nsommets,0:DATA%iordret-1))
       lp=n_theta(DATA%iordret)-1

       DO l=0,DATA%iordret-1
          ua1(:,l)=Var%ua(e1%nu(:),l)
          up1(:,l)=Var%up(e1%nu(:),l)
          ua2(:,l)=Var%ua(e2%nu(:),l)
          up2(:,l)=Var%up(e2%nu(:),l)

       ENDDO
       u1=0._dp
       DO l=1,e1%nsommets
#if (1==1)
          !interpolate in time
          DO lp=0, n_theta(DATA%iordret)-1
             u1(l)=u1(l)+theta(lp,k-1,DATA%iordret)* up1(l,lp)
          ENDDO
#else
          ! no interpolation in time
          lp=k-1
          u1(l)= up1(l,lp)
#endif
       ENDDO
       u2=0._dp
       DO l=1,e2%nsommets
#if (1==1)
          !interpolate in time
          DO lp=0, n_theta(DATA%iordret)-1
             u2(l)=u2(l)+theta(lp,k-1,DATA%iordret)* up2(l,lp)
          ENDDO
#else
          ! no interpolation in time
          lp=k-1
          u2(l)= up2(l,lp)
#endif   
       ENDDO

# if (1==0)
       !Gauss-Seidel like
       DO l=1,e1%nsommets
          DO lp=1, k-1
             u1(l)=u1(l)-GAMMA(lp,DATA%iordret-1,DATA%iordret)* (up1(l,lp)-ua1(l,lp))
          ENDDO

       ENDDO

       DO l=1,e2%nsommets
          DO lp=1,k-1
             u2(l)=u2(l)-GAMMA(lp,DATA%iordret-1,DATA%iordret)* (up2(l,lp)-ua2(l,lp))
          ENDDO

       ENDDO
#endif
       ! jump of grad u
       CALL jump( ed, e1, e2, u1, u2, resJ, DATA%alpha_jump,DATA%alpha_jump2)

       DO l=1, e1%nsommets
          Var%un(e1%nu(l))=Var%un(e1%nu(l))+resJ(l,1) * dt
       ENDDO
       DO l=1, e2%nsommets
          Var%un(e2%nu(l))=Var%un(e2%nu(l))+resJ(l,2) * dt
       ENDDO
       DEALLOCATE(resJ,u1,u2,ua1,ua2,up1,up2)
!!$
    END IF
  END IF
 ENDDO
END SUBROUTINE edge_main_update

SUBROUTINE test(k_iter,Debug,Var, mesh, DATA, flux_mood)
  IMPLICIT NONE

  ! tunable parameters:
  REAL(8), PARAMETER:: cour_max=10000.
  REAL:: eps1, eps2
  REAL, PARAMETER:: coeff=1.!0.
  ! variables to be checked
  INTEGER, PARAMETER:: n_list=1 !3
  INTEGER, DIMENSION(n_list), PARAMETER:: list=(/1/)!,2,3/)
  !
  ! emergency schemes
  INTEGER, PARAMETER:: theflux=1,theflux2=0
  !
  !
  INTEGER, INTENT(in):: k_iter
  TYPE(maillage), INTENT(inout):: mesh
  TYPE(variables), INTENT(in)::  debug, Var
  TYPE(donnees), INTENT(in):: DATA
  INTEGER, INTENT(IN):: flux_mood

  TYPE(arete):: ed
  TYPE(pvar), DIMENSION(:),ALLOCATABLE:: deb
  !REAL(8),DIMENSION(:),ALLOCATABLE:: coefficient!with Remi's version of u2
  TYPE(element):: e, eL, eR
  TYPE(pvar),DIMENSION(:),ALLOCATABLE:: coefficient !!with our version of u2
  TYPE(pvar), DIMENSION(:),ALLOCATABLE:: coefficient1, coefficient2

  TYPE(pvar), DIMENSION(:),ALLOCATABLE:: v_min, v_max,u2_min,u2_max, u2_minloc, u2_maxloc
  REAL,  DIMENSION(:),ALLOCATABLE:: w_min, w_max,vd_min, vd_max
  TYPE(Pvar),DIMENSION(:), ALLOCATABLE::phys, phys_d,phys_control, phys_d_control
  TYPE(Pvar),DIMENSION(:),ALLOCATABLE::temp
  REAL(8),DIMENSION(:),ALLOCATABLE:: temp_min, temp_max
  INTEGER:: jt, k,  l, lk,diag, is, jseg, lkk,r
  INTEGER:: jt1, jt2
  REAL:: val_min,val_max,eps,xg,yg,val_min_n, val_max_n
  REAL :: eps_i, absK
  REAL:: ratio, alphaR, alphaL, smooth_sensor
  TYPE(pvar),DIMENSION(:),ALLOCATABLE::u1_der, u1_derR, u1_derL
  REAL,DIMENSION(:),ALLOCATABLE::u1_derL_mean, u1_der_mean,u1_derR_mean,coefficient_mean
  REAL,DIMENSION(:),ALLOCATABLE:: vL_min,vL_max,vR_min,vR_max,vL,vR
  INTEGER,PARAMETER :: plateau = 1
  INTEGER,PARAMETER :: NAN_criteria = 2
  INTEGER,PARAMETER :: PAD_criteria = 3
  INTEGER,PARAMETER ::  DMP_B1=4
  INTEGER,PARAMETER :: DMP_nou2=5
  INTEGER,PARAMETER ::DMP_u2_2=6
  INTEGER,PARAMETER ::DMP_u2_1=7

  ! integer, dimension(2,e%nsommets-1):: nuloc
  Mesh%e(:)%diag2= Mesh%e(:)%diag

  Mesh%e(:)%diag=0

  ALLOCATE(v_min(Mesh%nt), v_max(Mesh%nt),w_min(Mesh%nt), w_max(Mesh%nt))
  ALLOCATE(vd_min(Mesh%nt), vd_max(Mesh%nt))
  ALLOCATE(phys(Mesh%ndofs),phys_d(Mesh%ndofs))
  ALLOCATE(phys_control(Mesh%ndofs),phys_d_control(Mesh%ndofs),u2_max(Mesh%nt),u2_min(Mesh%nt))

  ALLOCATE(vL(Mesh%nt),vR(Mesh%nt),vL_min(Mesh%nt),vL_max(Mesh%nt),vR_min(Mesh%nt),vR_max(Mesh%nt))
  ALLOCate(u1_der_mean(Mesh%nt),u1_derL_mean(Mesh%nt),u1_derR_mean(Mesh%nt),coefficient_mean(Mesh%nt))
  !compute physical quantities from interpolated quantities.
  ! is this really usefull for Bezier thanks to the TV property?
  ! If not done : more strict condition since Bezier>0==> positive function
  ! converse not true
  DO jt=1, Mesh%nt
     e=Mesh%e(jt)
     eps1=0.!10.**(-3)
     eps2=0.!10.**(-4)!%volume
     ALLOCATE(temp(e%nsommets))

     !---------------------------
     !---------------------------
     ! Define the vector of primitives at the initial subtimestep n,0
     temp=Control_to_cons(Var%ua(e%nu,0),e)! before ua. up: we compare with the solution before the update
     DO l=1,e%nsommets
        phys(e%nu(l))=convert_cons2prim(temp(l)) !vector of primitive variables in physical values for n,0
     ENDDO

     !...................................
     ! warning: for convergence study, better to work with phys variables'
     ! because of u2 criterion-> use the right way to compute second derivatives
     ! comment here
     temp=Cons_to_Control(phys(e%nu),e)
     phys_control(e%nu)=Var%ua(e%nu,0)!temp !vector of primitive variables in control values for n,0
     !end comment
     !...................................

     !---------------------------
     !---------------------------
     ! Define the vector of primitives at the current subtimestep n,m
     temp=Control_to_cons(debug%ua(e%nu,k_iter),e)
     DO l=1,e%nsommets
        phys_d(e%nu(l))=convert_cons2prim(temp(l)) !vector of primitive variables in physical values, for n,m
     ENDDO

     !...................................
     ! warning: for convergence study, better to work with phys variables'
     ! because of u2 criterion-> use the right way to compute second derivatives
     ! comment here
     temp=Cons_to_Control(phys_d(e%nu),e)
     phys_d_control(e%nu)=debug%ua(e%nu,k_iter)!temp !vector of primitive variables in control values for n,m
     !end comment
     !...................................
     DEALLOCATE(temp)
  ENDDO


  ListVar: DO lkk=1,n_list 
     lk=list(lkk)


     ! In the following we compute the neighobour cells - but this approach is only good for 1D
     !----------------------------------------------------------------------------------------------------------------
     DO jt = 1,Mesh%nt

        e=Mesh%e(jt)

        !-------------------------------------------------
        !-------------------------------------------------
        ! Definition of the minimum and maximum values of the neighbour elements at time n,0

        !////////////////////////////////////////////////////////////
        !Boundary conditions: Attention you may have to change them according to the test case
#if(1==1)
        !  outflow conditions
        IF (jt > 1) THEN
           eL=Mesh%e(jt-1)
        ELSE
           eL=e
        END IF
        IF (jt < Mesh%nt) THEN
           eR=Mesh%e(jt+1)
        ELSE
           eR=e
        END IF

#else
        ! periodic
        IF (jt > 1) THEN
           eL=Mesh%e(jt-1)
        ELSE
           eL=Mesh%e(Mesh%nt)
        END IF
        IF (jt < Mesh%nt) THEN
           eR=Mesh%e(jt+1)
        ELSE
           eR=Mesh%e(1)
        END IF
#endif
        !////////////////////////////////////////////////////////////

        w_min(jt) = MIN(MINVAL(phys(e%nu)%u(lk)),MINVAL(phys(eL%nu)%u(lk)),MINVAL(phys(eR%nu)%u(lk)))
        w_max(jt) = MAX(MAXVAL(phys(e%nu)%u(lk)),MAXVAL(phys(eL%nu)%u(lk)),MAXVAL(phys(eR%nu)%u(lk)))


        !-------------------------------------------
        !------------------------------------------
        ! Definition of the minimum and maximum values of the neighbour elements' second derivative at time n,0
        ALLOCATE(coefficient(e%nsommets))
        !ALLOCATE(coefficient(e%nsommets-1))
        DO l=1,e%nsommets                 
           coefficient(l)=e%eval_der2(phys_d_control(e%nu), e%x(:,l))
           !coefficient=e%der_sec(phys_control(eL%nu)%u(lk))    
        END DO

        ALLOCATE(coefficient1(eL%nsommets))
        !ALLOCATE(coefficient1(eL%nsommets-1))
        DO l=1,eL%nsommets
           !coefficient1=eL%der_sec(phys_control(eL%nu)%u(lk))           
           coefficient1(l)=eL%eval_der2(phys_d_control(eL%nu), eL%x(:,l))
        END DO

        !ALLOCATE(coefficient2(eR%nsommets-1))
        !coefficient2=eR%der_sec(phys_control(eR%nu)%u(lk))       
        ALLOCATE(coefficient2(eR%nsommets))
        DO l=1,eR%nsommets                 
           coefficient2(l)=eR%eval_der2(phys_d_control(eR%nu), eR%x(:,l))
        END DO

        u2_min(jt)=MIN(MINVAL(coefficient%u(lk)),MINVAL(coefficient1%u(lk)),MINVAL(coefficient2%u(lk)))
        u2_max(jt)=MAX(MAXVAL(coefficient%u(lk)),MAXVAL(coefficient1%u(lk)),MAXVAL(coefficient2%u(lk)))


        ALLOCATE(u1_derL(eL%nsommets))
        DO l=1,eL%nsommets             
           u1_derL(l)=eL%eval_der(phys_d_control(eL%nu),eL%x(:,l))
        END DO

        ALLOCATE(u1_der(e%nsommets))
        DO l=1,e%nsommets             
           u1_der(l)=e%eval_der(phys_d_control(e%nu),e%x(:,l))
        END DO

        ALLOCATE(u1_derR(eR%nsommets))
        DO l=1,eR%nsommets             
           u1_derR(l)=eR%eval_der(phys_d_control(eR%nu),eR%x(:,l))
        END DO

        u1_derL_mean(jt)=sum(u1_derL%u(lk))/REAL(eL%nsommets)
        u1_der_mean(jt)=sum(u1_der%u(lk))/REAL(e%nsommets)
        u1_derR_mean(jt)=sum(u1_derR%u(lk))/REAL(eR%nsommets)
        coefficient_mean(jt)=sum(coefficient%u(lk))/REAL(e%nsommets)

        vL(jt)=u1_der_mean(jt)-e%volume*0.5*coefficient_mean(jt)
        vR(jt)=u1_der_mean(jt)+e%volume*0.5*coefficient_mean(jt)

        vL_min(jt)=min(u1_derL_mean(jt),u1_der_mean(jt))
        vL_max(jt)=max(u1_derL_mean(jt),u1_der_mean(jt))
        vR_min(jt)=min( u1_der_mean(jt),u1_derR_mean(jt))
        vR_max(jt)=max( u1_der_mean(jt),u1_derR_mean(jt))

        DEALLOCATE(coefficient,coefficient1, coefficient2)
        DEALLOCATE(u1_der,u1_derR,u1_derL)

        !=========================================================
        !=========================================================       

     END DO



     !"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
     !"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
     !""""""""""""""" MOOD DETECTION CRITERIA """"""""""""""""""""""""""""""""""""""""""""""""""""
     !"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


     Loop:      DO jt=1,Mesh%nt
        e=Mesh%e(jt)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! NaN detection
        !.....................................
        ! at n+1
        IF (.NOT. ALLOCATED(deb)) ALLOCATE(deb(e%nsommets))

        deb=debug%ua(e%nu,k_iter)
        DO l=1, e%nsommets
           DO k=1,n_vars
              IF (deb(l)%u(k).NE.deb(l)%u(k) ) THEN
                 Mesh%e(jt)%diag=NAN_criteria
                 CYCLE loop
              ENDIF
           ENDDO
        ENDDO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! PAD test: for fluids, assumes: 1-> density, n_vars-> pressure
        !...................................................................................................................................................
        IF (MINVAL(phys_d(e%nu)%u(1)).LE.0. .OR. MINVAL(phys_d(e%nu)%u(n_vars)).LE.0.) THEN
           Mesh%e(jt)%diag=PAD_criteria

           CYCLE loop
        ENDIF

        val_min=MINVAL(phys_d(e%nu)%u(lk))  ! at n+1
        val_max=MAXVAL(phys_d(e%nu)%u(lk))
        val_min_n=MINVAL(phys(e%nu)%u(lk))  ! at n
        val_max_n=MAXVAL(phys(e%nu)%u(lk))


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! plateau detection
        !...............................................
        IF (ABS(w_max(jt)-w_min(jt)) .LT. e%volume**3) THEN
           Mesh%e(jt)%diag= plateau
           CYCLE Loop
        ENDIF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Extrema detection DMP+u2
        !.....................................................................
        eps=coeff *MAX(ABS(w_max(jt)-w_min(jt))*eps1,eps2)   ! at n

        IF (val_min.LT.w_min(jt)-eps.OR.val_max.GT.w_max(jt)+eps) THEN
!!$

           IF (e%nsommets .eq. 2) then
              Mesh%e(jt)%diag=DMP_B1
              CYCLE Loop
           ELSE

              IF (vL(jt) .gt. u1_der_mean(jt)) THEN
                 ratio=(vL_max(jt)-u1_der_mean(jt))/(vL(jt)-u1_der_mean(jt))
                 alphaL=min(1., ratio)
              ELSE IF  (vL(jt) .eq. u1_der_mean(jt)) THEN

                 alphaL=1.
              ELSE
                 ratio=(vL_min(jt)-u1_der_mean(jt))/(vL(jt)-u1_der_mean(jt))
                 alphaL=min(1.,ratio)
              END IF

              IF (vR(jt) .gt. u1_der_mean(jt)) THEN
                 ratio=(vR_max(jt)-u1_der_mean(jt))/(vR(jt)-u1_der_mean(jt))
                 alphaR=min(1., ratio)
              ELSE IF  (vR(jt) .eq. u1_der_mean(jt)) THEN

                 alphaR=1.
              ELSE
                 ratio=(vR_min(jt)-u1_der_mean(jt))/(vR(jt)-u1_der_mean(jt))
                 alphaR=min(1.,ratio)
              END IF

              smooth_sensor=min(alphaR,alphaL)

              IF (abs(smooth_sensor) .ge. 1.-Tiny(1.0)) THEN
                 Mesh%e(jt)%diag=DMP_nou2
            
                 CYCLE Loop
              ELSE
                 Mesh%e(jt)%diag=DMP_u2_2
                 CYCLE Loop
              END IF

           ENDIF


!!!!!!!!!!!!!!

        ELSE
           Mesh%e(jt)%diag=0
           cycle Loop
        END IF
        ! END DO Loop_loc
        !DEALLOCATE(coefficient,coefficient1,coefficient2,u2_minloc,u2_maxloc)
        DEALLOCATE(deb)
     ENDDO Loop

  END DO ListVar
  DEALLOCATE(phys,phys_d, phys_control, phys_d_control)
  DEALLOCATE(v_min,v_max,w_min,w_max,u2_min,u2_max,vd_min,vd_max)
  DEALLOCATE(VL, vR, vL_min, vL_max, vr_min, vr_max)
  DEALLOCATE(u1_derL_mean,u1_der_mean,u1_derR_mean,coefficient_mean)

  !-----------------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------------
  !---------------- Update of the Indicators that allow the scheme switch ---------------------------

  DO jt=1, Mesh%nt

     SELECT CASE(Mesh%e(jt)%diag)
     CASE(0,Plateau,DMP_nou2)
        ! In this case we don't touch the indicator
     CASE(DMP_B1,DMP_u2_1,DMP_u2_2)
        Mesh%e(jt)%type_flux=flux_mood
        ! In this case we switch to a more diffusive scheme
     CASE(PAD_criteria,NAN_criteria)
        Mesh%e(jt)%type_flux=theflux2
        ! In this case we take a first order monotone scheme
     CASE default
        PRINT*, "in test, bad behavior, jt=", jt
        STOP
     END SELECT
     !
  ENDDO
END SUBROUTINE test
END MODULE timestepping








