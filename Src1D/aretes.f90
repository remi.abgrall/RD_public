!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE arete_class
  USE PRECISION
  IMPLICIT NONE
  TYPE, PUBLIC:: arete
     INTEGER:: nsommets, itype, nvertex! nbre de dofs, type element: 1-> P1
     ! 2-> B2
     !3->P2
     ! nombre de sommet dans cet element arete
     LOGICAL:: bord
     INTEGER:: jt1=-1, jt2 =-1 ! les deux elements de par et d'autre de l'arete.
     INTEGER, DIMENSION(:,:), POINTER :: nu =>Null() ! nu( indice des elements, indice des voisins): on cherche a connaitre le numero local des 
     ! points communs (puisque le maillage est confome)  aux deux elements sur cette face dans chacun des elements jt1 et jt2
     REAL(dp), DIMENSION(:,:), POINTER   :: coor=>Null() ! il s'agit des coordonnees physique des dofs (communs) sur la face (commune)
     REAL(dp)                           :: volume=0  !
     REAL(dp)                           :: jump_flag=1.0 !(between 0 and 1 which gives the weight of the edge term
     ! for this one check if there is a discontinuity around and take the maximum value)
     REAL(dp),  DIMENSION(:), POINTER :: n   =>Null()   ! normales exterieures
     INTEGER                        :: log    ! logique
!!!!   quadrature de surface !
     REAL(dp),   DIMENSION(:,:),POINTER :: quad =>Null()  ! point de quadrature 
     REAL(dp),     DIMENSION(:),POINTER :: weight=>Null() ! poids 
     INTEGER                        :: nquad  ! nbre de points de quadrature 
!!! quadrature bord (dimension -1)
     REAL(dp),   DIMENSION(:,:),POINTER :: quad_1 =>Null()  ! point de quadrature 
     REAL(dp),     DIMENSION(:),POINTER :: weight_1=>Null() ! poids 
     INTEGER                        :: nquad_1  ! nbre de points de quadrature 
!!!
   CONTAINS
     PROCEDURE, PUBLIC:: aire=>aire_arete
     PROCEDURE, PUBLIC:: quadrature=>quadrature_arete
     PROCEDURE, PUBLIC:: normale=>normale_arete
     !FINAL:: clean_arete

  END TYPE arete


CONTAINS

  REAL(dp) FUNCTION aire_arete(e)
    CLASS(arete), INTENT(in):: e
    REAL(dp), DIMENSION(2):: a
    a= e%coor(:,2)-e%coor(:,1)

    aire_arete=SQRT(a(1)**2+ a(2)**2 )
  END FUNCTION aire_arete

  FUNCTION normale_arete(e) RESULT(n)
    CLASS(arete), INTENT(in)::e
    REAL(dp), DIMENSION(2):: n
    INTEGER:: l, k1, k2
    INTEGER, DIMENSION(3), PARAMETER:: ip1=(/2,3,1/)

    n(1)=e%coor(2,2)-e%coor(2,1)
    n(2)=e%coor(1,1)-e%coor(1,2)

  END FUNCTION normale_arete



  SUBROUTINE quadrature_arete(e)
    CLASS(arete), INTENT(inout):: e
    REAL(dp):: w,zo,xo,s
    INTEGER:: nquad

    !Gaussia formula, exact for polynomials of degree 5
    PRINT*, "quadrature_arete"
    STOP

    e%nquad=3
    nquad=e%nquad
    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))

    s=SQRT(0.6d0)
    e%quad(1,1)=0.5d0*(1.0d0 - s)
    e%quad(2,1)=0.5d0*(1.0d0 + s)
    e%weight(1) = 5.0d0/18.0d0
    e%quad(1,2)=0.5d0*(1.0d0 + s)
    e%quad(2,2)=0.5d0*(1.0d0 - s)
    e%weight(2) = 5.0d0/18.0d0
    e%quad(1,3)=0.5d0
    e%quad(2,3)=0.5d0
    e%weight(3)=8.0d0/18.0d0



  END SUBROUTINE quadrature_arete

  SUBROUTINE clean_arete(e)
    TYPE(arete):: e
    IF (ASSOCIATED(e%nu)) NULLIFY(e%nu)
    IF (ASSOCIATED(e%coor)) NULLIFY(e%coor)
    IF (ASSOCIATED(e%quad)) NULLIFY(e%quad)
    IF (ASSOCIATED(e%weight)) NULLIFY(e%weight)
  END SUBROUTINE clean_arete
END MODULE arete_class
