!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE init_bc

  USE param2d
  USE overloading
use precision
  IMPLICIT NONE

CONTAINS

  !---------------------------------------
  ! Setup domain
  !---------------------------------------

  SUBROUTINE init_geom(DATA)
    TYPE(donnees), INTENT(inout):: DATA

    SELECT CASE(DATA%test)
    CASE(0)
       ! Isentropic
       DATA%domain_left = -1.0_dp
       DATA%Length      =  2.0_dp

    CASE(1)
       ! Sod
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp
       
    CASE(2)
       ! Shu-Osher
       DATA%domain_left = -5.0_dp
       DATA%Length      = 10.0_dp

    CASE(3)
       ! Woodward-Colella
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE(4)
       ! channel
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE(5)
       ! Isentropic vortex
       DATA%domain_left = -1.0_dp
       DATA%Length      =  2.0_dp

    CASE(6)
       ! Woodward-Colella left
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp
    CASE(7)
       ! LeBlanc
       DATA%domain_left = 0.0_dp
       DATA%Length      = 9.0_dp
    CASE(8)
       !simetrie
       DATA%domain_left = 0.0_dp
       DATA%Length      = 10.0_dp
    CASE(9)
       ! 1-2-3
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE(10)
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp
    CASE default
       PRINT*, "Wrong test number for geom(), test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE init_geom

  !---------------------------------------
  ! Set initial and boundary conditions
  !---------------------------------------

  FUNCTION IC(x,DATA) RESULT(Var)
    REAL(dp),       INTENT(in)   :: x
    TYPE(donnees), INTENT(inout):: DATA
    TYPE(PVar) :: Var
    REAL(dp)       :: alph, p, u, ro
    REAL(dp):: ms, rhoR,uR,pR,aR,mr,S3, rho_star,u_star,p_star

    !---------------
    ! for Euler
    SELECT CASE(DATA%test)
    CASE(0)
        ! Convergence test: isentropic flow
        DATA%tmax = 0.1_dp
        gmm   = 3.0_dp
        alph = 0.9999995_dp
        Var%u(1) = 1._dp + 1._dp + alph*SIN(PI*x)
        Var%u(2) = 0._dp
        Var%u(3) = Var%u(1)*epsEOS(Var%u(1),(Var%u(1))**gmm)

    CASE(1)
        ! Sod
        DATA%tmax = 0.16_dp
        IF (x <= 0.5_dp) THEN
            Var%u(1) = 1._dp ! [kg/m^3]
            Var%u(2) = 0._dp ! [m/s]
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),1._dp) ! [J]
        ELSE
            Var%u(1) = 0.125_dp
            Var%u(2) = 0._dp
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),0.1_dp)
        END IF

    CASE(2)
        ! Shu-Osher
        DATA%tmax = 1.8_dp
        alph = 0.2_dp
        IF (x <= -4._dp) THEN
            Var%u(1) = 3.857143_dp! [kg/m^3]
            Var%u(2) = Var%u(1)*2.629369_dp ! [m/s]
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.33333333333_dp) + 0.5_dp*Var%u(2)**2_dp/Var%u(1) ! [J]
        ELSE
            Var%u(1) = 1._dp + alph*SIN(5._dp*x)
            Var%u(2) = 0._dp
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),1.0_dp)
        END IF

    CASE(3)
        ! Woodward-Colella
        DATA%tmax = 0.038_dp
        IF (x <= 0.1_dp) THEN
            Var%u(1) = 1._dp ! [kg/m^3]
            Var%u(2) = 0._dp ! [m/s]
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.0_dp**3_dp) ! [J]
        ELSE
             IF (x > 0.1_dp .AND. x < 0.9_dp) THEN
                Var%u(1) = 1._dp ! [kg/m^3]
                Var%u(2) = 0._dp ! [m/s]
                Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.0_dp**(-2_dp)) ! [J]
             ELSE
                Var%u(1) = 1._dp ! [kg/m^3]
                Var%u(2) = 0._dp ! [m/s]
                Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.0_dp**2_dp) ! [J]
             END IF
        END IF

    CASE(4)
        ! 1D version of 2D channel
        Var%u(1) = 0.5_dp ! [kg/m^3]
        Var%u(2) = 0.0_dp ! [m/s]
        Var%u(3) = 0.125_dp ! [J]

    CASE(5)
        ! Convergence test: isentropic vortex
        DATA%tmax = 0.5_dp
!        DATA%tmax = 0.25
        gmm   = 1.4_dp
        Var%u(1) = 1._dp + 0.1*EXP(-(x)**2_dp/(2._dp*0.01_dp))
!        Var%u(1) = 1.+EXP(-80.0d0*(x-0.4d0)**2)
        Var%u(2) = Var%u(1)*1._dp
        Var%u(3) = Var%u(1)*epsEOS(Var%u(1),1.0_dp) + 0.5_dp*Var%u(2)**2_dp/Var%u(1)

    CASE(6)
        ! Woodward-Colella left
        DATA%tmax = 0.012_dp
       IF (x <= 0.5_dp) THEN
          Var%u(1) = 1._dp ! [kg/m^3]
          Var%u(2) = 0._dp ! [m/s]
          Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.0_dp**3_dp) ! [J]
       ELSE
          Var%u(1) = 1._dp ! [kg/m^3]
          Var%u(2) = 0._dp ! [m/s]
          Var%u(3) = Var%u(1)*epsEOS(Var%u(1),10.0_dp**(-2_dp)) ! [J]
       END IF
    CASE(7)
       ! LeBlanc
       DATA%tmax = 6_dp
       gmm   = 5._dp/3._dp
       IF (x <= 3_dp) THEN
          ro=1._dp   ; u=0._dp; p=0.1_dp*(gmm-1_dp)
       ELSE
          ro=0.001_dp; u=0._dp; p=1.e-07*(gmm-1._dp) ! energie interne
       END IF
       Var%u(1)=ro
       Var%u(2)=u*ro
       Var%u(3)=ro*epsEOS(ro,p)+0.5_dp*ro*u*u
    CASE(8)
       DATA%tmax=0.003_dp
       gmm=5._dp/3._dp
       IF (x<=5) THEN
          ro=1._dp; u=-1000_dp; p=0.1_dp
       ELSE
          ro=1._dp; u=1000_dp; p=0.1_dp
       ENDIF
       Var%u(1)=ro
       Var%u(2)=ro*u
       Var%u(3)=ro*epsEOS(ro,p)+0.5_dp*ro*u*u
    CASE(9) ! 1-2-3
       DATA%tmax=0.15_dp
       gmm=1.4_dp
       IF (x<=0.5_dp) THEN
          ro=1.0_dp
          u=-2.0_dp
          p=0.4_dp
       ELSE
          ro=1.0_dp
          u=2.0_dp
          p=0.4_dp
       ENDIF
       Var%u(1)=ro
       Var%u(2)=ro*u
       Var%u(3)=ro*epsEOS(ro,p)+0.5_dp*ro*u*u
       
    CASE(10)
       ! Pure Shock
       ms = 3._dp
       DATA%tmax = 10_dp**(-5_dp)
       gmm=1.4_dp
       
       rhoR = 1.225_dp
       uR = 0._dp
       pR = 101325._dp
       
       aR = SQRT(gmm*pR/rhoR)
       mr=uR/aR
       S3 = ms*aR
       rho_star = rhoR*( (gmm+1.)*((mr-ms)**2_dp) )/( (gmm-1.)*((mr-ms)**2_dp)+2._dp  )
       u_star = (1._dp-(rhoR/rho_star)  )*S3+(ur*rhoR/rho_star)
       p_star = pR*( 2._dp*gmm*((mr-ms)**2_dp)-(gmm-1._dp))/(gmm+1._dp)
    
        IF (x <= 0.5_dp) THEN
            Var%u(1) = rho_star ! [kg/m^3]
            Var%u(2) = u_star*Var%u(1)  ! [m/s]
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),p_star) + 0.5_dp*Var%u(2)**2_dp/Var%u(1) ! [J]
        ELSE
            Var%u(1) = rhoR
            Var%u(2) = uR
            Var%u(3) = Var%u(1)*epsEOS(Var%u(1),pR) + 0.5_dp*Var%u(2)**2_dp/Var%u(1)
        END IF

    CASE default
       PRINT*, "Wrong test number for Euler_IC, test = ", DATA%test
       STOP
    END SELECT

  END FUNCTION IC

  SUBROUTINE BC(Var,i1,iN,DATA,k)
    TYPE(variables), INTENT(inout):: Var
    INTEGER,         INTENT(in)   :: i1, iN, k
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(Pvar):: a0,a1
    INTEGER:: p1, pN

    SELECT CASE(DATA%test)
    CASE(0)
!!$        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0._dp!a0+a1
        Var%un(pN)=0._dp!a0+a1
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

 

    CASE(1,2,6,7,8,10)
       ! outflow
       Var%un(i1)=0._dp
       Var%un(iN)=0.0_dp
    CASE(3)
        ! reflective
        Var%un(i1)%u(2) = 2.0_dp*Var%ua(k-1,i1)%u(2)
        Var%un(iN)%u(2) = 2.0_dp*Var%ua(k-1,iN)%u(2)

    CASE(4)
        ! inflow left + outflow right
        Var%ua(k-1,i1)%u(1) = 1.0_dp
        Var%ua(k-1,i1)%u(2) = 3.0_dp
        Var%ua(k-1,i1)%u(3) = 4.5_dp + 1.7857_dp

    CASE(5)
        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=0._dp!a0+a1
        Var%un(pN)=0._dp!a0+a1
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

    CASE default
       PRINT*, "Wrong test number for Euler_IC, test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE BC

  SUBROUTINE isPeriodic(DATA)
    TYPE(donnees),   INTENT(inout)   :: DATA

    SELECT CASE(DATA%test)
    CASE(0,5)
      DATA%periodicBC = .TRUE.
    CASE default
      DATA%periodicBC = .FALSE.
    END SELECT
  END SUBROUTINE isPeriodic


END MODULE init_bc
