!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE geometry
  USE param2d
  USE init_bc
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  PUBLIC:: geom

CONTAINS
  SUBROUTINE geom(Mesh, DATA)
    TYPE (maillage), INTENT(inout):: mesh
    TYPE(donnees),   INTENT(inout):: DATA
    REAL(dp), DIMENSION(:), ALLOCATABLE:: base

    INTEGER, PARAMETER,DIMENSION(14)::loc_ndofs=(/2,3,3,4,4,5,5,0,0,0,2,3,4,5/)
    REAL(dp):: dx, a, alpha
    TYPE(element):: e
    INTEGER:: nt, jt, itype, p1, p2, k, i, iq, l
    nt=DATA%nt; itype=DATA%itype
    SELECT CASE(DATA%itype)
    CASE(1,11)
       Mesh%nt=DATA%nt
       Mesh%ndofs=DATA%nt+1
    CASE(2,3,12)
       Mesh%nt=DATA%nt
       Mesh%ndofs=2*DATA%nt+1
    CASE(4,5,13)
       mesh%nt=DATA%nt
       Mesh%ndofs=nt+1+2*nt
    case(6,7,14)
       Mesh%nt=Data%nt
       Mesh%ndofs=nt+1+3*nt
    CASE default
       PRINT*, "Error: this element is not yet defined", DATA%itype
       STOP
    END SELECT

    CALL init_geom(DATA)

    dx=DATA%Length/REAL(nt)
    a =DATA%domain_left

    ALLOCATE(Mesh%e(nt))
    k=0
    DO jt=1, nt
       ALLOCATE(Mesh%e(jt)%coor(loc_ndofs(itype)),Mesh%e(jt)%nu(loc_ndofs(itype)))
       ALLOCATE(Mesh%e(jt)%x(2,loc_ndofs(itype)))
       mesh%e(jt)%itype=itype
       Mesh%e(jt)%nvertex = 2
       mesh%e(jt)%nsommets=loc_ndofs(itype)
       mesh%e(jt)%coor(1)=(jt-1)*dx+a
       mesh%e(jt)%coor(2)=(jt  )*dx+a
       mesh%e(jt)%nu(1)=jt
       mesh%e(jt)%nu(2)=jt+1

       SELECT CASE(itype)
       CASE(2,3,12)
          k=k+1
          mesh%e(jt)%coor(3)=mesh%e(jt)%coor(1)+dx/2.
          mesh%e(jt)%nu(3)=nt+1+jt
       CASE(4,5)
          mesh%e(jt)%coor(3)=mesh%e(jt)%coor(1)+dx/3.
          mesh%e(jt)%coor(4)=mesh%e(jt)%coor(1)+2.*dx/3.
          k=k+1
          mesh%e(jt)%nu(3)=nt+1+2*(jt-1)+1
          k=k+1
          mesh%e(jt)%nu(4)=nt+1+2*(jt-1)+2
       CASE(13)
          alpha= 0.5d0-SQRT(5.d0)/10.d0
          mesh%e(jt)%coor(3)=mesh%e(jt)%coor(1)+alpha*dx
          mesh%e(jt)%coor(4)=mesh%e(jt)%coor(1)+(1.d0-alpha)*dx
          k=k+1
          mesh%e(jt)%nu(3)=nt+1+2*(jt-1)+1
          k=k+1
          mesh%e(jt)%nu(4)=nt+1+2*(jt-1)+2
       case(6,7)
          mesh%e(jt)%coor(3)=mesh%e(jt)%coor(1)+dx/4.
          mesh%e(jt)%coor(4)=mesh%e(jt)%coor(1)+dx/2.
          mesh%e(jt)%coor(5)=mesh%e(jt)%coor(1)+dx*0.75
          mesh%e(jt)%nu(3)=nt+1+3*(jt-1)+1
          mesh%e(jt)%nu(4)=nt+1+3*(jt-1)+2
          mesh%e(jt)%nu(5)=nt+1+3*(jt-1)+3
       case(14)
          alpha = 0.5d0-SQRT(21.d0)/14.d0
          mesh%e(jt)%coor(3)=mesh%e(jt)%coor(1)+alpha*dx
          mesh%e(jt)%coor(4)=mesh%e(jt)%coor(1)+dx/2.
          mesh%e(jt)%coor(5)=mesh%e(jt)%coor(1)+(1.d0-alpha)*dx
          mesh%e(jt)%nu(3)=nt+1+3*(jt-1)+1
          mesh%e(jt)%nu(4)=nt+1+3*(jt-1)+2
          mesh%e(jt)%nu(5)=nt+1+3*(jt-1)+3

       END SELECT

       mesh%e(jt)%volume=mesh%e(jt)%aire()
       ALLOCATE(mesh%e(jt)%n(2))
       mesh%e(jt)%n     =mesh%e(jt)%normale()
       CALL mesh%e(jt)%quadrature()
       ALLOCATE(Mesh%e(jt)%base0(mesh%e(jt)%nsommets,mesh%e(jt)%nsommets))
       ALLOCATE(Mesh%e(jt)%base1(mesh%e(jt)%nsommets,mesh%e(jt)%nsommets))
       CALL mesh%e(jt)%base_ref()
       call mesh%e(jt)%eval_coeff()
    ENDDO


    ! connectivite: for each edge, we give the element before and after
    IF (DATA%periodicBC) THEN
        Mesh%nsegmt=nt!+1 ! periodicite: the last edge is the first one, one should not count it twice
        ALLOCATE(Mesh%edge(Mesh%nsegmt))
        DO jt=2,Mesh%nsegmt!-1
           Mesh%edge(jt)%jt1=jt-1
           Mesh%edge(jt)%jt2=jt
	       Mesh%edge(jt)%bord=.FALSE.
	    ENDDO
       ! for periodic BCs
       ! here I take into account the periodicity of the mesh
       Mesh%edge(1)%jt2= 1!Mesh%nt
       Mesh%edge(1)%jt1=Mesh%nt !1
       Mesh%edge(1)%bord=.FALSE.


    ELSE

        Mesh%nsegmt=nt+1 ! also first and last one

        ALLOCATE(Mesh%edge(Mesh%nsegmt))
        DO jt=2,Mesh%nsegmt-1
           Mesh%edge(jt)%jt1=jt-1
           Mesh%edge(jt)%jt2=jt
           Mesh%edge(jt)%bord=.false.
        ENDDO
       ! for outflow BCs
       ! here I take into account the periodicity of the mesh
       Mesh%edge(1)%jt2= 1
       Mesh%edge(1)%jt1= 1
       Mesh%edge(1)%bord=.true.

        Mesh%edge(Mesh%nsegmt)%jt2=Mesh%nt !Mesh%nt !1
        Mesh%edge(Mesh%nsegmt)%jt1=Mesh%nt
        Mesh%edge(Mesh%nsegmt)%bord=.true.
  
    END IF

      ALLOCATE(Mesh%aires(Mesh%ndofs))
      Mesh%aires=0.0_dp
      
    DO jt=1, nt
       e=Mesh%e(jt)
#if (0==1)
			! VERSION GOOD ONLY FOR B^k
       Mesh%aires(e%nu)=Mesh%aires(e%nu)+e%volume/REAL(e%nsommets)
#else
			!!!!! MORE GENERAL VERSION
			ALLOCATE( base(e%nsommets))
      DO iq=1, e%nquad
		    DO l=1, e%nsommets
		       base(l  )=e%base    (l, e%quad(:,iq))
		    ENDDO
				DO i=1, e%nsommets
		      Mesh%aires(e%nu(i)) = Mesh%aires(e%nu(i)) + (base(i))*e%weight(iq)* e%volume
		    ENDDO ! i
    ENDDO

			DEALLOCATE(base)
#endif			
    ! make the cells periodic.
    ! warning : the special numbering is a trap
    ENDDO

    ! make the cells periodic.
    ! warning : the special numbering is a trap

    IF (DATA%periodicBC) THEN
        !!ONLY IF PERIODIC BOUNDARY

      p1=Mesh%e(1)%nu(1)
      p2=Mesh%e(Mesh%nt)%nu(2)
      mesh%aires(p1)=Mesh%aires(p1)+Mesh%aires(p2)
      Mesh%aires(p2)=Mesh%aires(p1)

		END IF
    ! this to avoid further divisions

    Mesh%aires=1.0_dp/Mesh%aires

  END SUBROUTINE geom

END MODULE geometry
