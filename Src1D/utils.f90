!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE utils
!  USE param2d
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  ! PUBLIC:: fonc
CONTAINS

  ! TYPE(Pvar) FUNCTION fonc(x,DATA)
  !   REAL(dp),       INTENT(in)   :: x
  !   TYPE(donnees), INTENT(inout):: DATA

  !   REAL(dp):: y,z
  !   INTEGER:: p
  !   y=x!/lenght
  !   fonc = IC(x,DATA)
  ! END FUNCTION fonc

  REAL(dp) FUNCTION fbon(x)
    REAL(dp), INTENT(in)::x
    REAL(dp):: s, r, pi=ACOS(-1._dp)
    fbon=SIN(2.d0*pi*x)
  END FUNCTION fbon

  REAL(dp) FUNCTION fonc_d(x)
    REAL(dp), INTENT(in):: x
    !      integer, intent(in):: k
    REAL(dp):: y, pi=ACOS(-1._dp)
    INTEGER:: p
    p=1.d0
    fonc_d=2*pi*p*COS(2*pi*x*p)
  END FUNCTION fonc_d

  ! FUNCTION interpol(e,x,u) RESULT(v)
  !   REAL(dp), INTENT(in):: x
  !   TYPE(element), INTENT(in):: e
  !   TYPE(Pvar),DIMENSION(:), INTENT(in):: u
  !   REAL(dp),DIMENSION(n_vars):: v
  !   REAL(dp), DIMENSION(e%nsommets):: base
  !   INTEGER:: i
  !   REAL(dp), DIMENSION(2):: y
  !   y(1)=x; y(2)=1.d0-x
  !   v=0.0d0
  !   DO i=1,e%nsommets
  !      base(i)=e%base(i,y)
  !   ENDDO
  !   DO i=1,n_vars
  !      v(i)=SUM(base*u%u(i))
  !   ENDDO
  ! END FUNCTION interpol

END MODULE  utils
