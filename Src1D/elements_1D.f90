!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE element_class
  ! In this module are listed all the tools linked to the basis functions and quadrature formulas
  ! Structure of the module:
  ! ELEMENT CLASS
  ! - aire: area of the element
  ! - normale: normal of the element
  ! - base: basis function -- here we do have Lagrangian and Bernstein polynomials
  ! - eval_function: evaluation of the solution via basis functions SUM(base(:)*(u(:)%u(l))
  ! -  eval_der: evaluation of the derivative SUM( (u(:)%u(l) *grad(1,:)) 
  ! -  eval_der2: evaluation of the second derivative SUM( (u(:)%u(l) *grad2(1,:))
  ! - der_sec: alternative to eval_der2
  ! - gradient: corresponds to the gradient of the basis function
  ! - gradient2: corresponds to the second order gradient of the basis function
  ! - quadrature:  collects all the points and weights for each typology of element
  ! - base_ref:  computes the values of the basis functions at the physical dofs
  ! - bary: defines the barycentric coordinates of the points
  ! - clean: cleans the memory of the features defined within the ELEMENT

  USE algebra
  USE variable_def
  USE overloading
  USE PRECISION
  IMPLICIT NONE


  TYPE, PUBLIC:: element
     INTEGER:: type_flux=-10
     INTEGER:: diag=-1, diag2=-1
     INTEGER:: nsommets, itype, nvertex ! nbre de dofs, type element:
     !1->P1
     !2->B2
     !3->P2
     !4->P3
     !5->B3
     !6->B4
     !7->P4
     !11->PGL1
     !12->PGL2
     !13->PGL3
     !14->PGL4
     REAL(dp),  DIMENSION(:), POINTER :: coor =>NULL() ! 
     !   *-----*-----* for quadratic, *---*---*---* for cubic elements
     !   1     3     2                1   3   4   2
     REAL(dp), DIMENSION(:,:), POINTER:: x=> NULL()
     INTEGER, DIMENSION(:), POINTER   :: nu =>NULL() ! local connectivity table, see above for location
     ! For Bezier, this corresponds to the Greville points
     REAL(dp)                           :: volume =0.0_dp  ! volume
     REAL(dp),  DIMENSION(:), POINTER   :: n =>NULL()     ! external normals 
     INTEGER                        :: log    ! logic element  : this for boundary conditions, if needed
!!!!   quadrature de surface
     REAL(dp),   DIMENSION(:,:),POINTER :: quad =>NULL()  ! point de quadrature 
     REAL(dp),   DIMENSION(:)  ,POINTER :: weight=>NULL() ! poids 
     INTEGER                            :: nquad=0  ! nbre de points de quadrature
     REAL(dp),DIMENSION(:,:),POINTER:: base0=>NULL(),base1=>NULL()
     INTEGER, DIMENSION(:), POINTER :: dof2ind
!!! int nabla phi_sigma * phi_sigma'
     REAL(dp), DIMENSION(:,:), POINTER:: coeff,coeff_b=> NULL(), mass=> NULL()
   CONTAINS
     PRIVATE
     PROCEDURE, PUBLIC:: aire=>aire_element
     PROCEDURE, PUBLIC:: gradient=>gradient_element
     PROCEDURE, PUBLIC:: gradient2=>gradient2_element
     PROCEDURE, PUBLIC:: average => average_element
     PROCEDURE, PUBLIC:: base=>base_element
     PROCEDURE, PUBLIC:: normale=>normale_element
     PROCEDURE, PUBLIC:: quadrature=>quadrature_element
     PROCEDURE, PUBLIC:: base_ref=>base_ref_element
     PROCEDURE, PUBLIC:: eval_func=>eval_func_element
     PROCEDURE, PUBLIC:: eval_der=>eval_der_element
     PROCEDURE, PUBLIC:: eval_der2=>eval_der2_element
     PROCEDURE, PUBLIC:: der_sec=>der_sec_element
     PROCEDURE, PUBLIC:: eval_coeff=>eval_coeff_element
     PROCEDURE, PUBLIC:: der2_poly=>der2_poly_element
     FINAL:: clean
  END TYPE element

CONTAINS

  REAL(dp) FUNCTION aire_element(e) ! area of a element
    CLASS(element), INTENT(in):: e
    REAL(dp), DIMENSION(1):: a,b
    a= e%coor(2)-e%coor(1)
    aire_element=ABS ( a(1))
  END FUNCTION aire_element

  FUNCTION normale_element(e) RESULT(n) ! inward normals
    CLASS(element), INTENT(in)::e
    REAL(dp), DIMENSION(2):: n
    INTEGER:: l
    DO l=1,2
       n(1)= 1.0_dp ! at e%nu(1), i.e. x_{i+1}
       n(2)=-1.0_dp ! at e%nu(2), i.e. x_{i}
    ENDDO
  END FUNCTION normale_element

  REAL(dp) FUNCTION base_element(e,k,x) ! basis functions
    CHARACTER(Len=*), PARAMETER :: mod_name="base_element"
    CLASS(element), INTENT(in):: e
    INTEGER, INTENT(in):: k ! index of basis function
    REAL(dp), DIMENSION(2), INTENT(in):: x ! barycentric coordinate
    REAL(dp) :: alpha, beta, s
    SELECT CASE(e%itype)
    CASE(1,11) ! P1
       SELECT CASE(k)
       CASE(2)
          base_element=x(1)
       CASE(1)
          base_element=x(2)
       CASE default
          PRINT*,  mod_name

          PRINT*, "P1, numero base ", k
          STOP
       END SELECT

    CASE(2) ! B2 Bezier
       SELECT CASE(k)
       CASE(1)
          base_element=x(2)*x(2)!(1.0-x(1))*(1.0-x(1))
       CASE(2)
          base_element=x(1)*x(1)
       CASE(3)
          base_element=2.0_dp*x(1)*x(2)!(1.0-x(1))
       CASE default
          PRINT*,  mod_name

          PRINT*, "B2, numero base ", k
          STOP
       END SELECT

    CASE(3,12)! P2
       SELECT CASE(k)
       CASE(1)
          base_element=x(2)*(x(2)-x(1) )!(1.0-x(1))*(1.00-2.0*x(1))
       CASE(2)
          base_element=x(1)*(x(1)-x(2) )!x(1)*(2.0*x(1)-1.00)
       CASE(3)
          base_element=4._dp*x(1)*x(2) !4.00*x(1)*(1.00-x(1))
       CASE default
          PRINT*,  mod_name

          PRINT*, "P2, numero base ", k
          STOP
       END SELECT

    CASE(4) ! P3
       SELECT CASE(k)
       CASE(1)
          base_element = -0.5_dp*(3._dp*x(1)-1._dp)*(3.*x(1)-2._dp)*(x(1)-1._dp)
       CASE(2)
          base_element = 0.5_dp*x(1)*(3._dp*x(1)-1._dp)*(3._dp*x(1)-2._dp)
       CASE(3)
          base_element = 1.5_dp*x(1)*(3._dp*x(1)-2._dp)*(3._dp*x(1)-3._dp)
       CASE(4)
          base_element = -1.5_dp*x(1)*(3._dp*x(1)-1._dp)*(3._dp*x(1)-3._dp)
       CASE default
          PRINT*,  mod_name

          PRINT*, "P3, numero base ", k
          STOP
       END SELECT

    CASE(5) ! B3
       SELECT CASE(k)
       CASE(1)
          base_element =x(2)**3
       CASE(2)
          base_element = x(1)**3
       CASE(3)
          base_element = 3.0_dp*x(1)*x(2)*x(2)
       CASE(4)
          base_element = 3.0_dp*x(1)*x(1)*x(2)
       CASE default
          PRINT*,  mod_name

          PRINT*, "P3, numero base ", k
          STOP
       END SELECT
    CASE(6) ! B4
       SELECT CASE(k)
       CASE(1)
          base_element = x(2)**4! (1.-x(1))**3
       CASE(2)
          base_element = x(1)**4
       CASE(3)
          base_element = 4.0_dp*x(1)*x(2)*x(2)*x(2)!3.*x(1)*( (1-x(1))**2)
       CASE(4)
          base_element = 6.0_dp*x(1)*x(1)*x(2)*x(2)!3.*x(1)*x(1)*((1-x(1)))
       CASE(5)
          base_element = 4.0_dp*x(1)*x(1)*x(1)*x(2)!3.*x(1)*x(1)*((1-x(1)))
       CASE default
          PRINT*, "B4, numero base ", k
          STOP
       END SELECT
    CASE(7) ! P4
       s= x(1)
       SELECT CASE(k)
       CASE(1)
          base_element = (32._dp/3._dp*(s - 1._dp)*(s - 1._dp/2._dp)*(s - 1._dp/4._dp)*(s - 3._dp/4._dp))
       CASE(2)
          base_element = (32._dp*s*(s - 1._dp/2._dp)*(s - 1._dp/4._dp)*(s - 3._dp/4._dp))/3._dp
       CASE(3)
          base_element = -(128._dp*s*(s - 1._dp)*(s - 1._dp/2._dp)*(s - 3._dp/4._dp))/3._dp
       CASE(4)
          base_element = 64._dp*s*(s - 1._dp)*(s - 1._dp/4._dp)*(s - 3._dp/4._dp)
       CASE(5)
          base_element = -(128._dp*s*(s - 1._dp)*(s - 1._dp/2._dp)*(s - 1._dp/4._dp))/3._dp
    CASE default
          PRINT*, "P4, numero base ", k
          STOP
       END SELECT

    CASE(13) ! P3 Gauss Lobatto
			 alpha=0.5_dp-SQRT(5._dp)/10._dp
			 beta = 1._dp -alpha
       SELECT CASE(k)
       CASE(1)
          base_element = (-x(1)+alpha)*(x(1)-beta)*(x(1)-1.0)/(alpha*beta)
       CASE(2)
          base_element = x(1)*(x(1)-alpha)*(x(1)-beta)/alpha/beta
       CASE(3)
          base_element = x(1)*(x(1)-beta)*(x(1)-1.0_dp)/alpha/(-2._dp*alpha+1._dp)/beta
       CASE(4)
          base_element = x(1)*(x(1)-alpha)*(x(1)-1.0_dp)/alpha/(2._dp*alpha-1._dp)/beta
       CASE default
          PRINT*, "P3 Gauss Lobatto, numero base ", k
          STOP
       END SELECT

    CASE(14) ! P4 Gauss Lobatto
			 alpha=0.5_dp-SQRT(21._dp)/14._dp
			 beta = 1._dp -alpha
       SELECT CASE(k)
       CASE(1)
          base_element = (2._dp*(alpha-x(1))*(x(1)-1._dp)*(x(1)-0.5)*(x(1)-beta))/(-alpha*beta)
       CASE(2)
          base_element = (2._dp*x(1)*(-alpha + x(1))*(x(1) - 0.5)*(x(1) - beta))/(alpha*beta)
       CASE(3)
          base_element = (x(1)*(x(1)-1._dp)*(x(1)-0.5_dp)*(x(1)-beta))/(-alpha*(2._dp*alpha-1)*beta*(alpha-0.5_dp))
       CASE(4)
          base_element = -(4._dp*x(1)*(alpha -x(1))*(x(1)-1._dp)*(x(1)-beta))/(alpha-0.5)**2._dp
       CASE(5)
          base_element =-(x(1)*(alpha-x(1))*(x(1)-1._dp)*(x(1)-0.5_dp))/(-alpha*(2._dp*alpha-1._dp)*beta*(alpha-0.5))
       CASE default
          PRINT*, "P4 Gauss Lobatto, numero base ", k
          STOP
       END SELECT

    CASE default
       PRINT*, "Type non existant", e%itype
       STOP
    END SELECT

  END FUNCTION base_element

  TYPE(PVar) FUNCTION eval_func_element(e, u, y)
    CLASS(element),                       INTENT(in):: e
    TYPE(PVar),    DIMENSION(e%nsommets), INTENT(in):: u
    REAL(dp),       DIMENSION(2),          INTENT(in):: y
    REAL(dp),       DIMENSION(2)                     :: x
    REAL(dp),       DIMENSION(e%nsommets)            :: alpha, beta, base
    TYPE(PVar)                                      :: a,b,c, aa, cc
    INTEGER                                         :: l
    LOGICAL                                         :: flag

    DO l=1, e%nsommets
       base(l)=e%base(l,y)
    ENDDO
    DO l=1,n_vars
       eval_func_element%u(l)=u(1)%u(l)+SUM(base(:)*(u(:)%u(l)-u(1)%u(l)))
    END DO


  END FUNCTION eval_func_element

  TYPE(PVar) FUNCTION eval_der_element(e,u,y)
    CLASS(element), INTENT(in)::e
    TYPE(PVar), DIMENSION(e%nsommets),INTENT(in):: u
    REAL(dp), DIMENSION(2), INTENT(in):: y
    REAL(dp), DIMENSION(1,e%nsommets):: grad
    INTEGER:: l

    DO l=1, e%nsommets
       grad(:,l)=gradient_element(e,l, y)
    ENDDO
    DO l=1,n_vars
       eval_der_element%u(l)=SUM( ( u(:)%u(l)-u(1)%u(l) ) *grad(1,:) )
    END DO

  END FUNCTION eval_der_element

  TYPE(PVar) FUNCTION eval_der2_element(e,u,y)
    CLASS(element), INTENT(in)::e
    TYPE(PVar), DIMENSION(e%nsommets),INTENT(in):: u
    REAL(dp), DIMENSION(2), INTENT(in):: y
    REAL(dp), DIMENSION(1,e%nsommets):: grad2
    INTEGER:: l

    DO l=1, e%nsommets
       grad2(:,l)=e%gradient2(l, y)
    ENDDO
    DO l=1,n_vars
       eval_der2_element%u(l)=SUM( ( u(:)%u(l)-u(1)%u(l) ) *grad2(1,:)  )
    END DO
  END FUNCTION eval_der2_element

  FUNCTION der_sec_element(e,u) RESULT(coeff)
    CLASS(element), INTENT(in)::e
    REAL(dp),DIMENSION(e%nsommets), INTENT(in)::u
    REAL(dp),DIMENSION(MAX(1,e%nsommets-2)):: coeff
    SELECT CASE(e%itype)
    CASE(1)!P1
       coeff=0._dp   !(u(2)-u(1))/e%volume**2
    CASE(2)!B2
       coeff(1)=(u(2)-2._dp*u(3)+u(1))/e%volume**2
    CASE(5)!B3
       coeff(1)=6._dp*(u(1)-2._dp*u(3)+u(4) )/e%volume**2
       coeff(2)=6._dp*(u(3)-2._dp*u(4)+u(2) )/e%volume**2
    CASE default
       PRINT*, 'der_sec, element 1d'
       STOP
    END SELECT
  END FUNCTION der_sec_element


  FUNCTION der2_poly_element(e,u) RESULT(Uder)
    CLASS(element), INTENT(in)::e
    REAL(8),DIMENSION(e%nsommets), INTENT(in)::u
    REAL(8),DIMENSION(MAX(1,e%nsommets-2)):: coeff
    REAL(8),DIMENSION(e%nsommets):: Uder
    REAL(8):: u1,u2,u3,u4,x0,x1,x2,x3
    SELECT CASE(e%itype)
    CASE(1)!P1
       coeff(1)=0.
       Uder(1:2)=0.
    CASE(2)
       x0=0.
       x1=0.5
       x2=1.
       u1=u(1)
       u2=u(3)
       u3=u(2)
       coeff(1)=(2*(u1*x1 - u2*x0 - u1*x2 + u3*x0 + u2*x2 - u3*x1))&
            &/((x0 - x1)*(x0 - x2)*(x1 - x2))
       Uder(1:3)=coeff(1)/e%volume**2
    CASE(5)
       x0=0.
       x1=1./3.
       x2=2./3.
       x3=1.
       u1=u(1)
       u2=u(3)
       u3=u(4)
       u4=u(2)
       coeff(1)=(2*(u1*x1*(x2**3) - u1*(x1**3)*x2 - u2*x0*(x2**3) &
            &+ u2*(x0**3)*x2 + u3*x0*(x1**3) - u3*(x0**3)*x1 - u1*x1*(x3**3 )&
            & + u1*(x1**3)*x3 + u2*x0*(x3**3) - u2*(x0**3)*x3 - u4*x0*(x1**3) &
            &+ u4*(x0**3)*x1 + u1*x2*(x3**3) - u1*(x2**3)*x3 - u3*x0*(x3**3) &
            &+ u3*(x0**3)*x3 + u4*x0*(x2**3) - u4*(x0**3)*x2 - u2*x2*(x3**3)  &
            &+ u2*(x2**3)*x3 + u3*x1*(x3**3) - u3*(x1**3)*x3 - u4*x1*(x2**3) +  &
            & u4*(x1**3)*x2))/((x0 - x1)*(x0 - x2)*(x0 - x3)*(x1 - x2)*(x1 - x3)*(x2 - x3))

       coeff(2)= -(6*(u1*x1*(x2**2) - u1*(x1**2)*x2 - u2*x0*(x2**2) + u2*(x0**2)*x2 &
            & + u3*x0*(x1**2) - u3*(x0**2)*x1 - u1*x1*(x3**2) + u1*(x1**2)*x3 + u2*x0*(x3**2)  &
            & - u2*(x0**2)*x3 - u4*x0*(x1**2) + u4*(x0**2)*x1 + u1*x2*(x3**2) - u1*(x2**2)*x3 &
            & - u3*x0*(x3**2) + u3*(x0**2)*x3 + u4*x0*(x2**2) - u4*(x0**2)*x2 - u2*x2*(x3**2) &
            & + u2*(x2**2)*x3 + u3*x1*(x3**2) - u3*(x1**2)*x3 - u4*x1*(x2**2) + u4*(x1**2)*x2)) &
            &/((x0 - x1)*(x0 - x2)*(x0 - x3)*(x1 - x2)*(x1 - x3)*(x2 - x3))
       
       Uder(1)=(coeff(1)+coeff(2)*x0)/e%volume**2
       Uder(3)=(coeff(1)+coeff(2)*x1)/e%volume**2
       Uder(4)=(coeff(1)+coeff(2)*x2)/e%volume**2
       Uder(2)=(coeff(1)+coeff(2)*x3)/e%volume**2
  CASE default
       PRINT*, 'der2_poly, element 1d'
       STOP
    END SELECT
    
  END FUNCTION der2_poly_element
  
  FUNCTION gradient_element(e,k,x) RESULT (grad) ! gradient in reference element
    CLASS(element), INTENT(in):: e
    INTEGER, INTENT(in):: k ! numero de la fonction de base
    REAL(dp), DIMENSION(2), INTENT(in):: x ! coordonnees barycentriques
    REAL(dp),DIMENSION(n_dim):: grad
    REAL(dp):: fx,fy,fz, alpha, s

    SELECT CASE(e%itype)
    CASE(1,11)! P1
       SELECT CASE(k)
       CASE(1)
          fx=-1.0_dp
       CASE(2)
          fx= 1.0_dp
       END SELECT
    CASE(3,12) !P2
       SELECT CASE(k)
       CASE(1)
          fx=-3.0_dp+4.0_dp*x(1)
       CASE(2)
          fx=4.0_dp*x(1)-1.0_dp
       CASE(3)
          fx=4.0_dp-8.0_dp*x(1)
       END SELECT
    CASE(2) ! B2
       SELECT CASE(k)
       CASE(1)
          fx=-2.0_dp*(1.0_dp-x(1))
       CASE(2)
          fx=2.0_dp*x(1)
       CASE(3)
          fx=2.0_dp-4.0_dp*x(1)
       END SELECT
    CASE(4) ! P3
       SELECT CASE(k)
       CASE(1)
          fx=-11._dp/2._dp+(-27._dp/2._dp*x(1)+18._dp)*x(1)
       CASE(2)
          fx=1.0_dp+x(1)*(27._dp/2._dp*x(1)-9._dp)
       CASE(3)
          fx=9._dp+x(1)*(81._dp/2._dp*x(1)-45._dp)
       CASE(4)
          fx=-9._dp/2._dp+x(1)*(-81._dp/2._dp*x(1)+36._dp)
       END SELECT
    CASE(5)
       SELECT CASE(k)
       CASE(1)
          fx=-3.0_dp*x(2)*x(2)
          !fx=-3.0*x(2)*x(2)
       CASE(2)
          fx=3.0_dp*x(1)*x(1)
          !fx=3.0*x(1)*x(1)
       CASE(3)
          fx= 3.0_dp*x(2)*( x(2)-2._dp*x(1) )
          !fx=3.0*x(2)*( x(2)-2.*x(1) )
       CASE(4)
          fx= 3.0_dp*x(1)*(2._dp*x(2)-x(1))
          !fx=6*x(1)*x(2)-3*x(1)*x(1)
       END SELECT
    CASE(6) !B4
       SELECT CASE(k)
       CASE(1)
          fx=-4.0_dp*x(2)*x(2)*x(2)
       CASE(2)
          fx= 4.0_dp* x(1)*x(1)*x(1)
       CASE(3)
          fx= 4.0_dp*( x(2)-3.0_dp*x(1) )*x(2)*x(2)
       CASE(4) 
          fx=12.0_dp * x(1)*x(2) * ( x(2)-x(1) )
       CASE(5)
          fx=4.0_dp*( 3._dp*x(2)-x(1) )*x(1)*x(1)
       END SELECT

    CASE(7) !P4
       s=x(1)
       SELECT CASE(k)
       CASE(1)
          !fx=-4.0+12.0*x(1)-12.0*x(1)*x(1)+4.0*x(1)*x(1)*x(1)
          fx= (((128._dp*s)/3._dp - 80._dp)*s + 140._dp/3._dp)*s - 25._dp/3._dp
       CASE(2)
          !fx=4.0*x(1)*x(1)*x(1)
          fx=(((128._dp*s)/3._dp - 48._dp)*s + (44._dp)/3._dp)*s - 1._dp
       CASE(3)
          !fx=4.0-24.0*x(1)+36.0*x(1)*x(1)-16.0*x(1)*x(1)*x(1)
          fx= ((- (512._dp*s)/3._dp + 288._dp)*s - (416._dp)/3._dp)*s + 16._dp
       CASE(4) 
          !fx=12.0*x(1)-36.0*x(1)*x(1)+24.0*x(1)*x(1)*x(1)
          fx=((256._dp*s - 384._dp)*s + 152._dp)*s - 12._dp
       CASE(5)
          !fx=12.0*x(1)*x(1)-16.0*x(1)*x(1)*x(1)
          fx=  s*(s*(-(512._dp*s)/3._dp + 224._dp) - 224._dp/3._dp) + 16._dp/3._dp
       END SELECT

    CASE(13) !P3 Gauss Lobatto
			 alpha=0.5_dp-SQRT(5._dp)/10._dp
       SELECT CASE(k)
       CASE(1)
          fx=(- alpha**2._dp + alpha + 3._dp*x(1)**2._dp - 4._dp*x(1) + 1._dp)/(alpha*(alpha - 1._dp))
          !fx=-3.0*x(2)*x(2)
       CASE(2)
          fx=-(- alpha**2._dp + alpha + 3._dp*x(1)**2._dp - 2._dp*x(1))/(alpha*(alpha - 1._dp))
          !fx=3.0*x(1)*x(1)
       CASE(3)
          fx=(2._dp*alpha*x(1) - 4._dp*x(1) - alpha + 3._dp*x(1)**2._dp + 1._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
          !fx=3.0*x(2)*( x(2)-2.*x(1) )
       CASE(4)
          fx= -(alpha - 2._dp*x(1) - 2._dp*alpha*x(1) + 3._dp*x(1)**2._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
          !fx=6*x(1)*x(2)-3*x(1)*x(1)
       END SELECT
    CASE(14) !P4 Gauss Lobatto
			 alpha=0.5_dp-SQRT(21._dp)/14._dp
			 s=x(1)
       SELECT CASE(k)
       CASE(1)
          fx=(4*alpha**2*s - 3*alpha**2 - 4*alpha*s + 3*alpha - 8*s**3 + 15*s**2 - 8*s + 1)/(alpha*(alpha - 1))
          !fx=-3.0*x(2)*x(2)
       CASE(2)
          fx=-(- 4*alpha**2*s + alpha**2 + 4*alpha*s - alpha + 8*s**3 - 9*s**2 + 2*s)/(alpha*(alpha - 1))
          !fx=3.0*x(1)*x(1)
       CASE(3)
          fx=(alpha + 8*s - 6*alpha*s + 6*alpha*s**2 - 15*s**2 + 8*s**3 - 1)/(alpha*(2*alpha - 1)**2*(alpha - 1))
          !fx=3.0*x(2)*( x(2)-2.*x(1) )
       CASE(4)
          fx= (16*(2*s - 1)*(- alpha**2 + alpha + 2*s**2 - 2*s))/(2*alpha - 1)**2
          !fx=6*x(1)*x(2)-3*x(1)*x(1)
	   CASE(5)
	      fx = -(alpha - 2*s - 6*alpha*s + 6*alpha*s**2 + 9*s**2 - 8*s**3)/(alpha*(2*alpha - 1)**2*(alpha - 1))
       END SELECT

    CASE default
       PRINT*, "Type non existant", e%itype
       STOP
    END SELECT
    grad=fx/e%volume
  END FUNCTION gradient_element

  FUNCTION gradient2_element(e,k,x) RESULT (grad2) ! 2nd derivative in reference element
    CLASS(element), INTENT(in):: e
    INTEGER, INTENT(in):: k ! numero de la fonction de base
    REAL(dp), DIMENSION(2), INTENT(in):: x ! coordonnees barycentriques
    REAL(dp),DIMENSION(n_dim):: grad2
    REAL(dp):: fxx, alpha,s

    SELECT CASE(e%itype)
    CASE(1,11)! P1
       SELECT CASE(k)
       CASE(1)
          fxx=0.0_dp
       CASE(2)
          fxx=0.0_dp
       END SELECT
    CASE(3,12) !P2
       SELECT CASE(k)
       CASE(1)
          fxx=4.0_dp
       CASE(2)
          fxx=4.0_dp
       CASE(3)
          fxx=-8.0_dp
       END SELECT
    CASE(2) ! B2
       SELECT CASE(k)
       CASE(1)
          fxx=2.0_dp
       CASE(2)
          fxx=2.0_dp
       CASE(3)
          fxx=-4.0_dp
       END SELECT
    CASE(4) ! P3
			 alpha=1._dp/3._dp
       SELECT CASE(k)
       CASE(1)
          !fxx=-6.0*x(1)+6.0
          fxx=(6._dp*x(1) - 4._dp)/(alpha*(alpha - 1._dp))
       CASE(2)
          !fxx=6.0*x(1)
          fxx=-(6._dp*x(1) - 2._dp)/(alpha*(alpha - 1._dp))
       CASE(3)
          !fxx=18.0*x(1)-12.0
          fxx= (2._dp*alpha + 6._dp*x(1) - 4._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
       CASE(4)
          !fxx=-18.0*x(1)+6.0
          fxx=(2._dp*alpha - 6._dp*x(1) + 2._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
       END SELECT
    CASE(5)
       SELECT CASE(k)
       CASE(1)
          fxx=6.0_dp*x(2)
       CASE(2)
          fxx=6.0_dp*x(1)
       CASE(3)
          fxx= 6.0_dp*x(1)-12.0_dp*x(2)
       CASE(4)
          !fxx=-18.0*x(1)+6.0
          fxx=6.0_dp*x(2)-12.0_dp*x(1)
       END SELECT

    CASE(6)
       SELECT CASE(k)
       CASE(1)
          fxx=12.0_dp-24.0_dp*x(1)+12.0_dp*x(1)*x(1)
       CASE(2)
          fxx=12.0_dp*x(1)*x(1)
       CASE(3)
          fxx=-24.0_dp+72.0_dp*x(1)-48.0_dp*x(1)*x(1)
       CASE(4)
          fxx=12.0_dp-72.0_dp*x(1)+72.0_dp*x(1)*x(1)
       CASE(5)
          fxx=24.0_dp*x(1)-48.0_dp*x(1)*x(1)
       END SELECT

    CASE(7) !P4
       s=x(1)
       SELECT CASE(k)
       CASE(1)
          fxx=(128._dp*s - 160._dp)*s + 140._dp/3._dp
       CASE(2)
          fxx= (128._dp*s - 96._dp)*s + 44._dp/3._dp
       CASE(3)
          fxx= (-512._dp*s + 576._dp)*s - 416._dp/3._dp
       CASE(4)
          fxx= 768._dp*s*(s - 1._dp) + 152._dp
       CASE(5)
          fxx= (- 512._dp*s + 448._dp)*s - 224._dp/3._dp
       END SELECT

    CASE(13)
			 alpha=0.5_dp-SQRT(5._dp)/10._dp
       SELECT CASE(k)
       CASE(1)
          !fxx=-6.0*x(1)+6.0
          fxx=(6._dp*x(1) - 4._dp)/(alpha*(alpha - 1._dp))
       CASE(2)
          !fxx=6.0*x(1)
          fxx=-(6._dp*x(1) - 2._dp)/(alpha*(alpha - 1._dp))
       CASE(3)
          !fxx=18.0*x(1)-12.0
          fxx= (2._dp*alpha + 6._dp*x(1) - 4._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
       CASE(4)
          !fxx=-18.0*x(1)+6.0
          fxx=(2._dp*alpha - 6._dp*x(1) + 2._dp)/(alpha*(2._dp*alpha**2._dp - 3._dp*alpha + 1._dp))
       END SELECT

    CASE(14)
			 alpha=0.5_dp-SQRT(21._dp)/14._dp
			 s=x(1)
       SELECT CASE(k)
       CASE(1)
          !fxx=-6.0*x(1)+6.0
          fxx=-(- 4*alpha**2 + 4*alpha + 24*s**2 - 30*s + 8)/(alpha*(alpha - 1))
       CASE(2)
          !fxx=6.0*x(1)
          fxx=-(- 4*alpha**2 + 4*alpha + 24*s**2 - 18*s + 2)/(alpha*(alpha - 1))
       CASE(3)
          !fxx=18.0*x(1)-12.0
          fxx= (12*alpha*s - 30*s - 6*alpha + 24*s**2 + 8)/(alpha*(2*alpha - 1)**2*(alpha - 1))
       CASE(4)
          !fxx=-18.0*x(1)+6.0
          fxx=(32*(- alpha**2 + alpha + 6*s**2 - 6*s + 1))/(2*alpha - 1)**2
       CASE(5)
          fxx = (6*alpha - 18*s - 12*alpha*s + 24*s**2 + 2)/(alpha*(2*alpha - 1)**2*(alpha - 1))
       END SELECT

    CASE default
       PRINT*, "Type non existant", e%itype
       STOP
    END SELECT
    grad2=fxx/(e%volume**2)
  END FUNCTION gradient2_element

  FUNCTION average_element(e,u) RESULT(u_bar)
    CLASS(element), INTENT(in):: e		
    TYPE(PVar), DIMENSION(e%nsommets),INTENT(in):: u
    TYPE(PVar)  :: u_bar, u_loc
    INTEGER:: i,iq
    u_bar=0._dp

    DO iq = 1, e%nquad

       u_loc   =e%eval_func(u        ,e%quad(:,iq))

       u_bar = u_bar + (u_loc)*e%weight(iq)

    ENDDO ! iq
  END FUNCTION average_element


  SUBROUTINE quadrature_element(e)  ! quadrature points and weights
    ! for triangle and edges
    CLASS(element), INTENT(inout):: e
    REAL(dp):: w,xo,yo,zo,s
    INTEGER:: nquad, nquad_1
    ! degree 5
    REAL(dp), PARAMETER:: s1_3=SQRT(0.6_dp)
    REAL(dp), PARAMETER:: s2_3=0.5_dp*(1.0_dp - s1_3)
    REAL(dp), PARAMETER:: s3_3=0.5_dp*(1.0_dp + s1_3)
    REAL(dp), PARAMETER:: weight1_3=5._dp/18._dp
    REAL(dp), PARAMETER:: weight2_3=8._dp/18._dp
    ! degre 7
    REAL(dp), PARAMETER:: s1_7=(18._dp-SQRT(30._dp))/72._dp
    REAL(dp), PARAMETER:: s2_7=SQRT(525._dp+70._dp*SQRT(30._dp))/35._dp
    REAL(dp), PARAMETER:: s3_7=0.50_dp*(1.00_dp-s2_7)
    REAL(dp), PARAMETER:: s4_7=0.50_dp*(1.00_dp+s2_7)
    REAL(dp), PARAMETER:: s5_7=(18._dp+  SQRT(30._dp))/72._dp 
    REAL(dp), PARAMETER:: s6_7=SQRT(525._dp-70._dp*SQRT(30._dp))/35._dp
    REAL(dp), PARAMETER:: s7_7=0.50_dp*(1.00_dp-s6_7)
    REAL(dp), PARAMETER:: s8_7=0.50_dp*(1.00_dp+s6_7)
    ! degree 9
    REAL(dp),PARAMETER:: s1_9=1.0_dp/3._dp*SQRT(5._dp-2._dp*SQRT(10._dp/7._dp))
    REAL(dp),PARAMETER:: s2_9=0.50_dp*(1.00_dp - s1_9)
    REAL(dp),PARAMETER:: s3_9=0.50_dp*(1.00_dp + s1_9)
    REAL(dp),PARAMETER:: weight1_9=(322._dp+13._dp*SQRT(70.0_dp))/1800.0_dp


    REAL(dp),PARAMETER:: s5_9=1.0_dp/3._dp*SQRT(5._dp+2._dp*SQRT(10._dp/7._dp))
    REAL(dp),PARAMETER:: s6_9=0.50_dp*(1.00_dp - s5_9)
    REAL(dp),PARAMETER:: s7_9=0.50_dp*(1.00_dp + s5_9)
    REAL(dp),PARAMETER:: weight2_9=(322._dp-13._dp*SQRT(70.0_dp))/1800.0_dp
    REAL(dp),PARAMETER:: s4_9=1._dp/3._dp*SQRT(5._dp+2._dp*SQRT(10.0_dp/7.0_dp))

    SELECT CASE(e%itype)
    CASE(1,11)
       e%nquad=2
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))

       ! quadrature for edges (3 point gauss formula)




       e%quad(1,1) = 1.0_dp
       e%quad(2,1) = 0._dp
       e%weight(1) = 0.5_dp

       e%quad(1,2) = 0.0_dp
       e%quad(2,2) = 1.0_dp
       e%weight(2) = 0.5_dp


    CASE(2,3)!! exact for degree 5
       e%nquad=3
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))



       !       s=sqrt(0.6_dp) !0.7745966692414834_dp  !SQRT(0.6_dp)
       e%quad(1,1)=s2_3 !0.5_dp*(1.0_dp - s)
       e%quad(2,1)=s3_3 !0.5_dp*(1.0_dp + s)
       e%weight(1) =weight1_3 !5._dp/18._dp! 0.2777777777777778 !5.0_dp/18.0_dp

       e%quad(1,2)=s3_3 !0.5_dp*(1.0_dp + s)
       e%quad(2,2)=s2_3 !0.5_dp*(1.0_dp - s)
       e%weight(2) =weight1_3 !5._dp/18._dp! 0.2777777777777778 !5.0_dp/18.0_dp

       e%quad(1,3)=0.5_dp
       e%quad(2,3)=0.5_dp
       e%weight(3)= weight2_3 !8._dp/18._dp!0.4444444444444444 ! 8.0_dp/18.0_dp

    CASE(12)!! exact for degree 3, underintegration
       e%nquad = 3
       nquad = e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
       
       e%quad(1,1)=0._dp
       e%quad(2,1)=1.0_dp
       e%weight(1)=1._dp/6._dp


       e%quad(1,2)=0.5_dp
       e%quad(2,2)=0.5_dp
       e%weight(2)=2._dp/3._dp

       e%quad(1,3)=1._dp
       e%quad(2,3)=0._dp
       e%weight(3)=1._dp/6._dp

    CASE(4,5) !! exact for degree 7
       e%nquad=4 ! ordre 7
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad) )
       !      s=(18._dp-sqrt(30._dp))/72._dp !0.1739274225687269_dp  !(18.- SQRT(30.))/72. !SQRT(30._dp)*(-5.0_dp+3._dp*SQRT(30._dp))/360._dp
       e%weight(1:2)=s1_7
       !      s=SQRT(525._dp+70._dp*SQRT(30._dp))/35._dp !0.8611363115940526_dp!SQRT(525._dp+70._dp*SQRT(30._dp))/35._dp
       e%quad(1,1)=s3_7! 0.50_dp*(1.00_dp-s)
       e%quad(1,2)=s4_7 !0.50_dp*(1.00_dp+s)
       e%quad(2,1)=e%quad(1,2) !0.5_dp*(1.0_dp+s)
       e%quad(2,2)=e%quad(1,1) !0.5_dp*(1.0_dp-s)

       !      s=(18._dp+  SQRT(30._dp))/72._dp !0.3260725774312731_dp !(18.+  SQRT(30.))/72.! SQRT(30._dp)*(5.0_dp+3._dp*SQRT(30._dp))/360._dp
       e%weight(3:4)=s5_7
       !       s=SQRT(525._dp-70._dp*SQRT(30._dp))/35._dp !0.3399810435848563_dp !SQRT(525._dp-70._dp*SQRT(30._dp))/35._dp
       e%quad(1,3)=s7_7 !0.50_dp*(1.00_dp-s)
       e%quad(1,4)=s8_7 !0.50_dp*(1.00_dp+s)
       e%quad(2,3)=e%quad(1,4) !0.5_dp*(1.0_dp+s)
       e%quad(2,4)=e%quad(1,3) !0.5_dp*(1.0_dp-s)

       !       e%quad(2,:)=1.0_dp-e%quad(1,:)
    CASE(13)!! exact for order 4, underintegration
       e%nquad=4 
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad) )

       s=0.5_dp-SQRT(5._dp)/10._dp

       e%quad(1,1)=0._dp
       e%quad(2,1)=1.0_dp
       e%weight(1)=1._dp/12._dp


       e%quad(1,2)=s
       e%quad(2,2)=1._dp-s
       e%weight(2)=5._dp/12._dp

       e%quad(1,3)=1._dp -s
       e%quad(2,3)=s
       e%weight(3)=5._dp/12._dp

       e%quad(1,4)=1._dp
       e%quad(2,4)=0._dp
       e%weight(4)=1._dp/12._dp

    CASE(6,7) ! exact for degree 9

       e%nquad=5
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))

       !s=1.0_dp/3._dp*SQRT(5._dp-2._dp*SQRT(10._dp/7._dp))
       e%quad(1,1)=s2_9 !0.50_dp*(1.00_dp - s)
       e%quad(2,1)=s3_9 !0.50_dp*(1.00_dp + s)
       e%weight(1) = weight1_9 !(322._dp+13._dp*SQRT(70.0_dp))/1800.0_dp
       e%quad(1,2)=s3_9 !0.5_dp*(1.0_dp + s)
       e%quad(2,2)=s2_9 !0.5_dp*(1.0_dp - s)
       e%weight(2) = weight1_9 !(322._dp+13._dp*SQRT(70.0_dp))/1800.0_dp

       !      s=1._dp/3._dp*SQRT(5._dp+2._dp*SQRT(10.0_dp/7.0_dp))
       e%quad(1,3)=s6_9 !0.5_dp*(1.0_dp - s)
       e%quad(2,3)=s7_9 !0.5_dp*(1.0_dp + s)
       e%weight(3) = weight2_9 !(322.0_dp-13.0_dp*SQRT(70.0_dp))/1800.0_dp
       e%quad(1,4)=s7_9 !0.5_dp*(1.0_dp + s)
       e%quad(2,4)=s6_9 !0.5_dp*(1.0_dp - s)
       e%weight(4) = weight2_9 ! (322.0_dp-13.0_dp*SQRT(70.0_dp))/1800.0_dp

       e%quad(1,5)=0.5_dp
       e%quad(2,5)=0.5_dp
       e%weight(5)=64.0_dp/225.0_dp


    CASE(14)!! exact for order 5, underintegration
       e%nquad=5
       nquad=e%nquad
       ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad) )

       s=0.5_dp-SQRT(21._dp)/14._dp

       e%quad(1,1)=0._dp
       e%quad(2,1)=1.0_dp
       e%weight(1)=1._dp/20._dp


       e%quad(1,2)=s
       e%quad(2,2)=1._dp-s
       e%weight(2)=49._dp/180._dp

       e%quad(1,3)=0.5_dp
       e%quad(2,3)=0.5_dp
       e%weight(3)=16._dp/45._dp

       e%quad(1,4)=1._dp -s
       e%quad(2,4)=s
       e%weight(4)=49._dp/180._dp

       e%quad(1,5)=1._dp
       e%quad(2,5)=0._dp
       e%weight(5)=1._dp/20._dp

    END SELECT

    e%weight = e%weight/SUM(e%weight)
  END SUBROUTINE quadrature_element




  SUBROUTINE base_ref_element(e)
    ! compute the values of the basis functions at the physical dofs
    REAL(dp), PARAMETER:: ut=1./3., det=1.-ut
    CLASS(element), INTENT(inout)::e
    INTEGER:: l, l_base, l_dof
    REAL(dp), DIMENSION(2,4):: x ! barycentric coordinates for B3
    REAL(dp), DIMENSION(2,5):: z ! barycentric coordinates for B4
    REAL(dp), DIMENSION(2,3):: ba
    ! ebase0( number of the basis function, point l)
    e%base0=0.
    SELECT CASE(e%itype)
    CASE(1,3,4,7,11,12,13,14) ! lagrangian functions

       DO l=1,e%nsommets
          e%base0(l,l)=1.0
       ENDDO
    CASE(2) ! B2 Bezier
       ba=0.0
       ba(1,1)=0.0
       ba(1,2)=1.0 
       ba(1,3)=0.5
       ba(2,:)=1.0-ba(1,:)
       DO l_dof=1,3
          DO l_base=1,3
             e%base0(l_base,l_dof)=base_element(e,l_base,ba(:,l_dof))
          ENDDO
          !write(*,11)l_dof,e%base0(:,l_dof)
          !11        FORMAT(i2,3(1x,f10.4))
       ENDDO


    CASE(5)
       x=0.
       x(1,1)=0.0;
       x(1,2)=1.0
       x(1,3)=1.0/3.0
       x(1,4)=2.0/3.0
       x(2,:)=1.0-x(1,:)
       DO l_dof=1,4
          DO l_base=1,4
             e%base0(l_base,l_dof)=base_element(e,l_base,x(:,l_dof))
          ENDDO
       ENDDO

    CASE(6)
       z=0.
       z(1,1)=0.0;
       z(1,2)=1.0
       z(1,3)=1.0/4.0
       z(1,4)=1.0/2.0
       z(1,5)=0.75
       z(2,:)=1.0-z(1,:)
       DO l_dof=1,5
          DO l_base=1,5
             e%base0(l_base,l_dof)=base_element(e,l_base,z(:,l_dof))
          ENDDO
       ENDDO

       !10     FORMAT(i2,10(1x,f10.4))
    CASE default
       PRINT*, "Element type not yet take into account in base_ref_element", e%itype
       STOP
    END SELECT

    e%base0 = TRANSPOSE(e%base0)
    e%base1=inverse(e%base0)


    CALL bary_element(e)

  END SUBROUTINE base_ref_element

  SUBROUTINE bary_element(e)
    ! defines barycentric coordinates of Lagrange points
    CLASS(element), INTENT(inout)::e
    ALLOCATE(e%x(2,e%nsommets))
    ALLOCATE(e%dof2ind(e%nsommets))
    e%x(1,1)=0.0
    e%x(1,2)=1.0
    SELECT CASE (e%itype)
    CASE(1,11)
       e%dof2ind = (/1, 2/)
    CASE(2,3,12)
       e%x(1,3)=0.5
       e%dof2ind = (/1, 3, 2/)
    CASE(4,5)
       e%x(1,3)=1./3.
       e%x(1,4)=2./3.
       e%dof2ind = (/1, 3, 4, 2/)
    CASE(13)
       e%x(1,3)=0.5_dp-SQRT(5._dp)/10._dp
       e%x(1,4)=0.5_dp+SQRT(5._dp)/10._dp
       e%dof2ind = (/1, 3, 4, 2/)
    CASE(6,7)
       e%x(1,3)=0.25
       e%x(1,4)=0.5
       e%x(1,5)=0.75
       e%dof2ind = (/1, 3, 4, 5, 2/)
    CASE(14)
       e%x(1,3)=0.5_dp-SQRT(21._dp)/14._dp
       e%x(1,4)=0.5
       e%x(1,5)=0.5_dp+SQRT(21._dp)/14._dp
       e%dof2ind = (/1, 3, 4, 5, 2/)
    CASE default
       PRINT*, "bary_element, cas not implemented"
       STOP
    END SELECT
    e%x(2,:)=1.-e%x(1,:)
  END SUBROUTINE bary_element


  SUBROUTINE eval_coeff_element_old(e)
    ! compute \int_K \nabla phi_sigma * phi_sigma'
    CLASS(element), INTENT(inout)::e
    REAL(dp), DIMENSION(e%nsommets):: base
    REAL(dp), DIMENSION(n_dim,e%nsommets):: grad

    INTEGER:: i, iq, l

    ALLOCATE(e%coeff(e%nsommets,e%nsommets))
    ALLOCATE(e%mass (e%nsommets,e%nsommets))
    e%coeff=0._dp
    e%mass=0._dp
    DO iq=1,e%nquad
       DO i=1, e%nsommets
          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO
          DO l=1, e%nsommets
             e%coeff(l,i)=e%coeff(l,i)+ grad(1,l)*base(i)*e%weight(iq)
             e%mass (l,i)=e%mass (l,i)+ base(l)  *base(i)*e%weight(iq)
          ENDDO
       ENDDO
    ENDDO
    e%coeff=e%coeff * e%volume
    e%mass=e%mass * e%volume

  END SUBROUTINE eval_coeff_element_old

  SUBROUTINE eval_coeff_element(e)
    ! compute \int_K \nabla phi_sigma * phi_sigma'
    CLASS(element), INTENT(inout)::e
    REAL(dp), DIMENSION(e%nsommets):: base
    REAL(dp), DIMENSION(n_dim,e%nsommets):: grad

    REAL(dp):: eps
    INTEGER:: i, iq, l, j, k
    !    REAL(dp),  DIMENSION(2,2),parameter:: xp=reshape( (/0._dp,1._dp, 1._dp,0._dp/),(/2,2/) )
    REAL(dp), DIMENSION(2,2):: xp
    xp(1,1)=0.0_dp; xp(2,1)=1.0_dp; xp(1,2)=1.0_dp; xp(2,2)=0.0_dp


    ALLOCATE(e%coeff  (e%nsommets,e%nsommets))
    ALLOCATE(e%mass   (e%nsommets,e%nsommets))
!    ALLOCATE(e%coeff_b(e%nsommets,e%nsommets))

    e%coeff=0._dp
    e%mass=0._dp
    DO iq=1,e%nquad
       DO i=1, e%nsommets
          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO
          DO l=1, e%nsommets
             e%coeff(l,i)=e%coeff(l,i)+ grad(1,i)*base(l)*e%weight(iq)
             e%mass (l,i)=e%mass (l,i)+ base(l)  *base(i)*e%weight(iq)
          ENDDO
       ENDDO
    ENDDO
    e%coeff=e%coeff * e%volume
    e%mass=e%mass   * e%volume

!!$    ! coefficients for the boundary term
!!$    e%coeff_b=0._dp
!!$    eps=1._dp
!!$    
!!$    DO k=1,2
!!$       
!!$       DO l=1, e%nsommets
!!$          base(l)=e%base(l,xp(:,k))
!!$       ENDDO
!!$       
!!$       DO i=1,e%nsommets
!!$          DO j=1, e%nsommets
!!$             e%coeff_b(i,j)=e%coeff_b(i,j)+ eps*base(i)*base(j)
!!$          ENDDO
!!$       ENDDO
!!$
!!$       eps = -eps
!!$    ENDDO


  END SUBROUTINE eval_coeff_element

  SUBROUTINE clean(e)
    TYPE(element), INTENT(inout)::e
    IF (ASSOCIATED(e%coor))  NULLIFY(e%coor)
    IF (ASSOCIATED(e%n))  NULLIFY(e%nu)
    IF (ASSOCIATED(e%n))  NULLIFY(e%n)
    IF (ASSOCIATED(e%quad))  NULLIFY(e%quad)
    IF (ASSOCIATED(e%weight))  NULLIFY(e%weight)
    IF (ASSOCIATED(e%base0))  NULLIFY(e%base0)
    IF (ASSOCIATED(e%base1))  NULLIFY(e%base1)
  END SUBROUTINE clean

END MODULE element_class
