!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE overloading
  USE variable_def
  use precision
  IMPLICIT NONE

  INTERFACE ASSIGNMENT (=)
     MODULE PROCEDURE assign_var
     MODULE PROCEDURE assign_var_real
     MODULE PROCEDURE assign_var_vec
  END INTERFACE ASSIGNMENT (=)

  INTERFACE OPERATOR (+)
     MODULE PROCEDURE addition_var
     MODULE PROCEDURE addition_Vecvar
     MODULE PROCEDURE addition_Vecvar_var
     MODULE PROCEDURE addition_var_Vecvar
  END INTERFACE OPERATOR (+)

  INTERFACE OPERATOR (-)
     MODULE PROCEDURE subtraction_var
     MODULE PROCEDURE subtraction_Vecvar
     MODULE PROCEDURE subtraction_Vecvar_var
     MODULE PROCEDURE subtraction_var_Vecvar
  END INTERFACE OPERATOR (-)

  INTERFACE OPERATOR (*)
     MODULE PROCEDURE multiple_var_var
     MODULE PROCEDURE multiple_real_var
     MODULE PROCEDURE multiple_var_real
     MODULE PROCEDURE multiple_vec_tensor
     MODULE PROCEDURE multiple_tensor_vec
     MODULE PROCEDURE multiple_Vec_Vecvar
     MODULE PROCEDURE multiple_Vecvar_Vec
     MODULE PROCEDURE multiple_matrix_var
     MODULE PROCEDURE multiple_Vecvar_real
     MODULE PROCEDURE multiple_real_Vecvar
     MODULE PROCEDURE multiple_var_realvec
     MODULE PROCEDURE multiple_realvec_var
  END INTERFACE OPERATOR (*)

  INTERFACE OPERATOR (/)
     MODULE PROCEDURE divide_var_real
     MODULE PROCEDURE divide_Vecvar_Vecreal
  END INTERFACE OPERATOR (/)

  INTERFACE SUM
     MODULE PROCEDURE pvar_sum
  END INTERFACE SUM



CONTAINS

  SUBROUTINE assign_var_vec( var_result, var)
    TYPE(PVar), DIMENSION(:),INTENT(in)  :: var
    TYPE(PVar), DIMENSION(:),INTENT(out) :: var_result
    INTEGER:: i
    DO i=1, SIZE(Var,dim=1)
       var_result(i)%NVars = var(i)%NVars
       var_result(i)%u     = var(i)%u
    ENDDO

  END SUBROUTINE assign_var_vec


  ELEMENTAL SUBROUTINE assign_var( var_result, var)
    TYPE(PVar), INTENT(in)  :: var
    TYPE(PVar), INTENT(out) :: var_result

    var_result%NVars = var%NVars
    var_result%u     = var%u

  END SUBROUTINE assign_var

  ELEMENTAL  SUBROUTINE assign_var_real( var_result, alpha)
    REAL(dp),       INTENT(IN)  :: alpha
    TYPE(PVar), INTENT(out) :: var_result

    var_result%u(:) = alpha

  END SUBROUTINE assign_var_real


  FUNCTION addition_var(var1, var2) RESULT(add_var)
    TYPE(PVar), INTENT(IN) :: var1
    TYPE(PVar), INTENT(IN) :: var2
    TYPE(PVar)             :: add_var

    add_var%u = var1%u + var2%u

  END FUNCTION addition_var

  FUNCTION addition_Vecvar(var1, var2) RESULT(add_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var1
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var1))    :: add_var
    INTEGER:: i
    DO i=1, SIZE(var1)
       add_var(i)%u(:) = var1(i)%u(:) + var2(i)%u(:)
    ENDDO

  END FUNCTION addition_Vecvar

  FUNCTION addition_Vecvar_var(var1, var2) RESULT(add_vecvar_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var1
    TYPE(PVar),               INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var1))    :: add_vecvar_var
    INTEGER:: l
    DO l=1, SIZE(var1)
       add_vecvar_var(l)%u = var1(l)%u + var2%u
    ENDDO
  END FUNCTION addition_Vecvar_var

  FUNCTION addition_var_Vecvar(var1, var2) RESULT(add_vecvar_var)
    TYPE(PVar),               INTENT(IN) :: var1
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var2))    :: add_vecvar_var
    INTEGER:: l
    DO l=1, SIZE(var2)
       add_vecvar_var(l)%u = var1%u + var2(l)%u
    ENDDO
  END FUNCTION addition_var_Vecvar

  FUNCTION subtraction_var(var1, var2) RESULT(subtract_var)
    TYPE(PVar), INTENT(IN) :: var1
    TYPE(PVar), INTENT(IN) :: var2
    TYPE(PVar)             :: subtract_var

    subtract_var%u = var1%u - var2%u

  END FUNCTION subtraction_var

  FUNCTION subtraction_Vecvar(var1, var2) RESULT(subtract_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var1
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var1))    :: subtract_var
    INTEGER:: l
    DO l=1, SIZE(var1)
       subtract_var(l)%u = var1(l)%u - var2(l)%u
    ENDDO
  END FUNCTION subtraction_Vecvar

  FUNCTION subtraction_Vecvar_var(var1, var2) RESULT(subtract_vecvar_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var1
    TYPE(PVar),               INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var1))    :: subtract_vecvar_var
    INTEGER:: l
    DO l=1, SIZE(var1)
       subtract_vecvar_var(l)%u = var1(l)%u - var2%u
    ENDDO
  END FUNCTION subtraction_Vecvar_var

  FUNCTION subtraction_var_Vecvar(var1, var2) RESULT(subtract_vecvar_var)
    TYPE(PVar),               INTENT(IN) :: var1
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var2))    :: subtract_vecvar_var
    INTEGER:: l
    DO l=1, SIZE(var2)
       subtract_vecvar_var(l)%u = var1%u - var2(l)%u
    ENDDO
  END FUNCTION subtraction_var_Vecvar

  FUNCTION multiple_var_var(var1, var2) RESULT(mult_var_var)
    TYPE(PVar), INTENT(IN) :: var1
    TYPE(PVar), INTENT(IN) :: var2
    TYPE(PVar)             :: mult_var_var

    mult_var_var%u = var1%u * var2%u

  END FUNCTION multiple_var_var

  FUNCTION multiple_Vec_Vecvar(var1, var2) RESULT(mult_var_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var1
    REAL(dp),       DIMENSION(:), INTENT(IN) :: var2
    TYPE(PVar), DIMENSION(SIZE(var1))    :: mult_var_var
    INTEGER:: i
    DO i=1, SIZE(Var1)
       mult_var_var(i)%u = var1(i)%u * var2(i)
    ENDDO
  END FUNCTION multiple_Vec_Vecvar

  FUNCTION multiple_Vecvar_Vec(var1, var2) RESULT(mult_var_var)
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var2
    REAL(dp),       DIMENSION(:), INTENT(IN) :: var1
    TYPE(PVar), DIMENSION(SIZE(var1))    :: mult_var_var
    INTEGER:: i

    DO i=1, SIZE(Var1)
       mult_var_var(i)%u = var2(i)%u * var1(i)
    ENDDO

  END FUNCTION multiple_Vecvar_Vec

  FUNCTION multiple_real_var(alpha, var) RESULT(mult_real_var)
    REAL(dp),       INTENT(IN) :: alpha
    TYPE(PVar), INTENT(IN) :: var
    TYPE(PVar)             :: mult_real_var

    mult_real_var%u(:) = alpha * var%u(:)

  END FUNCTION multiple_real_var

  FUNCTION multiple_var_real(var,alpha) RESULT(mult_var_real)
    REAL(dp),       INTENT(IN) :: alpha
    TYPE(PVar), INTENT(IN) :: var
    TYPE(PVar)             :: mult_var_real

    mult_var_real%u(:) = alpha * var%u(:)

  END FUNCTION multiple_var_real

  FUNCTION multiple_Vecvar_real(var,alpha) RESULT(mult_var_real)
    REAL(dp),       INTENT(IN) :: alpha
    TYPE(PVar), DIMENSION(:),INTENT(IN) :: var
    TYPE(PVar),DIMENSION(SIZE(var))             :: mult_var_real
    INTEGER:: l
    DO l=1, SIZE(var)
       mult_var_real(l)%u(:) = alpha * var(l)%u(:)
    ENDDO

  END FUNCTION multiple_Vecvar_real

  FUNCTION multiple_real_Vecvar(alpha,var) RESULT(mult_var_real)
    REAL(dp),       INTENT(IN) :: alpha
    TYPE(PVar), DIMENSION(:),INTENT(IN) :: var
    TYPE(PVar),DIMENSION(SIZE(var))             :: mult_var_real
    INTEGER:: l
    DO l=1, SIZE(var)
       mult_var_real(l)%u(:) = alpha * var(l)%u(:)
    ENDDO

  END FUNCTION multiple_real_Vecvar

  FUNCTION multiple_var_realvec(var,alpha) RESULT(mult_var_vecreal)
    REAL(dp),    DIMENSION(:), INTENT(IN) :: alpha
    TYPE(PVar),               INTENT(IN) :: var
    TYPE(PVar), DIMENSION(SIZE(alpha))   :: mult_var_vecreal
    INTEGER :: l
    DO l=1, SIZE(alpha)
       mult_var_vecreal(l)%u(:) = alpha(l) * var%u(:)
    ENDDO

  END FUNCTION multiple_var_realvec

  FUNCTION multiple_realvec_var(alpha,var) RESULT(mult_var_vecreal)
    REAL(dp),    DIMENSION(:), INTENT(IN) :: alpha
    TYPE(PVar),               INTENT(IN) :: var
    TYPE(PVar), DIMENSION(SIZE(alpha))     :: mult_var_vecreal
    INTEGER :: l
    DO l=1, SIZE(alpha)
       mult_var_vecreal(l)%u(:) = alpha(l) * var%u(:)
    ENDDO

  END FUNCTION multiple_realvec_var

  FUNCTION divide_var_real(var,alpha) RESULT(div_var_real)
    REAL(dp),       INTENT(IN) :: alpha
    TYPE(PVar), INTENT(IN) :: var
    TYPE(PVar)             :: div_var_real

    div_var_real%u(:) =  var%u(:)/alpha

  END FUNCTION divide_var_real

  FUNCTION divide_Vecvar_Vecreal(var,alpha) RESULT(div_var_real)
    REAL(dp),       DIMENSION(:), INTENT(IN) :: alpha
    TYPE(PVar), DIMENSION(:), INTENT(IN) :: var
    TYPE(PVar), DIMENSION(SIZE(var))     :: div_var_real
    INTEGER:: i

    DO i=1, SIZE(Var)
       div_var_real(i)%u(:) =  var(i)%u(:)/alpha(i)
    ENDDO

  END FUNCTION divide_Vecvar_Vecreal

  FUNCTION multiple_vec_tensor(vec, tensor) RESULT (matrix)
    REAL(dp), DIMENSION(N_vars, N_vars, N_dim), INTENT(IN) :: tensor
    REAL(dp), DIMENSION(n_dim),                 INTENT(IN) :: vec
    REAL(dp), DIMENSION(n_vars,n_vars)                     :: matrix
    INTEGER:: i

    matrix=0.0_dp
    DO i=1, N_dim
       matrix(:,:)= matrix(:,:)+vec(i) * tensor(:,:,i)
    ENDDO

  END FUNCTION multiple_vec_tensor

  FUNCTION multiple_matrix_var(A,var1) RESULT(var2)
    REAL, INTENT(in), DIMENSION(:,:):: A
    TYPE(PVar), INTENT(in):: var1
    TYPE(PVar):: var2

    var2%u=MATMUL(A,var1%u)

  END FUNCTION multiple_matrix_var

  FUNCTION multiple_tensor_vec(tensor, vec) RESULT (matrix)
    REAL, DIMENSION(N_vars, N_vars, N_dim), INTENT(IN) :: tensor
    REAL, DIMENSION(n_dim),                 INTENT(IN) :: vec
    REAL, DIMENSION(n_vars,n_vars)                     :: matrix
    INTEGER:: i

    matrix=0.0_dp
    DO i=1, N_dim
       matrix(:,:)= matrix(:,:) + vec(i) * tensor(:,:,i)
    ENDDO

  END FUNCTION multiple_tensor_vec


  FUNCTION pvar_sum(pvar_array)
    TYPE(PVar), DIMENSION(:), INTENT(in) :: pvar_array
    TYPE(PVar)                           :: pvar_sum
    INTEGER :: i

    pvar_sum%u(:)=0.0_dp

    DO i=1,SIZE(pvar_array)
       pvar_sum%u(:) = pvar_sum%u(:) + pvar_array(i)%u(:)
    END DO

  END FUNCTION pvar_sum

!!!!-----------------------


END MODULE overloading
