MODULE init_bc

  USE param2d
  USE overloading
  USE precision
  USE utils

  IMPLICIT NONE

CONTAINS

  !---------------------------------------
  ! Setup domain
  !---------------------------------------

  SUBROUTINE init_geom(DATA)
    TYPE(donnees), INTENT(inout):: DATA

    SELECT CASE(DATA%test)
    CASE(0,40,41,42)
       ! Isentropic
       DATA%domain_left = -1.0_dp
       DATA%Length      =  2.0_dp

    CASE(1,11,12,13,32)
       ! Sod
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp
       
    CASE(2)
       ! Shu-Osher
       DATA%domain_left = -5.0_dp
       DATA%Length      = 10.0_dp

    CASE(3)
       ! Woodward-Colella
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE(4)
       ! channel
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp

    CASE(5)
       ! Isentropic vortex
       DATA%domain_left = -1.0_dp
       DATA%Length      =  2.0_dp

!       DATA%domain_left = 0.0_dp
!       DATA%Length      = 1.0_dp

    CASE(6)
       ! Woodward-Colella left
       DATA%domain_left = 0.0_dp
       DATA%Length      = 1.0_dp
       ! LeBlanc
    CASE(21, 20,24,25,19,14,26,27) !convection param = 2. for 20/21, conv = 5.5 for 24,conv = 3 for 25
       !simetrie
      DATA%domain_left=0.0_dp
      DATA%Length  = 25._dp
       ! 1-2-3
    CASE(22,23) !convection bigger 8.
      DATA%domain_left=0.0_dp
      DATA%Length  = 25._dp

    CASE(29,30) !convection bigger than 4.
      DATA%domain_left=0.0_dp
      DATA%Length  = 4._dp

    CASE(31) !convection bigger than 4.
      DATA%domain_left=0.0_dp
      DATA%Length  = 15._dp



    CASE default
       PRINT*, "Wrong test number for geom(), test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE init_geom

  !---------------------------------------
  ! Set initial and boundary conditions
  !---------------------------------------

  FUNCTION IC(x,DATA) RESULT(Var)
    REAL(dp),       INTENT(in)   :: x
    TYPE(donnees), INTENT(inout):: DATA
    REAL(dp), DIMENSION(n_dim) :: eta
    TYPE(PVar) :: Var
    REAL(dp)    :: alpha,r,x0

    eta = 0._dp ! profile_bathymetry( DATA%test, (/x/) )

    !---------------
    ! for Euler
    SELECT CASE(DATA%test)
    CASE(0)
        ! Convergence test: isentropic flow
        DATA%tmax = 1.
        alpha = 0.1
        Var%u(1) = 1. + alpha*SIN(PI*x) ! + 1. 
        Var%u(2) = 0.

    CASE(40,41)
        ! Convergence test: isentropic flow
        DATA%tmax = 1.
        alpha = 0.1
        Var%u(1) = 1. + alpha*SIN(PI*x) - bathymetry(DATA%test,x) ! + 1. 
        Var%u(2) = 0.05

    CASE(42)
        ! Convergence test: isentropic flow
        DATA%tmax = 1.0_dp
        alpha = 0.01_dp
        Var%u(1) = 1._dp + alpha*SIN(PI*x) - bathymetry(DATA%test,x) ! + 1. 
        Var%u(2) = 0._dp

    CASE(1)
        ! Sod
        DATA%tmax = 0.13
        IF (x < 0.5) THEN
            Var%u(1) = 1.-bathymetry(DATA%test,x) ! [kg/m^3]
            Var%u(2) = 0. ! [m/s]
        ELSE
            Var%u(1) = 0.05 -bathymetry(DATA%test,x)
            Var%u(2) = 0.
        END IF

    CASE(32)
        ! Dam break
        DATA%tmax = 0.05
        IF (x < 0.5) THEN
            Var%u(1) = 1.-bathymetry(DATA%test,x) ! [kg/m^3]
            Var%u(2) = 0. ! [m/s]
        ELSE
            Var%u(1) = 0.0
            Var%u(2) = 0.
        END IF


    CASE(11)
        ! Sod
        DATA%tmax = 5.3
        Var%u(1) = 0.01+ 0.3*EXP(-100.* (x-0.7)**2.) -bathymetry(DATA%test,x)
        Var%U(2) = 0.*Var%U(1)

    CASE(12,13)
      DATA%tmax = 5.

      Var%U(1) = 1.- bathymetry(DATA%test,x)
      Var%U(2) = 0.

    CASE(21)
      DATA%tmax = 5.
      Var%U(1) = 0.5 -bathymetry(DATA%test,x)
      Var%U(2) = 0.

    CASE(20)
      DATA%tmax = 20.
      Var%U(1) = 0.5 -bathymetry(DATA%test,x)
      Var%U(2) = 0.
      IF (x<18 .AND. x>16) THEN
        Var%U(1) = Var%U(1) + 0.1
      END IF

    CASE(14)
      DATA%tmax = 60.
      Var%U(1) = 0.205 -bathymetry(DATA%test,x)
      Var%U(2) = 1.*Var%U(1)

    CASE(19)
      DATA%tmax = 12.5
      Var%U(1) = 0.5 -bathymetry(DATA%test,x)
      Var%U(2) = -1.
      IF (x<18 .AND. x>16) THEN
        Var%U(1) = Var%U(1) + 0.05*EXP(1._dp-1._dp/(1.-(x-17.)**2.))
      END IF

    CASE(22)
      DATA%tmax = 100.
      Var%U(1) = 2. -bathymetry(DATA%test,x)
      Var%U(2) = 0.
    CASE(23)
      DATA%tmax = 100.
      Var%U(1) = 2. -1.5*bathymetry(DATA%test,x)
      Var%U(2) = 4.42_dp


    CASE(24)
      DATA%tmax = 60.
      IF (x<5.) THEN
        Var%U(1) = 1.01445 -bathymetry(DATA%test,x)
        Var%U(2) = 1.53_dp
      ELSE IF (x<15.) THEN
        Var%U(1) = (1.01445)*(15.-x)/10. + 0.405781*(x-5.)/10. -bathymetry(DATA%test,x)
        Var%U(2) = 1.53_dp
      ELSE
        Var%U(1) = 0.405781 -bathymetry(DATA%test,x)
        Var%U(2) = 1.53_dp
      END IF

      ! Var%U(1) = 0.66 -bathymetry(DATA%test,x)
      ! Var%U(2) = 0.

    CASE(25)
      DATA%tmax = 100.
      IF (x<8.) THEN
        Var%U(1) = 0.4 -bathymetry(DATA%test,x)
      ELSE
        Var%U(1) = 0.33 -bathymetry(DATA%test,x)
      END IF
      Var%U(2) = 0.14

    CASE(26)
      DATA%tmax = 60.
      Var%U(1) = 0.3 -bathymetry(DATA%test,x)
      Var%U(2) = 0.2*Var%U(1)

   CASE(30) ! thucker oscillations in parabola
       DATA%tmax = 10.0303_dp
       IF (x<2.5 .AND. x>0.5) THEN
          Var%U(1) = -0.5*((x-1.5)**2.-1)
       ELSE
          Var%U(1) = 0._dp
       END IF
       Var%U(2) = 0._dp
   CASE(29) !lake at rest in parabola
       DATA%tmax = 3._dp
       Var%U(1) = MAX(0._dp, 0.5-bathymetry(DATA%test,x))
       Var%U(2) = 0._dp

    CASE(31) !waves on the shore
        DATA%tmax = 20.0_dp
        Var%U(1) = MAX(0.37-bathymetry(DATA%test,x),0._dp)
        Var%U(2) = 0._dp

    CASE(2)
        ! Shu-Osher
        DATA%tmax = 1.8
        alpha = 0.2
        IF (x <= -4.) THEN
            Var%u(1) = 3.857143 ! [kg/m^3]
            Var%u(2) = Var%u(1)*2.629369 ! [m/s]
        ELSE
            Var%u(1) = 1. + alpha*SIN(5.*x)
            Var%u(2) = 0.
        END IF

    CASE(3)
        ! Woodward-Colella
        DATA%tmax = 0.038
        IF (x <= 0.1) THEN
            Var%u(1) = 1. ! [kg/m^3]
            Var%u(2) = 0. ! [m/s]
        ELSE
             IF (x > 0.1 .AND. x < 0.9) THEN
                Var%u(1) = 1. ! [kg/m^3]
                Var%u(2) = 0. ! [m/s]
             ELSE
                Var%u(1) = 1. ! [kg/m^3]
                Var%u(2) = 0. ! [m/s]
             END IF
        END IF

    CASE(4)
        ! 1D version of 2D channel
        Var%u(1) = 0.5_dp ! [kg/m^3]
        Var%u(2) = 0.0_dp ! [m/s]

    CASE(5)
        ! Convergence test: isentropic vortex
        DATA%tmax = 0.5
!        DATA%tmax = 0.25
        Var%u(1) = 1. + 0.1*EXP(-(x)**2/(2.*0.01))
!        Var%u(1) = 1.+EXP(-80.0_dp*(x-0.4_dp)**2)
        Var%u(2) = Var%u(1)*1.

    CASE(6)
        ! Woodward-Colella left
        DATA%tmax = 0.012
       IF (x <= 0.5) THEN
          Var%u(1) = 1. ! [kg/m^3]
          Var%u(2) = 0. ! [m/s]
       ELSE
          Var%u(1) = 1. ! [kg/m^3]
          Var%u(2) = 0. ! [m/s]
       END IF
           
    CASE(10)
       DAta%tmax=0.5
       Var%u(1) = 0.001
       IF(x< 0.) THEN
         Var%u(2)= -Var%u(1)
        ELSE
         Var%u(2) = Var%u(1)
        END IF

    CASE (27)
      DATA%tmax = 15.0_dp
      r=0.5d0
      x0=3d0
      alpha=0.1d0
      IF (x<x0+r .AND. x>x0-r) THEN
        var%u(1) = 1._dp +alpha*EXP(1.d0-1.d0/(1.-((x-x0)/r)**2.))!(4.0d0/9.81d0) **(1.d0/3d0) !*(1.d0-x) ! + 1. 
      ELSE
        var%u(1)= 1._dp ! (4.0d0/9.81d0) **(1.d0/3d0) 
      ENDIF
      Var%u(2) = 4_dp
    CASE default
       PRINT*, "Wrong test number for SW_IC, test = ", DATA%test
       STOP
    END SELECT


  END FUNCTION IC

  SUBROUTINE BC(Var,i1,iN,DATA,k)
    TYPE(variables), INTENT(inout):: Var
    INTEGER,         INTENT(in)   :: i1, iN, k
    TYPE(donnees),   INTENT(in)   :: DATA
    REAL(dp), DIMENSION(n_dim) :: eta
    TYPE(Pvar):: a0,a1
    INTEGER:: p1, pN

    SELECT CASE(DATA%test)
    CASE(0,11,12,20,19,14,40,41,42,27)
!!$        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

    CASE(1,2,6,10,13,30,29,32)
       ! outflow
       Var%un(i1)%u(:)=0._dp
       Var%un(iN)%u(:)=0.0_dp
    CASE(3)
        ! reflective
		!dirichlet for velocity

        Var%un(i1)%u(2) = 0.0
        Var%un(iN)%u(2) = 0.0
	  


    CASE(4)
        ! inflow left + outflow right
        Var%ua(i1,k-1)%u(1) = 1.0_dp
        Var%ua(i1,k-1)%u(2) = 3.0_dp
        ! Var%ua(k-1,i1)%u(3) = 4.5_dp + 1.7857_dp

    CASE(21)
      !dirichlet bc
       Var%ua(i1,k-1)%u(1) = 0.5_dp
       Var%ua(iN,k-1)%u(1) = 0.5_dp
       Var%ua(i1,k-1)%u(2) = 0.0_dp
       Var%ua(iN,k-1)%u(2) = 0.0_dp


    CASE(22,23)
      !dirichlet bc
       Var%ua(i1,k-1)%u(2) = 4.42_dp
       Var%ua(iN,k-1)%u(1) = 2._dp!MIN(2.0_dp,Var%ua(k-1,iN)%u_lim(1) ) 


    CASE(31) !waves on the shore
      !dirichlet bc
       eta = 0._dp !profile_bathymetry( DATA%test, (/ DATA%domain_left /) )
       Var%un(iN)%u(:) = 0._dp
       Var%ua(i1,k-1)%u(1) = 0.37_dp+0.0615*SIN(DATA%temps*12.*ASIN(1.)/10.)-bathymetry(DATA%test,DATA%domain_left)
       Var%ua(iN,k-1)%u(1) = 0.0_dp
       Var%ua(iN,k-1)%u(2) = 0.0_dp
       ! Var%ua(k-1,iN)%u_lim(1) = 2._dp!MIN(2.0_dp,Var%ua(k-1,iN)%u_lim(1) ) 



    CASE(24)
       !dirichlet bc
       ! PRINT*, "BC for i1. iN", i1, iN
       ! PRINT*, Var%ua(k-1,i1)
       ! PRINT*, Var%ua(k-1,iN)
       ! Var%un(i1) = 0._dp
       ! Var%un(iN) = 0._dp
       ! Var%ua(k-1,i1)%u_lim(1) = 1.01445
       Var%ua(i1,k-1)%u(2) = 1.53_dp
       IF (Var%ua(iN,k-1)%u(1) >0.66 ) THEN
          Var%ua(iN,k-1)%u(1) = 0.60
       ENDIF
       ! Var%ua(k-1,iN)%u_lim(1) = 0.405781 !MIN(0.66_dp,Var%ua(k-1,iN)%u_lim(1) ) 
       ! Var%ua(k-1,iN)%u_lim(2) = 1.53_dp


    CASE(25)
      !dirichlet bc
       Var%ua(i1,k-1)%u(2) = 0.18_dp
       Var%ua(iN,k-1)%u(1) = 0.33_dp


     CASE(26)
      !dirichlet bc
       Var%ua(i1,k-1)%u(2) = 0.3_dp
       Var%ua(iN,k-1)%u(1) = 0.3_dp


    CASE(5)
        ! periodic
        p1=i1
        pN=iN
        a0=Var%un(p1)
        a1=Var%un(pN)
        Var%un(p1)=a0+a1
        Var%un(pN)=a0+a1

    CASE default
       PRINT*, "Wrong test number for SW BC, test = ", DATA%test
       STOP
    END SELECT

  END SUBROUTINE BC

  SUBROUTINE isPeriodic(DATA)
    TYPE(donnees),   INTENT(inout)   :: DATA

    SELECT CASE(DATA%test)
    CASE(0,11,12,20,19,14,40,41,42,27)
      DATA%periodicBC = .TRUE.
    CASE default
      DATA%periodicBC = .FALSE.
    END SELECT
  END SUBROUTINE isPeriodic


END MODULE init_bc
