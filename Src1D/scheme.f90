!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE scheme !1D
  ! In this module all scheme for the flux are collected.
  ! Structure of this module:
  ! - schema -> allows to choose between different scheme for the flux approximation,
  !                 which are coded in this same module and are the following
  !                 a)  : galerkin as such, it is not quadrature free for non linear problems
  !                     : galerkin_new:  this one is quadrature free
  !                 b) galerkin + psi limiting, for galerkin : see above
  !                 c) local Lax-Friedrichs (=Rusanov). use Galerkin as central part, so see a)
  ! - stabilization: Burman's jump
  ! - limiting: generic function to activate the limiting,
  !              which can be done either via conservative variables, or characteristic ones
  USE overloading
  USE element_class
  USE variable_def
  USE arete_class
  USE param2d
  USE Model
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  PUBLIC:: schema, jump

CONTAINS
  FUNCTION schema(ischema, e, u, du, flux, source, dt, jt,mesh) RESULT(res)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! residual (supg or psi)
    ! 
    INTEGER, INTENT(IN):: jt
    TYPE(maillage),INTENT(IN):: mesh
    INTEGER, INTENT(in):: ischema
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: u
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: du, source
    TYPE(PVar), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res

    SELECT CASE (e%type_flux)
    CASE(4)
       res=galerkin_new(e, u, du, flux, source, dt,jt,mesh)
    CASE(5)
       res=psi_galerkin(e, u, du, flux, source, dt,jt,mesh)
    CASE(-1)
       res=lxf(e, u, du, flux, source, dt,jt,mesh)
    CASE default
       PRINT*, "scheme not defined"
       STOP
    END SELECT
  END FUNCTION schema

  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ SCHEME FOR FLUX ---------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------

  FUNCTION galerkin(e, u, du, flux, source, dt,jt,mesh) RESULT(res)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    INTEGER, INTENT(IN):: jt
    TYPE(maillage),INTENT(IN):: mesh
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: u
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: du, source
    TYPE(PVar), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)  ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res

    REAL(dp), DIMENSION(e%nsommets):: base
    REAL(dp), DIMENSION(n_dim,e%nsommets):: grad
    REAL(dp), DIMENSION(n_dim):: vit
    REAL(dp), PARAMETER, DIMENSION(n_dim)::xx=0._dp
    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(dp), DIMENSION(n_vars,n_vars,n_dim):: Jac
    REAL(dp), DIMENSION(n_vars,n_vars):: nnmat
    TYPE(PVar) :: du_loc, flux_loc, u_loc, source_loc

    REAL(dp)   :: hh, eps
    INTEGER   :: i, iq, l, k
    TYPE(Pvar),DIMENSION(n_dim) :: fluxloc

    TYPE(PVar) :: divflux
    REAL(dp),  DIMENSION(2,2):: xp
    xp(1,1)=0.0_dp; xp(2,1)=1.0_dp; xp(1,2)=1.0_dp; xp(2,2)=0.0_dp
    DO l=1, e%nsommets
       res(l)=0._dp
    ENDDO
    DO i=1, e%nsommets

       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO



          u_loc   = e%eval_func(u       ,e%quad(:,iq))
          du_loc  = e%eval_func(du      ,e%quad(:,iq))
          source_loc  = e%eval_func(source      ,e%quad(:,iq))
#if (1==1)
          ! not quadrature free
          fluxloc=u_loc%flux(xx) ! warning: mismatch between bary coord and e%coord
#else

          ! quadrature free !correct
          fluxloc(1)=e%eval_func(flux(1,:),e%quad(:,iq))
#endif


          res(i) = res(i) + ( base(i)*du_loc-dt*grad(1,i)*fluxloc(1) -dt*source_loc*base(i) )*e%weight(iq)



       ENDDO ! iq
       res(i) = res(i)* e%volume
    ENDDO ! i

    eps=-dt ! 
    DO k=1,2
       DO l=1, e%nsommets
          base(l)=e%base(l,xp(:,k))
       ENDDO

       fluxloc=e%eval_func(flux(1,:),xp(:,k))
       res(:) = res(:)+eps*fluxloc(1)*base
       eps = -eps
    ENDDO


  END FUNCTION galerkin


  FUNCTION galerkin_new(e, u, du, flux, source, dt,jt,mesh) RESULT(res)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    INTEGER, INTENT(IN):: jt
    TYPE(maillage),INTENT(IN):: mesh
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: u
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: du, source
    TYPE(PVar), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)  ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res



    INTEGER   ::  l




    DO l=1, SIZE(flux(1,1)%u)
       res(:)%u(l)=MATMUL(e%mass(:,:),du%u(l)-dt*source%u(l)) &
            &+dt*MATMUL(e%coeff   (:,:), flux(1,:)%u(l) )!&
       !       &+dt*MATMUL(e%coeff_b (:,:), flux(1,:)%u(l) )
    ENDDO


  END FUNCTION galerkin_new

  FUNCTION psi_galerkin(e, u, du, flux, source, dt,jt, Mesh) RESULT(res)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    INTEGER, INTENT(IN):: jt
    TYPE(maillage),INTENT(IN):: mesh
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: u
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: du, source
    TYPE(PVar), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)   ,                        INTENT(in):: dt
    TYPE(PVar), DIMENSION(e%nsommets)            :: res, phi_lxF,w
    INTEGER::  k, l

    phi_lxf= lxf(e,u,du,flux, source,dt,jt,mesh)
    CALL limit(e,u,res,phi_lxf)

  END FUNCTION psi_galerkin

  FUNCTION lxf(e, u, du, flux, source, dt,jt,mesh) RESULT(phi_lxf)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux

    INTEGER, INTENT(IN):: jt
    TYPE(maillage),INTENT(IN):: mesh
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: u
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in):: du, source
    TYPE(PVar), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res, phi_lxf,u_cons
    TYPE(PVar):: u_consL, u_consR
    REAL(dp),DIMENSION(n_dim):: xx=0._dp
    TYPE(PVar), DIMENSION(e%nsommets):: v,u_diff,u_diff2
    TYPE(PVar) ::  ubar
    TYPE(PVar), DIMENSION(e%nsommets)::vbar
    REAL(dp)::   alpha
    INTEGER:: l,i,iq, k

    res= galerkin_new(e, u, du, flux, source, dt,jt,mesh)

    u_cons=Control_to_cons(u,e)
    DO l = 1,n_vars
       ubar%u(l)= SUM(u%u(l))/e%nsommets
    END DO

    DO l=1,e%nsommets
       vbar(l)=ubar
    END DO
    DO i=1,e%nsommets
       u_diff(i)=(u(i)-vbar(i))
       alpha=ubar%spectral_radius(xx,e%n) 
    END DO
    alpha=ubar%spectral_radius(xx,e%n) 
    phi_lxF(:) = res(:) + dt*alpha*(u(:)-vbar(:))

  END FUNCTION lxf

  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ STABILIZATION ------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
!!!!!!!!!! Jump

 SUBROUTINE jump(ed ,e1, e2, uu1, uu2, resJ, alpha, alpha2)
    ! e1 is the element before the edge, e2 the one after
    ! e1-> uu1, e2->uu2

    REAL(dp),PARAMETER,DIMENSION(6)::TYPE=(/1.,2.,2.,3.,3.,4./)
    REAL(dp), INTENT(in) :: alpha, alpha2
    TYPE(arete), INTENT(in):: ed
    TYPE(element),                     INTENT(in):: e1
    TYPE(element),                     INTENT(in):: e2
    TYPE(PVar), DIMENSION(e1%nsommets), INTENT(in):: uu1, uu2
    TYPE(Pvar), DIMENSION(e1%nsommets,2):: resJ
    TYPE(Pvar):: u, ubar1, ubar2
    TYPE(Pvar):: divflux1, divflux2, jumpflux,jumpfluxder2, divdivflux1, divdivflux2
    INTEGER:: is, l
    real(dp),dimension(n_dim):: xx=0.
    REAL(dp):: theta, theta2, h,  umax, umin, l2, spectral_radius1, spectral_radius2
    REAL(dp), DIMENSION(n_vars,n_vars,n_dim)::jaco
    REAL(dp), DIMENSION(e1%nsommets):: base1
    REAL(dp),DIMENSION(n_dim,e1%nsommets):: grad1, grad1_der
    REAL(dp), DIMENSION(e2%nsommets):: base2
    REAL(dp),DIMENSION(n_dim,e2%nsommets):: grad2, grad2_der
    REAL(dp),DIMENSION(2), PARAMETER:: x2=(/0.d0,1.0d0/),x1=(/1.d0,0.d0/)

    
    DO l = 1,n_vars
       ubar1%u(l)= SUM(uu1%u(l))/REAL(SIZE(uu1))
    END DO
    DO l = 1,n_vars
       ubar2%u(l)= SUM(uu2%u(l))/REAL(SIZE(uu2))
    END DO

    spectral_radius1= ubar1%spectral_radius(xx,e1%n)
    spectral_radius2= ubar2%spectral_radius(xx,e2%n)



    DO l=1, e1%nsommets
       grad1(:,l) = e1%gradient(l,x1)!*e1%volume
       grad2(:,l) = e2%gradient(l,x2)!*e2%volume
       grad1_der(:,l) = e1%gradient2(l,x1)
       grad2_der(:,l) = e2%gradient2(l,x2)
    ENDDO

    ! jump of grad u
    divflux1 = e1%eval_der(uu1(:),x1)!*e1%volume!
    divflux2 = e2%eval_der(uu2(:),x2)!*e2%volume!
    divdivflux1 = e1%eval_der2(uu1(:),x1)
    divdivflux2 = e2%eval_der2(uu2(:),x2)

    !! h=1./((TYPE(e1%itype)*TYPE(e2%itype))*0.5) !* l2!
    h=1.d0/(0.5d0*(SUM(ABS(grad1(1,:))) + SUM( ABS(grad2(1,:))) ))

    jumpflux=(divflux2-divflux1)   !* ABS(u%spectral_radius((/0.5d0/),(/1.0d0/))) ! ????? !
    jumpfluxder2=(divdivflux2-divdivflux1)   !* ABS(u%spectral_radius((/0.5/),(/1.0/))) 

    theta=alpha* h **(2.)
    theta2=alpha2 * h**4. 

    DO l=1, e1%nsommets
       ResJ(l,1) = spectral_radius1*((-1.d0)*jumpflux * grad1(1,l) * theta +jumpfluxder2*(-grad1_der(1,l))*theta2)
       ResJ(l,2) = spectral_radius2*(jumpflux * grad2(1,l) * theta +jumpfluxder2*grad2_der(1,l)*theta2)

    ENDDO


  END SUBROUTINE jump



  SUBROUTINE jump_old(ed ,e1, e2, uu1, uu2, resJ, alpha_j,alpha2_j)
    CHARACTER(Len=*), PARAMETER :: mod_name="scheme/jump"
    ! e1 is the element before the edge, e2 the one after
    ! e1-> uu1, e2->uu2
    REAL(dp),PARAMETER,DIMENSION(6)::TYPE=(/1.,2.,2.,3.,3.,4./)
    REAL(dp), INTENT(in) :: alpha_j,alpha2_j
    TYPE(arete), INTENT(in):: ed
    TYPE(element),                     INTENT(in):: e1
    TYPE(element),                     INTENT(in):: e2
    TYPE(PVar), DIMENSION(e1%nsommets), INTENT(in):: uu1
    TYPE(PVar), DIMENSION(e1%nsommets), INTENT(in):: uu2
    TYPE(Pvar), DIMENSION(e1%nsommets,2):: resJ

    TYPE(Pvar):: divflux1, divflux2, jumpflux, divdivflux1, divdivflux2, jumpfluxder2
    INTEGER:: is, l,j,k
    REAL(dp):: theta_jump1, h, h2, h4,  theta_jump2
    REAL(dp), DIMENSION(e1%nsommets):: base1
    REAL(dp), DIMENSION(n_dim,e1%nsommets):: grad1, grad1_der
    REAL(dp), DIMENSION(e2%nsommets):: base2
    REAL(dp), DIMENSION(n_dim,e2%nsommets):: grad2,grad2_der
    REAL(dp), PARAMETER, DIMENSION(2):: x1=(/1.0_dp,0.0_dp/),x2=(/0.0_dp,1.0_dp/)


    !----------------------------------------------------------------------------------------------
    !----------------------------------------------------------------------------------------------
    ! Paola version with maybe improvements that follows P &S version


    DO l=1, e1%nsommets
       grad1(:,l) = e1%gradient(l,x1)*e1%volume !1
       grad2(:,l) = e2%gradient(l,x2)*e2%volume !2
       !       grad1_der(:,l) = e1%gradient2(l,x1)
       !       grad2_der(:,l) = e2%gradient2(l,x2)
    ENDDO

    h=0.5*(e1%volume +e2%volume)!1._dp/(0.5_dp*(SUM(ABS(grad1(1,:))) + SUM( ABS(grad2(1,:)))))

    !jump of grad u
    divflux1 = e1%eval_der(uu1(:),x1)*e1%volume
    divflux2 = e2%eval_der(uu2(:),x2)*e2%volume
    divdivflux1 = e1%eval_der2(uu1(:),x1)
    divdivflux2 = e2%eval_der2(uu2(:),x2)

    jumpflux%u=(divflux2%u-divflux1%u)
    jumpfluxder2%u=(divdivflux2%u-divdivflux1%u)

    h2=1._dp!h*h
    theta_jump1=alpha_j* h2

    h4=h2*h2
    theta_jump2= alpha2_j * h4
    DO l=1, e1%nsommets
       ResJ(l,1) =  jumpflux * (- grad1(1,l) ) * theta_jump1 !+ jumpfluxder2*(-grad1_der(1,l) )*theta_jump2
    ENDDO
    DO l=1,e2%nsommets
       ResJ(l,2) =  jumpflux *    grad2(1,l)   * theta_jump1 !+ jumpfluxder2*  grad2_der(1,l)  *theta_jump2
    ENDDO
    !----------------------------------------------------------------------------------

  END SUBROUTINE jump_old

  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ LIMITING ---------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------

  SUBROUTINE limit (e,u,res,phi)
    !using Characteristic variables
    IMPLICIT NONE
    TYPE(element),               INTENT(in):: e
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(out):: res
    TYPE(Pvar), DIMENSION(:), INTENT(in)   :: phi
    LOGICAL:: out

    CALL limit_char(e,u,res,phi,out)
    !if NaN detected, then do limitation by variables
    IF (out) THEN
       CALL limit_var(u,res,phi)
    ENDIF
  END SUBROUTINE limit

  SUBROUTINE limit_var(u,res,phi)
    !using conserved variables
    IMPLICIT NONE
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(out):: res
    TYPE(Pvar), DIMENSION(:), INTENT(in)   :: phi
    REAL(dp):: l1, l2, l,phi_tot, den, umax, umin, truc
    REAL(dp),DIMENSION(SIZE(res)):: x
    INTEGER:: i, k

    DO k = 1,n_vars
       L=0._dp
       res%u(k) = 0.0_dp
       phi_tot=SUM(phi(:)%u(k))
       IF (ABS(phi_tot)>TINY(1.0_dp)) THEN
          x=MAX(phi(:)%u(k)/phi_tot,TINY(1.0_dp))
          den=SUM(x)+TINY(1.0_dp)
          truc=SUM(ABS(phi(:)%u(k)))
          l=ABS(phi_tot)/(SUM(ABS(phi(:)%u(k))))
          res(:)%u(k)= (1.0_dp-l)*phi_tot * x/den+l*phi(:)%u(k)
       ENDIF
    END DO ! k

  END SUBROUTINE limit_var



  SUBROUTINE limit_char(e,u,res,phi,out)
    !using Characteristic variables
    IMPLICIT NONE
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(element),               INTENT(in):: e
    TYPE(Pvar), DIMENSION(:), INTENT(out):: res
    TYPE(Pvar), DIMENSION(:), INTENT(in)   :: phi
    LOGICAL, INTENT(out):: out
    TYPE(Pvar), DIMENSION(SIZE(phi)) :: phi_ch, res_ch
    REAL(dp)::  l, phi_tot, den,  truc
    REAL(dp), DIMENSION(SIZE(res)):: x
    INTEGER:: i, k
    REAL(dp), DIMENSION(n_Vars,n_Vars) :: EigR, EigL
    TYPE(Pvar) :: ubar, vbar
    TYPE(Pvar), DIMENSION(e%nsommets) :: v
    REAL(dp), DIMENSION(1) ::  v_nn=(/1.0_dp/)
    REAL(dp), PARAMETER:: eps=1.e-9_dp
    out=.FALSE.
    res=0._dp

    v= Control_to_Cons(u,e)
    DO k = 1,n_vars
       ubar%u(k)= SUM(v%u(k))/e%nsommets
    END DO

    EigR = ubar%rvectors(v_nn)
    IF (SUM(ABS(EigR)).NE.SUM(ABS(Eigr)) ) THEN
       out=.TRUE.
       RETURN
    ENDIF
    EigL = ubar%lvectors(v_nn)


    DO i = 1,SIZE(res)
       phi_ch(i)%u = MATMUL(EigL,phi(i)%u)
    END DO
    !print*, 'phi_ch',maxval(abs(phi_ch(:)%u(1)))

    DO k = 1,n_vars

       phi_tot=SUM(phi_ch(:)%u(k))
       !                 print*, k,((phi_ch(:)%u(1))),phi_tot
       IF (ABS(phi_tot)>TINY(1._dp)) THEN

          x=MAX(phi_ch(:)%u(k)/phi_tot,0._dp)
          den=SUM(x)
          truc=SUM(ABS(phi_ch(:)%u(k))) 

          l=ABS(phi_tot)/truc

          res(:)%u(k)=(1._dp-l)*(Phi_tot*x/den)+l*phi_ch(:)%u(k)
       ELSE
          res(:)%u(k)=0.0_dp 
       ENDIF
    ENDDO
    !read*
    DO i = 1,SIZE(res)
       phi_ch(i)%u=MATMUL(EigR,res(i)%u)
    END DO
    !    print*, phi_ch(:)%u(1)
    res=phi_ch
    !print*
  END SUBROUTINE limit_char

END MODULE scheme
