// square

lc = 0.1; // Mesh parameter


// Points
p1 = newp;
Point(p1) = {-0.5, 0.0, 0.0, lc};
p2 = newp;
Point(p2) = {0.5, 0.0, 0.0, lc};
p3 = newp;
Point(p3) = {0.5, 0.2, 0.0, lc};
p4=newp;
Point(p4)={-0.5,0.2,0,0, lc};


// lines
c1 = newreg;
Line(c1) = {p1, p2};
c2 = newreg;
Line(c2) = {p2, p3};
c3 = newreg;
Line(c3) = {p3, p4};
c4=newreg;
Line(c4)={p4,p1};


// Line loop
ll1 = newreg;
Line Loop(ll1) = {c1,c2,c3,c4};

// Plane surfaces
surf1 = news;
Plane Surface(surf1) = {ll1}; 


// Set boundary tags
Physical Line("steger") = {c4,c2};          // inflow
Physical Line("Wall") = {c1,c3}; // wall
Physical Surface(0) = {surf1};

n1=100;
n2=20;

// Make one square structured.
Transfinite Line {c1,-c3}=n1 Using Progression 1;
Transfinite Line {-c4,c2}=n2 Using Progression 1;
Transfinite Surface {surf1}={p1,p2,p3,p4};



Recombine Surface {surf1};
Mesh.MshFileVersion =2;

