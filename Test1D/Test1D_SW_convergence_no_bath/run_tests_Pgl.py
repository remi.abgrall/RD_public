import numpy as np
import shutil, os
import re
from joblib import Parallel, delayed
import multiprocessing

#In this function we run the tests for all ns for one parameter (e.g. B2)

def run_single_test(cand, param, test_folder):
	instruction ="" 
	foldName = "Pgl"+str(param)
	instruction +="mkdir "+foldName+" \n"
	instruction +="mkdir "+foldName+"/Data \n"
	instruction +="cp Data/don1d "+foldName+"/Data/don1d \n"
	os.system(instruction)
	modify_don(foldName+"/Data/don1d", param)
	ns= [int(k) for k in 2.**np.linspace(3.,10.,8)]  #here are the ns
	for n in ns:
#		n=param[1]
		instruction ="" 
		instruction +="cd "+foldName+" \n "  #changing folder into Bn
		instruction += "pwd \n"
		instruction +=  "../../bin1D/main_dec.out "+str(n)+ " \n"  #running the test with n. call as if we are in the Bn folder
		instruction += "mv TEST/sol_prim_last.dat test_scheme4_"+str(n)+".dat  \n"
		instruction += "mv TEST/sol_cons_last.dat test_cons_scheme4_"+str(n)+".dat  \n"
		instruction += "cd TEST  \n"
		instruction += "rm -rf * \n"
		os.system(instruction)
	return


#This is the function to run more paramaters
def modify_don(fname, param):
	don=[]
	with open(fname, 'r') as a:
		for idx, line in enumerate(a.readlines()):
			don.append(line)

	types=[11,12,13,14]
	order = [[2,2], [3,3], [4,4], [5,5]]
	burman=[[1., 0.],[0.1,0.1], [0.1,0.1],[0.1,0.1]]
	burman=[[0.119, 0],[0.00346,0],[0.000113,0],[0.1,0 ]]#[[0.05623, 0.],[0.0056,  0], [0.001778, 0],[0.1,0 ]]
	burman=[[0.119, 0],[0.05,0],[0.05,0],[0.01,0 ]]#[[0.05623, 0.],[0.0056,  0], [0.001778, 0],[0.1,0 ]]
	#burman=[[0.119, 0],[0.0,0],[0.0,0],[0.0,0 ]]#[[0.05623, 0.],[0.0056,  0], [0.001778, 0],[0.1,0 ]]
	CFLs=[0.6, 0.2, 0.1, 0.05]

	pol=int(param)-1
	f=open(fname,'w')
	for idx, line in enumerate(don):
		if idx==2:
			f.write(str(order[pol][0]) +' '+str(order[pol][1])+'  ordre 3: # of iteration for DEC \n')
		elif idx==1:				
			f.write(str(types[pol])+'    itype 1: P1, 2: B2, 3: P2, 4:P3, 5: B3\n')
		elif idx==4:				
			f.write(str(burman[pol][0])+'   theta parameter in Burman stabilization term \n')
		elif idx==5:				
			f.write(str(burman[pol][1])+'   theta2 parameter in Burman stabilization second derivative term \n')
		elif idx==6:				
			f.write(str(CFLs[pol])+'   cfl \n')
		else:
			f.write(don[idx])
	f.close()

	return




#vary B1/B2/B3
#vary theta burman between 0, 0.5
#vary alpha lax friedrics
test_folder = ""

num_cores = min(multiprocessing.cpu_count(), 4)

param_domain=[1,2,3,4] #[1,1]]#,[2,1],[3,1],[4,1]]#, [1]]#,[2]] [1,0],[2,0],[3,0],[4,0],

Parallel(n_jobs=num_cores)(delayed(run_single_test)(cand, param_domain[cand], test_folder) for cand in range(len(param_domain)))




