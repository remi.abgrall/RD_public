import numpy as np
import shutil, os
import re
from joblib import Parallel, delayed
import multiprocessing
import time

#In this function we run the tests for all ns for one parameter (e.g. B2)

def run_single_test(cand, param, test_folder):
	time.sleep(np.random.rand(1))
	if param[1]:
		instruction= "mkdir relax\n"
		instruction += "cd relax \n"
		folder = "relax/"
	else:
		instruction= "mkdir classic \n"
		instruction += "cd classic \n"
		folder = "classic/"
 	instruction += "mkdir scheme"+str(param[2])+" \n"
	instruction += "cd scheme"+str(param[2])+" \n"
	folder += "scheme"+str(param[2])+"/"
	instruction += "mkdir C" + str(param[0])+" \n "
	instruction +="cd C"+str(param[0])+" \n "
	folder += "C"+str(param[0])
	instruction +="mkdir Data \n"
	instruction +="cp ../../../Data/* Data/. \n"
	os.system(instruction)
	modify_don(folder+"/Data/don1d", param)
	ns= [int(k) for k in 2.**np.linspace(3.,10.,8)]  #here are the ns
	for n in ns:
#		n=param[1]
		print(folder + " N"+str(n))
		instruction ="" 
		instruction +="cd "+folder+" \n "  #changing folder into Bn
		if param[1]:
			instruction +=  "../../../../bin1D/main_dec_relaxation.out "+str(n)+ " \n"  #running the test with n. call as if we are in the Bn folder
		else:
			instruction += "../../../../bin1D/main_dec.out "+str(n)+ " \n"
		instruction += "mv TEST/sol_prim_last.dat test_scheme4_"+str(n)+".dat  \n"
		if param[1]:
		  instruction += "mv TEST/gammas.dat test_cons_scheme4_gammas_"+str(n)+".dat  \n"
		instruction += "cd TEST  \n"
		instruction += "rm -rf * \n"
		os.system(instruction)
		print(folder + " N"+str(n))
	return


#This is the function to run more paramaters
def modify_don(fname, param):
	don=[]
	with open(fname, 'r') as a:
		for idx, line in enumerate(a.readlines()):
			don.append(line)

	types=[11,12,13,14]
	order = [[2,2], [3,3], [4,4], [5,5]]
	burman=[[0.119, 0.],[0.0346,0.01], [0.00702, 0.01],[0.1,0.1]]
	burman=[[0.119, 0.],[0.0346,0.01], [0.00, 0.0],[0.,0.]]
	CFLs = [ 0.8, 0.3, 0.1, 0.08 ]
	pol=int(param[0])-1
	f=open(fname,'w')
	for idx, line in enumerate(don):
		if idx==2:
			f.write(str(order[pol][0]) +' '+str(order[pol][1])+'  ordre 3: # of iteration for DEC \n')
		elif idx==1:				
			f.write(str(types[pol])+'    itype 1: P1, 2: B2, 3: P2, 4:P3, 5: B3\n')
		elif idx==3:				
			f.write(str(param[2])+'   schemes: 3 entropy fix, 4galerkin+jump, 5 psi \n')
		elif idx==4:				
			f.write(str(burman[pol][0])+'   theta parameter in Burman stabilization term \n')
		elif idx==5:				
			f.write(str(burman[pol][1])+'   theta2 parameter in Burman stabilization second derivative term \n')
		elif idx==6:				
			f.write(str(CFLs[pol])+'   cfl \n')
		else:
			f.write(don[idx])
	f.close()

	return




#vary B1/B2/B3
#vary theta burman between 0, 0.5
#vary alpha lax friedrics
test_folder = ""

num_cores = multiprocessing.cpu_count()
schemes=[4]#,4]
relaxation=[False]
orders=[4,3,2,1]
param_domain=[]
for a3 in orders:
  for a1 in schemes:
    for a2 in relaxation:
      param_domain.append([a3,a2,a1]) 

#param_domain=[[1,False,4]]

#param_domain=[[1,1],[2,1],[3,1]]#,[4,1]]#, [1]]#,[2]]  [1,0],[2,0],[3,0],

Parallel(n_jobs=num_cores)(delayed(run_single_test)(cand, param_domain[cand], test_folder) for cand in range(len(param_domain)))




