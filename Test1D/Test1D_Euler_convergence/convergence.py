#!/usr/bin/python
import numpy as np 
from numpy import linalg as LA
import scipy
import matplotlib.pyplot as pl
import sys 
import itertools
import exact_sol as ex

pl.rc("text", usetex=True)
pl.rc("font", family="serif")

# Grids for convergence study
#nNodes = [5,10,20,40,80,160,320,640,1280,2560]
#nNodes = [5,10,20,40,80,160,320,640,1280]
#nNodes = [5,10,20,40,80,160,320,640]
#nNodes = [5,10,20,40,80,160,320]
#nNodes = [5,10,20,40,80,160]
#nNodes = [5,10,20,40,80]
#nNodes = [5,10,20,40]
nNodes = [8,16,32,64,128]#,256,512]#, 1024]

# Test name
test = "test1"

# basis
basis = "B"

# scheme
scheme = "scheme4"

# alpha
alpha = "alpha02"

# Scheme order
orders = [1 ,2,3]

marker = itertools.cycle(('s','o','*'))

varnames = ['rho','v','p']


for nvar, var in enumerate(varnames):

   # Initialize plots
   fig_err_L1 = pl.figure()
   ax_err_L1  = fig_err_L1.add_subplot(1,1,1)
  
   # Initialize array to store errors
   err_L1 = np.zeros((np.size(orders),np.size(nNodes)))
   dx     = np.zeros((np.size(orders),np.size(nNodes)))

   # Initialize array to store orders
   order_L1 = np.zeros((np.size(orders),np.size(nNodes)))
  
   for k, order in enumerate(orders):
      for i, N in enumerate(nNodes):

         # Load numerical solution
         fname = "B"+str(order)+"/"+str(test)+"_B"+str(order)+"_"+scheme+"_"+str(N)+".dat" 
         x, num_sol = np.loadtxt(fname,delimiter=None,usecols=(0,nvar+1), unpack=True)

         # Compute exact solution
         ex_sol = ex.exact_euler_smooth(x)
 
         # Compute errors
         #err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=np.inf)
         err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=1)/float(N)
         #err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=2)/float(np.sqrt(N))
         #err_L1[k,i] = LA.norm((num_sol-ex_sol[nvar])*dxvec,ord=1)
         
         if (i > 0):
            order_L1[k,i] = -( np.log(err_L1[k][i])-np.log(err_L1[k][i-1]) ) / ( np.log(nNodes[i])-np.log(nNodes[i-1]) )
      
      print "Variable : ", var
      print "L1-errors for order ", order, " :"
      print "Computed errors : "
      print err_L1[k,:]
      print "Computed orders : "
      print order_L1[k,:]

      # Plot figures

      # error vs mesh size
      ax_err_L1.loglog(nNodes,err_L1[k,:],"-"+marker.next(),label="B"+str(order))
      ref_err = [err_L1[k,0]*nNodes[0]**(float(order+1))*nNodes[i]**(-float(order+1)) for i in range(np.size(nNodes))]
      ax_err_L1.loglog(nNodes,ref_err,"--",label="order "+str(order+1))
      ax_err_L1.set_xlabel(r"$N$")
      ax_err_L1.set_ylabel(r"$L_1$-error")
      ax_err_L1.legend(loc=0)
      pl.title("Convergence of "+var)      

   # Save plots
   fig_err_L1.savefig(scheme+"_"+alpha+"_"+var+"_convergence_L1.pdf",format="pdf")

pl.show()
