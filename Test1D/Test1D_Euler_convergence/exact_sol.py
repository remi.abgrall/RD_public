#!/usr/bin/python

# Exact solution of the Euler equations in the isotropic case
# Initial data:
# rho_0(x) = 1 + alpha*sin(pi*x)
# u_0(x) = 0
# p_0(x) = rho_0(x)^gamma

def exact_euler_smooth(x):
   import numpy as np 
   from numpy import linalg as LA
   from scipy import optimize as opt
   import matplotlib.pyplot as pl
   import sys 
   import itertools
   
   pl.rc("text", usetex=True)
   pl.rc("font", family="serif")

   # Parameters
   gamma = 3
   alpha = 0.9999995

   # IC
   rho_0 = lambda x: 2. + alpha*np.sin(np.pi*x)
   u_0   = lambda x: 0
   p_0   = lambda x: rho_0(x)**gamma

   # Output time
   T = 0.1

   # Variables
   rho = np.zeros(np.size(x));
   u   = np.zeros(np.size(x));
   p   = np.zeros(np.size(x));
   e   = np.zeros(np.size(x));

   for i, xi in enumerate(x):
       # find the foot of characteristic lambda_1 
       fun = lambda x1: xi + np.sqrt(3)*rho_0(x1)*T - x1;
       x1 = opt.newton(fun,xi,tol=1e-12);
    
       # find the foot of characteristic lambda_2 
       fun = lambda x2: xi - np.sqrt(3)*rho_0(x2)*T - x2;
       x2 = opt.newton(fun,xi,tol=1e-12);
    
       # find the exact density
       rho[i] = 0.5*(rho_0(x1)+rho_0(x2));
       # find the exact velocity
       u[i]   = np.sqrt(3)*(rho[i]-rho_0(x1));
       # find the exact pressure
       p[i]   = rho[i]**gamma;
       # find the exact internal energy
       e[i]   = p[i]/((gamma-1.)*rho[i]);

   return rho, u, p, e
   
if __name__ == "__main__":   
   import numpy as np 
   from numpy import linalg as LA
   from scipy import optimize as opt
   import matplotlib.pyplot as pl
   import sys 
   import itertools
   
   # Mesh
   N = 1000
   dx = 2./N
   x = np.arange(-1.,1.,dx)
   
   [rho,u,p,e] = exact_euler_smooth(x)

   # Plot figures

   fig1 = pl.figure()
   ax1  = fig1.add_subplot(1,1,1)
   ax1.plot(x,rho)

   fig2 = pl.figure()
   ax2  = fig2.add_subplot(1,1,1)
   ax2.plot(x,u)

   fig3 = pl.figure()
   ax3  = fig3.add_subplot(1,1,1)
   ax3.plot(x,p)

   fig4 = pl.figure()
   ax4  = fig4.add_subplot(1,1,1)
   ax4.plot(x,e)

   pl.show()

   # Save exact solution
   np.savetxt('test0_exact_rho_'+str(N)+'.dat',np.transpose([x,rho]),fmt='%10.5f',delimiter=' ',newline='\n');
   np.savetxt('test0_exact_v_'+str(N)+'.dat',np.transpose([x,u]),fmt='%10.5f',delimiter=' ',newline='\n');
   np.savetxt('test0_exact_p_'+str(N)+'.dat',np.transpose([x,p]),fmt='%10.5f',delimiter=' ',newline='\n');
   np.savetxt('test0_exact_eps_'+str(N)+'.dat',np.transpose([x,e]),fmt='%10.5f',delimiter=' ',newline='\n');
