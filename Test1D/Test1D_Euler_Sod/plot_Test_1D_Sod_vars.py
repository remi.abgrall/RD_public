import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

testnr=3
conv=3
orders=["1","2","3"]
styles = ['--^', '-s', ':o']
msize=[7,6,7]
meve=[18,12,6]
lwd=[1,1,2.5]
vars=["rho","v","p" ]#, "Momentum", "Energy"]
#limits=[[[0.45,0.65], [0.37, 0.47]]] Sod first area
#limits=[[[0.63,0.83], [0.1, 0.3]]]  #sod second area
limits=[[[0.,1], [0.1, 1.1]], [[0.,1.],[-0.05,1.4]], [[0.,1.],[0.07,1.1]]]
mesh=["200"]
#or N in [int(2**k) for k in range()]:
sols=[]
ex_sol=[]
dashes = [10, 5, 100, 5]
for i, N in enumerate(mesh):
        for var, varname in enumerate(vars): 
	        fig, ax = plt.subplots()
	        #plt.title(varname+", Shu-Osher test, N="+str(N))
	        #axins = zoomed_inset_axes(ax, 2.5, loc=3)
	        for ord, ordname in enumerate(orders):
		        sols.append(np.loadtxt("./B"+ordname+"/TEST/test_Sod_B"+ordname+"_Mood_"+N+".dat"))
		        ax.plot(sols[ord][:,0],sols[ord][:,var+1],styles[ord],linewidth=lwd[ord],markersize=msize[ord],markevery=meve[ord], label="B"+ordname+", 400 DoFs")
		        #axins.plot(sols[ord][:,0],sols[ord][:,var+1])

		ex_sol.append(np.loadtxt("Reference_Sod/test_Sod_exact_"+varname+".dat"))
                ax.plot(ex_sol[var][:,0],ex_sol[var][:,1],'k',label="Exact")
                ax.set_xlim(limits[var][0])
		ax.set_ylim(limits[var][1])
                ax.legend(loc=1)
                ax.legend(frameon=False)
	        #mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
	        plt.savefig("Test_1D_Sod_"+varname+"_"+N+".pdf")
                #		plt.show()
	        plt.clf()
	        plt.close


limits=[[[0.45,0.85], [0.1, 0.46]], [[0.45,0.85],[-0.05,1.4]], [[0.45,0.85],[0.07,0.43]]]  #sod second area

mesh=["200"]
#or N in [int(2**k) for k in range()]:
sols=[]
ex_sol=[]
for i, N in enumerate(mesh):
        for var, varname in enumerate(vars): 
	        fig, ax = plt.subplots()
                for ord, ordname in enumerate(orders):
                        sols.append(np.loadtxt("./B"+ordname+"/TEST/test_Sod_B"+ordname+"_Mood_"+N+".dat"))
		        ax.plot(sols[ord][:,0],sols[ord][:,var+1],styles[ord],linewidth=lwd[ord],markersize=msize[ord],markevery=meve[ord],label="B"+ordname+", 400 DoFs")

                ex_sol.append(np.loadtxt("Reference_Sod/test_Sod_exact_"+varname+".dat"))
                ax.plot(ex_sol[var][:,0],ex_sol[var][:,1],'k',label="Exact")
		ax.set_xlim(limits[var][0])
		ax.set_ylim(limits[var][1])
        
	        ax.legend(loc=1)
                ax.legend(frameon=False)
	        #mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
	        plt.savefig("Test_1D_Sod_"+varname+"_"+N+"_zoom.pdf")
                #		plt.show()
	        plt.clf()
	        plt.close
