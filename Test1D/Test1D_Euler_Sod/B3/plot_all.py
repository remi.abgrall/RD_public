import numpy as np
import re
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
#def colors(theta):
#	return (theta[1], 0.5*theta[2],theta[2])
order=2
interval_min = [ 0.0 , 0.0]
interval_max = [ 1., 1.]
num_par 		 = [  6 , 6] 
grids=[]
for j in range(2):
	grids.append(np.linspace(interval_min[j],interval_max[j],num_par[j]))

param_domain=[]
for b in range(num_par[0]):
	for d in range(num_par[1]):
		param_domain.append([order,grids[0][b],grids[1][d]])

#selection=range(5,len(param_domain),6)
#selection=range(30,36)
selection=range(0,36)
for cand in selection:
        par=param_domain[cand]
	solut=np.loadtxt("param"+str(cand)+"/sol_prim_last.dat")
        par=param_domain[cand]
	plt.plot(solut[:,0],solut[:,1], label="alpha1="+str(par[1])+"alpha2="+str(par[2]))#,co lor=colors(par))
solut=np.loadtxt("test1_exact_rho.dat")
plt.plot(solut[:,0],solut[:,1],label="exact", color=[0,0,0])
plt.title("Different alphas")
plt.legend()
plt.show()
plt.savefig("alpha_comparison_B"+str(par[0])+".pdf")
