import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

testnr=3
conv=3
orders=["2"]
styles = ['r-', '-']
msize=[7,7]
meve=[5,10,15]
lwd=[1,2.5]
vars=["rho" ]#, "Momentum", "Energy"]
#limits=[[[0.45,0.65], [0.37, 0.47]]] Sod first area
#limits=[[[0.63,0.83], [0.1, 0.3]]]  #sod second area
limits=[[[0.,1], [0., 1.05]]]#, [[0.,1.],[-0.05,1.4]], [[0.,1.],[0.07,1.1]]]
mesh=["100"]
#or N in [int(2**k) for k in range()]:
sols=[]
det_sol=[]
solC=[]
ex_sol=[]
#dashes = [10, 5, 100, 5]
for i, N in enumerate(mesh):
        for var, varname in enumerate(vars): 
	        
                for ord, ordname in enumerate(orders):
                        fig, ax = plt.subplots()
                        sols.append(np.loadtxt("B"+ordname+"/TEST/test_Sod_B"+ordname+"_Mood_"+N+".dat"))
                        solC.append(np.loadtxt("B"+ordname+"/TEST/test_Sod_B"+ordname+"_Davide_"+N+".dat"))

       	             
	                ax.plot(sols[ord][:,0],sols[ord][:,1], styles[0],linewidth=lwd[0],markersize=msize[0],markevery=meve[ord],label="B"+ordname+", MOOD")
                        ax.plot(solC[ord][:,0],(solC[ord][:,1]),styles[1],linewidth=lwd[1],markersize=msize[1],markevery=meve[ord],label="B"+ordname+", new MOOD")
	                #axins.plot(sols[ord][:,0],sols[ord][:,var+1])
                        ex_sol.append(np.loadtxt("Reference_Sod/test_Sod_exact_"+varname+".dat"))
                        ax.plot(ex_sol[var][:,0],ex_sol[var][:,1],'k',label="Reference")
         
                        ax.set_xlim(limits[0][0])
                        ax.set_ylim(limits[0][1])
                        ax.legend(loc=1)
                        ax.legend(frameon=False)
	                #mark_inset(ax, axins, loc1=2, loc2=4, fc="none", ec="0.5")
	                plt.savefig("Test_1D_Sod_MoodOnOFF_B"+ordname+"_"+varname+"_"+N+".pdf")
                        #		plt.show()
                        plt.clf()
                        plt.close
