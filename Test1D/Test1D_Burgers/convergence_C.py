#!/usr/bin/python
import numpy as np 
from numpy import linalg as LA
import scipy
import matplotlib.pyplot as pl
import sys 
import itertools
import scipy.optimize
#import exact_sol as ex

# run the convergence without an exact solution
# files saved in columns x, h, u
# ns and the file are below and must be changed accordingly

# Initial condition
def exact_sol(xx,t):
  u0=lambda x0: 0.5*np.sin(2.0*np.pi*x0) # np.exp(-x0**2.*10)#
  #Burgers characteristics function
  def F(x0):
	  return x0+u0(x0)*t-xx
  x0 = scipy.optimize.broyden1(F,xx,f_tol=1e-13)
  return u0(x0)

T_end=0.1


def remove_extra(x, u):
   ind=[]
   for i in range(len(x)-1):
      if x[i]==x[i+1]:
          ind.append(i)
   x=np.delete(x,ind,0)
   u=np.delete(u,ind,0)
   return x,u

pl.rc("text", usetex=True)
pl.rc("font", family="serif")

extra_correction = False

if extra_correction ==True:
  corr_str="_corr" 
else:
  corr_str = ""

# Grids for convergence study
#nNodes = [5,10,20,40,80,160,320,640,1280,2560]
#nNodes = [5,10,20,40,80,160,320,640,1280]
#nNodes = [5,10,20,40,80,160,320,640]
#nNodes = [5,10,20,40,80,160,320]
#nNodes = [5,10,20,40,80,160]
#nNodes = [5,10,20,40,80]
#nNodes = [5,10,20,40]
nNodes = [8,16,32,64,128,256,512, 1024]#,2048,4096]
#nNodes = [25,50,100,200]#,400,800]
# Test name
test = "test"

# basis
basis = "B"

# scheme
schemes = ["scheme4"]#, "scheme3"]

relaxations=[  "classic"]#"relax",
# Scheme order
orders = [1, 2,3,4]

marker = itertools.cycle(('s','o','*'))

varnames = ['h']
linestyles=["-","-.",":"]

for sche in schemes:
  for nvar, var in enumerate(varnames):
	  fig_err_L1 = pl.figure()
	  ax_err_L1  = fig_err_L1.add_subplot(1,1,1)
	  #fig_entropy = pl.figure()
	  #ax_entropy  = fig_entropy.add_subplot(1,1,1)
	  for idrel, rel in enumerate(relaxations):
		   linSt=linestyles[idrel]
		
		   # Initialize array to store errors
		   err_L1 = np.zeros((np.size(orders),np.size(nNodes)))
		   dx     = np.zeros((np.size(orders),np.size(nNodes)))

		   # Initialize array to store orders
		   order_L1 = np.zeros((np.size(orders),np.size(nNodes)))
		
		   for k, order in enumerate(orders):
		      xs =[]
		      num_sols = []
		      #tEnd=np.zeros(len(nNodes));
		      #finalErrorEntropies=np.zeros(len(nNodes));

		      for i, N in enumerate(nNodes):

		         # Load numerical solution
		         fname = rel+"/"+sche+"/C"+str(order)+"/"+str(test)+"_scheme4_"+str(N)+".dat"  #here the folder and filenames
		         x, num_sol = np.loadtxt(fname,delimiter=None,usecols=(0,nvar+1), unpack=True)
		         x, num_sol = remove_extra(x,num_sol)
		         xs.append(x)
		         num_sols.append(num_sol)


		      for i in range(len(nNodes)):
		         exSol=exact_sol(xs[i],T_end)
		         err_L1[k,i]=LA.norm(num_sols[i]-exSol,ord=2)/float(np.sqrt(nNodes[i]))

		         # Compute exact solution
	  #         ex_sol = ex.exact_sw_smooth(x)
	   
		         # Compute errors
		         #err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=np.inf)
	  #         err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=1)/float(N)
		         #err_L1[k,i] = LA.norm(num_sol-ex_sol[nvar],ord=2)/float(np.sqrt(N))
		         #err_L1[k,i] = LA.norm((num_sol-ex_sol[nvar])*dxvec,ord=1)
		         
		         if (i > 0):
		            order_L1[k,i] = -( np.log(err_L1[k][i])-np.log(err_L1[k][i-1]) ) / ( np.log(nNodes[i])-np.log(nNodes[i-1]) )
		      
		      print "Variable : ", var
		      print "L1-errors for order ", order, " :"
		      print "Computed errors : "
		      print err_L1[k,:]
		      print "Computed orders : "
		      print order_L1[k,:]

		      # Plot figures

		      # error vs mesh size
		      
		      ax_err_L1.loglog(nNodes[:],err_L1[k,:],linestyle=linSt,marker=marker.next(),label="C"+str(order)+" "+rel)
		      ref_err = [err_L1[k,1]*nNodes[1]**(float(order+1))*nNodes[i]**(-float(order+1)) for i in range(np.size(nNodes[:]))]
		      ax_err_L1.loglog(nNodes,ref_err,":",label="order "+str(order+1))
		      ax_err_L1.set_xlabel(r"$N$")
		      ax_err_L1.set_ylabel(r"$L_2$-error")
		      pl.xlim(nNodes[1]*0.7,nNodes[-1]*1.3)
		      ax_err_L1.legend(loc=0)
		      ax_err_L1.set_title("Convergence of "+var)    
        # Save plots
		   fig_err_L1.savefig(var+"_convergence_C_"+sche+".pdf",format="pdf")
pl.show()
