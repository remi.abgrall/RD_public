// Circlar domain with hole


lc = 0.5; // Mesh parameter

//----------------------------------------
// inner circle
Ro = 0.8;   // radius of the outer circle

// Points
p1 = newp;
Point(p1) = {0,0,0,lc};
p2 = newp;
Point(p2) = { Ro,   0, 0, lc};
p3 = newp;
Point(p3) = {  0,  Ro, 0, lc};
p4 = newp;
Point(p4) = {-Ro,   0, 0, lc};
p5 = newp;
Point(p5) = {  0, -Ro, 0, lc};

// Circle arcs
c1 = newreg;
Circle(c1) = {p2, p1, p3};
c2 = newreg;
Circle(c2) = {p3, p1, p4};
c3 = newreg;
Circle(c3) = {p4, p1, p5};
c4 = newreg;
Circle(c4) = {p5, p1, p2};

// Line loop

//----------------------------------------
// outer circle
Ro = 1.6;   // radius of the outer circle

// Points
q1 = newp;
Point(q1) = {0,0,0,lc};
q2 = newp;
Point(q2) = { Ro,   0, 0, lc};
q3 = newp;
Point(q3) = {  0,  Ro, 0, lc};
q4 = newp;
Point(q4) = {-Ro,   0, 0, lc};
q5 = newp;
Point(q5) = {  0, -Ro, 0, lc};

// Circle arcs
d1 = newreg;
Circle(d1) = {q3, q1, q2};
d2 = newreg;
Circle(d2) = {q4, q1, q3};
d3 = newreg;
Circle(d3) = {q5, q1, q4};
d4 = newreg;
Circle(d4) = {q2, q1, q5};

l1 = newreg;
Line(l1)={p2,q2};
l2=newreg;
Line(l2)={p3,q3};
l3=newreg;
Line(l3)={p4,q4};
l4=newreg;
Line(l4)={p5,q5};

ll1=newreg;
Curve Loop(ll1)={-c1,-l2,-d1,l1};
ll2=newreg; 
Curve Loop(ll2)={-c2,-l3,-d2,l2};
ll3=newreg;
Curve Loop(ll3)={-c3,-l4,-d3,l3};
ll4=newreg;
Curve Loop(ll4)={-c4,-l1,-d4,l4};

n1=25;//75
Transfinite Curve{c1,d1}=n1 Using Progression 1;
Transfinite Curve{c2,d2}=n1 Using Progression 1;
Transfinite Curve{c3,d3}=n1 Using Progression 1;
Transfinite Curve{c4,d4}=n1 Using Progression 1;
n2=10;//50;
Transfinite Line{l1,l2}=n2 Using Progression 1;
Transfinite Line{l2,l3}=n2 Using Progression 1;
Transfinite Line{l3,l4}=n2 Using Progression 1;
Transfinite Line{l4,l1}=n2 Using Progression 1;




// Plane surfaces
surf1 = news;
Plane Surface(surf1) = {ll1};
surf2 = news;
Plane Surface(surf2) = {ll2};
surf3 = news;
Plane Surface(surf3) = {ll3};
surf4 = news;
Plane Surface(surf4) = {ll4};
Transfinite Surface{surf1};
Transfinite Surface{surf2};
Transfinite Surface{surf3};
Transfinite Surface{surf4};


// Finally we apply an elliptic smoother to the grid to have a more regular
// mesh:
Mesh.Smoothing = 100;

// Set boundary tags
Physical Line("wall") = {c1,c2,c3,c4,d1,d2,d3,d4};
Physical Surface(0) = {surf1,surf2,surf3,surf4};
//Recombine Surface{0};
