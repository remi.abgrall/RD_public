lc=0.1;
lc2=0.051;
lc3=0.02;
p1=newp;
Point(p1) = { -0.2, 0.000000, 0.000000,lc};
p2=newp;
Point(p2) = { 0.000000, 0.000000, 0.000000,lc2};
p3=newp;
Point(p3)  = {1.5,0.,0.,lc2};
p4=newp;
Point(p4) = { 1.5,  1., 0.000000,lc2};
p5=newp;
Point(p5) = { 1.5,  1.866025404, 0.000000,lc2};
p6=newp;
Point(p6) = {0.,1., 0.,lc3};
p7=newp;
Point(p7) = {-0.2, 1., 0.,lc};
p8=newp;
Point(p8) ={0.5,0.0,0.0,lc};
p9=newp;
Point(p9)={0.5,1.0,0.0,lc};
p10=newp;
Point(p10)={0.4330127018,1.25,0.,lc3};


c1=newl;
Line(c1) = {p1, p2};
c2=newl;
Line(c2) = {p2, p6};
c3=newl;
Line(c3) = {p6, p7};
c4=newl;
Line(c4) = {p7, p1};
//
c5=newl;
Line(c5) = {p2,p8};
c6=newl;
Line(c6) = {p8,p9};
c7=newl;
Line(c7) = {p9,p6};
c8=newl;
Line(c8) = {p8,p3};
c9=newl;
Line(c9) = {p3,p4};
c10=newl;
Line(c10) = {p4,p9};
c11=newl;
Line(c11) = {p4,p5};
c12=newl;
Line(c12) = {p5,p10};
//c13=newl;
//Line(c13) = {p10,p9};
c14=newl;
Line(c14) = {p10,p6};

ci1=newc;
Circle(ci1)={9,6,10};
ll1=newll;
Line Loop(ll1)     = {c1, c2, c3, c4};
//
ll2=newll;
Line Loop(ll2)     = {c5,c6,c7,-c2};
//
ll3=newll;
Line Loop(ll3) = {c8,c9,c10,-c6};
//
ll4=newll;
Line Loop(ll4) = {c11,c12,-ci1,-c10};
ll5=newll;
Line Loop(ll5) = {-c7,ci1,c14};

surf1=news;
Plane Surface(surf1) = {ll1};
surf2=news;
Plane Surface(surf2) = {ll2};
surf3=news;
Plane Surface(surf3) = {ll3};
surf4=news;
Plane Surface(surf4) = {ll4};
surf5=news;
Plane Surface(surf5) = {ll5};



//set boubdary tag//
Physical Line("Inflow")={c4};
Physical Line("Outflow")={c9,c11};
Physical Line("Wall")={c1,c5,c8,c12,c14};
Physical Surface(0)={surf1,surf2,surf3,surf4,surf5};

n1=12;
n2=60;//120;
n3=20;//144;
n4=20;

Transfinite Line{c1,c3}=n1 Using Progression 1;
Transfinite Line{c4,c2}=n2 Using Progression 1;
Transfinite Surface {surf1}={p1,p2,p6,p7};

Transfinite Line {c2,c6}=n2 Using Progression 1;
Transfinite Line {c5,c7}=n3 Using Progression 1;
Transfinite Surface {surf2}={p2,p8,p9,p6};

Transfinite Line {c9,c6}=n2 Using Progression 1;
Transfinite Line {c8,c10}=n3 Using Progression 1;
Transfinite Surface {surf3}={p8,p3,p4,p9};

Transfinite Line {ci1,c11}=n4 Using Progression 1;
Transfinite Line {c12,c10}=n3 Using Progression 1;
Transfinite Surface {surf4}={p4,p5,p10,p9};

//Recombine Surface {surf1,surf2,surf3,surf4};//

