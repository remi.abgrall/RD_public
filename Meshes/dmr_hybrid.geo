lc = 0.05; // Mesh parameter
lc2=0.5;
Point(1) = { -0.2 ,0., 0.,lc};
Point(2) = { 0.000000, 0.000000    , 0.000000,lc};
Point(3) = { 2.400000,  1.385640646, 0.000000,lc};
Point(4) = { 2.400000,  1.8        , 0.000000,lc};
Point(5) = {-0.2     ,  1.8        , 0.0     ,lc};
Point(6) = { 0.0     ,  1.8        , 0.0     ,lc2};
Point(7) = { 0.0     ,  0.414359354, 0.      ,lc};
Point(8) = {-0.2     ,  0.414359354, 0.      ,lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 6};
Line(5) = {6, 5};
Line(6) = {5, 8};
Line(7) = {8, 1};
Line(8) = {8, 7};
Line(9) = {2, 7};
Line(10)= {7, 6};
Line(11)= {4, 7};

L1=newreg;
Line Loop(L1)     = {1,  9, -8, 7};
L2=newreg;
Line Loop(L2)={2,3,11,-9};
L3=newreg;
Line Loop(L3)={4,-10,-11};
L4=newreg;
Line Loop(L4)={8,10,5,6};

surf1=news;
Plane Surface(surf1) = {L1};
surf2=news;
Plane Surface(surf2)={L2};
surf3=news;
Plane Surface(surf3)={L3};
surf4=news;
Plane Surface(surf4)={L4};


Physical Line("Wall")   = {1,2,4,5};
Physical Line("Inflow") = {6,7};
Physical Line("Outflow")={3};

n1=10;
n2=5;
n3=20;
n4=30;
//Transfinite Line{9,3}=n1 Using Progression 1;
//Transfinite Line{2,11}=n4 Using Progression 1;
//Transfinite Surface{surf2}={2,3,4,7};
//Recombine Surface(surf2);

Transfinite Line{9,7}=n1 Using Progression 1;
Transfinite Line{1,8}=n2 Using Progression 1;
Transfinite Surface{surf1}={1,2,7,8};
Recombine Surface(surf1);

Transfinite Line{6,10}=n3 Using Progression 1;
Transfinite Line{5,8}=n2 Using Progression 1;
Transfinite Surface{surf4}={7,6,5,8};
Recombine Surface(surf4);

//Recombine Surface (surf3);


Physical Surface(0) = {surf1,surf2,surf3,surf4};