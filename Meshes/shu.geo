// Circlar domain with hole


lc = 0.5; // Mesh parameter

//----------------------------------------
// Outer circle
Ro = 6.;   // radius of the outer circle

// Points
p1 = newp;
Point(p1) = {0,0,0,lc};
p2 = newp;
Point(p2) = { Ro,   0, 0, lc};
p3 = newp;
Point(p3) = {  0,  Ro, 0, lc};
p4 = newp;
Point(p4) = {  0, -Ro, 0, lc};
p5 = newp;
Point(p5) = {-Ro,   0, 0, lc};

// Circle arcs
c1 = newreg;
Circle(c1) = {p2, p1, p3};
c2 = newreg;
Circle(c2) = {p3, p1, p5};
c3 = newreg;
Circle(c3) = {p5, p1, p4};
c4 = newreg;
Circle(c4) = {p4, p1, p2};

// Line loop
ll1 = newreg;
Line Loop(ll1) = {c1,c2,c3,c4};



// Plane surfaces
surf1 = news;
Plane Surface(surf1) = {ll1}; 

// Set boundary tags
Physical Line("wall") = {c1,c2,c3,c4};
Physical Surface(0) = {surf1};
  
