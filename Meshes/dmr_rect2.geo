lc=0.02;
lc2=0.05;
p1=newp;
Point(p1) = { -0.2, 0.000000, 0.000000,lc};
p2=newp;
Point(p2) = { 0.000000, 0.000000, 0.000000,lc2};
p3=newp;
Point(p3)  = {0.6651075103,0.384,0.,lc2};
p4=newp;
Point(p4) = { 2.4000000,  1.385640646, 0.000000,lc};
p5=newp;
Point(p5) = { 2.4000000,  3.1856406461, 0.000000,lc2};
p6=newp;
Point(p6) = {1.734892490,2.801640646,0.,lc2};
p7=newp;
Point(p7) = {0.0 , 1.8, 0.,lc};
p8=newp;
Point(p8) = {-0.2, 1.8, 0.,lc};

c1=newreg;
Line(c1) = {p1, p2};
c2=newreg;
Line(c2) = {p2, p7};
c3=newreg;
Line(c3) = {p7, p8};
c4=newreg;
Line(c4) = {p8, p1};
c5=newreg;
Line(c5) = {p2,p3};
c6=newreg;
Line(c6) = {p3,p7};
c7=newreg;
Line(c7) = {p3,p4};
c8=newreg;
Line(c8) = {p4,p5};
c9=newreg;
Line(c9) = {p5,p6};
c10=newreg;
Line(c10) = {p6,p4};
c11=newreg;
Line(c11) = {p6,p7};

ll1=newreg;
Line Loop(ll1)     = {c1, c2, c3, c4};
ll2=newreg;
Line Loop(ll2)     = {c5,c6,-c2};
ll3=newreg;
Line Loop(ll3) = {c7,-c10,c11,-c6};
ll4=newreg;
Line Loop(ll4) ={ c8, c9, c10};

surf1=news;
Plane Surface(surf1) = {ll1};
surf2=news;
Plane Surface(surf2)={ll2};
surf3=news;
Plane Surface(surf3)={ll3};
surf4=news;
Plane Surface(surf4)={ll4};


//set boubdary tag//
Physical Line("Inflow")={c4};
Physical Line("Outflow")={c8};
Physical Line("Wall")={c1,c5,c7,c9,c11,c3};
Physical Surface(0)={surf1,surf2,surf3,surf4};

n1=6;//12;
n2=60;//120;
n3=72;//144;

Transfinite Line{c1,c3}=n1 Using Progression 1;
Transfinite Line{-c4,c2}=n2 Using Progression 1;
Transfinite Surface {surf1}={p1,p2,p7,p8};

Transfinite Line {c6,c10}=n2 Using Progression 1;
Transfinite Line {c7,c11}=n3 Using Progression 1;
Transfinite Surface {surf3}={p3,p4,p6,p7};

Recombine Surface {surf1,surf3};//

