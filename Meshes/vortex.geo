// square

lc = 0.1; // Mesh parameter
x1=0.5; y1=0.05;

// Points
p1 = newp;
Point(p1) = {-x1, -y1, 0.0, lc};
p2 = newp;
Point(p2) = {x1, -y1, 0.0, lc};
p3 = newp;
Point(p3) = {x1, y1, 0.0, lc};
p4=newp;
Point(p4)={-x1,y1,0,0, lc};


// lines
c1 = newreg;
Line(c1) = {p1, p2};
c2 = newreg;
Line(c2) = {p2, p3};
c3 = newreg;
Line(c3) = {p3, p4};
c4=newreg;
Line(c4)={p4,p1};


// Line loop
ll1 = newreg;
Line Loop(ll1) = {c1,c2,c3,c4};

// Plane surfaces
surf1 = news;
Plane Surface(surf1) = {ll1}; 


// Set boundary tags
Physical Line("inflow") = {c4};          // inflow
Physical Line("outflow") = {c2};          // outflow
Physical Line("Wall") = {c1,c3}; // wall

//Physical Line("Wall") = {c1,c2,c3,c4}; // wall
Physical Surface(0) = {surf1};

n1=100; //124;
n2=10;//124;

// Make one square structured.
Transfinite Line {c1,c3}=n1 Using Progression 1;
Transfinite Line {c4,c2}=n2 Using Progression 1;
Transfinite Surface {surf1}={1,2,3,4};



//Recombine Surface {surf1};


