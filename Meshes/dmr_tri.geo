lc = 0.05; // Mesh parameter
lc2=0.5;
Point(1) = { -0.2 ,0., 0.,lc};
Point(2) = { 0.000000, 0.000000    , 0.000000,lc};
Point(3) = { 2.400000,  1.385640646, 0.000000,lc};
Point(4) = { 2.400000,  1.8        , 0.000000,lc};
Point(5) = {-0.2     ,  1.8        , 0.0     ,lc};
Point(6) = { 0.0     ,  1.8        , 0.0     ,lc2};
Point(7) = { 0.0     ,  0.414359354, 0.      ,lc};
Point(8) = {-0.2     ,  0.414359354, 0.      ,lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 1};

L1=newreg;
Line Loop(L1)     = {1,  2, 3, 4, 5};


surf1=news;
Plane Surface(surf1) = {L1};



Physical Line("Wall")   = {1,2,4};
Physical Line("Inflow") = {5};
Physical Line("Outflow")={3};




Physical Surface(0) = {surf1};
