// square

lc = 0.01; // Mesh parameter
R=1.;R2=1.; R3=0.;

// Points
p1 = newp;
Point(p1) = {-0., 0, 0.0, lc};
p2 = newp;
Point(p2) = {R, 0, 0.0, lc};
p3 = newp;
Point(p3) = {R, R2, 0.0, lc};
p4=newp;
Point(p4)={-0.,R2,0.0, lc};
p5=newp;
Point(p5)={0.5,0.,0.,lc};
p6=newp;
Point(p6)= {0.5,R2,0.,lc};


// lines
c1 = newreg;
Line(c1) = {p1, p5};
c2 = newreg;
Line(c2) = {p5, p6};
c3 = newreg;
Line(c3) = {p6, p4};
c4=newreg;
Line(c4)={p4,p1};
c5=newreg;
Line(c5)={p5,p2};
c6=newreg;
Line(c6)={p2,p3};
c7=newreg;
Line(c7)={p3,p6};


// Line loop
ll1 = newreg;
Line Loop(ll1) = {c1,c2,c3,c4};
ll2=newreg;
Line Loop(ll2)={c5,c6,c7,-c2};

// Plane surfaces
surf1 = news;
Plane Surface(surf1) = {ll1};
surf2=news;
Plane Surface(surf2)={ll2};

ny=100;
nx=50;
Transfinite Line{c2,c6}=ny Using Progression 1;
Transfinite Line{c5,c7}=nx Using Progression 1;
Transfinite Surface{surf2};
Recombine Surface(surf2);

// Set boundary tags
Physical Line("inflow") = {c4};          // inflow
Physical Line("outflow") = {c6};          // outflow
Physical Line("Wall") = {c1,c5,c3,c7}; // wall
Physical Surface(0) = {surf1,surf2};

