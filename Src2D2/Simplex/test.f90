PROGRAM test
  use precision
  USE hull
  IMPLICIT NONE
  !  INTEGER, PARAMETER:: R8=SELECTED_REAL_KIND(13)
  REAL(dp), DIMENSION(:,:), ALLOCATABLE:: points
  REAL(dp), DIMENSION(:), ALLOCATABLE:: x
  INTEGER:: D, N
  LOGICAL:: is
  INTEGER :: i

  WRITE(*,*) " dimension 2/3?"
  READ(*,*) D
  WRITE(*,*) "nombre de points"
  READ(*,*) N
  ALLOCATE(Points(D,N),X(D) )
  DO i=1, N
     WRITE(*,*) "Point #", i
     READ(1,*) Points(:,i)
  ENDDO
  WRITE(*,*) " Point a tester"
  READ(2,*) X

  is=is_in_hull_simplex(Points, X)
  IF (is) THEN
     WRITE(*,*) "nous sommes dans le domaine"
  ELSE
     WRITE(*,*) "nous n'y sommes pas.."
  ENDIF
END PROGRAM test
