MODULE hull
  ! Locate the Delaunay simplex containing a point Q using linear programming.
  ! Consider a set of points PTS = {P_i} \in R^d, i = 1, ..., n.
  ! Let A be a (d+1) X (d+1) matrix whose rows are given by: A_i = [ p_i, 1 ];
  ! let B be a n-vector B_i = p_i \dot p_i; and
  ! let C be a (d+1)-vector C_i = -Q_i for i = 1, ..., d, C_{d+1} = -1.
  !
  ! If the problem
  !
  ! \max C^T X
  ! s.t. AX \leq B.
  !
  ! has a unique solution, then the vertices of the simplex S \in DT(PTS) that
  ! contains Q are given by the solution basis for solving and the affine
  ! interpolation weights are given by the dual solution for the corresponding
  ! basis.
  !
  ! In such a situation, since the dual solution is unique, it can be solved
  ! for via the asymetric dual
  !
  ! \min B^T Y
  ! s.t. A^T Y = C
  ! Y \geq 0.
  !
  ! If the solution is nonunique, the problem is significantly harder and cannot
  ! be solved via the above methodology. Therefore, this code is to be used for
  ! demonstrative purposes only.
  USE PRECISION
  USE DUALSIMP_MOD
  IMPLICIT NONE
  REAL(dp), PARAMETER::EPS2 = SQRT(EPSILON(0.0_dp))
  PRIVATE
  PUBLIC:: is_in_hull_simplex

CONTAINS
  LOGICAL FUNCTION is_in_hull_simplex(Points, x)
    ! Declare inputs and local data.
    REAL(dp), DIMENSION(:,:), INTENT(in):: points
    REAL(dp), DIMENSION(:)  , INTENT(in):: x
    INTEGER :: D, N, M,jj
    INTEGER, SAVE:: compte=0


    REAL(DP), ALLOCATABLE :: A(:,:),  C(:,:)
    REAL(DP) :: START, FINISH

    INTEGER, ALLOCATABLE :: BASIS(:), IERR(:)
    INTEGER :: I, J


    N=SIZE(Points, dim=2)
    D=SIZE(X, dim=1)
    M=1
    IF (D.NE.SIZE(X,dim=1)) THEN
       PRINT*, "Error in dimensions of A and X"
       PRINT*, "D= ",D, "x=", SIZE(X,dim=1)
       STOP
    ENDIF

    ! Allocate all necessarry arrays.
    ALLOCATE(A(D+1,N), C(D+1,M), BASIS(D+1), IERR(M), STAT=I)

    IF(I .NE. 0) THEN
       WRITE(*,*) "Memory allocation error."
    END IF

    ! Read the input data/training points into the matrix PTS(:,:).
    DO I = 1, N
       A(1:D,i)=Points(1:D,I)
       A(D+1,I) = -1.0_dp
       !     A(D+1,I) = 1.0_dp
    END DO
    !  A = -A
    ! Read the interpolation points into the matrix C(:,:).
    DO I = 1, M
       C(1:D,I)=X(I)
       !      C(D+1,I) = 1.0_dp
       C(D+1,I) = -1.0_dp
    END DO

    !    C = -C


    ! Compute the interpolation results and time.

    DO I = 1, M
       CALL FEASIBLEBASIS(D+1, N, A, C(:,I), BASIS, IERR(I),EPS=EPS2 )
    END DO

    IF (ierr(1)==0) THEN
       is_in_hull_simplex=.TRUE.
    ELSE
!!$       print*, "feasible simplex"
!!$       compte=compte+1
!!$       if (compte==10000)then
!!$          rewind(1); rewind(2)
!!$       print*, ierr, size(A,dim=2)
!!$       do jj=1, size(A,dim=2)
!!$          write(1,*) A(1:2,jj)
!!$       enddo
!!$       write(2,*) C(:,1)
!!$       stop
!!$       endif
       is_in_hull_simplex=.FALSE.
    ENDIF


    DEALLOCATE(A, C, basis, ierr)
  END FUNCTION is_in_hull_simplex

END MODULE hull
