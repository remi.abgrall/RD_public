!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE init_bc
  USE param2d
  USE overloading
  USE variable_def
  USE PRECISION
  IMPLICIT NONE

  PRIVATE
  PUBLIC:: IC
CONTAINS

  !---------------------------------------
  ! Setup domain - comes from external mesh, so ignore for now
  !---------------------------------------

  !---------------------------------------
  ! Set initial and boundary conditions
  !---------------------------------------
  TYPE(Pvar) FUNCTION IC(jt,x,initial_cond,time) RESULT(Var)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "init_bc_euler/IC"
    INTEGER, INTENT(in)                :: initial_cond, jt
    REAL(DP), DIMENSION(:), INTENT(in) :: x
    REAL(dp), INTENT(in), OPTIONAL:: time
    REAL(DP)                               :: alpha
    REAL(DP), DIMENSION(n_dim)             :: y, centre
    REAL(DP) :: r2, vt, cs, r0, epsil, r,beta
    REAL(dp):: uinf, vinf, Tinf, du, dv, deltaT, coeff
    REAL(dp):: ro, u, v, Ma,p, x0, theta
    REAL(dp):: xc, yc, rc, ro1, u1, v1, p1, ro0, u0, v0, p0
    REAL(dp):: T, T0, T1,  eps, rap, z(2)
    REAL(DP),            PARAMETER:: dpi=ACOS(-1._dp) ! pi
    !---------------
    ! for Euler

    SELECT CASE(initial_cond)
    CASE(0)
       ! First example
       Var%u(1) = 0.5_dp
       Var%u(2) = 1.0_dp
       Var%u(3) = 0.0_dp
       Var%u(4) = 0.125_dp/(gmm-1._dp)+0.5_dp*1._dp/0.5_dp
    CASE(1)
       ! Sod
       r = SQRT(SUM(x**2))

       IF (r <= 0.5_dp) THEN
          !                 IF (x(1) <= 0.0) THEN
          ! sod
          Var%u(1) = 1._dp ! [kg/m^3]
          Var%u(2) = 0._dp ! [m/s]
          Var%u(3) = 0._dp ! [m/s]
          Var%u(4) = Var%eEOS(Var%u(1),1.0_dp) ! [J]
       ELSE 
          Var%u(1) = 0.125_dp
          Var%u(2) = 0._dp
          Var%u(3) = 0._dp
          Var%u(4) = Var%eEOS(Var%u(1),0.1_dp)
       END IF
       !       end if
    CASE(-1)
       beta=5
       r = SQRT(SUM(x**2))
       ro=(1-((gmm-1._dp)*beta**2*EXP(1-r**2))/(8._dp*gmm*dpi**2))**(1._dp/(gmm-1._dp))
       p=ro**gmm
       Var%u(1) = ro
       Var%u(2) = -Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(2)/(2._dp*dpi*r)
       Var%u(3) = Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(1)/(2._dp*dpi*r)
       Var%u(4) = Var%eEOS(Var%u(1),p)+ 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)       
    CASE(2)
       ! hydrostatic
       Var%u(1) = EXP(-(x(1)+x(2)))   !1.0+exp(-20.*((x(1)-0.5)**2+(x(2)-0.5)**2))! [kg/m^3]
       Var%u(2) = 0._dp!Var%u(1)*1.0 ! [m/s]
       Var%u(3) = 0._dp!Var%u(1)*1.0 ! [m/s]
       Var%u(4) = Var%eEOS(Var%u(1),EXP(-(x(1)+x(2))))! [J]
    CASE(3) ! advection
       Var%u(1) = EXP(-20._dp*((x(1)-0.5_dp)**2+(x(2)-0.5_dp)**2))! [kg/m^3]
       Var%u(2) = Var%u(1)*1.0_dp ! [m/s]
       Var%u(3) = Var%u(1)*1.0_dp ! [m/s]
       Var%u(4) = Var%eEOS(Var%u(1),0.0000001_dp) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]
    CASE(4)
       ! disk
       r = SQRT((x(1)-0.5_dp)**2+(x(2)-0.5_dp)**2)
       IF ((r >= 0.1_dp).AND.(r<= 0.3_dp)) THEN
          Var%u(1) = 1.0_dp! [kg/m^3]
       ELSE
          Var%u(1) = 0.0001_dp! [kg/m^3]
       END IF

       IF ((r >= 0.08_dp).AND.(r<= 0.32_dp)) THEN
          Var%u(2) = -Var%u(1)*(x(2)-0.5_dp)/(r*SQRT(r)) ! [m/s]
          Var%u(3) = Var%u(1)*(x(1)-0.5)/(r*SQRT(r))!x(1)/r ! [m/s]
       ELSE
          Var%u(2) = 0.0_dp!-Var%u(1)*1.0!*x(2)/r ! [m/s]
          Var%u(3) = 0.0_dp!Var%u(1)*1.0!*x(1)/r ! [m/s]
       END IF
       Var%u(4) = Var%eEOS(Var%u(1),0.0001_dp) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]

    CASE(5) ! rotating blob without source
       r = SQRT((x(1)-0.5_dp)**2+(x(2)-0.5_dp)**2)
       IF ((r >= 0.1_dp).AND.(r<= 0.3_dp)) THEN
          Var%u(1) = 1.0_dp! [kg/m^3]
       ELSE
          Var%u(1) = 0.0001_dp! [kg/m^3]
       END IF

       IF ((r >= 0.08_dp).AND.(r<= 0.32_dp)) THEN
          Var%u(2) = -Var%u(1)*(x(2)-0.5_dp)/(r*SQRT(r)) ! [m/s]
          Var%u(3) = Var%u(1)*(x(1)-0.5_dp)/(r*SQRT(r))!x(1)/r ! [m/s]
       ELSE
          Var%u(2) = 0.0_dp!-Var%u(1)*1.0!*x(2)/r ! [m/s]
          Var%u(3) = 0.0_dp!Var%u(1)*1.0!*x(1)/r ! [m/s]
       END IF
       Var%u(4) = Var%eEOS(Var%u(1),0.0001_dp) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]
    CASE(6)
       ! constant density disk
       epsil = 0.25_dp
       r = SQRT((x(1)-0.5_dp)**2+(x(2)-0.5_dp)**2 + epsil**2)
       vt = SQRT(1._dp/r-0.03_dp**2/r)
       cs = 0.03_dp*vt
       r0 = 0.25_dp
       var%u(1) = 1._dp*1._dp/(1._dp+(r/r0)**20)
       var%u(2) = var%u(1)*(-vt/r)*(x(2)-0.5_dp)
       var%u(3) = var%u(1)*(vt/r)*(x(1)-0.5_dp)
       var%u(4) = Var%eEOS(Var%u(1),var%u(1)*cs**2) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]
    CASE(7)
       var%u(1) = EXP(-(x(1)+x(2)))
       var%u(2) = 0.0_dp !var%u(1)*(-vt/r)*(x(2)-0.5)
       var%u(3) = 0.0_dp! var%u(1)*(vt/r)*(x(1)-0.5)
       var%u(4) = Var%eEOS(Var%u(1),EXP(-(x(1)+x(2)))) !+ 0.5*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]
    CASE(8) ! stationary vortex ! Checked by maple
       uinf=0._dp
       vinf=0._dp
       Tinf=1._dp
       beta = 5._dp
       Ma= beta/(2._dp*dpi)

       r2 = ((x(1))**2+(x(2))**2)
       coeff=EXP ( (1._dp-r2) *0.5_dp)
       du= Ma*coeff*(-x(2))
       dv= Ma*coeff*( x(1))
       deltaT= -xka*0.5_dp*Ma*Ma*coeff*coeff/gmm
       IF (r2.GE.8._dp*8._dp) THEN
          du=0._dp
          dv=0._dp
          deltaT=0._dp
       ENDIF
       u=uinf+du
       v=vinf+dv
       ro= ( Tinf+deltaT)**(1._dp/xka)

       var%u(1) = ro
       var%u(2) = ro*u
       var%u(3) = ro*v
       var%u(4) =(ro**gmm)/xka + 0.5_dp*ro*(u*u+v*v)

    CASE(9) ! strong shock tube
       ! Sod
       r = SQRT(SUM(x**2))

       IF (r <= 0.5_dp) THEN
          ! sod
          Var%u(1) = 1.e-3_dp ! [kg/m^3]
          Var%u(2) = 0._dp ! [m/s]
          Var%u(3) = 0._dp ! [m/s]
          Var%u(4) = Var%eEos(Var%u(1),1.0e-3_dp) ! [J]
       ELSE
          Var%u(1) = 0.125_dp
          Var%u(2) = 0._dp
          Var%u(3) = 0._dp
          Var%u(4) = Var%eEOS(Var%u(1),0.1_dp)
       END IF
    CASE(10) ! DMR test
       x0=-0.05_dp
       x0=0._dp
       IF (x(1) .GE. x0) THEN
          ro=1.4_dp; u=0._dp; v=0._dp; p=1._dp
       ELSE
          ro=8._dp; u=8.25_dp; v=0._dp; p=116.5_dp
       END IF
       Var%u(1)=ro ! [kg/m^3]
       Var%u(2)=ro*u ! [m/s]
       Var%u(3)=ro*v ! [m/s]
       Var%u(4)= Var%eEOS(Var%u(1),p) + 0.5_dp*ro*(u*u+v*v)
       !  CASE(11) ! Step test
       !     Var%u(1) = 1.0_dp
       !    Var%u(2) = 3.0_dp
       !     Var%u(3) = 0.0_dp
       !     Var%u(4) = 4.5_dp + 1.7857_dp
       ! CASE(11)
       !    beta=5
       !    IF (x(1) .GE. 0._dp .AND. x(2) .GE. 0._dp) THEN
       !       r = SQRT((x(1)- 0.25_dp)**2+(x(2)- 0.25_dp)**2)
       !    ELSEIF (x(1) .LT. 0._dp .AND. x(2) .GE. 0._dp) THEN
       !       r = SQRT((x(1)+ 0.25_dp)**2+(x(2)- 0.25_dp)**2)
       !    ELSEIF (x(1) .LT. 0._dp .AND. x(2) .LT. 0._dp) THEN
       !       r = SQRT((x(1)+ 0.25_dp)**2+(x(2)+ 0.25_dp)**2)
       !    ELSEIF (x(1) .LE. 0._dp .AND. x(2) .LT. 0._dp) THEN
       !       r = SQRT((x(1)- 0.25_dp)**2+(x(2)+ 0.25_dp)**2)
       !    ENDIF
       !    ro=(1-((gmm-1._dp)*beta**2*EXP(1-r**2))/(8*gmm*dpi**2))**(1._dp/(gmm-1._dp))
       !    p=ro**gmm
       !    Var%u(1) = ro
       !    IF (x(1)*x(2) .GE. 0._dp) THEN
       !      Var%u(2) = -Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(2)/(2._dp*dpi)
       !       Var%u(3) = Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(1)/(2._dp*dpi)
       !    ELSE 
       !       Var%u(2) = Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(2)/(2._dp*dpi)
       !       Var%u(3) = -Var%u(1)*beta*EXP((1-r**2)/2._dp)*x(1)/(2._dp*dpi)
       !    ENDIF
       !    Var%u(4) = Var%eEOS(Var%u(1),p)+ 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)       
    CASE(12)
       beta=5
       xc=0.25_dp; yc=0.5_dp
       r = SQRT((x(1)- xc)**2+(x(2)- yc)**2)
       ro=(1-((gmm-1._dp)*beta**2*EXP(1-r**2))/(8*gmm*dpi**2))**(1._dp/(gmm-1._dp))
       p=ro**gmm
       Var%u(1) = ro
       Var%u(2) = Var%u(1)*(1._dp -beta*EXP((1-r**2)/2._dp)*(x(2)- 0._dp)/(2._dp*dpi))
       Var%u(3) = Var%u(1)*(1._dp +beta*EXP((1-r**2)/2._dp)*(x(1)- 1._dp)/(2._dp*dpi))
       Var%u(4) = Var%eEOS(Var%u(1),p)+ 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)    
    CASE(11)
       r = SQRT((x(1)-0.5_dp)**2+(x(2)-0.5_dp)**2)
       IF (r<0.2_dp) THEN
          vt=5._dp*r
       ELSEIF ((r >= 0.2_dp).AND.(r< 0.4_dp)) THEN
          vt=2._dp-5._dp*r
       ELSE
          vt=0
       END IF
       IF (r<0.2_dp) THEN
          p=5._dp+25._dp*(r**2)/2._dp
       ELSEIF ((r >= 0.2_dp).AND.(r< 0.4_dp)) THEN
          p=9._dp-4._dp*LOG10(0.2_dp) + 25._dp*(r**2)/2._dp-20._dp*r+4._dp*LOG10(r)
       ELSE
          p=3._dp+4._dp*LOG10(2._dp)
       END IF
       var%u(1) = 1._dp
       var%u(2) = var%u(1)*(-vt/r)*(x(2)-0.5_dp)
       var%u(3) = var%u(1)*(vt/r)*(x(1)-0.5_dp)
       var%u(4) = Var%eEOS(Var%u(1),p) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1)! [J]
    CASE(27) ! Shu Osher like 2D
       ! Sod
       r = SQRT(SUM(x**2))
       theta=5._dp
       IF (r <= 1.0_dp) THEN
          ! sod
          Var%u(1) = 3.857143_dp ! [kg/m^3]
          Var%u(2) = SQRT(Var%u(1)*2.629369_dp)*x(1)/(r+1.e-10_dp)  ! [m/s]
          Var%u(3) = SQRT(Var%u(1)*2.629369_dp)*x(2)/(r+1.e-10_dp) ! [m/s]
          Var%u(4) = Var%eEOS(Var%u(1),10.33333333333_dp) + 0.5_dp*(Var%u(2)**2+Var%u(3)**2)/Var%u(1) ! [J]
       ELSE
          IF (r.LE. 4.0_dp) THEN
             Var%u(1) = 1._dp+0.2_dp*SIN(theta*r)
             Var%u(2) = 0._dp
             Var%u(3) = 0._dp
             Var%u(4) = Var%eEOS(Var%u(1),1.0_dp)
          ELSE
             r=4.0_dp
             Var%u(1) = 1._dp+0.2_dp*SIN(theta*r)
             Var%u(2) = 0._dp
             Var%u(3) = 0._dp
             Var%u(4) = Var%eEOS(Var%u(1),1.0_dp)
          END IF
       ENDIF
!!$      CASE(42) ! Kevlvin-Helmholz test Ione and Iwto are the 
!!$      Ione = 0.0
!!$      Itwo = 0.0
!!$       DO ii=1, 10
!!$       Ione = Ione + 0.01*(a_1(ii)*Cos(b_1(ii)+2.*ii**x(1)))
!!$       Itwo = Itwo + 0.01*(a_2(ii)*Cos(b_2(ii)+2.*ii**x(1)))
!!$       EndDO 
!!$       Ione = Ione + 0.25
!!$       Itwo = Itwo + 0.75
!!$     !  Print*, "Ione=" , Ione, "Iwo=", Itwo, "Test_1=", Sum(b_1), Sum(b_2) , x(2) 
!!$       IF (Itwo.GE. x(2) .and. x(2) .GE. Ione) THEN
!!$          Var%u(1) = 2. 
!!$          Var%u(2) = -0.5 
!!$          Var%u(3) = 0.
!!$          Var%u(4)=2.5
!!$       ELSE
!!$          Var%u(1) = 1. 
!!$          Var%u(2) = 0.5 
!!$          Var%u(3) = 0. 
!!$          Var%u(4) = 2.5
!!$       END IF
    CASE(13) ! Rotating steady:wq




       ro=EXP(x(1)**2+x(2)**2)
       p=ro*0.5_dp

       u=-x(2); v=+x(1)

       Var%u(1)=ro ! [kg/m^3]
       Var%u(2)=ro*u ! [m/s]
       Var%u(3)=ro*v ! [m/s]
       Var%u(4)= Var%eEOS(Var%u(1),p) + 0.5_dp*ro*(u*u+v*v)

    CASE(14)
       x0=-0.05_dp
       x0=0._dp
       IF (x(1) .GE. x0) THEN
          ro=1.4_dp; u=1._dp; v=0._dp; p=1._dp
       ELSE
          ro=8._dp; u=1._dp; v=0._dp; p=1._dp
       END IF
       Var%u(1)=ro ! [kg/m^3]
       Var%u(2)=ro*u ! [m/s]
       Var%u(3)=ro*v ! [m/s]
       Var%u(4)= Var%eEOS(Var%u(1),p) + 0.5_dp*ro*(u*u+v*v)
    CASE(15) ! moving vortex
       xc=0.25_dp; yc=0.5_dp
       alpha=0.204_dp; rc=0.05; eps=0.3_dp
       ! shock parameters
       Ma=1.1
       ! before shock
       ro0=1._dp
       u0=Ma*SQRT(gmm); v0=0._dp; p0=1._dp
       T0=p0/ro0
       z(1)=x(1)-xc-u0*t; z(2)=x(2)-yc-v0*t; z=z/rc
       ! superposition
       !       IF (x(1).GE.0.5_dp) THEN
       !          ro=ro1;u=u1;v=0._dp;p=p1
       !       ELSE
       !!          ro=ro0; u=u0;v=v0; p=p0
       R2=z(1)**2+z(2)**2
       coeff=EXP(alpha*(1._dp-R2))
       u=u0+eps* z(2)*coeff
       v=v0-eps* z(1)*coeff
       T=1._dp-(gmm-1._dp)/(4._dp*alpha*gmm)*eps*eps*coeff*coeff
       ro= ro0*( T/T0)**(1._dp/gmm)
       p=ro*T
       !       ENDIF
       Var%u(1)=ro
       Var%u(2)=ro*u
       Var%u(3)=ro*v
       Var%u(4)=Var%eEOS(ro,p)+0.5_dp*ro*(u*u+v*v)
    CASE(16) ! shock vortex interaction
       xc=0.25_dp; yc=0.5_dp
       alpha=0.204_dp; rc=0.05; eps=0.3_dp
       ! shock parameters
       Ma=1.1
       ! before shock
       ro0=1._dp
       u0=Ma*SQRT(gmm); v0=0._dp; p0=1._dp
       ! before shock
       rap=(gmm-1._dp)/(gmm+1._dp)+2._dp/((gmm+1._dp)*Ma*Ma)
       ro1=ro0/rap
       rap=2._dp*gmm/(gmm+1._dp)*Ma*Ma-(gmm-1._dp)/(gmm+1._dp)
       p1=p0*rap
       u1=ro0*u0/ro1
       T0=p0/ro0
       z(1)=x(1)-xc-u0*t; z(2)=x(2)-yc-v0*t; z=z/rc
       ! superposition
       IF (x(1).GE.0.5_dp) THEN
          ro=ro1;u=u1;v=0._dp;p=p1
       ELSE
          R2=z(1)**2+z(2)**2
          coeff=EXP(alpha*(1._dp-R2))
          u=u0+eps* z(2)*coeff
          v=v0-eps* z(1)*coeff
          T=1._dp-(gmm-1._dp)/(4._dp*alpha*gmm)*eps*eps*coeff*coeff
          ro= ro0*( T/T0)**(1._dp/gmm)
          p=ro*T
       ENDIF
       Var%u(1)=ro
       Var%u(2)=ro*u
       Var%u(3)=ro*v
       Var%u(4)=Var%eEOS(ro,p)+0.5_dp*ro*(u*u+v*v)

    CASE default
       PRINT*, "Wrong test number for Euler_IC"
       PRINT*, "test=", initial_cond
       STOP
    END SELECT

  END FUNCTION IC


END MODULE init_bc
