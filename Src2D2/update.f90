!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE update

  ! In this module are collected the main_update, corresponding to the update of the Flux of the scheme
  ! and the edge_main_update which accounts for the stabilization of the scheme 

  USE overloading
  USE element_class
  USE variable_def
  USE arete_class
  USE param2d
  USE scheme
  USE Model
  USE PRECISION
  !  USE hull
  !  USE dans_mon_convexe
  USE quickhull
#ifdef parallel
  USE mpi
#endif

  IMPLICIT NONE
  PRIVATE
  ! MOOD CRITERIA
  INTEGER,PARAMETER :: plateau = 10  ! constant solution
  INTEGER,PARAMETER :: NAN_criteria = 30 ! NaN sol
  INTEGER,PARAMETER :: PAD_criteria = 20 ! positivity problem
  INTEGER,PARAMETER :: DMP_B1=7       ! failure of discrete max principle for B1
  INTEGER,PARAMETER :: DMP_nou2=5       ! true extrema to be protected
  INTEGER,PARAMETER :: DMP_u2_2=1       ! not an extrema
  INTEGER,PARAMETER :: RIEN=0           ! nothing to do

  !--------------------------------------
  INTEGER, PARAMETER:: theflux=5,theflux2=-1
  INTEGER, PARAMETER:: n_list=1!2
  INTEGER, DIMENSION(n_list), PARAMETER:: list=(/1/)!,4/)
  !  REAL(DP), PARAMETER:: eps1=1.e-4_dp,eps2=5.e-6_dp
  !  REAL(DP), PARAMETER:: eps1=1.e-3_dp,eps2=1.e-5_dp ! dmr
  !  REAL(DP), PARAMETER:: eps1=1.e-2_dp,eps2=1.e-5_dp ! vortex
  REAL(DP), PARAMETER:: eps1=1.e-3_dp,eps2=1.e-2_dp ! vortex
  REAL(DP), PARAMETER:: coeff=0._dp!1_dp!1.0_dp

  TYPE, PUBLIC:: gradi
     REAL(dp), DIMENSION(:,:),ALLOCATABLE:: grad
  END TYPE gradi


  INTERFACE main_update_corrected
     !     MODULE PROCEDURE main_update_cor_entropy
     MODULE PROCEDURE main_update_cor_kinetic
  END INTERFACE main_update_corrected


  INTERFACE test
     MODULE PROCEDURE test_12
  END  INTERFACE test

  PUBLIC:: main_update, edge_main_update, test, main_update_corrected

CONTAINS



  SUBROUTINE main_update(k,jt,dt, e,Var, DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "main_update"
    REAL(DP), INTENT(in):: dt
    INTEGER, INTENT(in):: k,jt
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma
    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta
    TYPE(element), INTENT(inout):: e
    TYPE(variables), INTENT(inout):: Var
    TYPE(donnees), INTENT(in):: DATA
!!!!!!!!!!!!!!!!!
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: flux, flux_c
    TYPE(Pvar),DIMENSION(0:DATA%iordret-1,e%nsommets):: u, up, u_p, up_p
    TYPE(Pvar),DIMENSION(e%nsommets):: res, difference, uu
    INTEGER:: l, lp, iordret

    IF (e%type_flux==-1) THEN
       iordret=-DATA%iordret
    ELSE
       iordret=DATA%iordret
    ENDIF


    DO l=1,e%nsommets
       up(:,l) =Var%up(:,e%nu(l))
    ENDDO
    u=up


    DO l=1,e%nsommets
       difference(l)=up(k-1,l)-up(0,l)
    ENDDO




    DO l=0,DATA%iordret-1
       u_P(l,:) =Control_to_Cons(u(l,:),e)
    ENDDO
    up_P=u_P

    flux_c=0.0_dp; uu=0.0_dp
    DO l=1, e%nsommets

       DO lp=0, n_theta(iordret)-1
          flux_c(:,l)= flux_c(:,l)+ theta(lp,k-1,iordret)* up_P(lp,l)%flux()
          uu(l)      = uu(l)      + theta(lp,k-1,iordret)* up(lp,l)
       ENDDO



    ENDDO
    DO l=1, n_dim
       flux(l,:)=Cons_to_control(flux_c(l,:),e)
    ENDDO


    res=schema( DATA%limit,  e, uu, difference, flux,  dt)

    DO l=1, e%nsommets
       Var%un(l,jt)=Var%un(l,jt)+res(l)
    ENDDO
  END SUBROUTINE main_update


  SUBROUTINE main_update_cor_kinetic(k,jt,dt, e,Var, DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "main_update"
    REAL(DP), INTENT(in):: dt
    INTEGER, INTENT(in):: k,jt

    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta




    TYPE(element), INTENT(in):: e
    TYPE(variables), INTENT(inout):: Var

    TYPE(donnees), INTENT(in):: DATA


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: flux, flux_c
    TYPE(PVar),DIMENSION(e%nsommets)::new_cons
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: fluxg
    TYPE(Pvar),DIMENSION(0:DATA%iordret-1,e%nsommets):: u, up, u_p, up_p
    TYPE(Pvar),DIMENSION(e%nsommets):: res, difference, uu
    INTEGER:: l, lp
    REAL(qp)::resk
    REAL(qp), DIMENSION(n_dim):: xbar
    REAL(qp):: alphaa, cor
    REAL(qP)                                     :: we,psi

    DO l=1,e%nsommets
       up(:,l) =Var%up(:,e%nu(l))
    ENDDO
    u=up
    DO l=1,e%nsommets
       difference(l)=up(k-1,l)-up(0,l)
    ENDDO



    DO l=0,DATA%iordret-1
       u_P(l,:) =Control_to_Cons(u(l,:),e)
    ENDDO
    up_P=u_P

    flux_c=0.0_dp; uu=0.0_dp
    DO l=1, e%nsommets

       DO lp=0, n_theta(DATA%iordret)-1
          flux_c(:,l)= flux_c(:,l)+ theta(lp,k-1,DATA%iordret)* up_P(lp,l)%flux()!(/e%coor(:,l)/))
          uu(l)      = uu(l)      + theta(lp,k-1,DATA%iordret)* up(lp,l)
       ENDDO



    ENDDO
    DO l=1, n_dim
       flux(l,:)=Cons_to_control(flux_c(l,:),e)
    ENDDO

    we=0._qp
    DO l=1, e%nsommets
       we=we+REAL(var%un(l,jt)%u(3),qp)*REAL(e%y(1,l),qp)-REAL(var%un(l,jt)%u(2),qp)*REAL(e%y(2,l),qp)
    END DO

    resk=phij(e, uu, difference, flux, dt)
    psi=resk-we

    xbar(1)=REAL(SUM(e%y(1,:)),qp)/REAL(e%nsommets,qp)
    xbar(2)=REAL(SUM(e%y(2,:)),qp)/REAL(e%nsommets,qp)
    cor=SUM( (REAL( e%y(1,:),qp)-xbar(1) )**2+(REAL( e%y(2,:),qp)-xbar(2) )**2)
    alphaa=psi/(cor)


    DO l=1,e%nsommets
       var%un(l,jt)%u(2)=var%un(l,jt)%u(2)-REAL(alphaa*(REAL(e%y(2,l),qp)-xbar(2) ),dp)
       var%un(l,jt)%u(3)=var%un(l,jt)%u(3)+REAL(alphaa*(REAL(e%y(1,l),qp)-xbar(1) ),dp)
    END DO

  END SUBROUTINE main_update_cor_kinetic


  SUBROUTINE main_update_cor_kinetic_old(k,jt,dt, e,Var, DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "main_update"
    REAL(DP), INTENT(in):: dt
    INTEGER, INTENT(in):: k,jt
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta

    TYPE(element), INTENT(in):: e
    TYPE(variables), INTENT(inout):: Var

    TYPE(donnees), INTENT(in):: DATA


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: flux, flux_c
    TYPE(PVar),DIMENSION(e%nsommets)::new_cons
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: fluxg
    TYPE(Pvar),DIMENSION(0:DATA%iordret-1,e%nsommets):: u, up, u_p, up_p
    TYPE(Pvar),DIMENSION(e%nsommets):: res, difference, uu
    INTEGER:: l, lp
    TYPE(Pvar), DIMENSION(e%nsommets):: psi  
    DO l=1,e%nsommets
       up(:,l) =Var%up(:,e%nu(l))
    ENDDO
    u=up
    DO l=1,e%nsommets
       difference(l)=up(k-1,l)-up(0,l)
    ENDDO



    DO l=0,DATA%iordret-1
       u_P(l,:) =Control_to_Cons(u(l,:),e)
    ENDDO
    up_P=u_P

    flux_c=0.0_dp; uu=0.0_dp
    DO l=1, e%nsommets

       DO lp=0, n_theta(DATA%iordret)-1
          flux_c(:,l)= flux_c(:,l)+ theta(lp,k-1,DATA%iordret)* up_P(lp,l)%flux()!(/e%coor(:,l)/))
          uu(l)      = uu(l)      + theta(lp,k-1,DATA%iordret)* up(lp,l)
       ENDDO



    ENDDO
    DO l=1, n_dim
       flux(l,:)=Cons_to_control(flux_c(l,:),e)
    ENDDO

    CALL correction_kinetic(e, var%un(:, jt) ,difference,u, flux, dt)
    !    CALL correction(e, var%un(:, jt) ,difference,u, flux, dt)


  END SUBROUTINE main_update_cor_kinetic_old

  SUBROUTINE main_update_cor_entropy(k,jt,dt, e,Var, DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "main_update"
    REAL(DP), INTENT(in):: dt
    INTEGER, INTENT(in):: k,jt
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta

    TYPE(element), INTENT(in):: e
    TYPE(variables), INTENT(inout):: Var

    TYPE(donnees), INTENT(in):: DATA


    REAL(dp),DIMENSION(n_dim,e%nsommets):: flux, flux_c
    TYPE(PVar),DIMENSION(e%nsommets)::new_cons
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets):: fluxg
    TYPE(Pvar),DIMENSION(0:DATA%iordret-1,e%nsommets):: u, up, u_p, up_p
    TYPE(Pvar), DIMENSION(e%nsommets):: uu
    REAL(dp),DIMENSION(e%nsommets):: difference,difference_c
    INTEGER:: l, lp
    REAL(dp):: entropy
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    DO l=1,e%nsommets
       up(:,l) =Var%up(:,e%nu(l))
    ENDDO
    u=up
    ! now we evaluate the entropy flux
    DO l=0,DATA%iordret-1
       u_P(l,:) =Control_to_Cons(u(l,:),e)
    ENDDO
    up_P=u_P


!!$        ! compute the dfifference in entropy
    DO l=1,e%nsommets
       difference_c(l)=u_p(k-1,l)%entropy_function()-u_p(0,l)%entropy_function()
    ENDDO
    ! go in control
    !    do l=1, e%nsommets
    difference=MATMUL(e%inv_base_at_dofs,difference_c)
    !       enddo

!!$    DO l=1,e%nsommets
!!$       difference(l)=up(k-1,l)-up(0,l)
!!$    ENDDO




    flux_c=0.0_dp; uu=0.0_dp
    DO l=1, e%nsommets

       DO lp=0, n_theta(DATA%iordret)-1
          flux_c(:,l)= flux_c(:,l)+ theta(lp,k-1,DATA%iordret)* up_P(lp,l)%entropy_flux()
          uu(l)      = uu(l)      + theta(lp,k-1,DATA%iordret)* up(lp,l)
       ENDDO



    ENDDO
    DO l=1, n_dim
       flux(l,:)=MATMUL(e%inv_base_at_dofs,flux_c(l,:) )
    ENDDO
    entropy=Phi_e(e, difference, flux, dt)
    CALL correction_entropy(e, var%un(:, jt) ,entropy,u, dt)
    !    CALL correction(e, var%un(:, jt) ,entropy,u, dt)


  END SUBROUTINE main_update_cor_entropy

  SUBROUTINE main_update_cor_vorticity(k,jt,dt, e,Var,wn, DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *),            PARAMETER :: mod_name = "main_update_cor_vorticity"
    REAL(DP),                      INTENT(in):: dt
    INTEGER,                       INTENT(in):: k,jt
    REAL(DP),DIMENSION(3,3:4),     INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4),   INTENT(in):: beta, gamma

    INTEGER, DIMENSION(-4:4),         INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta

    TYPE(element),                 INTENT(in):: e
    TYPE(variables),            INTENT(inout):: Var
    TYPE(Pvar), DIMENSION(:),      INTENT(in):: wn

    TYPE(donnees), INTENT(in)                :: DATA 
    INTEGER                                  :: l, lp
    REAL(dp), DIMENSION(n_dim)               :: mu


    DO l=1,2
       mu(l)=SUM(wn(e%nu(:))%u(l))/3._dp
    ENDDO
    DO l=1, e%nsommets
       var%un(l,jt)%u(1:2)=var%un(l,jt)%u(1:2)  + (wn(e%nu(l))%u(1:2)-mu(1:2))
    ENDDO


  END SUBROUTINE main_update_cor_vorticity


  SUBROUTINE test_12(k_iter,Debug,Var, mesh, DATA, flux_mood)
    !26.04.2021
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="test_12"
    !===========================================================


    INTEGER, INTENT(in)                        :: k_iter
    INTEGER, INTENT(IN)                        :: flux_mood
    TYPE(maillage), INTENT(inout)              :: mesh
    TYPE(variables), INTENT(in)                ::  debug, Var
    TYPE(donnees), INTENT(in)                  :: DATA
    !================================================================
    TYPE(arete)                                :: ed
    TYPE(element)                              :: e
    TYPE(Pvar), DIMENSION(Mesh%nt   )          ::umin, umax,dumin,dumax
    TYPE(PVAR), DIMENSION(Mesh%ndofs)          ::dvmin,dvmax,vmin, vmax
    !================================================================
    TYPE(PVar), DIMENSION(:),ALLOCATABLE       :: u_old, v_old, u_new, v_new
    TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE    :: phys, phys_d
    INTEGER, DIMENSION(Mesh%ndofs)             :: diag, diag2
    TYPE(gradi), DIMENSION(:),ALLOCATABLE, SAVE:: gradient_d, gradient
    REAL(dp), DIMENSION(:,:),ALLOCATABLE       :: grad
    REAL(dp), DIMENSION(:,:), ALLOCATABLE      :: points
    REAL(dp), DIMENSION(:,:), ALLOCATABLE      :: x
    REAL(dp), DIMENSION(n_dim)                 :: xg
    LOGICAL                                    :: test_ouille
    INTEGER                                    :: jt, k,  l, lk, is, jseg, lkk, jtt, nbre, p,id
    REAL(DP)                                   :: eps,val_min_n, val_max_n
    INTEGER                                    :: compte
    INTEGER                                    :: testouille
    TYPE(pvar)                                 :: u_min,u_max
    INTEGER, DIMENSION(1)                      :: nombre
    TYPE(Pvar),DIMENSION(SIZE(debug%un,dim=1),SIZE(debug%un,dim=2)):: up_e, ua_e
    ! note: The analysis makes sense here only for the interior elements: we may violate
    ! things on the boundary.

    DO jt=1, mesh%nt
       e=mesh%e(jt)
       up_e(1:e%nsommets,jt)=var%ua(0,e%nu)
       ua_e(1:e%nsommets,jt)=debug%ua(k_iter,e%nu)-REAL(e%nsommets,dp)/e%volume*debug%un(1:e%nsommets,jt)
    ENDDO



    IF (.NOT.ALLOCATED(gradient_d)) THEN
       ALLOCATE(gradient_d(Mesh%nt), gradient(Mesh%nt))
       DO jt=1, Mesh%nt
          ALLOCATE(gradient_d(jt)%grad(n_dim,mesh%e(jt)%nvertex) )
          ALLOCATE(gradient(  jt)%grad(n_dim,mesh%e(jt)%nvertex) )
       ENDDO
    ENDIF

    diag=0
    diag2=0



    !
    ALLOCATE (phys(SIZE(debug%un,dim=1),SIZE(debug%un,dim=2)),&
         &phys_d(SIZE(debug%un,dim=1),SIZE(debug%un,dim=2)) )

    !compute physical quantities from interpolated quantities.
    ! is this really usefull for Bezier thanks to the TV property?
    ! If not done : more strict condition since Bezier>0==> positive function
    ! converse not true
    umin   =  HUGE(1.0_dp)
    vmin   =  HUGE(1.0_dp)
    dumin  =  HUGE(1.0_dp)
    dvmin  =  HUGE(1.0_dp)
    umax   = -HUGE(1.0_dp)
    dvmax  = -HUGE(1.0_dp)
    dumax  = -HUGE(1.0_dp)
    vmax   = -HUGE(1.0_dp)

    ! compute physical variables (old & new) in physical variable
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u_old(e%nsommets),u_new(e%nsommets),v_old(e%nsommets),v_new(e%nsommets))
       u_old=up_e(1:e%nsommets,jt)
       u_new=ua_e(1:e%nsommets,jt)
       v_old = control_to_cons(u_old,e)
       v_new = control_to_cons(u_new,e)
       DO l=1, e%nsommets
          phys  (l,jt)=v_old(l)%cons2prim()
          phys_d(l,jt)=v_new(l)%cons2prim()
       ENDDO
       DEALLOCATE(u_old, u_new, v_old, v_new)
    ENDDO
    DO jt=1, Mesh%nt
       DO k=1, mesh%e(jt)%nsommets
          DO l=1, n_vars
             IF (isnan(phys_d(k,jt)%u(l))) THEN
                phys_d(k,jt)%u(l)=-HUGE(1._dp)

             ENDIF
          ENDDO
       ENDDO
    ENDDO

    ! then compute min/max of these variables (old and new) in a neighborhood of points
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       DO lk=1,e%nsommets
          DO l=1, n_vars
             vmin(e%nu(lk))%u(l)  = MIN( vmin(e%nu(lk))%u(l),   MINVAL(phys  (1:e%nsommets,jt)%u(l)) )
             vmax(e%nu(lk))%u(l)  = MAX( vmax(e%nu(lk))%u(l),   MAXVAL(phys  (1:e%nsommets,jt)%u(l)) )
          ENDDO
          DO l=1, n_vars
             dvmin(e%nu(lk))%u(l) = MIN( dvmin(e%nu(lk))%u(l),  MINVAL(phys_d(1:e%nsommets,jt)%u(l)) )
             dvmax(e%nu(lk))%u(l) = MAX( dvmax(e%nu(lk))%u(l),  MAXVAL(phys_d(1:e%nsommets,jt)%u(l)) )
          ENDDO
       ENDDO
    ENDDO
    ! Compute the min and max of the 'physical' variable in a neighborood of the element jt.
    ! This should be good estimate of
    ! the 'true' local min and max. Old variables
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       DO l=1, n_vars
          umin (jt)%u(l) = MINVAL( vmin(e%nu)%u(l) )
          umax (jt)%u(l) = MAXVAL( vmax(e%nu)%u(l) )
          dumin(jt)%u(l) = MINVAL(dvmin(e%nu)%u(l) )
          dumax(jt)%u(l) = MAXVAL(dvmax(e%nu)%u(l) )
       ENDDO
    ENDDO

    DO l=1, n_vars
       u_min%u(l)=MINVAL(phys%u(l))
       u_max%u(l)=MAXVAL(phys%u(l))
    ENDDO



    DO lkk=1,n_list !loop on variables to be tested
       lk=list(lkk)

       DO jt=1, Mesh%nt ! compute gradients of debug physical variables
          e=Mesh%e(jt)

          DO id=1, e%nvertex

             gradient_d(jt)%grad(1,id)=SUM((phys_d(1:e%nsommets,jt)%u(lk) - phys_d(1,jt)%u(lk) ) &
                  &*e%grad_at_dofs(1,1,id) ) !grad(1,:))
             gradient_d(jt)%grad(2,id)=SUM((phys_d(1:e%nsommets,jt)%u(lk) - phys_d(1,jt)%u(lk) )*&
                  & e%grad_at_dofs(2,1,id) ) !grad(2,:))

             gradient(jt)%grad(1,id)=SUM((phys(1:e%nsommets,jt)%u(lk)  - phys(1,jt)%u(lk) ) &
                  & *e%grad_at_dofs(1,1,id) ) !grad(1,:))
             gradient(jt)%grad(2,id)=SUM( (phys(1:e%nsommets,jt)%u(lk) - phys(1,jt)%u(lk) )*&
                  & e%grad_at_dofs(2,1,id) ) !grad(2,:))



          ENDDO

       ENDDO
    ENDDO



    !""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    !""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    !""""""""""""""" MOOD DETECTION CRITERIA """"""""""""""""""""""""""""""""""""
    !""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    LoopNaN:     DO jt=1, Mesh%nt
       DO l=1, mesh%e(jt)%nsommets
          is=mesh%e(jt)%nu(l)
          DO k=1,n_vars
             IF (phys_d(l,jt)%u(k)==-HUGE(1.0_dp)) THEN !isnan( phys_d(is)%u(k) ) ) THEN
                diag(is)= MAX(NAN_criteria,diag(is)) !worst case scenario
                diag2(is)=diag2(is)+diag(is)

                CYCLE LoopNaN
             ENDIF
          ENDDO
       ENDDO
    ENDDO LoopNaN


    LoopPad: DO jt=1, mesh%nt
       e=mesh%e(jt)
       LoopPad2:  DO  l=1, e%nsommets
          is=e%nu(l)

          IF(diag(is)==NAN_criteria) CYCLE loopPad2

          ! PAD test: for fluids, assumes: 1-> density, n_vars-> pressure
          !.................................................................
          ! here test if the min of density or pressure for the debug variables are >0

          !IF ((dvmin(is)%u(1).LT.0._dp) .OR. (dvmin(is)%u(n_vars).LT.0._dp )) THEN
          IF ((phys_d(l,jt)%u(1) .LT.0._dp) .OR. (phys_d(l,jt)%u(n_vars) .LT.0._dp) ) THEN
             diag(is)=MAX(PAD_criteria,diag(is))
             diag2(is)=diag2(is)+diag(is)

          ENDIF
       ENDDO LoopPad2
    ENDDO LoopPad


    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       mesh%e(jt)%diag =MAX(mesh%e(jt)%diag , MAXVAL(diag (e%nu)) )
       Mesh%e(jt)%diag2=MAX(mesh%e(jt)%diag2, MAXVAL(diag2(e%nu)) )
    ENDDO

    DO lkk=1, n_list
       lk=list(lkk)
       LoopPlateau: DO jt=1, Mesh%nt
          e=Mesh%e(jt)
          !          if (e%diag.GE.Pad_criteria) print*, mod_name, jt
          IF (e%diag.GE.Pad_criteria) CYCLE loopPlateau


          ! here test if the solution is flat
          ! plateau, compute a relative coefficient. Better to have non dimensional quantities
          ! plateau detection. Done on the old variables

          IF ( ABS(umax(jt)%u(lk)-umin(jt)%u(lk)) .LT. SQRT(e%volume)**3) THEN
             Mesh%e(jt)%diag=MAX(Plateau,Mesh%e(jt)%diag)
             Mesh%e(jt)%diag2=Mesh%e(jt)%diag2+Mesh%e(jt)%diag
             DO l=1, e%nsommets
                diag(e%nu(l))=e%diag
                diag2(e%nu(l))=e%diag2
             ENDDO

          ENDIF
       ENDDO LoopPlateau
    ENDDO

    DO lkk=1, n_list
       lk=list(lkk)

       LoopDMP: DO jt=1, Mesh%nt
          e=mesh%e(jt)
          IF (e%diag.GE.Plateau) CYCLE loopDMP
          ! Extrema detection: compare old and new variables

          eps=coeff*MAX(( umax(jt)%u(lk)-umin(jt)%u(lk))*eps1, eps2*( u_max%u(lk)-u_min%u(lk)) )!Comparison with local min/max and global min/max
          IF (MINVAL(phys_d(1:e%nsommets,jt)%u(lk)).LT.umin(jt)%u(lk)-eps&
               &.OR.&
               & MAXVAL(phys_d(1:e%nsommets,jt)%u(lk)).GT.umax(jt)%u(lk)+eps) THEN
             !          IF (dumin(jt)%u(lk).LT.umin(jt)%u(lk)-eps.OR.dumax(jt)%u(lk).GT.umax(jt)%u(lk)+eps) THEN




             nbre=SUM( Mesh%vois(e%nu)%nbre )-e%nvertex ! we want the elements that are not jt !)
             IF (ALLOCATED(points)) DEALLOCATE(points)

             ALLOCATE( points(n_dim,nbre))
             IF (ALLOCATED(x)) DEALLOCATE(x)
             ALLOCATE(x(n_dim,e%nvertex))
             compte=0
             DO l=1,e%nvertex
                DO p=1, Mesh%vois(e%nu(l))%nbre
                   jtt=Mesh%vois(e%nu(l))%nvois(p)
                   IF (jtt.NE.jt) THEN
                      compte=compte+1

                      points(:,compte)=gradient_d(jtt)%grad(:,Mesh%vois(e%nu(l))%loc(p) )

                   ENDIF
                ENDDO
             ENDDO

             IF (compte.NE.nbre) THEN
                PRINT*, mod_name,"pb nbre", jt,compte,nbre
             ENDIF
             DO  l=1, e%nvertex ! we look to see if the gradients in jt are in the convex hull of all the neighbords
                x(:,l)=gradient_d(jt)%grad(:,l)
             ENDDO

             test_ouille=test_quick(nbre,points,x,e%nvertex,jt, lk)!
             IF (test_ouille) THEN ! x is in the convex hull, so all is fine
                mesh%e(jt)%diag=MAX(  DMP_nou2, Mesh%e(jt)%diag )
                Mesh%e(jt)%diag2=Mesh%e(jt)%diag2+ DMP_nou2

                CYCLE LoopDMP
             ELSE


                Mesh%e(jt)%diag=MAX(DMP_B1,Mesh%e(jt)%diag)
                Mesh%e(jt)%diag2=Mesh%e(jt)%diag2+DMP_B1


                CYCLE LoopDMP
             ENDIF

          ELSE ! rien

             Mesh%e(jt)%diag=MAX(DMP_u2_2,Mesh%e(jt)%diag)
             Mesh%e(jt)%diag2=Mesh%e(jt)%diag2+DMP_u2_2

          ENDIF




       ENDDO LoopDMP


    ENDDO !LoopDMP

    DEALLOCATE(phys,phys_d)



    !-----------------------------------------------------------------------------------------------------------------
    !-----------------------------------------------------------------------------------------------------------------
    !-----------------------------------------------------------------------------------------------------------------
    !---------------- Update of the Indicators that allow the scheme switch ---------------------------
#if (1==0)
    ! worst case scenario on the boundary.
    DO jt=1, Mesh%Nsegfr
       jtt=mesh%fr(jt)%jt1
       mesh%e(jtt)%diag=DMP_B1
    ENDDO
#endif      
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       SELECT CASE(Mesh%e(jt)%diag)
       CASE(RIEN) !do nothing
          mesh%e(jt)%flag=.FALSE.
!          Mesh%e(jt)%type_flux=DATA%ischema
       CASE(plateau) ! do nothing
          mesh%e(jt)%flag=.FALSE.
!          Mesh%e(jt)%type_flux=DATA%ischema
       CASE(DMP_B1) ! back to more diffusive
          mesh%e(jt)%flag=.FALSE.
          Mesh%e(jt)%type_flux= flux_mood!flux_mood
       CASE(DMP_u2_2) ! not an extrema
          mesh%e(jt)%flag=.FALSE.
!          Mesh%e(jt)%type_flux=DATA%ischema!flux_mood

       CASE(DMP_nou2) ! true extrema: keep it, do noting
          !          we do nothing   !!!!
          mesh%e(jt)%flag=.FALSE.
!          Mesh%e(jt)%type_flux=DATA%ischema!flux_mood

          ! In this case we switch to a more diffusive scheme
       CASE(PAD_criteria) ! positivity problem
          mesh%e(jt)%flag=.TRUE.
          Mesh%e(jt)%type_flux= theflux2
       CASE(NAN_criteria) ! NaN
          mesh%e(jt)%flag=.TRUE.
          Mesh%e(jt)%type_flux=theflux2
          ! In this case we take a first order monotone scheme
       CASE default
          PRINT*, "in test, bad behavior, jt=", jt
          STOP
       END SELECT
       !
    ENDDO


  END SUBROUTINE test_12




  FUNCTION mymaxval(u) RESULT(x)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "mymaxval"
    REAL(dp), DIMENSION(:), INTENT(in):: u
    REAL(dp):: x
    INTEGER:: i
    x=-HUGE(1.0_dp)
    DO i=1, SIZE(u)
       IF( isnan(u(i)) ) THEN
          x=HUGE(1.0_dp)
          RETURN
       ENDIF
       x=MAX(x, u(i) )
    ENDDO
  END FUNCTION mymaxval

  SUBROUTINE mymi(u,x,zob)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "mymi"
    REAL(dp), DIMENSION(:), INTENT(in):: u
    LOGICAL, INTENT(out):: zob
    REAL(dp), INTENT(out):: x
    INTEGER:: i
    zob=.FALSE.
    x=-HUGE(1.0_dp)
    DO i=1, SIZE(u)
       IF( isnan(u(i)) ) THEN
          x=HUGE(1.0_dp)
          PRINT*, mod_name,u(i),x
          zob=.TRUE.
          RETURN
       ENDIF
       x=MAX(x, u(i) )
    ENDDO
  END SUBROUTINE mymi

  SUBROUTINE myma(u,x,zob)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "myma"
    REAL(dp), DIMENSION(:), INTENT(in):: u
    LOGICAL, INTENT(out):: zob
    REAL(dp), INTENT(out):: x
    INTEGER:: i
    zob=.FALSE.
    x=HUGE(1.0_dp)
    DO i=1, SIZE(u)
       IF( isnan(u(i)) ) THEN
          x=-HUGE(1.0_dp)
          PRINT*, mod_name,u(i),x
          zob=.TRUE.
          RETURN
       ENDIF
       x=MIN(x, u(i) )
    ENDDO
  END SUBROUTINE myma
  FUNCTION myminval(u) RESULT(x)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "myminval"
    REAL(dp), DIMENSION(:), INTENT(in):: u
    REAL(dp):: x
    INTEGER:: i
    x=HUGE(1.0_dp)
    DO i=1, SIZE(u)
       IF( isnan(u(i)) ) THEN
          x=-HUGE(1.0_dp)
          !          print*, mod_name,u(i),x
          RETURN
       ENDIF
       x=MIN(x, u(i) )
    ENDDO
  END FUNCTION myminval

#ifdef parallel
  SUBROUTINE edge_main_update_parallel(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta, com)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "edge_update"
    INTEGER, INTENT(IN):: k
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma
    INTEGER, DIMENSION(4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,2:4), INTENT(in):: theta
    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    TYPE(variables), INTENT(inout):: Var
    REAL(DP), INTENT(in):: dt
    TYPE(element):: e, e1, e2
    TYPE(arete):: ed
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
    TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
    TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
    INTEGER:: jt1, jt2, iseg, l, lp, schema, i, j, m, maxNdofs, ierror, currentRequest, statInfo
    INTEGER, DIMENSION(mpi_status_size) :: status
    REAL(dp), DIMENSION(:), ALLOCATABLE :: sendUaVals, sendUpVals
    REAL(dp), DIMENSION(:,:,:,:), ALLOCATABLE :: receiveUaVals, receiveUpVals
    REAL(dp), DIMENSION(:,:,:), ALLOCATABLE :: testUa
    LOGICAL :: onBoundary
    INTEGER, DIMENSION(:), ALLOCATABLE :: request
    TYPE(meshCom) :: Com
    ! question : nothing on Jump for Version 1 ?


    maxNdofs = MAXVAL(mesh%subMeshsNdofs)

    ALLOCATE(sendUaVals(mesh%ndofs),sendUpVals(mesh%ndofs))
    ALLOCATE(receiveUaVals(com%ntasks,DATA%iordret,4,maxNdofs))
    ALLOCATE(receiveUpVals(com%ntasks,DATA%iordret,4,maxNdofs))
    ALLOCATE(testUa(DATA%iordret,4,maxNdofs))
    ALLOCATE(request(SIZE(mesh%neighbourIds)*DATA%iordret*4*2))

    receiveUaVals = -1
    receiveUpVals = -1

    DO l = 1, DATA%iordret
       DO m = 1,4
          DO jt1 = 1,mesh%ndofs
             sendUpVals(jt1) = Var%up(l-1,jt1)%u(m)
             sendUaVals(jt1) = Var%ua(l-1,jt1)%u(m)
          ENDDO
          receiveUpVals(com%me+1,l,m,:) = sendUpVals
          receiveUaVals(com%me+1,l,m,:) = sendUaVals
       ENDDO
    ENDDO


    DO i = 1,SIZE(mesh%transferPlan,2)
       IF (com%me+1 == mesh%transferPlan(1,i)) THEN
          DO l = 1, DATA%iordret
             DO m = 1,4
                DO jt1 = 1,mesh%ndofs
                   sendUpVals(jt1) = Var%up(l-1,jt1)%u(m)
                   sendUaVals(jt1) = Var%ua(l-1,jt1)%u(m)
                ENDDO

                CALL mpi_send(sendUaVals,mesh%ndofs,mpi_real8,mesh%transferPlan(2,i)-1,&
                     &l*100+m*10+1,mpi_comm_world,ierror)

                CALL mpi_send(sendUpVals,mesh%ndofs,mpi_real8,mesh%transferPlan(2,i)-1,&
                     &l*100+m*10+2,mpi_comm_world,ierror)
             ENDDO
          ENDDO
       ENDIF
       IF (com%me+1 == mesh%transferPlan(2,i)) THEN
          DO l = 1, DATA%iordret
             DO m = 1,4
                CALL mpi_recv(receiveUaVals(mesh%transferPlan(1,i),l,m,:), &
                     & maxNdofs,mpi_real8,mesh%transferPlan(1,i)-1,& 
                     & l*100+m*10+1,mpi_comm_world,status,StatInfo)
                CALL mpi_recv(receiveUpVals(mesh%transferPlan(1,i),l,m,:),&
                     & maxNdofs,mpi_real8,mesh%transferPlan(1,i)-1,&
                     & l*100+m*10+2,mpi_comm_world,status,StatInfo)
             ENDDO
          ENDDO
       ENDIF
       CALL mpi_barrier(mpi_comm_world,ierror)
    ENDDO

    DO iseg=1, Mesh%nsegmt
       ed=Mesh%edge(iseg)

       IF (ed%bord)CYCLE

       e1 = mesh%boundaryE(ed%touchingElements(1))
       e2 = mesh%boundaryE(ed%touchingElements(2))
       ALLOCATE(u1(e1%nsommets), u2(e2%nsommets), resJ(e1%nsommets,2) )
       ALLOCATE(ua1(0:DATA%iordret-1,e1%nsommets),&
            & ua2(0:DATA%iordret-1,e2%nsommets),&
            & up1(0:DATA%iordret-1,e1%nsommets),&
            & up2(0:DATA%iordret-1,e2%nsommets))

       lp=n_theta(DATA%iordret)-1

       DO l=0,DATA%iordret-1
          DO j = 1,e1%nsommets
             DO i = 1,4
                ua1(l,j)%u(i) = receiveUaVals(e1%subMeshId,l+1,i,e1%localNu(j))
                up1(l,j)%u(i) = receiveUpVals(e1%subMeshId,l+1,i,e1%localNu(j))
             ENDDO
          ENDDO
          DO j = 1,e2%nsommets
             DO i = 1,4
                ua2(l,j)%u(i) = receiveUaVals(e2%subMeshId,l+1,i,e2%localNu(j))
                up2(l,j)%u(i) = receiveUpVals(e2%subMeshId,l+1,i,e2%localNu(j))
             ENDDO
          ENDDO
       ENDDO

       u1=0.0_dp
       DO l=1,e1%nsommets
          lp=k-1!n_theta(DATA%iordret)-1
          u1(l) = up1(lp,l)
       ENDDO

       u2=0.0_dp

       DO l=1,e2%nsommets
          lp=k-1!n_theta(DATA%iordret)-1
          u2(l) = up2(lp,l)
       ENDDO
       ! jump of grad u
       CALL jump( ed, e1, e2, u1, u2, resJ, DATA%theta_jump, DATA%theta_jump2)

       IF (e1%subMeshId == com%me + 1) THEN
          DO l=1, e1%nsommets
             Var%un(e1%nu(l))=Var%un(e1%nu(l))+resJ(l,1) * dt
          ENDDO
       ENDIF
       IF (e2%subMeshId == com%me + 1) THEN
          DO l=1, e2%nsommets
             Var%un(e2%nu(l))=Var%un(e2%nu(l))+resJ(l,2) * dt
          ENDDO
       ENDIF
       DEALLOCATE(resJ,u1,u2,ua1,ua2,up1,up2)
    ENDDO
  END SUBROUTINE edge_main_update_parallel
#endif

  SUBROUTINE edge_main_update(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "edge_update"
    INTEGER, INTENT(IN):: k
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

    INTEGER, DIMENSION(4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,2:4), INTENT(in):: theta

    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    TYPE(variables), INTENT(inout):: Var
    REAL(DP), INTENT(in):: dt
    TYPE(element):: e, e1, e2
    TYPE(arete):: ed
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
    TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE:: resJ1, ResJ2
    INTEGER:: jt1, jt2, iseg, l, lp, schema
    ! question : nothing on Jump for Version 1 ?

    DO iseg=1, Mesh%nsegmt

       ed=Mesh%edge(iseg)

       IF (ed%bord) THEN
          CYCLE
       ENDIF

       jt1 = ed%jt1

       e1=Mesh%e(jt1)
       jt2 = ed%jt2
       e2=Mesh%e(jt2)


       ALLOCATE(u1(e1%nsommets), u2(e2%nsommets), resJ1(e1%nsommets), ResJ2(e2%nsommets) )
       ALLOCATE(ua1(0:DATA%iordret-1,e1%nsommets),&
            & ua2(0:DATA%iordret-1,e2%nsommets),&
            & up1(0:DATA%iordret-1,e1%nsommets),&
            & up2(0:DATA%iordret-1,e2%nsommets))
       lp=n_theta(DATA%iordret)-1

       DO l=0,DATA%iordret-1
          ua1(l,:)=Var%ua(l,e1%nu(:))
          up1(l,:)=Var%up(l,e1%nu(:))
          ua2(l,:)=Var%ua(l,e2%nu(:))
          up2(l,:)=Var%up(l,e2%nu(:))
       ENDDO


       u1=0.0_dp
       DO l=1,e1%nsommets
#if (1==0)
          DO lp=0, n_theta(DATA%iordret)-1
             u1(l)=u1(l)+theta(lp,k-1,DATA%iordret)* up1(lp,l)
          ENDDO
#else
          lp=k-1!n_theta(DATA%iordret)-1
          u1(l)= up1(lp,l)
#endif
       ENDDO
       u2=0.0_dp
       DO l=1,e2%nsommets
#if (1==0)
          DO lp=0, n_theta(DATA%iordret)-1
             u2(l)=u2(l)+theta(lp,k-1,DATA%iordret)* up2(lp,l)
          ENDDO
#else


          lp=k-1!n_theta(DATA%iordret)-1
          u2(l)= up2(lp,l)
#endif   
       ENDDO

       ! jump of grad u

       CALL jump( ed, e1, e2, u1, u2, resJ1, ResJ2, DATA%theta_jump, DATA%theta_jump2)

       IF (e1%type_flux==4.OR.e1%type_flux==5.or.e1%type_flux==8) THEN
          DO l=1, e1%nsommets


             Var%un(l,jt1)=Var%un(l,jt1)+resJ1(l) * dt


          ENDDO
       ENDIF
       IF (e2%type_flux==4.OR.e2%type_flux==5.or.e2%type_flux==8) THEN
          DO l=1, e2%nsommets
             Var%un(l,jt2)=Var%un(l,jt2)+resJ2(l) * dt
          ENDDO
       ENDIF
       DEALLOCATE(resJ1, ResJ2,u1,u2,ua1,ua2,up1,up2)
    ENDDO
  END SUBROUTINE edge_main_update



END MODULE update
