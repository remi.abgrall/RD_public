!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE arete_class
  USE PRECISION

  IMPLICIT NONE

  TYPE, PUBLIC:: arete
     ! number of dofs and element type for kinematics and thermodynamics
     !1->P1
     !2->B2
     !3->P2
     INTEGER :: nsommets, itype, nvertex
     LOGICAL :: bord
     INTEGER               :: jt1, jt2  ! les deux elements de par et d'autre de l'arete.
          integer:: edge(2) ! numero des edges dans les elements jt1,et jt2
     INTEGER, DIMENSION(:,:), POINTER :: nu=>NULL()   ! nu( indice des elements, indice des voisins): on cherche a connaitre le numero local des 
     ! points communs (puisque le maillage est confome)  aux deux elements sur cette face dans chacun des elements jt1 et jt2
     REAL(DP),    DIMENSION(:,:), POINTER :: coor =>NULL() ! il s'agit des coordonnees physique des dofs (communs) sur la face (commune)
     REAL(DP)                             :: volume=0._dp  !
     REAL(DP)                            :: jump_flag=1.0_dp ! between 0 and 1 which gives the weight of the edge term
     REAL(DP),    DIMENSION(:),   POINTER :: n   =>NULL()     ! normales exterieures
     INTEGER                          :: log    ! logique
!!!!   quadrature de surface
     REAL(DP),    DIMENSION(:,:), POINTER :: quad =>NULL()   ! point de quadrature
     REAL(DP),    DIMENSION(:),   POINTER :: weight =>NULL() ! poids
     INTEGER                          :: nquad  ! nbre de points de quadrature
!!! quadrature bord (dimension -1)
     REAL(DP),    DIMENSION(:,:), POINTER :: quad_1 =>NULL()   ! point de quadrature
     REAL(DP),    DIMENSION(:),   POINTER :: weight_1 =>NULL() ! poids
     INTEGER                          :: nquad_1  ! nbre de points de quadrature
!!!
#ifdef parallel
     integer, dimension(2) :: touchingElements
#endif

   CONTAINS
     PROCEDURE, PUBLIC:: aire       =>aire_arete
     PROCEDURE, PUBLIC:: quadrature =>quadrature_arete
     PROCEDURE, PUBLIC:: normale    =>normale_arete
     FINAL:: clean_arete
  END TYPE arete

CONTAINS

  REAL(DP) FUNCTION aire_arete(e)
    CLASS(arete), INTENT(in):: e
    REAL(DP), DIMENSION(2):: a
    a= e%coor(:,2)-e%coor(:,1)

    aire_arete=SQRT(a(1)**2+ a(2)**2 )
  END FUNCTION aire_arete

  FUNCTION normale_arete(e) RESULT(n)
    CLASS(arete), INTENT(in)::e
    REAL(DP), DIMENSION(2):: n
    INTEGER:: l, k1, k2
    INTEGER, DIMENSION(3), PARAMETER:: ip1=(/2,3,1/)

    n(1)=e%coor(2,2)-e%coor(2,1)
    n(2)=e%coor(1,1)-e%coor(1,2)

  END FUNCTION normale_arete



  SUBROUTINE quadrature_arete(e)
    CLASS(arete), INTENT(inout):: e
    REAL(DP):: w,zo,xo,s
    INTEGER:: nquad



    e%nquad=3
    nquad=e%nquad
    nullify(e%quad, e%weight)
    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))

    s=SQRT(0.6_dp)
    e%quad(1,1)=0.5_dp*(1.0_dp - s)
    e%quad(2,1)=0.5_dp*(1.0_dp + s)
    e%weight(1) = 5.0_dp/18.0_dp
    e%quad(1,2)=0.5_dp*(1.0_dp + s)
    e%quad(2,2)=0.5_dp*(1.0_dp - s)
    e%weight(2) = 5.0_dp/18.0_dp
    e%quad(1,3)=0.5_dp
    e%quad(2,3)=0.5_dp
    e%weight(3)=8.0_dp/18.0_dp


  END SUBROUTINE quadrature_arete


  SUBROUTINE clean_arete(e)
    TYPE(arete), intent(inout):: e
    IF (ASSOCIATED(e%nu)) NULLIFY(e%nu)
    IF (ASSOCIATED(e%coor)) NULLIFY(e%coor)
    IF (ASSOCIATED(e%quad)) NULLIFY(e%quad)
    IF (ASSOCIATED(e%weight)) NULLIFY(e%weight)

  END SUBROUTINE clean_arete
END MODULE arete_class


