!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE element_class
  ! assumes that the element is positively oriented.
  ! In this module are listed all the tools linked to the basis functions and quadrature formulas
  ! Structure of the module:
  ! ELEMENT CLASS
  ! - aire: area of the element
  ! - normale: normal of the element
  ! - base: basis function -- here we do have Lagrangian and Bernstein polynomials
  ! - eval_function: evaluation of the solution via basis functions SUM(base(:)*(u(:)%u(l))
  ! -  eval_der: evaluation of the derivative SUM( (u(:)%u(l) *grad(1,:)) 
  ! -  eval_hess: evaluation of the second derivative via the hessian SUM( (u(:)%u(l) *grad2(1,:))
  ! - der_sec: alternative to eval_der2
  ! - gradient: corresponds to the gradient of the basis function
  ! - hessian: corresponds to the second order gradient of the basis function
  ! - quadrature:  collects all the points and weights for each typology of element
  USE PRECISION
  USE algebra
  USE arete_class
  USE variable_def
  USE overloading
  IMPLICIT NONE
  REAL(dp), DIMENSION(7,7)::&
       &AA=TRANSPOSE(RESHAPE ( (/-1._dp,0._dp,0._dp,4._dp,0._dp,0._dp,0._dp&
       &,0._dp,0._dp,-1._dp,0._dp,0._dp,0._dp,4._dp&
       &,2._dp,0._dp,0._dp,-4._dp,0._dp,0._dp,0._dp&
       &, 1._dp,-3._dp,-3._dp,-12._dp,4._dp,4._dp,4._dp&
       &,0._dp,0._dp,2._dp,0._dp,0._dp,0._dp,-4._dp&
       &,-4._dp,2._dp,4._dp,12._dp,0._dp,-4._dp,-8._dp&
       &,2._dp,2._dp,-2._dp,0._dp,-4._dp,0._dp,4._dp/),(/7,7/) ))
  TYPE, PUBLIC:: element
     INTEGER:: type_flux=-10, diag=-10, diag2=-10
     ! number of dofs and element type
     !1->P1
     !2->B2
     !3->P2
     !4->P3
     INTEGER                                  :: nsommets, itype, nvertex, nsegmts, nelem
     TYPE(arete), DIMENSION(:),   POINTER     :: segm  =>Null()      ! segments of the element
     INTEGER,     DIMENSION(:),   POINTER     :: nsegm =>Null()      ! global numbers of the element segments
     INTEGER,     DIMENSION(:),   POINTER     :: neigh=>Null()       ! global numbers of the neighbouring elements
     REAL(DP),        DIMENSION(:,:), POINTER :: coor =>Null()       ! coordinates of the degrees of freedom
     REAL(DP),        DIMENSION(:,:), POINTER :: coorL =>Null()      ! barycentric coordinates of DOFs
     INTEGER,     DIMENSION(:),   POINTER     :: nu  =>Null()        ! local connectivity table

     REAL(DP)                                 :: volume      ! volume
     REAL(dp), DIMENSION(:,:), POINTER:: y, yy  ! for kinetic momentum preservation
     REAL(DP),        DIMENSION(:),   POINTER :: c_el =>Null()       ! center of the element
     !     REAL(DP),        DIMENSION(:,:), POINTER :: n_ed        ! outward unit normals to edges
     REAL(DP),        DIMENSION(:,:), POINTER :: n   =>Null()        ! normals for RD (inward)
     INTEGER                                  :: log         ! logique element
     REAL(DP), DIMENSION(:,:), POINTER        :: base_at_dofs      ! matrix of values of the basis functions at the physical dofs
     REAL(DP), DIMENSION(:,:), POINTER        :: inv_base_at_dofs  ! inverse of base_at_dofs
     REAL(dp), DIMENSION(:,:,:),POINTER   :: grad_at_dofs    =>Null()   ! the 
     REAL(dp), DIMENSION(:,:,:),POINTER   :: grad_at_quad   =>Null()   ! the 
     REAL(dp), DIMENSION(:,:),POINTER     :: base_at_quad   =>Null()   ! the 
     REAL(dp), DIMENSION(:,:,:),POINTER   :: base_at_quad_edge =>Null() ! the 
     !  : this for boundary conditions
!!!!   quadrature de surface
     REAL(DP),        DIMENSION(:,:), POINTER :: quad    =>Null()    ! point de quadrature
     REAL(DP),        DIMENSION(:),   POINTER :: weight  =>Null()    ! poids
     INTEGER                                  :: nquad       ! nbre de points de quadrature
!!! quadrature bord (dimension -1)
     REAL(DP),      DIMENSION(:,:,:), POINTER :: quad_edge =>Null()  ! quadrature points at the edges
     REAL(DP),      DIMENSION(:,:),   POINTER :: weight_edge =>Null()! weights
     INTEGER                                  :: nquad_edge  ! number of quadrature points
!!!
     INTEGER,     DIMENSION(:,:), POINTER     :: edge     =>Null()   ! connectivity table of the element edges

     REAL(DP),        DIMENSION(:,:), POINTER :: MatLoc    =>Null()  ! local mass matrix
     REAL(DP),        DIMENSION(:,:), POINTER :: MatLocInv =>Null()  ! inverse of the local mass matrix
     LOGICAL                                  :: flag=.TRUE. ! true: on prend l'element complet, false: on subdivise
     REAL(DP),        DIMENSION(:),   POINTER :: poids=>Null()
     REAL(DP),        DIMENSION(:),   POINTER :: MatLoc_lumped  =>Null()    ! lumped local mass matrix
REAL(DP)::h
     LOGICAL                                  :: isBoundary  ! true if element is at the boundary of the domain

     REAL(dp), DIMENSION(:,:,:), POINTER      :: coeff =>Null() ! \int_K phi_i*nabla phi_j for Galerkin
     REAL(DP), DIMENSION(:,:), POINTER        :: masse=>Null() !  the local mass matrix
     real(dp)                                 :: l=1._dp  ! B limiter. modified in scheme/scheme8 (bidouille/lim)
#ifdef parallel
     INTEGER :: id
     INTEGER :: subMeshId
     INTEGER, DIMENSION(:), ALLOCATABLE :: localNu
     LOGICAL :: isOnBoundary
#endif

   CONTAINS
     PROCEDURE, PUBLIC:: next         =>next_edge
     PROCEDURE, PUBLIC:: aire         =>aire_element
     PROCEDURE, PUBLIC:: base         =>base_element
     PROCEDURE, PUBLIC:: gradient     =>gradient_element
     PROCEDURE, PUBLIC:: hessian      =>hessian_element
     PROCEDURE, PUBLIC:: center_elem  =>center_element
     !     PROCEDURE, PUBLIC:: normale_edge =>normale_edge
     PROCEDURE, PUBLIC:: normale      =>normale_element
     PROCEDURE, PUBLIC:: quadrature   =>quadrature_element
     PROCEDURE, PUBLIC:: eval_func    =>interp
     PROCEDURE, PUBLIC:: eval_der     =>dinterp
     PROCEDURE, PUBLIC:: av           =>average
     PROCEDURE, PUBLIC:: matrix       =>matrix_element
     PROCEDURE, PUBLIC:: getCoorL     =>getBarycentricCoor
     PROCEDURE, PUBLIC:: phys2bary    =>convertPhys2BarycCoor
     PROCEDURE, PUBLIC:: base_ref     =>base_ref_element
     PROCEDURE, PUBLIC:: grad_ref     =>grad_ref_element
     PROCEDURE, PUBLIC:: eval_hess    =>eval_hess_element
     PROCEDURE, PUBLIC:: eval_coeff   =>eval_coeff_element
     PROCEDURE, PUBLIC:: iso          =>iso_element
     PROCEDURE, PUBLIC:: d_iso          =>d_iso_element
     PROCEDURE, PUBLIC:: basegradATquad =>basegradATquad_element
     PROCEDURE, PUBLIC:: baseATquad_edge=>baseATquad_edge_element
     FINAL:: deallocate_element
  END TYPE element



CONTAINS

  !==================================================================
  ! Sequence of edges
  !==================================================================

  FUNCTION next_edge(e,l) ! center of an element
    CHARACTER(LEN = *), PARAMETER :: mod_name = "next_edge"
    CLASS(element), INTENT(in) :: e
    INTEGER,        INTENT(in) :: l
    INTEGER :: next_edge
    IF (l .NE. e%nvertex) THEN
       next_edge = l+1
    ELSE
       next_edge = 1
    END IF
  END FUNCTION next_edge

  !==================================================================
  ! Element area
  !==================================================================

  REAL(DP) FUNCTION aire_element(e) ! area of an element
    CHARACTER(LEN = *), PARAMETER :: mod_name = "aire"
    CLASS(element), INTENT(in) :: e
    REAL(DP), DIMENSION(2)         :: a,b
    SELECT CASE (e%nvertex)
    CASE(3)   ! triangle
       a= e%coor(:,2)-e%coor(:,1)
       b= e%coor(:,3)-e%coor(:,1)
       aire_element = 0.5_dp* ( a(1)*b(2)-a(2)*b(1)) ! ABS
       IF (aire_element.LE.0._dp) THEN
          PRINT*, mod_name, "negative area, orientation problem"
          STOP
       ENDIF
    CASE(4)   ! quadrangle
       a= e%coor(:,2)-e%coor(:,1)
       b= e%coor(:,4)-e%coor(:,1)
       aire_element = 0.5_dp* ( a(1)*b(2)-a(2)*b(1)) !ABS
       IF (aire_element.LE.0._dp) THEN
          PRINT*, mod_name, "negative area quad 1, orientation problem"
          STOP
       ENDIF
       b= e%coor(:,2)-e%coor(:,3)
       a= e%coor(:,4)-e%coor(:,3)
       aire_element = aire_element + 0.5_dp* ( a(1)*b(2)-a(2)*b(1)) !ABS
       IF (( a(1)*b(2)-a(2)*b(1)).LE.0._dp) THEN
          PRINT*, mod_name, "negative area quad 2, orientation problem"
          STOP
       ENDIF

    END SELECT
  END FUNCTION aire_element

  !==================================================================
  ! Center of the element
  !==================================================================

  FUNCTION center_element(e) ! center of an element
    CHARACTER(LEN = *), PARAMETER :: mod_name = "center_element"
    CLASS(element), INTENT(in) :: e
    REAL(DP), DIMENSION(2)         :: center_element
    center_element = SUM(e%coor(:,1:e%nvertex),DIM=2)/e%nvertex
  END FUNCTION center_element

!!$  !==================================================================
!!$  ! Outward unit normal to the edge
!!$  !==================================================================
!!$
  !  FUNCTION normale_edge(e) RESULT(n)
  !    CHARACTER(LEN = *), PARAMETER :: mod_name = "normale_edge"
  !    CLASS(element), INTENT(in)      :: e
  !    REAL(DP),    DIMENSION(2,e%nvertex) :: n
  !    INTEGER                         :: l, k1, k2
  !    DO l = 1,e%nvertex
  !       k1 = l
  !       k2 = e%next(l)
  !       n(1,l) = e%coor(2,k2)-e%coor(2,k1)
  !       n(2,l) = e%coor(1,k1)-e%coor(1,k2)
  !       n(:,l) = n(:,l)/SQRT(SUM(n(:,l)**2))
  !    ENDDO
  !  END FUNCTION normale_edge

  !==================================================================
  ! Inward normal at the vertex pointing towards the opposite edge
  !==================================================================

  FUNCTION normale_element(e) RESULT(n)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "normale_element"
    ! compute outward normal opposite of vertex k
    ! usefull for triangles.

    CLASS(element), INTENT(in)          :: e
    REAL(DP),    DIMENSION(2,e%nvertex) :: n
    INTEGER                             :: l, k1, k2
    DO l = 1,e%nvertex
       k1 = e%next(l)
       k2 = e%next(k1)
       n(1,l) = e%coor(2,k2)-e%coor(2,k1)
       n(2,l) = e%coor(1,k1)-e%coor(1,k2)
    ENDDO
    n=-n
  END FUNCTION normale_element


  !==================================================================
  ! Definition of basis functions
  !==================================================================
  ! Linear Bernstein polynomials in 1D
  REAL(DP) FUNCTION B1(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "B1"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       B1 = (1._dp-t)
    CASE(2)
       B1 = t
    END SELECT
  END FUNCTION B1
  REAL(DP) FUNCTION dB1(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "dB1"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       dB1 = -1._dp
    CASE(2)
       dB1 = 1._dp
    END SELECT
  END FUNCTION dB1
  ! Quadratic Bernstein polynomials in 1D
  REAL(DP) FUNCTION B2(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "B2"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       B2 = (1._dp-t)**2
    CASE(2)
       B2 = t**2
    CASE(3)
       B2 = 2._dp*t*(1._dp-t)
    END SELECT
  END FUNCTION B2

  ! Derivatives of quadratic Bernstein polynomials in 1D wrt t
  REAL(DP) FUNCTION dB2(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "dB2"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       dB2 = 2._dp*t-2._dp
    CASE(2)
       dB2 = 2._dp*t
    CASE(3)
       dB2 = -4._dp*t+2._dp
    END SELECT
  END FUNCTION dB2

  ! cubic Bernstein polynomials in 1D
  REAL(DP) FUNCTION B3(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "B3"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       B3 = (1._dp-t)**3
    CASE(2)
       B3 = t**3
    CASE(3)
       B3 = 3._dp*t*(1._dp-t)**2
    CASE(4)
       B3 = 3._dp*t**2*(1._dp-t)
    END SELECT
  END FUNCTION B3

  ! Derivatives of cubic Bernstein polynomials in 1D wrt t
  REAL(DP) FUNCTION dB3(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "dB3"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       dB3 = -3._dp*(1._dp-t)**2
    CASE(2)
       dB3 = 3._dp*t**2
    CASE(3)
       dB3 = 3._dp*(1._dp-t)**2 + 6._dp*t*(t-1._dp)
    CASE(4)
       dB3 = 6._dp*t*(1._dp-t) - 3._dp*t**2
    END SELECT
  END FUNCTION dB3

  ! Quadratic Lagrange polynomials in 1D
  REAL(DP) FUNCTION P2(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "P2"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       P2 = (1._dp-t)*(1._dp-2._dp*t)
    CASE(2)
       P2 = t*(2._dp*t-1._dp)
    CASE(3)
       P2 = 4._dp*t*(1._dp-t)
    END SELECT
  END FUNCTION P2

  ! Derivatives of quadratic Lagrange polynomials in 1D wrt t
  REAL(DP) FUNCTION dP2(k,t)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "dP2"
    INTEGER, INTENT(in) :: k
    REAL(DP),    INTENT(in) :: t
    SELECT CASE(k)
    CASE(1)
       dP2 = 4._dp*t-3._dp
    CASE(2)
       dP2 = 4._dp*t-1._dp
    CASE(3)
       dP2 = -8._dp*t+4._dp
    END SELECT
  END FUNCTION dP2

  REAL(DP) FUNCTION base_element(e,k,x)   ! basis functions
    CHARACTER(LEN = *),          PARAMETER :: mod_name = "base_element"
    CLASS(element),             INTENT(in) :: e ! calling element
    INTEGER,                    INTENT(in) :: k ! index of basis function
    REAL(DP), DIMENSION(:    ), INTENT(in) :: x ! barycentric coordinates

    SELECT CASE(e%nvertex)

    CASE(3)

       SELECT CASE(e%itype)
       CASE(0) ! P0
          SELECT CASE(k)
          CASE(1)
             base_element = 1.0_dp
          CASE default
             PRINT*, "P0, numero base ", k
             STOP
          END SELECT

       CASE(1) ! P1
          SELECT CASE(k)
          CASE(1)
             base_element=x(1)
          CASE(2)
             base_element=x(2)
          CASE(3)
             base_element=x(3)
          CASE default
             PRINT*, "P1, numero base ", k
             STOP
          END SELECT

       CASE(2) ! B2 Bezier
          SELECT CASE(k)
          CASE(1,2,3)
             base_element=x(k)*x(k)
          CASE(4)
             base_element=2._dp*x(1)*x(2)
          CASE(5)
             base_element=2._dp*x(2)*x(3)
          CASE(6)
             base_element=2._dp*x(3)*x(1)
          CASE default
             PRINT*, "B2, numero base ", k
             STOP
          END SELECT

       CASE(3)! P2
          SELECT CASE(k)
          CASE(1)
             base_element=(2._dp*x(1)-1._dp)*x(1)
          CASE(2)
             base_element=(2._dp*x(2)-1._dp)*x(2)
          CASE(3)
             base_element=(2._dp*x(3)-1._dp)*x(3)
          CASE(4)
             base_element=4._dp*x(1)*x(2)
          CASE(5)
             base_element=4._dp*x(2)*x(3)
          CASE(6)
             base_element=4._dp*x(3)*x(1)
          CASE default
             PRINT*, "P2, numero base ", k
             STOP
          END SELECT

       CASE(4) ! P3
          SELECT CASE(k)
          CASE(1)
             base_element = 0.5_dp*x(1)*(3.0_dp*x(1)-1.0_dp)*(3.0_dp*x(1)-2.0_dp)
          CASE(2)
             base_element = 0.5_dp*x(2)*(3.0_dp*x(2)-1.0_dp)*(3.0_dp*x(2)-2.0_dp)
          CASE(3)
             base_element = 0.5_dp*x(3)*(3.0_dp*x(3)-1.0_dp)*(3.0_dp*x(3)-2.0_dp)
          CASE(4)
             base_element = 4.5_dp*x(1)*x(2)*(3.0_dp*x(1)-1.0_dp)
          CASE(5)
             base_element = 4.5_dp*x(1)*x(2)*(3.0_dp*x(2)-1.0_dp)
          CASE(6)
             base_element = 4.5_dp*x(2)*x(3)*(3.0_dp*x(2)-1.0_dp)
          CASE(7)
             base_element = 4.5_dp*x(2)*x(3)*(3.0_dp*x(3)-1.0_dp)
          CASE(8)
             base_element = 4.5_dp*x(3)*x(1)*(3.0_dp*x(3)-1.0_dp)
          CASE(9)
             base_element = 4.5_dp*x(3)*x(1)*(3.0_dp*x(1)-1.0_dp)
          CASE(10)
             base_element = 27.0_dp*x(1)*x(2)*x(3)
          CASE default
             PRINT*, "P3, numero base ", k
             STOP
          END SELECT
       CASE(5) ! B3

          SELECT CASE(k)
          CASE(1,2,3)
             base_element = x(k)*x(k)*x(k)
          CASE(4)
             base_element = 3.0_dp*x(1)*x(1)*x(2)
          CASE(5)
             base_element = 3.0_dp*x(1)*x(2)*x(2)
          CASE(6)
             base_element = 3.0_dp*x(2)*x(2)*x(3)
          CASE(7)
             base_element = 3.0_dp*x(2)*x(3)*x(3)
          CASE(8)
             base_element = 3.0_dp*x(1)*x(3)*x(3)
          CASE(9)
             base_element = 3.0_dp*x(1)*x(1)*x(3)
          CASE(10)
             base_element = 6.0_dp*x(1)*x(2)*x(3)
          CASE default
             PRINT*, "B3, numero base ", k
             STOP
          END SELECT

       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

    CASE(4) !quadrangle

       SELECT CASE(e%itype)
       CASE(0) ! P0
          SELECT CASE(k)
          CASE(1)
             base_element = 1.0_dp
          CASE default
             PRINT*, "P0, numero base ", k
             STOP
          END SELECT

       CASE(1) ! P1
          SELECT CASE(k)
          CASE(1)
             base_element=B1(1,x(1))*B1(1,x(2))!(1._dp-x(1))*(1._dp-x(2))
          CASE(2)
             base_element=B1(2,x(1))*B1(1,x(2))!x(1)*(1._dp-x(2))
          CASE(3)
             base_element=B1(2,x(1))*B1(2,x(2))!x(1)*x(2)
          CASE(4)
             base_element=B1(1,x(1))*B1(2,x(2))!(1._dp-x(1))*x(2)
          CASE default
             PRINT*, "P1, numero base ", k
             STOP
          END SELECT

       CASE(2) ! B2 Bezier
          SELECT CASE(k)
          CASE(1)
             base_element=B2(1,x(1))*B2(1,x(2))
          CASE(2)
             base_element=B2(2,x(1))*B2(1,x(2))
          CASE(3)
             base_element=B2(2,x(1))*B2(2,x(2))
          CASE(4)
             base_element=B2(1,x(1))*B2(2,x(2))
          CASE(5)
             base_element=B2(3,x(1))*B2(1,x(2))
          CASE(6)
             base_element=B2(2,x(1))*B2(3,x(2))
          CASE(7)
             base_element=B2(3,x(1))*B2(2,x(2))
          CASE(8)
             base_element=B2(1,x(1))*B2(3,x(2))
          CASE(9)
             base_element=B2(3,x(1))*B2(3,x(2))
          CASE default
             PRINT*, "B2, numero base ", k
             STOP
          END SELECT

       CASE(3) ! P2
          SELECT CASE(k)
          CASE(1)
             base_element=P2(1,x(1))*P2(1,x(2))
          CASE(2)
             base_element=P2(2,x(1))*P2(1,x(2))
          CASE(3)
             base_element=P2(2,x(1))*P2(2,x(2))
          CASE(4)
             base_element=P2(1,x(1))*P2(2,x(2))
          CASE(5)
             base_element=P2(3,x(1))*P2(1,x(2))
          CASE(6)
             base_element=P2(2,x(1))*P2(3,x(2))
          CASE(7)
             base_element=P2(3,x(1))*P2(2,x(2))
          CASE(8)
             base_element=P2(1,x(1))*P2(3,x(2))
          CASE(9)
             base_element=P2(3,x(1))*P2(3,x(2))
          CASE default
             PRINT*, "P2, numero base ", k
             STOP
          END SELECT
       CASE(5) ! B3 Bezier
!!$          SELECT CASE(k)
!!$          CASE(1)
!!$             base_element=B3(1,x(1))*B3(1,x(2))
!!$          CASE(2)
!!$             base_element=B3(1,x(1))*B3(2,x(2))
!!$          CASE(3)
!!$             base_element=B3(1,x(1))*B3(3,x(2))
!!$          CASE(4)
!!$             base_element=B3(1,x(1))*B3(4,x(2))
!!$          CASE(5)
!!$             base_element=B3(2,x(1))*B3(1,x(2))
!!$          CASE(6)
!!$             base_element=B3(2,x(1))*B3(2,x(2))
!!$          CASE(7)
!!$             base_element=B3(2,x(1))*B3(3,x(2))
!!$          CASE(8)
!!$             base_element=B3(2,x(1))*B3(4,x(2))
!!$          CASE(9)
!!$             base_element=B3(3,x(1))*B3(1,x(2))
!!$          CASE(10)
!!$             base_element=B3(3,x(1))*B3(2,x(2))
!!$          CASE(11)
!!$             base_element=B3(3,x(1))*B3(3,x(2))
!!$          CASE(12)
!!$             base_element=B3(3,x(1))*B3(4,x(2))
!!$          CASE(13)
!!$             base_element=B3(4,x(1))*B3(1,x(2))
!!$          CASE(14)
!!$             base_element=B3(4,x(1))*B3(2,x(2))
!!$          CASE(15)
!!$             base_element=B3(4,x(1))*B3(3,x(2))
!!$          CASE(16)
!!$             base_element=B3(4,x(1))*B3(4,x(2))

          SELECT CASE(k)
          CASE(1) !(1,1)
             base_element=B3(1,x(1))*B3(1,x(2))
          CASE(2) !(2,1)
             base_element=B3(2,x(1))*B3(1,x(2))
          CASE(3) !(2,2)
             base_element=B3(2,x(1))*B3(2,x(2))
          CASE(4) !(1,2)
             base_element=B3(1,x(1))*B3(2,x(2))
          CASE(5)!(3,1)
             base_element=B3(3,x(1))*B3(1,x(2))
          CASE(6)!(4,1)
             base_element=B3(4,x(1))*B3(1,x(2))
          CASE(7)!(3,4)
             base_element=B3(2,x(1))*B3(4,x(2))
          CASE(8)!(3,3)
             base_element=B3(2,x(1))*B3(3,x(2))
          CASE(9)!(4,2)
             base_element=B3(4,x(1))*B3(2,x(2))
          CASE(10)!(3,2)
             base_element=B3(3,x(1))*B3(2,x(2))
          CASE(11)!(1,4)
             base_element=B3(1,x(1))*B3(4,x(2))
          CASE(12)!(1,3)
             base_element=B3(1,x(1))*B3(3,x(2))
          CASE(13)!(3,3)
             base_element=B3(3,x(1))*B3(3,x(2))
          CASE(14)!(4,3)
             base_element=B3(4,x(1))*B3(3,x(2))
          CASE(15)!(4,4)
             base_element=B3(4,x(1))*B3(4,x(2))
          CASE(16)!(3,4)
             base_element=B3(3,x(1))*B3(4,x(2))

          CASE default
             PRINT*, "B3, numero base ", k
             STOP
          END SELECT
       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

    CASE DEFAULT
       PRINT*, mod_name
       PRINT*, ' L478, Wrong nvertex = ', e%nvertex
    END SELECT

  END FUNCTION base_element

  !==================================================================
  ! Calculation of gradients of basis functions
  !==================================================================

  ! with respect to reference coordinates
  FUNCTION gradient_element_ref(e,k,x) RESULT (grad) ! gradient in reference element
    CHARACTER(LEN = *), PARAMETER :: mod_name = "gradient_element_ref"
    CLASS(element),             INTENT(in) :: e  ! calling element
    INTEGER,                    INTENT(in) :: k  ! index of basis function
    REAL(DP), DIMENSION(:), INTENT(in)     :: x  ! barycentric coordinates
    REAL(DP), DIMENSION(n_dim)             :: grad
    REAL(DP)                               :: fx,fy,fz

    SELECT CASE(e%nvertex)

    CASE(3)

       SELECT CASE(e%itype)
       CASE(0)! P0
          SELECT CASE(k)
          CASE(1)
             fx=0.0_dp; fy=0.0_dp; fz=0.0_dp
          END SELECT

       CASE(1)! P1
          SELECT CASE(k)
          CASE(1)
             fx=1.0_dp; fy=0.0_dp; fz=0.0_dp
          CASE(2)
             fx=0.0_dp; fy=1.0_dp; fz=0.0_dp
          CASE(3)
             fx=0.0_dp; fy=0.0_dp; fz=1.0_dp
          END SELECT

       CASE(2) !B2
          SELECT CASE(k)
          CASE(1)
             fx=2._dp*x(1);fy=0._dp     ;fz=0._dp
          CASE(2)
             fx=0._dp     ;fy=2._dp*x(2);fz=0._dp
          CASE(3)
             fx=0._dp     ;fy=0._dp     ;fz=2._dp*x(3)
          CASE(4)
             fx=2._dp*x(2);fy=2._dp*x(1);fz=0._dp
          CASE(5)
             fx=0._dp     ;fy=2._dp*x(3);fz=2._dp*x(2)
          CASE(6)
             fx=2._dp*x(3);fy=0._dp     ;fz=2._dp*x(1)
          END SELECT

       CASE(3) ! P2
          SELECT CASE(k)
          CASE(1)
             fx=4._dp*x(1)-1._dp;fy=0._dp;fz=0._dp
          CASE(2)
             fx=0._dp;fy=4._dp*x(2)-1._dp;fz=0._dp
          CASE(3)
             fx=0._dp;fy=0._dp;fz=4._dp*x(3)-1._dp
          CASE(4)
             fx=4._dp*x(2);fy=4._dp*x(1);fz=0._dp
          CASE(5)
             fx=0._dp;fy=4._dp*x(3);fz=4._dp*x(2)
          CASE(6)
             fx=4._dp*x(3);fy=0._dp;fz=4._dp*x(1)
          END SELECT

       CASE(4) ! P3
          SELECT CASE(k)
          CASE(1)
             !base_element = 0.5*x(1)*(3.0*x(1)-1.0)*(3.0*x(1)-2.0)
             fx = 0.5_dp*( 27.0_dp*x(1)**2 - 18.0_dp*x(1) + 2.0_dp )
             fy = 0.0_dp
             fz = 0.0_dp
          CASE(2)
             !base_element = 0.5*x(2)*(3.0*x(2)-1.0)*(3.0*x(2)-2.0)
             fx = 0.0_dp
             fy = 0.5_dp*( 27.0_dp*x(2)**2 - 18.0_dp*x(2) + 2.0_dp )
             fz = 0.0_dp
          CASE(3)
             !base_element = 0.5*x(3)*(3.0*x(3)-1.0)*(3.0*x(3)-2.0)
             fx = 0.0_dp
             fy = 0.0_dp
             fz = 0.5_dp*( 27.0_dp*x(3)**2 - 18.0_dp*x(3) + 2.0_dp )
          CASE(4)
             !base_element = 4.5*x(1)*x(2)*(3.0*x(1)-1.0)
             fx = 4.5_dp*( 6.0_dp*x(1)*x(2) - x(2) )
             fy = 4.5_dp*( 3.0_dp*x(1)**2   - x(1) )
             fz = 0.0_dp
          CASE(5)
             !base_element = 4.5*x(1)*x(2)*(3.0*x(2)-1.0)
             fx = 4.5_dp*( 3.0_dp*x(2)**2   - x(2) )
             fy = 4.5_dp*( 6.0_dp*x(1)*x(2) - x(1) )
             fz = 0.0_dp
          CASE(6)
             !base_element = 4.5*x(2)*x(3)*(3.0*x(2)-1.0)
             fx = 0.0_dp
             fy = 4.5_dp*( 6.0_dp*x(2)*x(3) - x(3) )
             fz = 4.5_dp*( 3.0_dp*x(2)**2   - x(2) )
          CASE(7)
             !base_element = 4.5*x(2)*x(3)*(3.0*x(3)-1.0)
             fx = 0.0_dp
             fy = 4.5_dp*( 3.0_dp*x(3)**2   - x(3) )
             fz = 4.5_dp*( 6.0_dp*x(2)*x(3) - x(2) )
          CASE(8)
             !base_element = 4.5*x(3)*x(1)*(3.0*x(3)-1.0)
             fx = 4.5_dp*( 3.0_dp*x(3)**2   - x(3) )
             fy = 0.0_dp
             fz = 4.5_dp*( 6.0_dp*x(1)*x(3) - x(1) )
          CASE(9)
             !base_element = 4.5*x(3)*x(1)*(3.0*x(1)-1.0)
             fx = 4.5_dp*( 6.0_dp*x(1)*x(3) - x(3) )
             fy = 0.0_dp
             fz = 4.5_dp*( 3.0_dp*x(1)**2   - x(1) )
          CASE(10)
             !base_element = 27.0*x(1)*x(2)*x(3)
             fx = 27.0_dp*x(2)*x(3)
             fy = 27.0_dp*x(1)*x(3)
             fz = 27.0_dp*x(1)*x(2)
          END SELECT
       CASE(5) ! B3
          SELECT CASE(k)
          CASE(1)
             !x(k)*x(k)*x(k)
             fx = 3.0_dp*x(1)*x(1)
             fy = 0.0_dp
             fz = 0.0_dp
          CASE(2)
             !base_element = x(k)*x(k)*x(k)
             fx = 0.0_dp
             fy = 3.0_dp*x(2)*x(2)
             fz = 0.0_dp
          CASE(3)
             !base_element =x(k)*x(k)*x(k)
             fx = 0.0_dp
             fy = 0.0_dp
             fz = 3.0_dp*x(3)*x(3)
          CASE(4)
             !base_element = 3.0*x(1)*x(1)*x(2)
             fx = 6.0_dp*x(1)*x(2)
             fy = 3.0_dp*x(1)*x(1)
             fz = 0.0_dp
          CASE(5)
             !base_element = 3.0*x(1)*x(2)*x(2)
             fx = 3.0_dp*x(2)*x(2)
             fy = 6.0_dp*x(1)*x(2)
             fz = 0.0_dp
          CASE(6)
             !base_element = 3.0*x(2)*x(2)*x(3)
             fx = 0.0_dp
             fy = 6.0_dp*x(2)*x(3)
             fz = 3.0_dp*x(2)*x(2)
          CASE(7)
             !base_element = 3.0*x(2)*x(3)*x(3)
             fx = 0.0_dp
             fy = 3.0_dp*x(3)*x(3)
             fz = 6.0_dp*x(2)*x(3)
          CASE(8)
             !base_element = 3.0*x(1)*x(3)*x(3)
             fx = 3.0_dp*x(3)*x(3)
             fy = 0.0_dp
             fz = 6.0_dp*x(1)*x(3)
          CASE(9)
             !base_element = 3.0*x(1)*x(1)*x(3)
             fx = 6.0_dp*x(1)*x(3)
             fy = 0.0_dp
             fz = 3.0_dp*x(1)*x(1)
          CASE(10)
             !base_element = 6.0*x(1)*x(2)*x(3)
             fx = 6.0_dp*x(2)*x(3)
             fy = 6.0_dp*x(1)*x(3)
             fz = 6.0_dp*x(1)*x(2)
          END SELECT
       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

    CASE(4)

       SELECT CASE(e%itype)
       CASE(0) ! P0
          SELECT CASE(k)
          CASE(1)
             !base_element = 1.0
             fx=0.0_dp; fy=0.0_dp
          CASE default
             PRINT*, "P0, numero base ", k
             STOP
          END SELECT

       CASE(1) ! P1
          SELECT CASE(k)
          CASE(1)
             !base_element=(1.-x(1))*(1.-x(2))
             fx = dB1(1,x(1))*B1 (1,x(2))!-(1._dp-x(2))
             fy = B1 (1,x(1))*dB1(1,x(2))!-(1._dp-x(1))
          CASE(2)
             !base_element=x(1)*(1.-x(2))
             fx = dB1(2,x(1))*B1(1,x(2))!1._dp-x(2);
             fy = B1(2,x(1))*dB1(1,x(2))!-x(1)
          CASE(3)
             !base_element=x(1)*x(2)
             fx = dB1(2,x(1))*B1(2,x(2))!x(2);
             fy = B1(2,x(1))*dB1(2,x(2))!x(1);
          CASE(4)
             !base_element=(1.-x(1))*x(2)
             fx = dB1(1,x(1))*B1(2,x(2))!-x(2);
             fy = B1(1,x(1))*dB1(2,x(2))!1._dp-x(1)
          CASE default
             PRINT*, "P1, numero base ", k
             STOP
          END SELECT

       CASE(2) ! B2 Bezier
          SELECT CASE(k)
          CASE(1)
             !base_element=B2(1,x(1))*B2(1,x(2))
             fx = dB2(1,x(1))*B2(1,x(2))
             fy = B2(1,x(1))*dB2(1,x(2))
          CASE(2)
             !base_element=B2(2,x(1))*B2(1,x(2))
             fx = dB2(2,x(1))*B2(1,x(2))
             fy = B2(2,x(1))*dB2(1,x(2))
          CASE(3)
             !base_element=B2(2,x(1))*B2(2,x(2))
             fx = dB2(2,x(1))*B2(2,x(2))
             fy = B2(2,x(1))*dB2(2,x(2))
          CASE(4)
             !base_element=B2(1,x(1))*B2(2,x(2))
             fx = dB2(1,x(1))*B2(2,x(2))
             fy = B2(1,x(1))*dB2(2,x(2))
          CASE(5)
             !base_element=B2(3,x(1))*B2(1,x(2))
             fx = dB2(3,x(1))*B2(1,x(2))
             fy = B2(3,x(1))*dB2(1,x(2))
          CASE(6)
             !base_element=B2(2,x(1))*B2(3,x(2))
             fx = dB2(2,x(1))*B2(3,x(2))
             fy = B2(2,x(1))*dB2(3,x(2))
          CASE(7)
             !base_element=B2(3,x(1))*B2(2,x(2))
             fx = dB2(3,x(1))*B2(2,x(2))
             fy = B2(3,x(1))*dB2(2,x(2))
          CASE(8)
             !base_element=B2(1,x(1))*B2(3,x(2))
             fx = dB2(1,x(1))*B2(3,x(2))
             fy = B2(1,x(1))*dB2(3,x(2))
          CASE(9)
             !base_element=B2(3,x(1))*B2(3,x(2))
             fx = dB2(3,x(1))*B2(3,x(2))
             fy = B2(3,x(1))*dB2(3,x(2))
          CASE default
             PRINT*, "B2, numero base ", k
             STOP
          END SELECT

       CASE(3) ! P2
          SELECT CASE(k)
          CASE(1)
             !base_element=P2(1,x(1))*P2(1,x(2))
             fx = dP2(1,x(1))*P2(1,x(2)); fy = P2(1,x(1))*dP2(1,x(2))
          CASE(2)
             !base_element=P2(2,x(1))*P2(1,x(2))
             fx = dP2(2,x(1))*P2(1,x(2)); fy = P2(2,x(1))*dP2(1,x(2))
          CASE(3)
             !base_element=P2(2,x(1))*P2(2,x(2))
             fx = dP2(2,x(1))*P2(2,x(2)); fy = P2(2,x(1))*dP2(2,x(2))
          CASE(4)
             !base_element=P2(1,x(1))*P2(2,x(2))
             fx = dP2(1,x(1))*P2(2,x(2)); fy = P2(1,x(1))*dP2(2,x(2))
          CASE(5)
             !base_element=P2(3,x(1))*P2(1,x(2))
             fx = dP2(3,x(1))*P2(1,x(2)); fy = P2(3,x(1))*dP2(1,x(2))
          CASE(6)
             !base_element=P2(2,x(1))*P2(3,x(2))
             fx = dP2(2,x(1))*P2(3,x(2)); fy = P2(2,x(1))*dP2(3,x(2))
          CASE(7)
             !base_element=P2(3,x(1))*P2(2,x(2))
             fx = dP2(3,x(1))*P2(2,x(2)); fy = P2(3,x(1))*dP2(2,x(2))
          CASE(8)
             !base_element=P2(1,x(1))*P2(3,x(2))
             fx = dP2(1,x(1))*P2(3,x(2)); fy = P2(1,x(1))*dP2(3,x(2))
          CASE(9)
             !base_element=P2(3,x(1))*P2(3,x(2))
             fx = dP2(3,x(1))*P2(3,x(2)); fy = P2(3,x(1))*dP2(3,x(2))
          CASE default
             PRINT*, "P2, numero base ", k
             STOP
          END SELECT
       CASE(5) ! B3 Bezier
          SELECT CASE(k)
          CASE(1)
             !base_element=B3(1,x(1))*B3(1,x(2))
             fx = dB3(1,x(1))*B3(1,x(2)); fy = B3(1,x(1))*dB3(1,x(2))
          CASE(2)
             !base_element=B3(1,x(1))*B3(2,x(2))
             fx = dB3(2,x(1))*B3(1,x(2)); fy = B3(2,x(1))*dB3(1,x(2))
          CASE(3)
             !base_element=B3(1,x(1))*B3(3,x(2))
             fx = dB3(2,x(1))*B3(2,x(2)); fy = B3(2,x(1))*dB3(2,x(2))
          CASE(4)
             !base_element=B3(1,x(1))*B3(4,x(2))
             fx = dB3(1,x(1))*B3(2,x(2)); fy = B3(1,x(1))*dB3(2,x(2))
          CASE(5)
             !base_element=B3(2,x(1))*B3(1,x(2))
             fx = dB3(3,x(1))*B3(1,x(2)); fy = B3(3,x(1))*dB3(1,x(2))
          CASE(6)
             !base_element=B3(2,x(1))*B3(2,x(2))
             fx = dB3(4,x(1))*B3(1,x(2)); fy = B3(4,x(1))*dB3(1,x(2))
          CASE(7)
             !base_element=B3(2,x(1))*B3(3,x(2))
             fx = dB3(2,x(1))*B3(4,x(2)); fy = B3(2,x(1))*dB3(4,x(2))
          CASE(8)
             !base_element=B3(2,x(1))*B3(4,x(2))
             fx = dB3(2,x(1))*B3(3,x(2)); fy = B3(2,x(1))*dB3(3,x(2))
          CASE(9)
             !base_element=B3(3,x(1))*B3(1,x(2))
             fx = dB3(4,x(1))*B3(2,x(2)); fy = B3(4,x(1))*dB3(2,x(2))
          CASE(10)
             !base_element=B3(3,x(1))*B3(2,x(2))
             fx = dB3(3,x(1))*B3(2,x(2)); fy = B3(3,x(1))*dB3(2,x(2))
          CASE(11)
             !base_element=B3(3,x(1))*B3(3,x(2))
             fx = dB3(1,x(1))*B3(4,x(2)); fy = B3(1,x(1))*dB3(4,x(2))
          CASE(12)
             !base_element=B3(3,x(1))*B3(4,x(2))
             fx = dB3(1,x(1))*B3(3,x(2)); fy = B3(1,x(1))*dB3(3,x(2))
          CASE(13)
             !base_element=B3(4,x(1))*B3(1,x(2))
             fx = dB3(3,x(1))*B3(3,x(2)); fy = B3(3,x(1))*dB3(3,x(2))
          CASE(14)
             !base_element=B3(4,x(1))*B3(2,x(2))
             fx = dB3(4,x(1))*B3(3,x(2)); fy = B3(4,x(1))*dB3(3,x(2))
          CASE(15)
             !base_element=B3(4,x(1))*B3(3,x(2))
             fx = dB3(4,x(1))*B3(4,x(2)); fy = B3(4,x(1))*dB3(4,x(2))
          CASE(16)
             !base_element=B3(4,x(1))*B3(4,x(2))
             fx = dB3(3,x(1))*B3(4,x(2)); fy = B3(3,x(1))*dB3(4,x(2))
          CASE default
             PRINT*, "B3, numero base ", k
             STOP
          END SELECT

       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

    CASE DEFAULT
       PRINT*, mod_name
       PRINT*, ' L478, Wrong nvertex = ', e%nvertex
    END SELECT

  END FUNCTION gradient_element_ref



  ! with respect to real(dp) coordinates
  FUNCTION gradient_element(e,k,x) RESULT (grad) ! gradient in reference element
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "gradient_element"
    CLASS(element),             INTENT(in)     :: e  ! calling element
    INTEGER,                    INTENT(in)     :: k  ! index of basis function
    REAL(DP), DIMENSION(:), INTENT(in)         :: x  ! barycentric coordinates
    REAL(DP), DIMENSION(n_dim)                 :: grad
    REAL(DP)                                   :: fx,fy,fz

    REAL(DP), DIMENSION(e%nsommets)            :: xx, yy
    REAL(DP), DIMENSION(n_dim,e%nsommets)      :: grad_ref
    REAL(DP), DIMENSION(n_dim,n_dim)           :: Jac, JacInv
    INTEGER                                    :: i
    REAL(DP)                                   :: a,b,c,d

    SELECT CASE(e%nvertex)

    CASE(3)

       SELECT CASE(e%itype)
       CASE(0)! P0
          SELECT CASE(k)
          CASE(1)
             fx=0.0_dp; fy=0.0_dp; fz=0.0_dp
          END SELECT

       CASE(1)! P1
          SELECT CASE(k)
          CASE(1)
             fx=1.0_dp; fy=0.0_dp; fz=0.0_dp
          CASE(2)
             fx=0.0_dp; fy=1.0_dp; fz=0.0_dp
          CASE(3)
             fx=0.0_dp; fy=0.0_dp; fz=1.0_dp
          END SELECT

       CASE(2) !B2
          SELECT CASE(k)
          CASE(1)
             fx=2._dp*x(1);fy=0._dp     ;fz=0._dp
          CASE(2)
             fx=0._dp     ;fy=2._dp*x(2);fz=0._dp
          CASE(3)
             fx=0._dp     ;fy=0._dp     ;fz=2._dp*x(3)
          CASE(4)
             fx=2._dp*x(2);fy=2._dp*x(1);fz=0._dp
          CASE(5)
             fx=0._dp     ;fy=2._dp*x(3);fz=2._dp*x(2)
          CASE(6)
             fx=2._dp*x(3);fy=0._dp     ;fz=2._dp*x(1)
          END SELECT

       CASE(3) ! P2
          SELECT CASE(k)
          CASE(1)
             fx=4._dp*x(1)-1._dp;fy=0._dp;fz=0._dp
          CASE(2)
             fx=0._dp;fy=4._dp*x(2)-1._dp;fz=0._dp
          CASE(3)
             fx=0._dp;fy=0._dp;fz=4._dp*x(3)-1._dp
          CASE(4)
             fx=4._dp*x(2);fy=4._dp*x(1);fz=0._dp
          CASE(5)
             fx=0._dp;fy=4._dp*x(3);fz=4._dp*x(2)
          CASE(6)
             fx=4._dp*x(3);fy=0._dp;fz=4._dp*x(1)
          END SELECT

       CASE(4) ! P3
          SELECT CASE(k)
          CASE(1)
             !base_element = 0.5*x(1)*(3.0*x(1)-1.0)*(3.0*x(1)-2.0)
             fx = 0.5_dp*( 27.0_dp*x(1)**2 - 18.0_dp*x(1) + 2.0_dp )
             fy = 0.0_dp
             fz = 0.0_dp
          CASE(2)
             !base_element = 0.5*x(2)*(3.0*x(2)-1.0)*(3.0*x(2)-2.0)
             fx = 0.0_dp
             fy = 0.5_dp*( 27.0_dp*x(2)**2 - 18.0_dp*x(2) + 2.0_dp )
             fz = 0.0_dp
          CASE(3)
             !base_element = 0.5*x(3)*(3.0*x(3)-1.0)*(3.0*x(3)-2.0)
             fx = 0.0_dp
             fy = 0.0_dp
             fz = 0.5_dp*( 27.0_dp*x(3)**2 - 18.0_dp*x(3) + 2.0_dp )
          CASE(4)
             !base_element = 4.5*x(1)*x(2)*(3.0*x(1)-1.0)
             fx = 4.5_dp*( 6.0_dp*x(1)*x(2) - x(2) )
             fy = 4.5_dp*( 3.0_dp*x(1)**2 - x(1) )
             fz = 0.0_dp
          CASE(5)
             !base_element = 4.5*x(1)*x(2)*(3.0*x(2)-1.0)
             fx = 4.5_dp*( 3.0_dp*x(2)**2 - x(2) )
             fy = 4.5_dp*( 6.0_dp*x(1)*x(2) - x(1) )
             fz = 0.0_dp
          CASE(6)
             !base_element = 4.5*x(2)*x(3)*(3.0*x(2)-1.0)
             fx = 0.0_dp
             fy = 4.5_dp*( 6.0_dp*x(2)*x(3) - x(3) )
             fz = 4.5_dp*( 3.0_dp*x(2)**2 - x(2) )
          CASE(7)
             !base_element = 4.5*x(2)*x(3)*(3.0*x(3)-1.0)
             fx = 0.0_dp
             fy = 4.5_dp*( 3.0_dp*x(3)**2 - x(3) )
             fz = 4.5_dp*( 6.0_dp*x(2)*x(3) - x(2) )
          CASE(8)
             !base_element = 4.5*x(3)*x(1)*(3.0*x(3)-1.0)
             fx = 4.5_dp*( 3.0_dp*x(3)**2 - x(3) )
             fy = 0.0_dp
             fz = 4.5_dp*( 6.0_dp*x(1)*x(3) - x(1) )
          CASE(9)
             !base_element = 4.5*x(3)*x(1)*(3.0*x(1)-1.0)
             fx = 4.5_dp*( 6.0_dp*x(1)*x(3) - x(3) )
             fy = 0.0_dp
             fz = 4.5_dp*( 3.0_dp*x(1)**2 - x(1) )
          CASE(10)
             !base_element = 27.0*x(1)*x(2)*x(3)
             fx = 27.0_dp*x(2)*x(3)
             fy = 27.0_dp*x(1)*x(3)
             fz = 27.0_dp*x(1)*x(2)
          END SELECT
       CASE(5) ! B3

          SELECT CASE(k)
          CASE(1)
             !x(k)*x(k)*x(k)
             fx = 3.0_dp*x(1)*x(1)
             fy = 0.0_dp
             fz = 0.0_dp
          CASE(2)
             !base_element = x(k)*x(k)*x(k)
             fx = 0.0_dp
             fy = 3.0_dp*x(2)*x(2)
             fz = 0.0_dp
          CASE(3)
             !base_element =x(k)*x(k)*x(k)
             fx = 0.0_dp
             fy = 0.0_dp
             fz = 3.0_dp*x(3)*x(3)
          CASE(4)
             !base_element = 3.0*x(1)*x(1)*x(2)
             fx = 6.0_dp*x(1)*x(2)
             fy = 3.0_dp*x(1)*x(1)
             fz = 0.0_dp
          CASE(5)
             !base_element = 3.0*x(1)*x(2)*x(2)
             fx = 3.0_dp*x(2)*x(2)
             fy = 6.0_dp*x(1)*x(2)
             fz = 0.0_dp
          CASE(6)
             !base_element = 3.0*x(2)*x(2)*x(3)
             fx = 0.0_dp
             fy = 6.0_dp*x(2)*x(3)
             fz = 3.0_dp*x(2)*x(2)
          CASE(7)
             !base_element = 3.0*x(2)*x(3)*x(3)
             fx = 0.0_dp
             fy = 3.0_dp*x(3)*x(3)
             fz = 6.0_dp*x(2)*x(3)
          CASE(8)
             !base_element = 3.0*x(1)*x(3)*x(3)
             fx = 3.0_dp*x(3)*x(3)
             fy = 0.0_dp
             fz = 6.0_dp*x(1)*x(3)
          CASE(9)
             !base_element = 3.0*x(1)*x(1)*x(3)
             fx = 6.0_dp*x(1)*x(3)
             fy = 0.0_dp
             fz = 3.0_dp*x(1)*x(1)
          CASE(10)
             !base_element = 6.0*x(1)*x(2)*x(3)
             fx = 6.0_dp*x(2)*x(3)
             fy = 6.0_dp*x(1)*x(3)
             fz = 6.0_dp*x(1)*x(2)
          END SELECT
       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

       grad= ((fx-fz)*e%n(:,1)+(fy-fz)*e%n(:,2))/(2._dp*e%volume)

    CASE(4)

       SELECT CASE(e%itype)
       CASE(0) ! P0
          SELECT CASE(k)
          CASE(1)
             !base_element = 1.0
             fx=0.0_dp; fy=0.0_dp
          CASE default
             PRINT*, "P0, numero base ", k
             STOP
          END SELECT

!!$       CASE(-1) ! P1
!!$          SELECT CASE(k)
!!$          CASE(1)
!!$             !base_element=(1.-x(1))*(1.-x(2))
!!$             fx = -(1._dp-x(2)); fy = -(1._dp-x(1))
!!$          CASE(2)
!!$             !base_element=x(1)*(1.-x(2))
!!$             fx = 1._dp-x(2); fy = -x(1)
!!$          CASE(3)
!!$             !base_element=x(1)*x(2)
!!$             fx = x(2); fy = x(1);
!!$          CASE(4)
!!$             !base_element=(1.-x(1))*x(2)
!!$             fx = -x(2); fy = 1._dp-x(1)
!!$          CASE default
!!$             PRINT*, "P1, numero base ", k
!!$             STOP
!!$          END SELECT
       CASE(1)
          SELECT CASE(k)
          CASE(1)
             !base_element=(1.-x(1))*(1.-x(2))
             fx = dB1(1,x(1))*B1(1,x(2))!-(1._dp-x(2))
             fy = B1(1,x(1))*dB1(1,x(1))!-(1._dp-x(1))
          CASE(2)
             !base_element=x(1)*(1.-x(2))
             fx = dB1(2,x(1))*B1(1,x(2))!1._dp-x(2);
             fy = B1(2,x(1))*dB1(1,x(2))!-x(1)
          CASE(3)
             !base_element=x(1)*x(2)
             fx = dB1(2,x(1))*B1(2,x(2))!x(2);
             fy = B1(2,x(1))*dB1(2,x(2))!x(1);
          CASE(4)
             !base_element=(1.-x(1))*x(2)
             fx = dB1(1,x(1))*B1(2,x(2))!-x(2);
             fy = B1(1,x(1))*dB1(2,x(2))!1._dp-x(1)
          CASE default
             PRINT*, "P1, numero base ", k
             STOP
          END SELECT
       CASE(2) ! B2 Bezier
          SELECT CASE(k)
          CASE(1)
             !base_element=B2(1,x(1))*B2(1,x(2))
             fx = dB2(1,x(1))*B2(1,x(2)); fy = B2(1,x(1))*dB2(1,x(2))
          CASE(2)
             !base_element=B2(2,x(1))*B2(1,x(2))
             fx = dB2(2,x(1))*B2(1,x(2)); fy = B2(2,x(1))*dB2(1,x(2))
          CASE(3)
             !base_element=B2(2,x(1))*B2(2,x(2))
             fx = dB2(2,x(1))*B2(2,x(2)); fy = B2(2,x(1))*dB2(2,x(2))
          CASE(4)
             !base_element=B2(1,x(1))*B2(2,x(2))
             fx = dB2(1,x(1))*B2(2,x(2)); fy = B2(1,x(1))*dB2(2,x(2))
          CASE(5)
             !base_element=B2(3,x(1))*B2(1,x(2))
             fx = dB2(3,x(1))*B2(1,x(2)); fy = B2(3,x(1))*dB2(1,x(2))
          CASE(6)
             !base_element=B2(2,x(1))*B2(3,x(2))
             fx = dB2(2,x(1))*B2(3,x(2)); fy = B2(2,x(1))*dB2(3,x(2))
          CASE(7)
             !base_element=B2(3,x(1))*B2(2,x(2))
             fx = dB2(3,x(1))*B2(2,x(2)); fy = B2(3,x(1))*dB2(2,x(2))
          CASE(8)
             !base_element=B2(1,x(1))*B2(3,x(2))
             fx = dB2(1,x(1))*B2(3,x(2)); fy = B2(1,x(1))*dB2(3,x(2))
          CASE(9)
             !base_element=B2(3,x(1))*B2(3,x(2))
             fx = dB2(3,x(1))*B2(3,x(2)); fy = B2(3,x(1))*dB2(3,x(2))
          CASE default
             PRINT*, "B2, numero base ", k
             STOP
          END SELECT

       CASE(3) ! P2
          SELECT CASE(k)
          CASE(1)
             !base_element=P2(1,x(1))*P2(1,x(2))
             fx = dP2(1,x(1))*P2(1,x(2)); fy = P2(1,x(1))*dP2(1,x(2))
          CASE(2)
             !base_element=P2(2,x(1))*P2(1,x(2))
             fx = dP2(2,x(1))*P2(1,x(2)); fy = P2(2,x(1))*dP2(1,x(2))
          CASE(3)
             !base_element=P2(2,x(1))*P2(2,x(2))
             fx = dP2(2,x(1))*P2(2,x(2)); fy = P2(2,x(1))*dP2(2,x(2))
          CASE(4)
             !base_element=P2(1,x(1))*P2(2,x(2))
             fx = dP2(1,x(1))*P2(2,x(2)); fy = P2(1,x(1))*dP2(2,x(2))
          CASE(5)
             !base_element=P2(3,x(1))*P2(1,x(2))
             fx = dP2(3,x(1))*P2(1,x(2)); fy = P2(3,x(1))*dP2(1,x(2))
          CASE(6)
             !base_element=P2(2,x(1))*P2(3,x(2))
             fx = dP2(2,x(1))*P2(3,x(2)); fy = P2(2,x(1))*dP2(3,x(2))
          CASE(7)
             !base_element=P2(3,x(1))*P2(2,x(2))
             fx = dP2(3,x(1))*P2(2,x(2)); fy = P2(3,x(1))*dP2(2,x(2))
          CASE(8)
             !base_element=P2(1,x(1))*P2(3,x(2))
             fx = dP2(1,x(1))*P2(3,x(2)); fy = P2(1,x(1))*dP2(3,x(2))
          CASE(9)
             !base_element=P2(3,x(1))*P2(3,x(2))
             fx = dP2(3,x(1))*P2(3,x(2)); fy = P2(3,x(1))*dP2(3,x(2))
          CASE default
             PRINT*, "P2, numero base ", k
             STOP
          END SELECT
       CASE(5) ! B3 Bezier
          SELECT CASE(k)
          CASE(1)
             !base_element=B3(1,x(1))*B3(1,x(2))
             fx = dB3(1,x(1))* B3(1,x(2));
             fy =  B3(1,x(1))*dB3(1,x(2))
          CASE(2)
             !base_element=B3(2,x(1))*B3(2,x(2))
             fx = dB3(2,x(1))* B3(1,x(2));
             fy =  B3(2,x(1))*dB3(1,x(2))
          CASE(3)
             !base_element=B3(2,x(1))*B3(2,x(2))
             fx = dB3(2,x(1))* B3(2,x(2));
             fy =  B3(2,x(1))*dB3(2,x(2))
          CASE(4)
             !base_element=B3(1,x(1))*B3(2,x(2))
             fx = dB3(1,x(1))* B3(2,x(2));
             fy =  B3(1,x(1))*dB3(2,x(2))
          CASE(5)
             !base_element=B3(3,x(1))*B3(1,x(2))
             fx = dB3(3,x(1))* B3(1,x(2));
             fy =  B3(3,x(1))*dB3(1,x(2))
          CASE(6)
             !base_element=B3(4,x(1))*B3(1,x(2))
             fx = dB3(4,x(1))* B3(1,x(2));
             fy =  B3(4,x(1))*dB3(1,x(2))
          CASE(7)
             !base_element=B3(2,x(1))*B3(4,x(2))
             fx = dB3(2,x(1))* B3(4,x(2));
             fy =  B3(2,x(1))*dB3(4,x(2))
          CASE(8)
             !base_element=B3(2,x(1))*B3(3,x(2))
             fx = dB3(2,x(1))* B3(3,x(2));
             fy =  B3(2,x(1))*dB3(3,x(2))
          CASE(9)
             !base_element=B3(4,x(1))*B3(2,x(2))
             fx = dB3(4,x(1))* B3(2,x(2));
             fy =  B3(4,x(1))*dB3(2,x(2))
          CASE(10)
             !base_element=B3(3,x(1))*B3(2,x(2))
             fx = dB3(3,x(1))* B3(2,x(2));
             fy =  B3(3,x(1))*dB3(2,x(2))
          CASE(11)
             !base_element=B3(1,x(1))*B3(4,x(2))
             fx = dB3(1,x(1))* B3(4,x(2));
             fy =  B3(1,x(1))*dB3(4,x(2))
          CASE(12)
             !base_element=B3(1,x(1))*B3(3,x(2))
             fx = dB3(1,x(1))* B3(3,x(2));
             fy =  B3(1,x(1))*dB3(3,x(2))
          CASE(13)
             !base_element=B3(3,x(1))*B3(3,x(2))
             fx = dB3(3,x(1))* B3(3,x(2));
             fy =  B3(3,x(1))*dB3(3,x(2))
          CASE(14)
             !base_element=B3(4,x(1))*B3(3,x(2))
             fx = dB3(4,x(1))* B3(3,x(2));
             fy =  B3(4,x(1))*dB3(3,x(2))
          CASE(15)
             !base_element=B3(4,x(1))*B3(4,x(2))
             fx = dB3(4,x(1))* B3(4,x(2));
             fy =  B3(4,x(1))*dB3(4,x(2))
          CASE(16)
             !base_element=B3(3,x(1))*B3(4,x(2))
             fx = dB3(3,x(1))* B3(4,x(2));
             fy =  B3(3,x(1))*dB3(4,x(2))
          CASE default
             PRINT*, "B3, numero base ", k
             STOP
          END SELECT
       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT


       Jac=d_iso_element(e,x)

       JacInv(1,:) = (/  Jac(2,2) , -Jac(1,2) /)
       JacInv(2,:) = (/ -Jac(2,1) ,  Jac(1,1) /)
       IF ( ABS( Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1) ).LE.1.e-13 ) THEN
          PRINT*, mod_name, Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1)
          PRINT*, jac
          STOP
       ENDIF
       JacInv = JacInv/(Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1))

       grad(1) = fx*JacInv(1,1) + fy*JacInv(2,1) ! the transpose. (J*)^(-T)\nabla_X phi
       grad(2) = fx*JacInv(1,2) + fy*JacInv(2,2)

    CASE DEFAULT
       PRINT*, mod_name

       PRINT*, 'L1271, Wrong nvertex = ', e%nvertex
       STOP
    END SELECT

  END FUNCTION gradient_element

  FUNCTION hessian_element(e,k,x) RESULT (Hessian) ! gradient in reference element
    CHARACTER(LEN = *), PARAMETER :: mod_name = "hessian_element"
    CLASS(element), INTENT(in):: e
    INTEGER, INTENT(in):: k ! numero de la fonction de base
    REAL(DP), DIMENSION(3), INTENT(in):: x ! coordonnees barycentriques
    REAL(DP),DIMENSION(2,2):: n
    REAL(DP), DIMENSION(3,3):: H  !Hessian in bary coords
    REAL(DP), DIMENSION(2,2):: Hessian !hessian in geometrical coords

    n=e%n
    H=0.
    SELECT CASE(e%itype)
    CASE(1)! P1
       H=0.

    CASE(2) !B2
       SELECT CASE(k)
       CASE(1)
          H(1,1)=2._dp
       CASE(2)
          H(2,2)=2._dp
       CASE(3)
          H(3,3)=2._dp
       CASE(4)
          H(1,2)=2._dp
          H(2,1)=2._dp
       CASE(5)
          H(2,3)=2._dp
          H(3,2)=2._dp
       CASE(6)
          H(1,3)=2._dp
          H(3,1)=2._dp
       END SELECT

    CASE(3) !P2
       SELECT CASE(k)
       CASE(1)
          H(1,1)=4._dp
       CASE(2)
          H(2,2)=4._dp
       CASE(3)
          H(3,3)=4._dp
       CASE(4)
          H(1,2)=4._dp
          H(2,1)=4._dp
       CASE(5)
          H(2,3)=4._dp
          H(3,2)=4._dp
       CASE(6)
          H(1,3)=4._dp
          H(3,1)=4._dp
       END SELECT

    CASE(4) ! P3
       SELECT CASE(k)
       CASE(1)
          H(1,1)=27._dp*x(1)-9._dp
       CASE(2)
          H(2,2)=27._dp*x(2)-9._dp
       CASE(3)
          H(3,3)=27._dp*x(3)-9._dp

       CASE(4)
          H(1,1)=27._dp*x(2)
          H(1,2)=27._dp*x(1)-4.5_dp
          H(2,1)=27._dp*x(1)-4.5_dp

       CASE(5)
          H(2,2)=27._dp*x(1)
          H(1,2)=27._dp*x(2)-4.5_dp
          H(2,1)=27._dp*x(2)-4.5_dp
       CASE(6)
          H(2,2)=27._dp*x(3)
          H(3,2)=27._dp*x(2)-4.5_dp
          H(2,3)=27._dp*x(2)-4.5_dp
       CASE(7)
          H(3,3)=27._dp*x(2)
          H(3,2)=27._dp*x(3)-4.5_dp
          H(2,3)=27._dp*x(3)-4.5_dp
       CASE(8)
          H(3,3)=27._dp*x(1)
          H(3,1)=27._dp*x(3)-4.5_dp
          H(1,3)=27._dp*x(3)-4.5_dp
       CASE(9)
          H(1,1)=27._dp*x(3)
          H(3,1)=27._dp*x(1)-4.5_dp
          H(1,3)=27._dp*x(1)-4.5_dp
       CASE(10)
          H(1,2)=27._dp*x(3)
          H(1,3)=27._dp*x(2)
          H(2,3)=27._dp*x(1)
          H(2,1)=27._dp*x(3)
          H(3,1)=27._dp*x(2)
          H(3,2)=27._dp*x(1)
       END SELECT


    CASE(5) ! B3
       SELECT CASE(k)
       CASE(1)
          !x(k)*x(k)*x(k)
          H(1,1)=6._dp*x(1)
       CASE(2)
          !base_element = x(k)*x(k)*x(k)
          H(2,2)=6._dp*x(2)
       CASE(3)
          !base_element =x(k)*x(k)*x(k)
          H(3,3)=6._dp*x(3)

       CASE(4)
          !base_element = 3._dp*x(1)*x(1)*x(2)
          H(1,1)=6._dp*x(2)
          H(1,2)=6._dp*x(1)
          H(2,1)=6._dp*x(1)

       CASE(5)
          !base_element = 3._dp*x(1)*x(2)*x(2)
          H(2,2)=6._dp*x(1)
          H(1,2)=6._dp*x(2)
          H(2,1)=6._dp*x(2)
       CASE(6)
          !base_element = 3._dp*x(2)*x(2)*x(3)
          H(2,2)=6.*x(3)
          H(3,2)=6.*x(2)
          H(2,3)=6.*x(2)
       CASE(7)
          !base_element = 3._dp*x(2)*x(3)*x(3)
          H(3,3)=6._dp*x(2)
          H(3,2)=6._dp*x(3)
          H(2,3)=6._dp*x(3)
       CASE(8)
          !base_element = 3._dp*x(1)*x(3)*x(3)
          H(3,3)=6._dp*x(1)
          H(3,1)=6._dp*x(3)
          H(1,3)=6._dp*x(3)
       CASE(9)
          !base_element = 3._dp*x(1)*x(1)*x(3)
          H(1,1)=6._dp*x(3)
          H(3,1)=6._dp*x(1)
          H(1,3)=6._dp*x(1)
       CASE(10)
          !base_element = 6._dp*x(1)*x(2)*x(3)
          H(1,2)=6._dp*x(3)
          H(1,3)=6._dp*x(2)
          H(2,3)=6._dp*x(1)
          H(2,1)=6._dp*x(3)
          H(3,1)=6._dp*x(2)
          H(3,2)=6._dp*x(1)
       END SELECT

    CASE default
       PRINT*, "Type non existant", e%itype
       STOP
    END SELECT

    Hessian(1,1)=(    H(1,1)*n(1,1)**2  &
         &+2._dp*H(1,2)*n(1,1)*n(1,2)  &
         & -2._dp*H(1,3)*n(1,1)*(n(1,1)+n(1,2))  &
         &	+2._dp*H(2,2)*n(1,2)**2. &
         &	-2._dp*H(2,3)*n(1,2)*(n(1,2)+n(1,1)) &
         &		 +H(3,3)*(n(1,1)+n(1,2))**2. &
         &	)/(2._dp*e%volume)**2

    Hessian(1,2)=(  n(1,1)*n(2,1)*H(1,1)& 
         &+ n(1,2)*n(2,2)*H(2,2) & 
         &   + (n(1,1) + n(1,2))*(n(2,1)+n(2,2))*H(3,3) & 
         &   + H(1,2)*n(1,1)*n(2,2) & 
         &   + H(1,2)*n(1,2)*n(2,1) & 
         &   - 2._dp*H(1,3)*n(1,1)*n(2,1) & 
         &  - H(1,3)*n(1,1)*n(2,2) & 
         &  - H(1,3)*n(1,2)*n(2,1) & 
         &   - H(2,3)*n(1,1)*n(2,2) & 
         &   - H(2,3)*n(1,2)*n(2,1) & 
         &   - 2._dp*H(2,3)*n(1,2)*n(2,2) &
         &)/(2._dp*e%volume)**2

    Hessian(2,1)=Hessian(1,2)

    Hessian(2,2)= (    n(2,1)**2 *H(1,1) & 
         & + n(2,2)**2 *H(2,2) & 
         & + (n(2,1) + n(2,2))**2 *H(3,3) & 
         & + 2._dp*H(1,2)*n(2,1)*n(2,2) & 
         & - 2._dp*H(2,3)*n(2,2)**2 & 
         & - 2._dp*H(1,3)*n(2,1)**2 & 
         & - 2._dp*H(1,3)*n(2,1)*n(2,2) & 
         & - 2._dp*H(2,3)*n(2,1)*n(2,2)  &
         & )/(2._dp*e%volume)**2

  END FUNCTION hessian_element
  !==================================================================
  ! Definition of quadrature rules
  !==================================================================

  SUBROUTINE quadrature_element(e)    ! quadrature points and weights
    CHARACTER(LEN = *), PARAMETER :: mod_name = "quadrature_element"
    ! for triangle and edges
    CLASS(element), INTENT(inout)     :: e
    REAL(DP)                              :: w,xo,yo,zo,s
    INTEGER                           :: nquad
    REAL(DP), DIMENSION(:,:), ALLOCATABLE :: quad_edge
    REAL(DP), DIMENSION(:),   ALLOCATABLE :: weight_edge
    INTEGER                           :: edgenum, iq, iq1, iq2, k1, k2
    REAL(DP), DIMENSION(3) :: qp, qw ! for 3-point Gauss

    NULLIFY(e%quad, e%weight, e%edge, e%quad_edge, e%weight_edge)
    SELECT CASE(e%nvertex)

    CASE(3)

       ! for triangles
       SELECT CASE(e%itype)
       CASE(1,2,3)
          !---- 7-point, exact for degree 5 ----!
          e%nquad=7
          nquad=e%nquad

          ALLOCATE(e%quad(3,e%nquad),e%weight(e%nquad))

          w=0.225_dp
          e%quad(:,1)=1._dp/3._dp
          e%weight(1)=w

          w =0.125939180544827_dp
          xo=0.797426985353087_dp
          zo=0.5_dp*(1.0_dp-xo)
          e%weight(2:4)=w
          e%quad(1,2)=xo; e%quad(2,2)=zo; e%quad(3,2)=zo
          e%quad(1,3)=zo; e%quad(2,3)=xo; e%quad(3,3)=zo
          e%quad(1,4)=zo; e%quad(2,4)=zo; e%quad(3,4)=xo

          e%weight(5:7)=0.132394152788506_dp
          xo=0.059715871789770_dp
          zo=0.5_dp*(1.0_dp-xo)
          e%quad(1,5)=xo; e%quad(2,5)=zo; e%quad(3,5)=zo
          e%quad(1,6)=zo; e%quad(2,6)=xo; e%quad(3,6)=zo
          e%quad(1,7)=zo; e%quad(2,7)=zo; e%quad(3,7)=xo

          e%quad(3,:)=1.-e%quad(1,:)-e%quad(2,:)
          e%weight(1:nquad)=e%weight(1:nquad)/SUM(e%weight(1:nquad))

          ! and edges definition
          ALLOCATE(e%edge(2,3))  !

          e%edge(1,1)=1;e%edge(2,1)=2 ! edge #1 between i=1 and i=2,
          e%edge(1,2)=2;e%edge(2,2)=3 ! etc
          e%edge(1,3)=3;e%edge(2,3)=1

          ! ! quadrature for edges (midpoint formula)
          !    e%nquad_edge=1
          !    ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          !    ALLOCATE(e%quad_edge(3,3,e%nquad_edge),e%weight_edge(3,e%nquad_edge))   ! (edgenum,x,iq)
          !
          !    e%quad_edge   = 0
          !    e%weight_edge = 0
          !
          !    DO edgenum = 1,3
          !             k1=e%next(edgenum)
          !   k2=e%next(k1)
          !        quad_edge(1,1) = 0.5
          !        quad_edge(2,1) = 0.5
          !        weight_edge(1) = 1.0
          !
          !        DO iq = 1,e%nquad_edge
          !            e%quad_edge(edgenum,k1,iq)         = quad_edge(1,iq)
          !            e%quad_edge(edgenum,k2,iq) = quad_edge(2,iq)
          !            e%weight_edge(edgenum,iq)               = weight_edge(iq)
          !        END DO
          !
          !    END DO

          ! quadrature for edges (3-point Gauss formula)
          e%nquad_edge=3
          ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          ALLOCATE(e%quad_edge(3,3,e%nquad_edge),e%weight_edge(3,e%nquad_edge))   ! (edgenum,x,iq)

          s=SQRT(0.6_dp)

          e%quad_edge   = 0
          e%weight_edge = 0

          DO edgenum = 1,3
             k1=e%next(edgenum)
             k2=e%next(k1)
             quad_edge(1,1) = 0.5_dp*(1.0_dp - s)
             quad_edge(2,1) = 0.5_dp*(1.0_dp + s)
             weight_edge(1) = 5.0_dp/18.0_dp
             quad_edge(1,2) = 0.5_dp*(1.0_dp + s)
             quad_edge(2,2) = 0.5_dp*(1.0_dp - s)
             weight_edge(2) = 5.0_dp/18.0_dp
             quad_edge(1,3) = 0.5_dp
             quad_edge(2,3) = 0.5_dp
             weight_edge(3) = 8.0_dp/18.0_dp

             DO iq = 1,e%nquad_edge
                e%quad_edge(edgenum,k1,iq)              = quad_edge(1,iq)
                e%quad_edge(edgenum,k2,iq)              = quad_edge(2,iq)
                e%weight_edge(edgenum,iq)               = weight_edge(iq)
             END DO

          END DO

       CASE(4,5)
          !---- 12-point, exact for degree 6 ----!
          e%nquad=12
          nquad=e%nquad
          ALLOCATE(e%quad(3,e%nquad),e%weight(e%nquad))

          w =0.116786275726379_dp
          xo=0.501426509658179_dp
          zo=0.5_dp*(1._dp-xo)
          e%weight(1:3)=w
          e%quad(1,1)=xo; e%quad(2,1)=zo; e%quad(3,1)=zo
          e%quad(1,2)=zo; e%quad(2,2)=xo; e%quad(3,2)=zo
          e%quad(1,3)=zo; e%quad(2,3)=zo; e%quad(3,3)=xo

          w=0.050844906370207_dp
          xo=0.873821971016996_dp
          zo=0.5_dp*(1._dp-xo)
          e%weight(4:6)=w
          e%quad(1,4)=xo; e%quad(2,4)=zo; e%quad(3,4)=zo
          e%quad(1,5)=zo; e%quad(2,5)=xo; e%quad(3,5)=zo
          e%quad(1,6)=zo; e%quad(2,6)=zo; e%quad(3,6)=xo

          w=0.082851075618374_dp
          xo=0.053145049844816_dp!7
          yo=0.310352451033785_dp!4
          zo=1._dp-xo-yo
          e%weight(7:12)=w
          e%quad(1,7) =xo; e%quad(2,7) =yo; e%quad(3,7) =zo
          e%quad(1,8) =xo; e%quad(2,8) =zo; e%quad(3,8) =yo
          e%quad(1,9) =yo; e%quad(2,9) =xo; e%quad(3,9) =zo
          e%quad(1,10)=yo; e%quad(2,10)=zo; e%quad(3,10)=xo
          e%quad(1,11)=zo; e%quad(2,11)=xo; e%quad(3,11)=yo
          e%quad(1,12)=zo; e%quad(2,12)=yo; e%quad(3,12)=xo

          e%quad(3,:)=1._dp-e%quad(1,:)-e%quad(2,:)


          ! quadrature for edges (5-point Gauss formula)
          e%nquad_edge=5
          ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          ALLOCATE(e%quad_edge(3,3,e%nquad_edge),e%weight_edge(3,e%nquad_edge))!    (edgenum,x,iq)

          e%quad_edge   = 0
          e%weight_edge = 0

          DO edgenum = 1,3
             k1=e%next(edgenum)
             k2=e%next(k1)
             s=1._dp/3._dp*SQRT(5._dp-2._dp*SQRT(10._dp/7._dp))
             quad_edge(1,1)=0.5_dp*(1.0_dp - s)
             quad_edge(2,1)=0.5_dp*(1.0_dp + s)
             weight_edge(1) = (322._dp+13._dp*SQRT(70._dp))/900._dp
             quad_edge(1,2)=0.5_dp*(1.0_dp + s)
             quad_edge(2,2)=0.5_dp*(1.0_dp - s)
             weight_edge(2) = (322._dp+13._dp*SQRT(70._dp))/900._dp

             s=1._dp/3._dp*SQRT(5._dp+2._dp*SQRT(10._dp/7._dp))
             quad_edge(1,3)=0.5_dp*(1.0_dp - s)
             quad_edge(2,3)=0.5_dp*(1.0_dp + s)
             weight_edge(3) = (322._dp-13._dp*SQRT(70._dp))/900._dp
             quad_edge(1,4)=0.5_dp*(1.0_dp + s)
             quad_edge(2,4)=0.5_dp*(1.0_dp - s)
             weight_edge(4) = (322._dp-13._dp*SQRT(70._dp))/900._dp

             quad_edge(1,5)=0.5_dp
             quad_edge(2,5)=0.5_dp
             weight_edge(5)=128._dp/225._dp

             weight_edge = 0.5_dp*weight_edge

             DO iq = 1,e%nquad_edge
                e%quad_edge(edgenum,k1,iq)        = quad_edge(1,iq)
                e%quad_edge(edgenum,k2,iq)        = quad_edge(2,iq)
                e%weight_edge(edgenum,iq)         = weight_edge(iq)
             END DO

          END DO
       END SELECT

       DEALLOCATE(quad_edge,weight_edge)

    CASE(4)
       SELECT CASE (e%itype)
       CASE(1)
          ! for quads

          !        !---- midpoint ----!
          e%nquad=1
          nquad=e%nquad
          ALLOCATE(e%quad(e%nvertex,e%nquad),e%weight(e%nquad))
          !
          DO iq = 1,e%nquad
             e%quad(1:2,iq) = (/ 0.5_dp, 0.5_dp /); e%weight(iq) = 1.0_dp
          END DO
          !
          DO iq = 1,e%nquad
             e%quad(3:4,iq) = 1.0_dp - e%quad(1:2,iq)
          END DO
          !
          !        ! quadrature for edges (midpoint formula)
          e%nquad_edge=1
          ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          ALLOCATE(e%quad_edge(4,e%nvertex,e%nquad_edge),e%weight_edge(4,e%nquad_edge))   ! (edgenum,baryc_coord,iq)
          !
          e%quad_edge   = 0
          e%weight_edge = 0
          !
          quad_edge(1,1) = 0.5_dp
          quad_edge(2,1) = 0.5_dp
          weight_edge(1) = 1.0_dp
       CASE(2,3)
          !---- 4-point, 2d product of 2-point Gauss ----!
          e%nquad=4
          nquad=e%nquad
          ALLOCATE(e%quad(e%nvertex,e%nquad),e%weight(e%nquad))

          s=SQRT(1._dp/3._dp)
          qp(1) = 0.5_dp*(1.0_dp - s)
          qp(2) = 0.5_dp*(1.0_dp + s)
          qw(1) = 0.5_dp
          qw(2) = 0.5_dp

          iq = 0
          DO iq2 = 1,2
             DO iq1 = 1,2
                iq = iq+1
                e%quad(1:2,iq) = (/ qp(iq1), qp(iq2) /); e%weight(iq) = qw(iq1)*qw(iq2)
             END DO
          END DO

          DO iq = 1,e%nquad
             e%quad(3:4,iq) = 1.0_dp - e%quad(1:2,iq)
          END DO

          ! quadrature for edges (3-point Gauss formula)
          e%nquad_edge=3
          ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          ALLOCATE(e%quad_edge(4,e%nvertex,e%nquad_edge),e%weight_edge(4,e%nquad_edge))   ! (edgenum,baryc_coord,iq)

          s=SQRT(0.6_dp)

          e%quad_edge   = 0
          e%weight_edge = 0

          quad_edge(1,1) = 0.5_dp*(1.0_dp - s)
          quad_edge(2,1) = 0.5_dp*(1.0_dp + s)
          weight_edge(1) = 5.0_dp/18.0_dp
          quad_edge(1,2) = 0.5_dp*(1.0_dp + s)
          quad_edge(2,2) = 0.5_dp*(1.0_dp - s)
          weight_edge(2) = 5.0_dp/18.0_dp
          quad_edge(1,3) = 0.5_dp
          quad_edge(2,3) = 0.5_dp
          weight_edge(3) = 8.0_dp/18.0_dp

       CASE(4,5)
          !        !---- 9-point, 2d product of 3-point Gauss ----!
          e%nquad=9
          nquad=e%nquad
          ALLOCATE(e%quad(e%nvertex,e%nquad),e%weight(e%nquad))
          !
          s=SQRT(0.6_dp)
          qp(1) = 0.5_dp*(1.0_dp - s)
          qp(2) = 0.5_dp
          qp(3) = 0.5_dp*(1.0_dp + s)
          qw(1) = 5.0_dp/18.0_dp
          qw(2) = 8.0_dp/18.0_dp
          qw(3) = 5.0_dp/18.0_dp
          !
          iq = 0
          DO iq2 = 1,3
             DO iq1 = 1,3
                iq = iq+1
                e%quad(1:2,iq) = (/ qp(iq1), qp(iq2) /); e%weight(iq) = qw(iq1)*qw(iq2)
             END DO
          END DO
          !
          DO iq = 1,e%nquad
             e%quad(3:4,iq) = 1.0_dp - e%quad(1:2,iq)
          END DO
          !
          !        ! quadrature for edges (3-point Gauss formula)
          e%nquad_edge=3
          ALLOCATE(quad_edge(2,e%nquad_edge),weight_edge(e%nquad_edge))
          ALLOCATE(e%quad_edge(4,e%nvertex,e%nquad_edge),e%weight_edge(4,e%nquad_edge))   ! (edgenum,baryc_coord,iq)
          !
          s=SQRT(0.6_dp)
          !
          e%quad_edge   = 0
          e%weight_edge = 0
          !
          quad_edge(1,1) = 0.5_dp*(1.0_dp - s)
          quad_edge(2,1) = 0.5_dp*(1.0_dp + s)
          weight_edge(1) = 5.0_dp/18.0_dp
          quad_edge(1,2) = 0.5_dp*(1.0_dp + s)
          quad_edge(2,2) = 0.5_dp*(1.0_dp - s)
          weight_edge(2) = 5.0_dp/18.0_dp
          quad_edge(1,3) = 0.5_dp
          quad_edge(2,3) = 0.5_dp
          weight_edge(3) = 8.0_dp/18.0_dp
       END SELECT
       ! DO NOT CHANGE

       edgenum = 1
       DO iq = 1,e%nquad_edge
          e%quad_edge(edgenum,1,iq)   = quad_edge(1,iq)
          e%quad_edge(edgenum,2,iq)   = 0.0_dp
          e%quad_edge(edgenum,3:4,iq) = 1.0_dp - e%quad_edge(edgenum,1:2,iq)
       END DO
       edgenum = 2
       DO iq = 1,e%nquad_edge
          e%quad_edge(edgenum,1,iq)   = 1.0_dp
          e%quad_edge(edgenum,2,iq)   = quad_edge(1,iq)
          e%quad_edge(edgenum,3:4,iq) = 1.0_dp - e%quad_edge(edgenum,1:2,iq)
       END DO
       edgenum = 3
       DO iq = 1,e%nquad_edge
          e%quad_edge(edgenum,1,iq)   = quad_edge(1,iq)
          e%quad_edge(edgenum,2,iq)   = 1.0_dp
          e%quad_edge(edgenum,3:4,iq) = 1.0_dp - e%quad_edge(edgenum,1:2,iq)
       END DO
       edgenum = 4
       DO iq = 1,e%nquad_edge
          e%quad_edge(edgenum,1,iq)   = 0.0
          e%quad_edge(edgenum,2,iq)   = quad_edge(1,iq)
          e%quad_edge(edgenum,3:4,iq) = 1.0_dp - e%quad_edge(edgenum,1:2,iq)
       END DO

       DO edgenum = 1,4
          DO iq = 1,e%nquad_edge
             e%weight_edge(edgenum,iq)   = weight_edge(iq)
          END DO
       END DO

       ! and edges definition
       ALLOCATE(e%edge(2,4))  !

       e%edge(1,1)=1;e%edge(2,1)=2 ! edge #1 between i=1 and i=2,
       e%edge(1,2)=2;e%edge(2,2)=3 ! etc
       e%edge(1,3)=3;e%edge(2,3)=4
       e%edge(1,4)=4;e%edge(2,4)=1

    CASE default
       PRINT*, mod_name
       PRINT*, 'L 1814, Wrong nvertex = ', e%nvertex
       STOP

    END SELECT
    NULLIFY(e%base_at_quad, e%grad_at_quad)
    ALLOCATE(e%base_at_quad(e%nsommets,e%nquad),e%grad_at_quad(n_dim,e%nsommets,e%nquad))
    CALL e%basegradATquad()

    NULLIFY(e%base_at_quad_edge, e%grad_at_quad)
    ALLOCATE(e%base_at_quad_edge(SIZE(e%quad_edge,1), e%nsommets,e%nquad_edge ))
    CALL e%baseATquad_edge()
  END SUBROUTINE quadrature_element



  SUBROUTINE basegradATquad_element(e)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "basgradATquad"
    CLASS(element), INTENT(inout)     :: e
    INTEGER:: l, iq

    DO iq=1,e%nquad
       DO l=1,e%nsommets
          e%base_at_quad(l  ,iq)=e%base    (l, e%quad(:,iq))
          e%grad_at_quad(:,l,iq)=e%gradient(l, e%quad(:,iq))
       ENDDO
    ENDDO
  END SUBROUTINE basegradATquad_element

  SUBROUTINE baseATquad_edge_element(e)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "baseATquad_edge"
    CLASS(element), INTENT(inout)     :: e
    INTEGER:: l, iq,k
  
    DO k=1, SIZE(e%quad_edge,1)
       DO iq=1,e%nquad_edge
          DO l=1,e%nsommets!nvertex
             e%base_at_quad_edge(k,l  ,iq)=e%base    (l, e%quad_edge(k,:,iq))
          ENDDO
       ENDDO
    ENDDO
  END SUBROUTINE baseATquad_edge_element



  !==================================================================
  ! Interpolation of the function value
  !==================================================================

  REAL(DP) FUNCTION interp(e,f,x)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "interp"
    CLASS(element), INTENT(in)                   :: e
    REAL(DP),           INTENT(in), DIMENSION(:) :: f  ! function values at DOFs
    REAL(DP),           INTENT(in), DIMENSION(:) :: x  ! barycentric coordinates
    INTEGER                                      :: i
    REAL(DP)                                     :: fval

    fval = 0._dp

    DO i = 1,e%nsommets
       fval = fval + base_element(e,i,x)*f(i)
    ENDDO

    interp = fval

  END FUNCTION interp

  !==================================================================
  ! Interpolation of the function derivative
  !==================================================================

  FUNCTION dinterp(e,f,x)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "dinterp"
    CLASS(element), INTENT(in)                        :: e
    REAL(DP),           INTENT(in), DIMENSION(:)  :: f  ! function values at DOFs
    REAL(DP),           INTENT(in), DIMENSION(:)  :: x  ! barycentric coordinates
    REAL(DP),                       DIMENSION(2)  :: dinterp
    INTEGER                                       :: i
    REAL(DP), DIMENSION(2)                        :: dfval

    dfval = 0._dp

    DO i = 1,e%nsommets
       dfval = dfval + gradient_element(e,i,x)*f(i)
    ENDDO

    dinterp = dfval

  END FUNCTION dinterp

  !==================================================================
  ! Average of the function over the element
  !==================================================================

  REAL(DP) FUNCTION average(e,f)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "average"
    CLASS(element), INTENT(in)                        :: e
    REAL(DP),           INTENT(in), DIMENSION(e%nsommets) :: f  ! function values at DOFs
    INTEGER :: i

    SELECT CASE(e%itype)
    CASE(0) ! P0
       average = f(1)

    CASE(1) ! P1
       average = SUM(f)/REAL(e%nsommets,dp)

    CASE(2) ! B2 Bezier
       average = SUM(f)/REAL(e%nsommets,dp)

    CASE(3)! P2
       IF (e%nvertex == 3) average = SUM(f(4:6))/3.0_dp
       IF (e%nvertex == 4) THEN
          average = SUM(f(1:4))/36._dp + SUM(f(5:8))/9._dp + f(9)*4._dp/9._dp
       END IF

    CASE(4) ! P3
       PRINT*, 'Average not implemented for P3'
       STOP

    CASE(5) ! B3
       average = SUM(f)/REAL(e%nsommets,dp)
    CASE default
       PRINT*, "Average: bad e%itype = ", e%itype
       STOP
    END SELECT

  END FUNCTION average


  FUNCTION eval_hess_element(e, u, x) RESULT(d2u)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "eval_hess_element"
    CLASS(element),                       INTENT(in):: e
    TYPE(PVar),    DIMENSION(e%nsommets), INTENT(in):: u
    REAL(DP),          DIMENSION(3),          INTENT(in):: x
    TYPE(PVar),    DIMENSION(n_dim,n_dim)           :: d2u
    REAL(DP),          DIMENSION(n_dim,n_dim,e%nsommets):: hess
    INTEGER :: k, l, j

    DO l=1, e%nsommets
       hess(:,:,l)=e%hessian(l,x)
    ENDDO
    DO k=1, n_vars
       DO l = 1, n_dim
          DO j=1, n_dim
             d2u(l,j)%u(k)=SUM(hess(l,j,:)*u%u(k))
          END DO
       END DO
    END DO


  END FUNCTION eval_hess_element





  !==================================================================
  ! Evaluation of local mass matrix
  !==================================================================

  SUBROUTINE matrix_element(e)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "matrix_element"
    CLASS(element), INTENT(inout)           :: e
    REAL(DP), DIMENSION(e%nsommets, e%nsommets) :: MatLoc
    REAL(DP), DIMENSION(e%nsommets)             :: base
    REAL(DP)    :: uloc, h, xx, yy, nmat
    INTEGER :: i, iq, l, k

    MatLoc = 0.
    ALLOCATE(e%MatLoc(e%nsommets,e%nsommets))

    DO i = 1,e%nsommets
       DO iq = 1,e%nquad
          DO l = 1,e%nsommets
             base(l) = e%base(l,e%quad(:,iq))
          ENDDO

          DO k = 1,e%nsommets
             MatLoc(i,k) = MatLoc(i,k) + base(i) * base(k) * e%weight(iq)
          ENDDO
       ENDDO
    ENDDO

    MatLoc   = MatLoc*e%volume

    e%MatLoc = MatLoc

  END SUBROUTINE matrix_element

  !==================================================================
  ! Get barycentric coordinates of DOFs
  !==================================================================

  SUBROUTINE getBarycentricCoor(e)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "getBarycentric"
    CLASS(element), INTENT(inout) :: e
    INTEGER :: k

    !    ALLOCATE(e%coorL(e%nvertex,e%nsommets))
    SELECT CASE(e%nvertex)

       !-----------------------------
       ! for triangles
    CASE(3)
       SELECT CASE(e%itype)
       CASE(0) ! P0
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 1._dp/3._dp
                e%coorL(2,k) = 1._dp/3._dp
                e%coorL(3,k) = 1._dp/3._dp
             CASE default
                PRINT*, "P0, numero base ", k
                STOP
             END SELECT
          END DO ! k

       CASE(1) ! P1
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 1.0_dp
             CASE default
                PRINT*, "P1, numero base ", k
                STOP
             END SELECT
          END DO ! k

       CASE(2,3) ! B2 or P2
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 1.0_dp
             CASE(4)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 0.5_dp
                e%coorL(3,k) = 0.0_dp
             CASE(5)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.5_dp
                e%coorL(3,k) = 0.5_dp
             CASE(6)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 0.5_dp
             CASE default
                PRINT*, "B2, numero base ", k
                STOP
             END SELECT
          END DO ! k
       CASE(4,5) ! P3
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
                e%coorL(3,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 1.0_dp
             CASE(4)
                e%coorL(1,k) = 2._dp/3._dp
                e%coorL(2,k) = 1._dp/3._dp
                e%coorL(3,k) = 0.0_dp
             CASE(5)
                e%coorL(1,k) = 1._dp/3._dp
                e%coorL(2,k) = 2._dp/3._dp
                e%coorL(3,k) = 0.0_dp
             CASE(6)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 2._dp/3._dp
                e%coorL(3,k) = 1._dp/3._dp
             CASE(7)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1._dp/3._dp
                e%coorL(3,k) = 2._dp/3._dp
             CASE(8)
                e%coorL(1,k) = 1._dp/3._dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 2._dp/3._dp
             CASE(9)
                e%coorL(1,k) = 2._dp/3._dp
                e%coorL(2,k) = 0.0_dp
                e%coorL(3,k) = 1._dp/3._dp
             CASE(10)
                e%coorL(1,k) = 1._dp/3._dp
                e%coorL(2,k) = 1._dp/3._dp
                e%coorL(3,k) = 1._dp/3._dp
             CASE default
                PRINT*, "B3, numero base ", k
                STOP
             END SELECT
          END DO ! k

       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

       !-----------------------------
       ! for quads
    CASE(4)

       SELECT CASE(e%itype)
       CASE(0) ! P0
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 0.5_dp
             CASE default
                PRINT*, "P0, numero base ", k
                STOP
             END SELECT
             e%coorL(3:4,k) = 1.0 - e%coorL(1:2,k)
          END DO ! k

       CASE(1) ! P1
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(4)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE default
                PRINT*, "P1, numero base ", k
                STOP
             END SELECT
             e%coorL(3:4,k) = 1.0_dp - e%coorL(1:2,k)
          END DO ! k

       CASE(2,3) ! B2 or P2
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(4)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(5)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 0.0_dp
             CASE(6)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.5_dp
             CASE(7)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 1.0_dp
             CASE(8)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.5_dp
             CASE(9)
                e%coorL(1,k) = 0.5_dp
                e%coorL(2,k) = 0.5_dp
             CASE default
                PRINT*, "B2, numero base ", k
                STOP
             END SELECT
          END DO !k
       CASE(4,5) ! P3 or B3
          DO k = 1,e%nsommets
             SELECT CASE(k)
             CASE(1)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(2)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(3)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(4)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(5)
                e%coorL(1,k) = 1.0_dp/3.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(6)
                e%coorL(1,k) = 2.0_dp/3.0_dp
                e%coorL(2,k) = 0.0_dp
             CASE(7)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 1.0_dp/3.0_dp
             CASE(8)
                e%coorL(1,k) = 1.0_dp
                e%coorL(2,k) = 2.0_dp/3.0_dp
             CASE(9)
                e%coorL(1,k) = 2.0_dp/3.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(10)
                e%coorL(1,k) = 1.0_dp/3.0_dp
                e%coorL(2,k) = 1.0_dp
             CASE(11)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 2.0_dp/3.0_dp
             CASE(12)
                e%coorL(1,k) = 0.0_dp
                e%coorL(2,k) = 1.0_dp/3.0_dp
             CASE(13)
                e%coorL(1,k) = 1.0_dp/3.0_dp
                e%coorL(2,k) = 1.0_dp/3.0_dp
             CASE(14)
                e%coorL(1,k) = 2.0_dp/3.0_dp
                e%coorL(2,k) = 1.0_dp/3.0_dp
             CASE(15)
                e%coorL(1,k) = 2.0_dp/3.0_dp
                e%coorL(2,k) = 2.0_dp/3.0_dp
             CASE(16)
                e%coorL(1,k) = 1.0_dp/3.0_dp
                e%coorL(2,k) = 2.0_dp/3.0_dp
             CASE default
                PRINT*, "P3/B3, numero base ", k
                STOP
             END SELECT
             e%coorL(3:4,k) = 1.0_dp - e%coorL(1:2,k)
          END DO ! k



       CASE default
          PRINT*, "Type non existant", e%itype
          STOP
       END SELECT

    CASE default
       PRINT*, mod_name

       PRINT*, "L=2185, Wrong number of vertices", e%nvertex
       STOP
    END SELECT

  END SUBROUTINE getBarycentricCoor

  !==================================================================
  ! Convert physical coordinates to barycentric
  !==================================================================

  FUNCTION convertPhys2BarycCoor(e,x,y) RESULT(L)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "convertPhys2BarycCoor"
    CLASS(element), INTENT(inout) :: e
    REAL(DP),           INTENT(in)    :: x, y
    REAL(DP), DIMENSION(e%nvertex)    :: L
    INTEGER :: i, j, k
    REAL(DP):: a, b, c;

    SELECT CASE(e%nvertex)
    CASE(3)
       DO i = 1, 3
          j = e%next(i);
          k = e%next(j);
          a = e%coor(1,j)*e%coor(2,k) - e%coor(1,k)*e%coor(2,j)
          b = e%coor(2,j) - e%coor(2,k)
          c = e%coor(1,k) - e%coor(1,j)
          L(i) = (a + b*x + c*y)/(2.0*e%volume);
       END DO
    CASE(4)
       L(1) = (x-e%coor(1,1))/(e%coor(1,2)-e%coor(1,1))
       L(2) = (y-e%coor(2,2))/(e%coor(2,3)-e%coor(2,2))
       L(3:4) = 1.0_dp - L(1:2)
    CASE default
    END SELECT

  END FUNCTION convertPhys2BarycCoor

  !==================================================================
  ! Values of the basis function at the physical DOFs
  ! (needed to convert the coefficients from Lagrangian to Bezier basis,
  !  e.g. for the initialization of the fields)
  !==================================================================

  SUBROUTINE base_ref_element(e)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "base_ref_element"
    CLASS(element), INTENT(inout)::e
    REAL(DP), PARAMETER      :: ut=1._dp/3._dp, det=1._dp-ut
    INTEGER              :: l, l_base, l_dof
    REAL(DP), DIMENSION(3,10):: x ! barycentric coordinates for B3
    REAL(DP), DIMENSION(e%nvertex,e%nsommets):: xL ! coordinates of the dofs

    ! base_at_dofs( number of the basis function, point l)
    e%base_at_dofs=0._dp

    SELECT CASE(e%nvertex)

       !---------------------
       ! for triangles
    CASE(3)

       SELECT CASE(e%itype)

       CASE(0) ! P0
          e%base_at_dofs(1,1)=1.0_dp

       CASE(1) ! P1
          DO l=1,3
             e%base_at_dofs(l,l)=1.0_dp
          ENDDO

       CASE(2) ! B2 Bezier
          DO l=1,3
             e%base_at_dofs(l,l)=1.0_dp
          ENDDO
          ! dof_4
          e%base_at_dofs(1,4)=1._dp/4._dp
          e%base_at_dofs(2,4)=1._dp/4._dp
          e%base_at_dofs(4,4)=1._dp/2._dp
          !dof_5
          e%base_at_dofs(2,5)=1._dp/4._dp
          e%base_at_dofs(3,5)=1._dp/4._dp
          e%base_at_dofs(5,5)=1._dp/2._dp
          !dof_6
          e%base_at_dofs(1,6)=1._dp/4._dp
          e%base_at_dofs(3,6)=1._dp/4._dp
          e%base_at_dofs(6,6)=1._dp/2._dp

       CASE(3) ! P2
          DO l=1,6
             e%base_at_dofs(l,l)=1.0_dp
          ENDDO

       CASE(4) ! P3
          DO l=1,10
             e%base_at_dofs(l,l)=1.0_dp
          ENDDO

       CASE(5) ! B3
          x=0.
          x(1,1)=1.0_dp;x(2,2)=1.0_dp;x(3,3)=1.0_dp
          x(1,4)=det; x(2,4)=ut
          x(1,5)=ut; x(2,5)=det
          x(2,6)=det; x(3,6)=ut
          x(2,7)=ut; x(3,7)=det
          x(1,8)=ut; x(3,8)=det
          x(1,9)=det; x(3,9)=ut
          x(:,10)=ut

          DO l_dof=1,10
             DO l_base=1,10
                e%base_at_dofs(l_base,l_dof)=base_element(e,l_base,x(:,l_dof))
             ENDDO
          ENDDO

10        FORMAT(i2,10(1x,f10.4))
       CASE default
          PRINT*, "Element type not yet take into account in base_ref_element", e%itype
          STOP
       END SELECT

       !---------------------
       ! for quads
    CASE(4)

       xL = e%coorL
       DO l_dof=1,e%nsommets
          DO l_base=1,e%nsommets
             e%base_at_dofs(l_base,l_dof)=base_element(e,l_base,xL(:,l_dof))
          ENDDO
       ENDDO

    CASE default
       PRINT*, mod_name
       PRINT*, 'L=2318, Wrong e%nvertex = ', e%nvertex

    END SELECT

    e%inv_base_at_dofs=inverse(e%base_at_dofs)

  END SUBROUTINE base_ref_element


  SUBROUTINE grad_ref_element(e)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "grad_ref_eleemnts"
    CLASS(element), INTENT(in):: e
    INTEGER:: i,j,k
    DO i=1, 2!ndim
       DO j=1, e%nvertex
          DO k=1, e%nsommets
             e%grad_at_dofs(:,j,k)=e%gradient(k,e%coorL(:,j))
          ENDDO
       ENDDO
    ENDDO
  END SUBROUTINE grad_ref_element

  SUBROUTINE eval_coeff_element(e)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "eval_coefff_element"
    ! evaluation of the inverse of the local Galerkin mass matrix
    ! evaluation of the integrals \int_K phi_i * nabla phi_j dx
    CLASS(element), INTENT(inout):: e

    REAL(dp), DIMENSION(e%nsommets)     :: base
    REAL(dp), DIMENSION(n_dim,e%nsommets):: grad
    REAL(dp), DIMENSION(2):: a,b,c, x
    REAL(dp), DIMENSION(2,2):: Jac
    REAL(dp):: vol
    INTEGER:: i, iq, l, k, ll, k1, k2
    REAL(dp), DIMENSION(2):: n
    REAL(DP), DIMENSION(:),ALLOCATABLE:: yy
    NULLIFY(e%masse,e%coeff)
    ALLOCATE(e%masse(e%nsommets,e%nsommets))
    ALLOCATE(e%coeff(e%nsommets,e%nsommets,n_dim))
    e%masse=0._dp;e%coeff=0._dp


    SELECT CASE(e%nvertex)
    CASE(3) ! triangle
       DO i=1, e%nsommets

          DO iq=1, e%nquad
             DO l=1, e%nsommets

                base(l  )=e%base    (l, e%quad(:,iq))
                grad(:,l)=e%gradient(l, e%quad(:,iq))

             ENDDO

             DO k=1,e%nsommets

                e%masse(i,k)= e%masse(i,k)+   ( &
                     & ( base(i) )  *base(k)&
                     &)*e%weight(iq)

                !                                e%coeff(i,k,:)=e%coeff(i,k,:)+ &
                !                                     & base(i)*grad(:,k) *e%weight(iq)

             ENDDO
          ENDDO
       ENDDO

       e%masse=e%masse * e%volume
       !             e%coeff=e%coeff * e%volume


       DO i=1, e%nsommets

          DO iq=1, e%nquad

             DO l=1, e%nsommets

                base(l  )=e%base    (l, e%quad(:,iq) )
                grad(:,l)=e%gradient(l, e%quad(:,iq) )

             ENDDO
             DO k=1,e%nsommets


                e%coeff(k,i,:)=e%coeff(k,i,:)- &
                     & base(i)*grad(:,k) *e%weight(iq)

             ENDDO
          ENDDO
       ENDDO
       e%coeff=e%coeff * e%volume
       DO k=1, e%nvertex
          k1=e%next(k)
          k2=e%next(k1)
          ! normale exterieure
          n(1)= ( e%coor(2,k2)-e%coor(2,k1)) !?-
          n(2)=-(e%coor(1,k2)-e%coor(1,k1)) !?+

          DO iq=1,e%nquad_edge


             DO l=1,e%nsommets
                base(l)=e%base(l,e%quad_edge(k,:,iq) )
             ENDDO

             DO l=1, e%nsommets
                DO ll=1, e%nsommets
                   e%coeff(l,ll,:)=e%coeff(l,ll,:)+base(l)*base(ll)*n*e%weight_edge(k,iq)
                ENDDO
             ENDDO

          ENDDO !iq
       ENDDO !edge


    CASE(4) ! quadrangle


       DO i=1, e%nsommets

          DO iq=1, e%nquad
             x=e%quad(1:2,iq)
             DO l=1, e%nsommets

                base(l  )=e%base    (l, x )
                grad(:,l)=e%gradient(l, x )

             ENDDO
             !             Jac(1,1)=a(1)+x(2)*c(1); Jac(1,2)=b(1)+x(1)*c(1)
             !             Jac(2,1)=a(2)+x(2)*c(2); Jac(2,2)=b(2)+x(2)*c(2)
             Jac=d_iso_element(e,x)
             vol=ABS( Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1) )
             DO k=1,e%nsommets

                e%masse(i,k)= e%masse(i,k)+   ( &
                     & ( base(i) )  *base(k)&
                     &)*e%weight(iq)*vol

                e%coeff(i,k,:)=e%coeff(i,k,:)- &
                     & base(k)*grad(:,i) *e%weight(iq)*vol

             ENDDO
          ENDDO
       ENDDO
       DO k=1, e%nvertex
          k1=e%edge(1,k)
          k2=e%edge(2,k)
          n(1)= (e%coor(2,k2)-e%coor(2,k1)) ! exterior normal
          n(2)=-(e%coor(1,k2)-e%coor(1,k1))
          DO iq=1,e%nquad_edge


             DO l=1,e%nsommets
                base(l)=e%base(l,e%quad_edge(k,:,iq) )
             ENDDO

             DO l=1, e%nsommets
                DO ll=1, e%nsommets
                   e%coeff(l,ll,:)=e%coeff(l,ll,:)+base(l)*base(ll)*n*e%weight_edge(k,iq)
                ENDDO
             ENDDO

          ENDDO !iq
       ENDDO !edge

    CASE default
       PRINT*, mod_name, " this element does not exists"
       STOP
    END SELECT

  END SUBROUTINE eval_coeff_element

  FUNCTION iso_element(e,x) RESULT(y)
    ! isoparametric transformation for quads
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "iso_element"
    CLASS(element), INTENT(in):: e
    REAL(dp), DIMENSION(:):: x
    REAL(dp), DIMENSION(2):: y
    REAL(dp), DIMENSION(2):: a,b,c
    REAL(dp), DIMENSION(2,7):: f,g
    INTEGER:: l
    SELECT CASE(e%nvertex)
    CASE(4)! quad
       SELECT CASE(e%nsommets)
       CASE(4,9,16)
          a=e%coor(:,2)-e%coor(:,1)
          b=e%coor(:,4)-e%coor(:,1)
          c=e%coor(:,3)-e%coor(:,2)+e%coor(:,1)-e%coor(:,4)
          y=e%coor(:,1)+a*x(1)+b*x(2)+c*x(1)*x(2)
       CASE(-9)
          DO l=2,8
             f(:,l-1)=e%coor(:,l)-e%coor(:,1)
          ENDDO
          g(1,:)=MATMUL(AA,f(1,:))
          g(2,:)=MATMUL(AA,f(2,:))
          y=e%coor(:,1)+x(1)*(&
               & g(:,1)+x(1)*g(:,3)+0.5_dp*x(2)*g(:,4)+x(1)*x(2)*g(:,6) &
               &)+ x(2)*( &
               & g(:,2)+x(2)*g(:,5)+0.5_dp*x(1)*g(:,4)+x(1)*x(2)*g(:,7) )
       END SELECT
    CASE(3)! tri
       y(1)=SUM(x(1:e%nvertex)*e%coor(1,1:e%nvertex))
       y(2)=SUM(x(1:e%nvertex)*e%coor(2,1:e%nvertex))
    CASE default
       PRINT*, mod_name, " this element does not exist"
    END SELECT
  END FUNCTION iso_element

  FUNCTION d_iso_element(e,x) RESULT(jac)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "iso_element"
    CLASS(element), INTENT(in):: e
    REAL(dp), DIMENSION(:):: x
    REAL(dp), DIMENSION(2):: y
    REAL(dp), DIMENSION(2):: a,b,c
    REAL(dp), DIMENSION(2,7):: f,g
    REAL(dp), DIMENSION(2,2):: Jac
    INTEGER:: l
    SELECT CASE(e%nvertex)
    CASE(4)! assumes linear elements
       SELECT CASE(e%nsommets)
       CASE(4,9,16)
          a=e%coor(:,2)-e%coor(:,1)
          b=e%coor(:,4)-e%coor(:,1)
          c=e%coor(:,3)-e%coor(:,2)+e%coor(:,1)-e%coor(:,4)
          Jac(:,1)=a+x(2)*c
          Jac(:,2)=b+x(1)*c
       CASE(-9)
          DO l=2,8
             f(:,l-1)=e%coor(:,l)-e%coor(:,1)
          ENDDO
          g(1,:)=MATMUL(AA,f(1,:))
          g(2,:)=MATMUL(AA,f(2,:))

          Jac(:,1)=g(:,1)+2._dp*x(1)*( g(:,3)+g(:,6)*x(2) ) +x(2)* (g(:,4)+x(2)*g(:,7) )
          Jac(:,2)=g(:,2)+x(1)*( g(:,4)+x(1)* g(:,6) )+2._dp*x(2)*( g(:,5)+x(1)*g(:,7) )
       END SELECT
    CASE default
       PRINT*, mod_name, " this element does not exist"
    END SELECT
  END FUNCTION d_iso_element

  SUBROUTINE deallocate_element(e) ! pedantic
    CHARACTER(LEN = *), PARAMETER :: mod_name = "deallocate_element"
    TYPE(element), INTENT(inout) :: e
    IF(ASSOCIATED(e%segm))              NULLIFY(e%segm)
    IF(ASSOCIATED(e%nsegm))             NULLIFY(e%nsegm)
    IF(ASSOCIATED(e%neigh))             NULLIFY(e%neigh)
    IF (ASSOCIATED(e%coor))             NULLIFY(e%coor)
    IF (ASSOCIATED(e%coorL))            NULLIFY(e%coorL)
    IF (ASSOCIATED(e%nu))               NULLIFY(e%nu)
    IF (ASSOCIATED(e%y))                 NULLIFY(e%y)
    IF (ASSOCIATED(e%yy))                NULLIFY(e%yy)
    IF (ASSOCIATED(e%c_el))             NULLIFY(e%c_el)
    IF (ASSOCIATED(e%n))                NULLIFY(e%n)
    IF (ASSOCIATED(e%base_at_dofs))     NULLIFY(e%base_at_dofs)
    IF (ASSOCIATED(e%inv_base_at_dofs)) NULLIFY(e%inv_base_at_dofs)
    IF (ASSOCIATED(e%grad_at_quad))     NULLIFY(e%grad_at_quad)
    IF (ASSOCIATED(e%base_at_quad))     NULLIFY(e%base_at_quad)
    IF (ASSOCIATED(e%quad))             NULLIFY(e%quad)
    IF (ASSOCIATED(e%weight))           NULLIFY(e%weight)
    IF (ASSOCIATED(e%quad_edge))        NULLIFY(e%quad_edge)
    IF (ASSOCIATED(e%weight_edge))      NULLIFY(e%weight_edge)
    IF (ASSOCIATED(e%edge))             NULLIFY(e%edge)
    IF (ASSOCIATED(e%MatLoc))           NULLIFY(e%MatLoc)
    IF (ASSOCIATED(e%MatLocInv))        NULLIFY(e%MatLocInv)
    IF (ASSOCIATED(e%poids))            NULLIFY(e%poids)
    IF (ASSOCIATED(e%MatLoc_lumped))    NULLIFY(e%MatLoc_lumped)
    IF (ASSOCIATED(e%coeff))            NULLIFY(e%coeff)
    IF (ASSOCIATED(e%masse))            NULLIFY(e%masse)

  END SUBROUTINE deallocate_element


END MODULE element_class
