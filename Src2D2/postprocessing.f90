!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE postprocessing

  ! This module is intended to output the postprocessing files with the data to generate plots.
  ! The data can in general easily read via visit (as example)
  ! In the data files are printed ALL the DOFs of the domain 

  USE variable_def
  USE param2d
  USE utils
  USE Model
  USE PRECISION

  IMPLICIT NONE

CONTAINS
  SUBROUTINE visu(DATA,Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of triangles only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(inout):: Mesh
    TYPE(variables), INTENT(in):: Var

    INTEGER:: n_tri, n_quad,TYPE,i

    n_tri=COUNT(Mesh%e(:)%nvertex==3)
    n_quad=COUNT(Mesh%e(:)%nvertex==4)

    IF (n_tri==0 ) THEN
       TYPE=1 ! only quad
    ELSE IF (n_quad==0) THEN
       TYPE=2 ! only tri
    ELSE
       TYPE=3 ! mixed
    ENDIF

    SELECT CASE (TYPE)

    CASE(1)
       !       CALL visu_quad(DATA, Lnombre, Mesh, Var)
       CALL visu_quad_sub(DATA, Lnombre, Mesh, Var)
    CASE(2)
       CALL visu_tri(DATA, Lnombre, Mesh, Var)
    CASE(3)
       CALL visu_mixed(DATA, Lnombre, Mesh, Var)
    CASE default
       PRINT*, mod_name
       PRINT*, "wrong case. We assume a full triangle or a full quad mesh"
    END SELECT
  END SUBROUTINE visu



  SUBROUTINE visu_tri(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_tri"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of triangles only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss
    REAL(dp), DIMENSION(:,:),ALLOCATABLE,SAVE:: indic


    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max

    TYPE(PVar) :: sol


    TYPE(PVar), DIMENSION(:), ALLOCATABLE, SAVE :: W ! solution in primitive variables

    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    SELECT CASE(itype)
    CASE(1)
       IF(.NOT.ALLOCATED(nu2)) THEN

          ALLOCATE(nu2(3,Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(indic(3,Mesh%nt))
       ENDIF

    CASE(2,3)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(3,4*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(indic(3,4*Mesh%nt))
       ENDIF
    CASE(4,5)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(3,9*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(indic(3,9*Mesh%nt))
       ENDIF
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L 114 ordre non pris en compte, type d'element: ", itype
       STOP
    END SELECT




    ip1(1)=2; ip1(2)=3; ip1(3)=1
    SELECT CASE(itype)
    CASE(1)
       DO jt=1, Mesh%nt
          nu2(1:3,jt)=Mesh%e(jt)%nu(1:3)
          coor(1,nu2(1:3,jt))=mesh%e(jt)%coor(1,:)
          coor(2,nu2(1:3,jt))=mesh%e(jt)%coor(2,:)
          indic(1,jt)=Mesh%e(jt)%type_flux
          indic(2,jt)=Mesh%e(jt)%diag
          indic(3,jt)=Mesh%e(jt)%diag2
          nt2=Mesh%nt
       ENDDO
    CASE(2,3)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)

          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          jtt=jtt+1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(6)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(5)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(6)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(6)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2
       ENDDO
       nt2=jtt

    CASE(4,5)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          jtt=jtt+1!1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1! 2
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(10)
          nu2(3,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!3
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(10)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!4
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(6)
          nu2(3,jtt)=iss(10)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!5
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(6)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!6
          nu2(1,jtt)=iss(6)
          nu2(2,jtt)=iss(7)
          nu2(3,jtt)=iss(10)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!7
          nu2(1,jtt)=iss(10)
          nu2(2,jtt)=iss(7)
          nu2(3,jtt)=iss(8)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!8
          nu2(1,jtt)=iss(10)
          nu2(2,jtt)=iss(8)
          nu2(3,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1!9
          nu2(1,jtt)=iss(7)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(8)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2


       ENDDO
       nt2=jtt
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L215 pas encore implemente"
    END SELECT
    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution

    ! Recalculate solution at vertices for B2 case


    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)
va2(e%nu)=Control_to_Cons(var%ua(0,e%nu),e)
!!$       DO i = 1,e%nsommets
!!$
!!$
!!$          DO j=1, n_vars
!!$             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j)*e%base_at_dofs(:,i))
!!$          ENDDO
!!$       END DO ! i

    END DO ! jt






    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )


    IF (ios /= 0) THEN
       PRINT*, mod_name
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,*)'VARIABLES = "x","y", "ro", "u", "v", "p", "Flux_indic", "Diag_ind" , "Diag2"'
    WRITE (10,*) 'ZONE DATAPACKING=BLOCK, ZONETYPE=FETRIANGLE, ', &
         'VARLOCATION=([7-9]=CELLCENTERED), ',      &
         'N=',Var%ncells , ' E=',nt2






    ! write data in Tecplot format

    DO is = 1, Var%ncells
       W(is) = va2(is)%cons2prim()
    END DO

    WRITE(10,130) (coor(1,is),is=1, Var%ncells)
    WRITE(10,130) (coor(2,is),is=1, Var%ncells)
    WRITE(10,130) (( W(is)%u(l),is=1, Var%ncells),l=1,n_vars)
    WRITE(10,130) ((indic(l,jt),jt=1,Nt2),l=1,3)
    ! numéros des noeuds de chaque triangle (dans l'ordre où l'on écrit les coordonnées)
    WRITE(10,*) ((nu2(l,jt), l=1,3), jt=1,nt2)

100 FORMAT(6(e16.8))
110 FORMAT(a)
130 FORMAT(4e20.10) 

    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_tri

  SUBROUTINE visu_quad_sub(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_quad_sub"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of quad2 only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss
    REAL(dp), DIMENSION(:,:),ALLOCATABLE,SAVE:: indic

    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max

    TYPE(PVar) :: sol

    TYPE(PVar), DIMENSION(:), ALLOCATABLE, SAVE :: W ! solution in primitive variables


    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    SELECT CASE(itype)
    CASE(1)
       IF(.NOT.ALLOCATED(nu2)) THEN

          ALLOCATE(nu2(4,Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(indic(3,Mesh%nt))
       ENDIF

    CASE(2,3)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(4,4*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(indic(3,4*Mesh%nt))
       ENDIF
    CASE(4,5)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(4,9*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
          ALLOCATE(W(Var%ncells))
          ALLOCATE(indic(3,9*Mesh%nt))
       ENDIF
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L 114 ordre non pris en compte, type d'element: ", itype
       STOP
    END SELECT



    SELECT CASE(itype)
    CASE(1)
       nt2=Mesh%nt
       DO jt=1, Mesh%nt
          nu2(:,jt)=Mesh%e(jt)%nu(:)
          coor(1,nu2(:,jt))=mesh%e(jt)%coor(1,:)
          coor(2,nu2(:,jt))=mesh%e(jt)%coor(2,:)
          indic(1,jt)=Mesh%e(jt)%type_flux
          indic(2,jt)=Mesh%e(jt)%diag
          indic(3,jt)=Mesh%e(jt)%diag2
       ENDDO
    CASE(2,3) !Q2
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)

          jtt=jtt+1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(9)
          nu2(4,jtt)=iss(8)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(6)
          nu2(4,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(6)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(7)
          nu2(4,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          jtt=jtt+1
          nu2(1,jtt)=iss(7)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(8)
          nu2(4,jtt)=iss(9)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2
       ENDDO
       nt2=jtt

    CASE(4,5)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          !1          
          jtt=jtt+1
          nu2(1,jtt)=iss(1 )
          nu2(2,jtt)=iss(5 )
          nu2(3,jtt)=iss(13)
          nu2(4,jtt)=iss(12)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !2
          jtt=jtt+1
          nu2(1,jtt)=iss(5 )
          nu2(2,jtt)=iss(6 )
          nu2(3,jtt)=iss(14)
          nu2(4,jtt)=iss(13)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !3
          jtt=jtt+1
          nu2(1,jtt)=iss(6 )
          nu2(2,jtt)=iss(2 )
          nu2(3,jtt)=iss(7 )
          nu2(4,jtt)=iss(14)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !4
          jtt=jtt+1
          nu2(1,jtt)=iss(12)
          nu2(2,jtt)=iss(13)
          nu2(3,jtt)=iss(16)
          nu2(4,jtt)=iss(11)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !5
          jtt=jtt+1
          nu2(1,jtt)=iss(13)
          nu2(2,jtt)=iss(14)
          nu2(3,jtt)=iss(15)
          nu2(4,jtt)=iss(16)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !6
          jtt=jtt+1
          nu2(1,jtt)=iss(14)
          nu2(2,jtt)=iss(7 )
          nu2(3,jtt)=iss(8 )
          nu2(4,jtt)=iss(15)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !7
          jtt=jtt+1
          nu2(1,jtt)=iss(11)
          nu2(2,jtt)=iss(16)
          nu2(3,jtt)=iss(10)
          nu2(4,jtt)=iss(4)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !8
          jtt=jtt+1
          nu2(1,jtt)=iss(16)
          nu2(2,jtt)=iss(15)
          nu2(3,jtt)=iss(9 )
          nu2(4,jtt)=iss(10)
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2

          !9
          jtt=jtt+1
          nu2(1,jtt)=iss(15)
          nu2(2,jtt)=iss(8 )
          nu2(3,jtt)=iss(3 )
          nu2(4,jtt)=iss(9 )
          indic(1,jtt)=Mesh%e(jt)%type_flux
          indic(2,jtt)=Mesh%e(jt)%diag
          indic(3,jtt)=Mesh%e(jt)%diag2



       ENDDO
       nt2=jtt
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L215 pas encore implemente"
    END SELECT

    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution


    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)
va2(e%nu)=Control_to_Cons(var%ua(0,e%nu),e)
!!$       DO i = 1,e%nsommets
!!$
!!$
!!$          DO j=1, n_vars
!!$             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j)*e%base_at_dofs(:,i))
!!$          ENDDO
!!$       END DO ! i

    END DO ! jt


 


    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )

    IF (ios /= 0) THEN
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,110)'VARIABLES = "x","y", "ro", "u", "v", "p", "Flux_indic", "Diag_ind" , "Diag2"'
    WRITE (10,*) 'ZONE DATAPACKING=BLOCK, ZONETYPE=FEQUADRILATERAL, ', &
         'VARLOCATION=([7-9]=CELLCENTERED), ', 'N='//TRIM(ADJUSTL(nombre1))//&
         &     ', E='//TRIM(ADJUSTL(nombre2))

    ! write data in Tecplot format

    DO is = 1, Var%ncells
       W(is) = va2(is)%cons2prim()
    END DO

    WRITE(10,130) (coor(1,is),is=1, Var%ncells)
    WRITE(10,130) (coor(2,is),is=1, Var%ncells)
    WRITE(10,130) (( W(is)%u(l),is=1, Var%ncells),l=1,n_vars)
    WRITE(10,130) ((indic(l,jt),jt=1,Nt2),l=1,3)
    ! numéros des noeuds de chaque triangle (dans l'ordre où l'on écrit les coordonnées)
    WRITE(10,*) ((nu2(l,jt), l=1,4), jt=1,nt2)

100 FORMAT(6(e16.8))
110 FORMAT(a)
130 FORMAT(4e20.10) 
    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_quad_sub

  SUBROUTINE visu_mixed(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_mixed"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of quad2 only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(inout):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2

    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va_tri, va_quad
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(dp), DIMENSION(:,:),ALLOCATABLE::  indic_quad, indic_tri
    INTEGER, ALLOCATABLE, DIMENSION(:,:):: nu_tri, nu_quad

    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 100) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, &
         &nvar_str, format_str, vars3, format_str1,zoneT, zoneQ, titre,&
         & variable
    TYPE(element):: e, et
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max
    INTEGER:: itriquad, n_t,n_q, jt_tri, jt_q, ns_tri, ns_quad, js

    TYPE(PVar) :: sol
    TYPE(maillage):: triquad
    REAL(dp), DIMENSION(:,:), ALLOCATABLE:: c_tri,c_quad
    INTEGER, ALLOCATABLE, DIMENSION(:):: iloc,jloc, kloc
    TYPE(PVar), DIMENSION(:), ALLOCATABLE :: W, W2 ! solution in primitive variables




    itriquad=0
    DO jt=1, Mesh%nt
       SELECT CASE(DATA%itype)
       CASE(1)
          itriquad=itriquad+1
       CASE(2,3)
          itriquad=itriquad+4
       CASE(4,5)
          itriquad=itriquad+9
       END SELECT
    ENDDO

    triquad%nt=itriquad
    ALLOCATE(triquad%e(itriquad), va_tri(Mesh%ndofs), va_quad(Mesh%ndofs))
    ALLOCATE(W(Mesh%ndofs),w2(Mesh%ndofs) )

    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)
       w2(e%nu)=Control_to_Cons(var%ua(0,e%nu),e)
    END DO ! jt
    DO is=1, Mesh%ndofs
       w(is)=w2(is)%cons2prim()
    ENDDO
    DEALLOCATE(w2)


    WRITE(nombre, *) lnombre

    itype=DATA%itype
    ALLOCATE(coor(2,Var%ncells))

    SELECT CASE(itype)
    CASE(1)
       nt2=Mesh%nt
       DO jt=1, Mesh%nt
          e=Mesh%e(jt)

          triquad%e(jt)%nvertex=Mesh%e(jt)%nvertex
          triquad%e(jt)%nsommets=Mesh%e(jt)%nsommets
          ALLOCATE(Triquad%e(jt)%nu(e%nsommets))

          triquad%e(jt)%nu=Mesh%e(jt)%nu

          triquad%e(jt)%type_flux= Mesh%e(jt)%type_flux
          triquad%e(jt)%diag     = Mesh%e(jt)%diag
          triquad%e(jt)%diag2    = Mesh%e(jt)%diag2


          coor(1,e%nu)=e%coor(1,:)
          coor(2,e%nu)=e%coor(2,:)
       ENDDO
    CASE(2,3) !Q2
       jtt=0
       DO jt=1,Mesh%nt
          e=mesh%e(jt)
          coor(1,e%nu)=e%coor(1,:)
          coor(2,e%nu)=e%coor(2,:)
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(1)
             triquad%e(jtt)%nu(2)=e%nu(5)
             triquad%e(jtt)%nu(3)=e%nu(9)
             triquad%e(jtt)%nu(4)=e%nu(8)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(1)
             triquad%e(jtt)%nu(2)=e%nu(4)
             triquad%e(jtt)%nu(3)=e%nu(6)

          END SELECT
          triquad%e(jtt)%type_flux= Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag     = Mesh%e(jt)%diag
          triquad%e(jtt)%diag2    = Mesh%e(jt)%diag2

          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(5)
             triquad%e(jtt)%nu(2)=e%nu(2)
             triquad%e(jtt)%nu(3)=e%nu(6)
             triquad%e(jtt)%nu(4)=e%nu(9)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(4)
             triquad%e(jtt)%nu(2)=e%nu(2)
             triquad%e(jtt)%nu(3)=e%nu(5)

          END SELECT
          triquad%e(jtt)%type_flux= Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag     = Mesh%e(jt)%diag
          triquad%e(jtt)%diag2    = Mesh%e(jt)%diag2

          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(6)
             triquad%e(jtt)%nu(2)=e%nu(3)
             triquad%e(jtt)%nu(3)=e%nu(7)
             triquad%e(jtt)%nu(4)=e%nu(9)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(5)
             triquad%e(jtt)%nu(2)=e%nu(3)
             triquad%e(jtt)%nu(3)=e%nu(6)

          END SELECT
          triquad%e(jtt)%type_flux= Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag     = Mesh%e(jt)%diag
          triquad%e(jtt)%diag2    = Mesh%e(jt)%diag2

          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(7)
             triquad%e(jtt)%nu(2)=e%nu(4)
             triquad%e(jtt)%nu(3)=e%nu(8)
             triquad%e(jtt)%nu(4)=e%nu(9)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(4)
             triquad%e(jtt)%nu(2)=e%nu(5)
             triquad%e(jtt)%nu(3)=e%nu(6)

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2
       ENDDO
       nt2=jtt

    CASE(4,5)
       jtt=0
       DO jt=1,Mesh%nt
          e=Mesh%e(jt)

          coor(1,e%nu)=e%coor(1,:)
          coor(2,e%nu)=e%coor(2,:)
          !1          

          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(1)
             triquad%e(jtt)%nu(2)=e%nu(5)
             triquad%e(jtt)%nu(3)=e%nu(13)
             triquad%e(jtt)%nu(4)=e%nu(12)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(1)
             triquad%e(jtt)%nu(2)=e%nu(4)
             triquad%e(jtt)%nu(3)=e%nu(9)
          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2



          !2
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(5)
             triquad%e(jtt)%nu(2)=e%nu(6)
             triquad%e(jtt)%nu(3)=e%nu(14)
             triquad%e(jtt)%nu(4)=e%nu(13)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(4 )
             triquad%e(jtt)%nu(2)=e%nu(10)
             triquad%e(jtt)%nu(3)=e%nu(9 )

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2


          !3
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(6)
             triquad%e(jtt)%nu(2)=e%nu(2)
             triquad%e(jtt)%nu(3)=e%nu(7)
             triquad%e(jtt)%nu(4)=e%nu(14)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(4 )
             triquad%e(jtt)%nu(2)=e%nu(5 )
             triquad%e(jtt)%nu(3)=e%nu(10)
          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2

          !4
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(12)
             triquad%e(jtt)%nu(2)=e%nu(13)
             triquad%e(jtt)%nu(3)=e%nu(16)
             triquad%e(jtt)%nu(4)=e%nu(11)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(5 )
             triquad%e(jtt)%nu(2)=e%nu(6 )
             triquad%e(jtt)%nu(3)=e%nu(10)
          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2


          !5
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(13)
             triquad%e(jtt)%nu(2)=e%nu(14)
             triquad%e(jtt)%nu(3)=e%nu(15)
             triquad%e(jtt)%nu(4)=e%nu(16)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(5)
             triquad%e(jtt)%nu(2)=e%nu(2)
             triquad%e(jtt)%nu(3)=e%nu(6)

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2


          !6
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(14)
             triquad%e(jtt)%nu(2)=e%nu(7 )
             triquad%e(jtt)%nu(3)=e%nu(8 )
             triquad%e(jtt)%nu(4)=e%nu(15)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(6 )
             triquad%e(jtt)%nu(2)=e%nu(7 )
             triquad%e(jtt)%nu(3)=e%nu(10)

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2


          !7
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(11)
             triquad%e(jtt)%nu(2)=e%nu(16)
             triquad%e(jtt)%nu(3)=e%nu(10)
             triquad%e(jtt)%nu(4)=e%nu(14)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(10)
             triquad%e(jtt)%nu(2)=e%nu(7 )
             triquad%e(jtt)%nu(3)=e%nu(8 )

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2


          !8
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(16)
             triquad%e(jtt)%nu(2)=e%nu(15)
             triquad%e(jtt)%nu(3)=e%nu(9 )
             triquad%e(jtt)%nu(4)=e%nu(10)

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(10)
             triquad%e(jtt)%nu(2)=e%nu(8 )
             triquad%e(jtt)%nu(3)=e%nu(9 )

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2

          !9
          jtt=jtt+1
          ALLOCATE(triquad%e(jtt)%nu(e%nvertex))
          triquad%e(jtt)%nvertex=e%nvertex
          triquad%e(jtt)%nsommets=e%nvertex
          SELECT CASE(e%nvertex)
          CASE(4)
             triquad%e(jtt)%nu(1)=e%nu(15)
             triquad%e(jtt)%nu(2)=e%nu(8 )
             triquad%e(jtt)%nu(3)=e%nu(3 )
             triquad%e(jtt)%nu(4)=e%nu(9 )

          CASE(3)
             triquad%e(jtt)%nu(1)=e%nu(7)
             triquad%e(jtt)%nu(2)=e%nu(3)
             triquad%e(jtt)%nu(3)=e%nu(8)

          END SELECT
          triquad%e(jtt)%type_flux=Mesh%e(jt)%type_flux
          triquad%e(jtt)%diag=Mesh%e(jt)%diag
          triquad%e(jtt)%diag2=Mesh%e(jt)%diag2



       ENDDO
       nt2=jtt
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L215 pas encore implemente"
    END SELECT
    ! now the points
    ! count the number of quad and tri
    n_t=COUNT(triquad%e(:)%nvertex==3)
    n_q=COUNT(triquad%e(:)%nvertex==4)
    ALLOCATE(nu_tri(3,n_t),nu_quad(4,n_q), indic_tri(3,n_t), indic_quad(3, n_q) )
    ! assign the quad and tri
    jt_tri=0
    jt_q=0
    DO jt=1, triquad%nt
       e=triquad%e(jt)
       SELECT CASE(e%nvertex)
       CASE(3)
          jt_tri=jt_tri+1
          nu_tri(:,jt_tri)=e%nu ! dans la numerotation globale
          indic_tri(1,jt_tri)=triquad%e(jt)%type_flux
          indic_tri(2,jt_tri)=triquad%e(jt)%diag
          indic_tri(3,jt_tri)=triquad%e(jt)%diag2
       CASE(4)
          jt_q=jt_q+1
          nu_quad(:,jt_q)=e%nu ! dans la numerotation globale
          indic_quad(1,jt_q)=triquad%e(jt)%type_flux
          indic_quad(2,jt_q)=triquad%e(jt)%diag
          indic_quad(3,jt_q)=triquad%e(jt)%diag2
       END SELECT
    ENDDO
    IF (jt_tri.NE.n_t) THEN
       WRITE(*,*) mod_name
       WRITE(*,*) ' the number of points belonging to triangles is not correct'
       WRITE(*,*) "it should be ", n_t, " we get ", jt_tri
       STOP
    ENDIF
    IF (jt_q.NE.n_q) THEN
       WRITE(*,*) mod_name
       WRITE(*,*) ' the number of points belonging to triangles is not correct'
       WRITE(*,*) "it should be ", n_q, " we get ", jt_q
       STOP
    ENDIF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! tri mesh
    ! pointers to see if a point is in the tri mesh
    ALLOCATE(iloc(mesh%ndofs), jloc(mesh%ndofs), kloc(Mesh%ndofs) )
    iloc=-1; jloc=-1; kloc=-1
    DO jt=1,n_t
       DO l=1,3
          IF (iloc(nu_tri(l,jt))==-1) iloc(nu_tri(l,jt))=0 ! dans la numerotation globale
       ENDDO
    ENDDO
    ! number of such points
    ns_tri=COUNT(iloc==0)
    ! allocate the pointer old number-> new numbers
    js=0
    DO is=1, Mesh%ndofs
       IF (iloc(is)==0) THEN
          js=js+1
          jloc(js)=is! nlle-> ancienne
          kloc(is)=js! ancienne-> nlle
       ENDIF
    ENDDO
    IF (js.NE.ns_tri) THEN
       WRITE(*,*) mod_name
       WRITE(*,*) ' the number of points belonging to triangles is not correct'
       WRITE(*,*) "it should be ", ns_tri, " we get ", js
       STOP
    ENDIF

    ALLOCATE(c_tri(2,ns_tri))

    DO jt=1, n_t

       DO l=1,3
          IF (iloc(nu_tri(l,jt))==0) THEN
             is=kloc(nu_tri(l,jt))
             c_tri(:,is)=coor(:, nu_tri(l,jt)  )
             iloc(nu_tri(l,jt))=-1 ! in order not to count it twice, and we have the index in the new connectivity

          ENDIF
       ENDDO
    ENDDO
    ! then we need to map the new indices
    DO jt=1, n_t
       nu_tri(:, jt)=kloc(nu_tri(:, jt))
    ENDDO
    DO is=1, ns_tri
       va_tri(is)=w(jloc(is))
    ENDDO

    ! now we should be all set for the triangle
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! quad mesh 
    ! pointers to see if a point is in the tri mesh

    iloc=-1; jloc=-1; kloc=-1
    DO jt=1,n_q
       DO l=1,4
          IF (iloc(nu_quad(l,jt))==-1) iloc(nu_quad(l,jt))=0 ! dans la numerotation globale
       ENDDO
    ENDDO
    ! number of such points
    ns_quad=COUNT(iloc==0)
    ! allocate the pointer old number-> new numbers
    js=0
    DO is=1, Mesh%ndofs
       IF (iloc(is)==0) THEN
          js=js+1
          jloc(js)=is! nlle-> ancienne
          kloc(is)=js! ancienne-> nlle
       ENDIF
    ENDDO
    IF (js.NE.ns_quad) THEN
       WRITE(*,*) mod_name
       WRITE(*,*) ' the number of points belonging to triangles is not correct'
       WRITE(*,*) "it should be ", ns_quad, " we get ", js
       STOP
    ENDIF

    ALLOCATE(c_quad(2,ns_quad))

    DO jt=1, n_q
       DO l=1,4
          IF (iloc(nu_quad(l,jt))==0) THEN
             is=kloc(nu_quad(l,jt))
             c_quad(:,is)=coor(:, (nu_quad(l,jt) ) )
             iloc(nu_quad(l,jt))=-1 ! in order not to count it twice, and we have the index in the new connectivity
          ENDIF
       ENDDO
    ENDDO
    ! then we need to map the new indices
    DO jt=1, n_q
       nu_quad(:, jt)=kloc(nu_quad(:, jt))
    ENDDO
    DO is=1, ns_quad
       va_quad(is)=w(jloc(is))
    ENDDO

    ! now we should be all set for the triangle
    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution







    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )

    IF (ios /= 0) THEN
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    titre='TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    variable='VARIABLES = "x","y", "ro", "u", "v", "p", "Flux_indic", "Diag_ind" , "Diag2"'

    ! write data in Tecplot format


    WRITE(nombre1,*) ns_tri
    WRITE(nombre2,*) n_t

    WRITE(10,*) TRIM(ADJUSTL(titre))
    WRITE(10,110) TRIM(ADJUSTL(variable))
    WRITE(10,*) 'ZONE DATAPACKING=BLOCK, ZONETYPE=FETRIANGLE,VARLOCATION=([7-9]=CELLCENTERED),'&
         &,'N='//TRIM(ADJUSTL(nombre1)),',E='//TRIM(ADJUSTL(nombre2))
    WRITE(10,130) (c_tri(1,is), is=1, ns_tri)
    WRITE(10,130) (c_tri(2,is), is=1, ns_tri)
    WRITE(10,130) ((va_tri(is)%u(l),is=1,ns_tri),l=1, n_vars)
    WRITE(10,130) ((indic_tri(l,jt),jt=1,n_t),l=1,3)
    WRITE(10,*)   ((nu_tri(l,jt), l=1,3), jt=1, n_t)

    WRITE(nombre1,*) ns_quad
    WRITE(nombre2,*) n_q

    WRITE(10,110) variable
    WRITE(10,*) 'ZONE DATAPACKING=BLOCK, ZONETYPE=FEQUADRILATERAL, VARLOCATION=([7-9]=CELLCENTERED),' &
         &,'N='//TRIM(ADJUSTL(nombre1)),',E='//TRIM(ADJUSTL(nombre2))
    WRITE(10,130) (c_quad(1,is), is=1, ns_quad)
    WRITE(10,130) (c_quad(2,is), is=1, ns_quad)
    WRITE(10,130) ((va_quad(is)%u(l),is=1,ns_quad),l=1, n_vars)
    WRITE(10,130) ((indic_quad(l,jt),jt=1, n_q),l=1,3)
    WRITE(10,*)   ((nu_quad(l,jt), l=1,4), jt=1, n_q)




100 FORMAT(6(e16.8))
110 FORMAT(a)
130 FORMAT(4e20.10) 
    CLOSE(10)


    DEALLOCATE(coor,  indic_tri, indic_quad, va_tri, va_quad, w, triquad%e, iloc, jloc, kloc)
    DEALLOCATE(c_tri, c_quad)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)
  END SUBROUTINE visu_mixed

  SUBROUTINE visu_quad(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_quad"

    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss
    REAL(dp), DIMENSION(:,:),ALLOCATABLE,SAVE:: indic


    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max

    TYPE(PVar) :: sol

    TYPE(PVar), DIMENSION(:), ALLOCATABLE, SAVE :: W ! solution in primitive variables

    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    IF(.NOT.ALLOCATED(nu2)) THEN
       ALLOCATE(nu2(Mesh%e(1)%nsommets,Mesh%nt))
       ALLOCATE(va2(Var%ncells))
       ALLOCATE(va (Var%ncells))
       ALLOCATE(W(Var%ncells))
       ALLOCATE(indic(3,Mesh%nt))
    ENDIF

    DO jt=1, Mesh%nt
       nu2(:,jt)=Mesh%e(jt)%nu(:)
       coor(1,nu2(:,jt))=mesh%e(jt)%coor(1,:)
       coor(2,nu2(:,jt))=mesh%e(jt)%coor(2,:)
       nt2=Mesh%nt
       indic(1,jt)=Mesh%e(jt)%type_flux
       indic(2,jt)=Mesh%e(jt)%diag
       indic(3,jt)=Mesh%e(jt)%diag2
    ENDDO


    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution


    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)

       DO i = 1,e%nsommets

          DO l=1, e%nsommets
             base(l) = e%base(l, e%coorL(:,i) )
          END DO

          DO j=1, n_vars
             !va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j) * e%base_at_dofs(:,i))
             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j) * base )
          ENDDO
       END DO ! i

    END DO ! jt

    MyUnit                         = 8

    myUnit=10

    DO is = 1, Var%ncells

       W(is) = va2(is)%cons2prim()
    END DO

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )

    IF (ios /= 0) THEN
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,*)'VARIABLES = "x","y", "ro", "u", "v", "p", "Flux_indic", "Diag_ind" , "Diag2"'
    WRITE (10,*) 'ZONE DATAPACKING=BLOCK, ZONETYPE=FEQUADRILATERAL, ', &
         'VARLOCATION=([7-9]=CELLCENTERED), ', 'N='//TRIM(ADJUSTL(nombre1))//&
         &     ', E='//TRIM(ADJUSTL(nombre2))

    ! file output format
    WRITE(nvar_str,*) n_vars+4
    WRITE(format_str1,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    WRITE(nvar_str,*) n_vars
    WRITE(format_str,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    ! write data in Tecplot format
    WRITE(10,130) (coor(1,is),is=1, Var%ncells)
    WRITE(10,130) (coor(2,is),is=1, Var%ncells)
    WRITE(10,130) (( W(is)%u(l),is=1, Var%ncells),l=1,n_vars)
    WRITE(10,130) ((indic(l,jt),jt=1,Nt2),l=1,3)
    ! numéros des noeuds de chaque triangle (dans l'ordre où l'on écrit les coordonnées)
    WRITE(10,*) ((nu2(l,jt), l=1,4), jt=1,nt2)
130 FORMAT(4e20.10) 

    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_quad


  SUBROUTINE errors(DATA, Mesh, Var, error_L1, error_L2, error_linf)
    TYPE(donnees),  INTENT(in)  :: DATA
    TYPE(maillage), INTENT(in)  :: Mesh
    TYPE(variables), INTENT(in)  :: Var
    TYPE(PVar),     INTENT(out) :: error_L1, error_L2, error_Linf

    TYPE(element)              :: e
    REAL(DP),    ALLOCATABLE       :: base(:)
    INTEGER, ALLOCATABLE       :: iss(:)
    TYPE(PVar),ALLOCATABLE,DIMENSION(:)                 :: error_L1_loc, error_L2_loc
    INTEGER                    :: jt, n_ord, iq, l, ios
    REAL(DP)                       :: xx, yy, h
    TYPE(PVar)                 :: sol_appr, sol_ex

!!$    error_L1%u = 0.0_dp
!!$    error_L2%u = 0.0_dp
!!$    error_linf%u=0.0_dp
!!$    ALLOCATE(error_L1_loc(Mesh%nt),error_L2_loc(Mesh%nt) )
!!$
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       error_L1_Loc%u(l)=0.0_dp
!!$       error_L2_Loc%u(l)=0.0_dp
!!$    ENDDO
!!$
!!$    ! Loop through mesh elements
!!$    DO jt=1, Mesh%nt
!!$
!!$
!!$       ! get element
!!$       e=Mesh%e(jt)
!!$
!!$       n_ord=e%nsommets
!!$
!!$       ALLOCATE(base(n_ord))
!!$       ALLOCATE(iss(n_ord))
!!$
!!$       iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
!!$
!!$       ! Loop through the quadrature points on the element e
!!$       DO iq=1, e%nquad
!!$
!!$          ! get the shape function
!!$          DO l=1,n_ord
!!$             base(l) = e%base(l,e%quad(:,iq))
!!$          ENDDO ! l
!!$
!!$          xx=SUM(e%coor(1,1:3)*e%quad(:,iq)); yy=SUM(e%coor(2,1:3)*e%quad(:,iq))
!!$
!!$          DO l=1, SIZE(sol_appr%u)
!!$             sol_appr%u(l) = SUM(Var%ua(0,e%nu(:))%u(l)*base)
!!$          ENDDO
!!$          sol_appr = convert_cons2prim(sol_appr)
!!$          sol_ex   = exact_vortex(xx,yy, DATA%temps)
!!$          DO l=1, Var%ua(0,1)%nvars
!!$             error_L1_loc(jt)%U(l)   = error_L1_loc(jt)%U(l) + ( ABS(sol_appr%U(l)-sol_ex%U(l))    )*e%weight(iq)
!!$             error_L2_loc(jt)%U(l)   = error_L2_loc(jt)%U(l) + ( ABS(sol_appr%U(l)-sol_ex%U(l))**2 )*e%weight(iq)
!!$             error_Linf%u(l)     = MAX(error_linf%u(l),ABS(sol_appr%U(l)-sol_ex%U(l)))
!!$          ENDDO
!!$       END DO ! iq
!!$
!!$       DEALLOCATE(base)
!!$       DEALLOCATE(iss)
!!$       DO l=1, Var%ua(0,1)%nvars
!!$          error_L1_loc(jt)%U(l)= error_L1_loc(jt)%U(l)*e%volume
!!$          error_L2_loc(jt)%U(l)= error_L2_loc(jt)%U(l)*e%volume
!!$       ENDDO
!!$
!!$    END DO ! jt
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       error_L2%U(l) = SQRT(SUM(error_L2_loc(:)%U(l)))
!!$       error_L1%U(l) =      SUM(error_L1_loc(:)%U(l))
!!$    ENDDO
!!$
!!$    OPEN( UNIT = 14, FILE = TRIM(ADJUSTL(DATA%maillage))//".err", IOSTAT = ios )
!!$
!!$    h= SQRT(MAX( SUM(Mesh%e(jt)%n(:,1)**2),SUM(Mesh%e(jt)%n(:,2)**2),SUM(Mesh%e(jt)%n(:,3)**2)))
!!$
!!$    WRITE (14,fmt=50) error_L1%U, Mesh%ns, h
!!$    WRITE (14,fmt=50) error_L2%U, Mesh%ns, h
!!$    WRITE (14,fmt=50) error_Linf%U, Mesh%ns, h
!!$    CLOSE(14)
!!$
!!$50  FORMAT((f15.10,1x,f15.10,1x,f15.10,1x,f15.10,1x,I6,1x,f15.10))
!!$
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       PRINT*, 'variable= ', l, ' error_L1 = ', error_L1%U(l)
!!$       PRINT*, 'variable= ', l, ' error_L2 = ', error_L2%U(l)
!!$       PRINT*, 'variable = ',l, ' error Linf=', error_Linf%u(l)
!!$    ENDDO
!!$    DEALLOCATE(error_L1_loc,error_L2_loc )
!!$    CALL coupesol(Mesh,Var)
  END SUBROUTINE errors


  SUBROUTINE errors_old(DATA, Mesh, Var, error_L1, error_L2, error_linf)
    TYPE(donnees),  INTENT(in)  :: DATA
    TYPE(maillage), INTENT(in)  :: Mesh
    TYPE(variables), INTENT(in)  :: Var
    TYPE(PVar),     INTENT(out) :: error_L1, error_L2, error_Linf

    TYPE(element)              :: e
    REAL(DP),    ALLOCATABLE       :: base(:)
    INTEGER, ALLOCATABLE       :: iss(:)

    INTEGER                    :: jt, n_ord, iq, l, ios
    REAL(DP)                       :: xx, yy, h
    TYPE(PVar)                 :: sol_appr, sol_ex

    error_L1%u = 0.0_dp
    error_L2%u = 0.0_dp
    error_linf%u=0._dp
!!$
!!$    ! Loop through mesh elements
!!$    DO jt=1, Mesh%nt
!!$
!!$       ! get element
!!$       e=Mesh%e(jt)
!!$
!!$       n_ord=e%nsommets
!!$
!!$       ALLOCATE(base(n_ord))
!!$       ALLOCATE(iss(n_ord))
!!$
!!$       iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
!!$
!!$       ! Loop through the quadrature points on the element e
!!$       DO iq=1, e%nquad
!!$
!!$          ! get the shape function
!!$          DO l=1,n_ord
!!$             base(l) = e%base(l,e%quad(:,iq))
!!$          ENDDO ! l
!!$
!!$          xx=SUM(e%coor(1,1:3)*e%quad(:,iq)); yy=SUM(e%coor(2,1:3)*e%quad(:,iq))
!!$
!!$          DO l=1, SIZE(sol_appr%u)
!!$             sol_appr%u(l) = SUM(Var%ua(0,e%nu(:))%u(l)*base)
!!$          ENDDO
!!$  !        sol_ex   = solution(xx,yy, DATA%temps)
!!$
!!$          error_L1%U   = error_L1%U + ( ABS(sol_appr%U-sol_ex%U)    )*e%weight(iq)*e%volume
!!$          error_L2%U   = error_L2%U + ( ABS(sol_appr%U-sol_ex%U)**2 )*e%weight(iq)*e%volume
!!$          error_Linf%u = MAX(error_linf%u,ABS(sol_appr%U-sol_ex%U))
!!$
!!$       END DO ! iq
!!$
!!$       DEALLOCATE(base)
!!$       DEALLOCATE(iss)
!!$
!!$
!!$    END DO ! jt
!!$
!!$    error_L2%U = SQRT(error_L2%U)
!!$
!!$    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//".err", IOSTAT = ios )
!!$    h= MAXVAL(Mesh%e(:)%h)
!!$
!!$    WRITE (10, *) error_L1%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    WRITE (10, *) error_L2%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    WRITE (10, *) error_Linf%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    CLOSE(10)
!!$
!!$    PRINT*, 'error_L1 = ', error_L1%U
!!$    PRINT*, 'error_L2 = ', error_L2%U
!!$    PRINT*, "error Linf", error_Linf%u
!!$
!!$    CALL coupesol(Mesh,Var)
  END SUBROUTINE errors_old

  SUBROUTINE writesol(DATA,Var,kt)
    TYPE(variables), INTENT(in):: Var
    INTEGER, INTENT(in):: kt
    TYPE(donnees), INTENT(in)::DATA
    OPEN(unit=10, file="output.sol", form="unformatted")
    REWIND(10)
    WRITE(10) SIZE(Var%ua,dim=2)
    WRITE(10) DATA%temps,kt
    WRITE(10) Var%ua(0,:)
    CLOSE(10)
  END SUBROUTINE writesol

  SUBROUTINE readsol(DATA,ndofs,kt,Var)
    TYPE(variables), INTENT(inout):: Var
    TYPE(donnees), INTENT(inout)::DATA
    INTEGER, INTENT(out):: kt
    INTEGER, INTENT(in):: ndofs
    INTEGER:: n, j
    PRINT*, "hello"
    OPEN(unit=10, file="restart.sol", form="unformatted")
    READ(10) n
    IF (n.NE.ndofs) THEN
       WRITE(*,*) "The restart files and the mesh are not compatible"
       WRITE(*,*) "The file contains ", n, "dofs"
       WRITE(*,*) "The mesh has ", ndofs, "dofs"
       STOP
    ENDIF
    READ(10) DATA%temps,kt
    READ(10) Var%ua(0,:)
    CLOSE(10)
  END SUBROUTINE readsol

  SUBROUTINE coupesol(Mesh,Var)
    TYPE(variables), INTENT(in):: Var
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(element):: e
    INTEGER:: is, jt, l
    TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: u
    REAL(DP):: r

    DO l=1, n_vars
       REWIND(l)
    ENDDO

    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u(e%nsommets))
       u=Control_to_Cons(Var%ua(0,e%nu(:)),e)
       DO is=1, e%nsommets
          DO l=1, Var%ua(0,1)%nvars
             r=SQRT(SUM(e%coor(:,is)**2))
             WRITE(l,*) r,u(is)%u(l)
          ENDDO
       ENDDO
       DEALLOCATE(u)
    ENDDO

    DO l=1, n_vars
       CLOSE(l)
    ENDDO

  END SUBROUTINE coupesol

END MODULE postprocessing
