MODULE variable_def
  USE algebra
  USE PRECISION
  IMPLICIT NONE
  INTEGER, PUBLIC,PARAMETER:: n_vars = 4   ! number of primitive variables
  INTEGER, PUBLIC,PARAMETER:: n_dim  = 2   ! number of physical dimensions
  REAL(DP),    PARAMETER:: pi=ACOS(-1.) ! pi
  REAL(DP), PUBLIC,PARAMETER:: gam=1.4, u0=0.0,v0=0.0, p0=1./gam, ro0=1.0,s0=p0**(-gam)/ro0
  REAL(DP), PARAMETER:: unsro0=1./ro0, gp0=gam*p0
  REAL(DP), PARAMETER:: c0=SQRT(gp0/ro0)


  ! Type for the vector of primitive variables
  TYPE, PUBLIC :: PVar
     INTEGER                              :: NVars = n_vars
     REAL(DP), DIMENSION(n_vars)              :: U

   CONTAINS 
     PROCEDURE, PUBLIC:: flux            => flux_eul
     !     PROCEDURE, PUBLIC:: evalues         => evalues_eul
     PROCEDURE, PUBLIC:: spectral_radius => spectral_radius_eul
     PROCEDURE, PUBLIC:: rvectors        => rvectors_eul
     PROCEDURE, PUBLIC:: lvectors        => lvectors_eul
     PROCEDURE, PUBLIC:: Jacobian        => Jacobian_eul
     PROCEDURE, PUBLIC:: Nmat            => Nmat_eul
     PROCEDURE, PUBLIC:: min_mat         => min_mat_eul
     !     PROCEDURE, PUBLIC:: max_mat         => max_mat_eul
     !     PROCEDURE, PUBLIC:: roe             => roe_eul
  END TYPE PVar

  PRIVATE
  PUBLIC::  convert_cons2prim, convert_prim2cons

CONTAINS


!!!!-----------------------


  FUNCTION roe_eul(e,u,n) RESULT (J)
    ! evaluate a roe average to estimate a rough speed for 
    ! Burman jump operator
    CLASS(Pvar), INTENT(in):: e
    REAL(DP),DIMENSION(N_Vars), INTENT(in):: u
    REAL(DP), DIMENSION(n_dim), INTENT(in):: n ! here it will be a normal of norme 1
    REAL(DP), DIMENSION(n_vars,n_vars):: J
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: JJ
    REAL(DP),DIMENSION(n_dim):: v=0.
    JJ=Jacobian_eul(e,v)
    J(:,:) = JJ(:,:,1)*n(1)+JJ(:,:,2)*n(2)

  END FUNCTION roe_eul

  FUNCTION flux_eul(e,x) RESULT(f)
    CLASS(PVar),                  INTENT(in) :: e
    REAL(DP),       DIMENSION(:), INTENT(in), OPTIONAL :: x
    TYPE(PVar), DIMENSION(n_dim)             :: f
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: JJ
    REAL(DP),DIMENSION(n_dim):: v=0.
    JJ=Jacobian_eul(e,v)
    F(1)%u(:)=MATMUL(JJ(:,:,1),e%u)
    F(2)%u(:)=MATMUL(JJ(:,:,2),e%u)

  END FUNCTION flux_eul

  FUNCTION Jacobian_eul(e,x) RESULT(J)
    CLASS(Pvar),              INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim),   INTENT(in) :: x
    REAL(DP), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    J=0.
    J(1,1,1)=u0; J(1,2,1)=ro0;
    J(2,2,1)=u0; J(2,4,1)=unsro0
    J(3,3,1)=u0;
    J(4,2,1)=gp0; J(4,4,1)=u0

    J(1,1,2)=v0; J(1,3,2)=ro0;
    J(2,2,2)=v0
    J(3,3,2)=v0; J(3,4,2)=unsro0
    J(4,3,2)=gp0; J(4,4,2)=gp0





  END FUNCTION Jacobian_eul

  FUNCTION evalues_eul(e,x,n) RESULT(lambda)
    ! eigenvalues: diagonal matrix. It is written as a matrix for ease of calculations
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: x
    REAL(DP), DIMENSION(n_dim)             :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda

    REAL(DP), DIMENSION(n_vars, n_vars, n_dim) :: J
    REAL(DP):: un
    INTEGER:: i

    lambda=0._dp
    un=u0*n(1) + v0*n(2) 
    lambda(1,1) = un
    lambda(2,2) = un
    lambda(3,3) = un-c0
    lambda(4,4) = un+c0

  END FUNCTION evalues_eul

  REAL(DP) FUNCTION spectral_radius_eul(e,x,n)
    ! compute the maximum value of eigenvalues:
    ! max_i {lambda_ii}
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: x
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda1,lambda2
    REAL(DP):: vi
    vi=SQRT(u0*u0+v0*v0)
    spectral_radius_eul =SQRT(SUM(n(:)**2))*(vi+c0)

  END  FUNCTION spectral_radius_eul

  FUNCTION rvectors_eul(e,n) RESULT(R)
    ! right e-vectors
    ! assume ||n||=1
    CLASS(PVar),        INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim)         :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: R
    R=0._dp
    R(1,1)=ro0; R(2,1)=c0*n(1); R(3,1)=c0*n(2);R(3,1)=ro0*c0*c0
    R(1,2)=ro0; R(2,2)=-c0*n(1); R(3,2)=-c0*n(2);R(3,2)=ro0*c0*c0
    R(2,3)=-n(2); R(3,2)=n(1)
    R(1,4)=1


  END FUNCTION rvectors_eul

  FUNCTION lvectors_eul(e,n) RESULT(L)
    ! left e-vectors
    ! assumes ||n||=1
    CLASS(PVar),         INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim)          :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars)  :: L
    L=0._dp
    L(1,1)=u0*n(1)+v0*n(2)+c0
    L(2,2)=u0*n(1)+v0*n(2)-c0
    L(3,3)=u0*n(1)+v0*n(2)
    L(4,4)=u0*n(1)+v0*n(2)
  END FUNCTION lvectors_eul



  FUNCTION Nmat_eul(e, n_ord, grad, x) RESULT (Nmat)
    CLASS(Pvar),                  INTENT(in) :: e
    INTEGER,                      INTENT(in) :: n_ord
    REAL(DP), DIMENSION(n_dim),       INTENT(in) :: x
    REAL(DP), DIMENSION(n_dim,n_ord), INTENT(in) :: grad
    REAL(DP), DIMENSION(n_vars,n_vars)           :: Nmat
    REAL(DP), DIMENSION(n_vars, n_vars, n_dim)   :: J
    INTEGER:: l

    J= Jacobian_eul(e,x)
    Nmat=0
    DO l=1, n_ord
       Nmat = Nmat + ABS(grad(1,l)*J(1,1,1)+grad(2,l)*J(1,1,2) )
    ENDDO
    Nmat =Inverse(Nmat)

  END FUNCTION Nmat_eul

  FUNCTION min_mat_eul(e, x, alpha, n) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: x
    REAL(DP),                   INTENT(in) :: alpha
    REAL(DP), DIMENSION(n_dim), INTENT(IN) :: n
    REAL(DP), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_eul(e,x,n)
    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( MIN(lambda(:,:),alpha), L) )

  END FUNCTION min_mat_eul


  FUNCTION max_mat_eul(e,x,n,alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: x
    REAL(DP),                   INTENT(in) :: alpha
    REAL(DP), DIMENSION(n_dim), INTENT(in) :: n
    REAL(DP), DIMENSION(N_vars, N_vars)    :: Ap
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: R
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_eul(e,x,n)
    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( MAX(lambda(:,:),alpha), L) )

  END FUNCTION max_mat_eul
  !--------------------------


  !---------------------------------------------
  ! Convert conservative variables to primitive
  !---------------------------------------------
  FUNCTION convert_cons2prim(Q) RESULT(W)
    IMPLICIT NONE
    TYPE(Pvar), INTENT(in) :: Q
    TYPE(Pvar)             :: W
    REAL(DP)                   :: eps
    W%U = Q%U
  END FUNCTION convert_cons2prim

  !---------------------------------------------
  ! Convert primitive variables to conservative
  !---------------------------------------------
  FUNCTION convert_prim2cons(W) RESULT(Q)
    IMPLICIT NONE
    TYPE(Pvar), INTENT(in) :: W
    TYPE(Pvar)             :: Q
    REAL(DP)                   :: eps

    Q%U = W%U
  END FUNCTION convert_prim2cons

END MODULE variable_def
