module ParallelFunctions
    USE param2d
    USE scheme
    USE overloading
    USE Model
    use update 
    use arete_class
    use precision
    use Boundary

    integer, private        :: StatInfo
    integer, private, save  :: FluidBox_Comm = 0

    TYPE :: ele_str
        INTEGER, DIMENSION(:), ALLOCATABLE :: NU
        INTEGER, DIMENSION(:), ALLOCATABLE :: VV
        INTEGER :: log_type, bc_tag, id, indexInArray, isOnBoundary, subMeshId, ele_type
    END TYPE ele_str

    Type :: elementSend
        sequence
        integer :: id
        ! Only working for B1 for now, because of the function createDependencyMap
        ! Should be not a huge problem to fix though
        integer, dimension(3) :: indexInNu
        real(dp), dimension(3*2) :: coords
    End Type elementSend

    INTEGER, DIMENSION(31, 2), PARAMETER :: GMSH_ELE = &
        RESHAPE( (/ 2, 3, 4, 4, 8, 6, 5, 3, 6, 9, 10, 27, 18, 14, 1, 8, 20, 15, 13, 9, 10, 12, 15, 15, 21, 4, 5, 6, 20, 35, 56, &
                    2, 3, 4, 4, 8, 6, 5, 2, 3, 4,  4,  8,  6,  5, 1, 4,  8,  6,  5, 3,  3,  3,  3,  3,  3, 2, 2, 2,  4,  4,  4 /), &
        (/31, 2/) )
    integer, dimension(31),parameter::vertex_ele=&
      !1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     (/2,3,4,4,8,6,5,2,3,4,4,8,6,5,1,4,8,6,5,3,3,3,3,3,3,2,2,2,4,4,4/)
    contains
    subroutine initializeData(Com,parameters, Mesh, data)
        TYPE(maillage), intent(inout) :: Mesh
        TYPE(donnees),INTENT(in)    :: data
        type(MeshCom) :: com
        type(mainNodeParameters), intent(inout) :: parameters
        TYPE(ele_str), DIMENSION(:), ALLOCATABLE :: ele, tempEle
        integer :: currentNSubdomains, myUnit, nTriangles, currentSubmesh, boundary, &
            & nSubmesh, dummy, currentSize, nEdges, n_Ele, n_Nodes, N_E_max, N_B_max, nNames
        integer, dimension(:), allocatable :: maxBoundaryTrianglesVector, tempMaxBoundaryTrianglesVector, eleIds
        REAL(DP), DIMENSION(:,:), ALLOCATABLE :: Coords

        character(LEN=6), dimension(:), allocatable:: PhysName
        myUnit = 10
        nSubmesh = 0

        open(myUnit, file = 'globalmesh')

        read(myunit,*) !! Names
        read(myunit,*) nNames

        allocate (PhysName(nNames))
        do i = 1,nNames
          read(myunit,*)idummy,PhysName(idummy)
        enddo

        read(myUnit,*)
        read(myUnit,*) n_Nodes
        !!! Read Nodes !!!
        allocate( Coords(3, N_Nodes) )
        allocate( Mesh%coordsIds(2,N_Nodes) )

        do i = 1, N_nodes
           read(myUnit, *) Mesh%coordsIds(1,i), Coords(1, i), Coords(2, i)
           Mesh%coordsIds(2,i) = i
           Coords(3, i) = 0           
        enddo

        read(myUnit,*)
        read(myUnit,*) nTriangles
        allocate(maxBoundaryTrianglesVector(0))

        do i = 1,nTriangles
            read(myUnit,*)dummy,dummy,currentSubmesh,boundary

            if (currentSubmesh > size(maxBoundaryTrianglesVector)) then     
                currentSize = size(maxBoundaryTrianglesVector)
                allocate(tempMaxBoundaryTrianglesVector(currentSize))
                tempMaxBoundaryTrianglesVector(1:currentSize) = maxBoundaryTrianglesVector       
                deallocate(maxBoundaryTrianglesVector)
                allocate(maxBoundaryTrianglesVector(currentSize+1))      
                maxBoundaryTrianglesVector(1:currentSize) = tempMaxBoundaryTrianglesVector
                maxBoundaryTrianglesVector(currentSize+1) = 0
                deallocate(tempMaxBoundaryTrianglesVector)
            endif
            if (boundary == 1) then
                maxBoundaryTrianglesVector(currentSubmesh) = maxBoundaryTrianglesVector(currentSubmesh) + 1
            endif
        enddo

        read(myUnit,*)
        read(myUnit,*)nEdges
        n_Ele = nEdges + nTriangles

        allocate(parameters%maxBoundaryTrianglesVector(size(maxBoundaryTrianglesVector)))
        parameters%maxBoundaryTrianglesVector = maxBoundaryTrianglesVector

        !!! Create the globalMesh !!!
        close(myUnit)
        open(myUnit, file = 'globalmesh')

        !!! Same code as below !!!
        N_E_max = 0
        N_B_max = 0

        allocate(eleIds(nTriangles))
        allocate( ele(n_Ele) )        

        read(myunit,*) !! Names
        read(myunit,*)

        do i = 1,nNames
          read(myunit,*)
        enddo

        do i = 1,n_Nodes + 4
            read(myUnit,*)
        enddo

        do i = 1,nTriangles
            read(myUnit,*)ele(i)%id, ele_type, ele(i)%subMeshId, ele(i)%isOnBoundary
            ele(i)%ele_type = ele_type
            ele(i)%bc_tag = 6   

            ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
            ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

            BACKSPACE(myUnit)
            ele(i)%log_type = 0

            read(myUnit,*)dummy, dummy, dummy, dummy, ele(i)%nu

            do j=1,size(ele(i)%NU)
                ele(i)%NU(j) = findIndexInArray(mesh%coordsIds(1,:),ele(i)%Nu(j))
            enddo

            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(ele_type, 2) )

            ele(i)%indexInArray = i
            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(INT(ele_type), 2) )
    
        end do
        read(myUnit,*)
        read(myUnit,*)

        do i = nTriangles + 1, N_ele
            ele(i)%isOnBoundary = 0

            read(myUnit,*)ele(i)%id, ele_type, ele(i)%log_type
            ele(i)%ele_type = ele_type

            ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
            ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

            BACKSPACE(myUnit)

            read(myUnit,*)dummy, dummy, ele(i)%log_type, ele(i)%nu

            do j=1,size(ele(i)%NU)
                ele(i)%NU(j) = findIndexInArray(mesh%coordsIds(1,:),ele(i)%Nu(j))
            enddo

            do j=1,size(ele(i)%NU)
                ele(i)%NU(j) = findIndexInArray(mesh%coordsIds(1,:),ele(i)%Nu(j))
            enddo

            call boundary_log(ele(i)%log_type, ele(i)%bc_tag, Nphys, PhysName)

            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(INT(ele_type), 2))
        end do

        close(myUnit)

        !! Default behaviour after this

        Mesh%Ns = N_Nodes
        Mesh%Ndofs=N_nodes

        Ndim=2

        N_b = COUNT(ele%log_type /= 0)  !extract boundary elements, physical surface must be marked with 0 in .geo file
        PRINT*, 'N_b = ', N_b

        Mesh%Nt = N_ele - N_b

        allocate (Mesh%e(Mesh%nt))

        PRINT*, "Gmsh ns,Nt", Mesh%ns, Mesh%nt

        !!! Load Elements into Mesh !!!
        ii = 0
        j1 = 1
        DO i = 1, nTriangles
           ii=ii+1

           ALLOCATE(Mesh%e(ii)%nu(SIZE(ele(i)%nu,1)))
           ALLOCATE(Mesh%e(ii)%coor(ndim,SIZE(ele(i)%nu,1)))

           Mesh%e(ii)%itype=DATA%itype
           Mesh%e(ii)%nvertex=vertex_ele(ele(i)%ele_type)
           Mesh%e(ii)%nsommets= SIZE( ele(i)%NU )
           Mesh%e(ii)%Nu = ele(i)%NU
           Mesh%e(ii)%localNu = ele(i)%NU
           Mesh%e(ii)%coor(1,:)=coords(1,ele(i)%nu(:) )
           Mesh%e(ii)%coor(2,:)=coords(2,ele(i)%nu(:) )
           Mesh%e(ii)%subMeshId = ele(i)%subMeshId

           allocate( Mesh%e(ii)%coorL(Mesh%e(ii)%nvertex,SIZE(ele(i)%nu,1)) )
           call Mesh%e(ii)%getCoorL()

           ALLOCATE(Mesh%e(ii)%n(2,Mesh%e(ii)%nvertex))
           mesh%e(ii)%volume=Mesh%e(ii)%aire()
           Mesh%e(ii)%n=Mesh%e(ii)%normale()
           CALL mesh%e(ii)%quadrature()
           ALLOCATE(Mesh%e(ii)%base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))
           ALLOCATE(Mesh%e(ii)%inv_base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))

           CALL Mesh%e(ii)%base_ref()

           Mesh%e(ii)%id = ele(i)%id
           mesh%e(ii)%flag=.TRUE.

           mesh%e(ii)%isOnBoundary = .FALSE.

           if (ele(i)%isOnBoundary == 1) then
                Mesh%e(ii)%isOnBoundary = .TRUE.
                j1 = j1 + 1
           endif
           CALL mesh%e(ii)%eval_coeff()
        ENDDO

        Mesh%NsegFr = N_b

        ALLOCATE(Mesh%fr(N_b), Mesh%nsfacfr(2,N_b), Mesh%LogFac(n_b))

        !!! Load Triangles into Mesh !!!

        j = 0
        DO i =nTriangles + 1, N_ele
           j = j+1
           Mesh%fr(j)%Nsommets= SIZE(ele(i)%NU)
           Mesh%fr(j)%Nvertex= vertex_ele(ele(i)%ele_type)
           ALLOCATE(Mesh%fr(j)%nu( SIZE(ele(i)%NU) ))

           Mesh%fr(j)%nu  = ele(i)%nu
           Mesh%fr(j)%log = ele(i)%log_type
           Mesh%fr(j)%bc_tag = ele(i)%bc_tag

           Mesh%nsFacFr(:,j)=ele(i)%nu(1:2)
           Mesh%LogFac(j)=ele(i)%log_type
           ALLOCATE(Mesh%fr(j)%coor(ndim,SIZE(ele(i)%nu,1)))
           mesh%fr(j)%coor(1,:)=coords(1,Mesh%fr(j)%nu(:))
           mesh%fr(j)%coor(2,:)=coords(2,Mesh%fr(j)%nu(:))
           Mesh%fr(j)%itype_b=DATA%itype_b
           ALLOCATE(Mesh%fr(j)%n(2))
           mesh%fr(j)%volume=mesh%fr(j)%aire()
           mesh%fr(j)%n=mesh%fr(j)%normale()
           CALL mesh%fr(j)%quadrature()
        ENDDO 
        DEALLOCATE(ele, coords)
    end subroutine initializeData

    subroutine readMeshParallel(data, Mesh, com)
        IMPLICIT NONE
        TYPE(donnees),INTENT(in)    :: DATA
        TYPE(maillage), INTENT(inout):: mesh
        TYPE(MeshCom)               :: Com
        character(len=30)           :: fileString
        REAL(DP), DIMENSION(:,:), ALLOCATABLE :: Coords
        integer, dimension(:), allocatable :: eleIds
        TYPE(ele_str), DIMENSION(:), ALLOCATABLE :: ele, tempEle

        INTEGER :: N_nodes, N_ele, N_b, i, j, &
             N_E_max, N_B_max, idummy, &
             ele_type, n_tag
        INTEGER, DIMENSION(:), ALLOCATABLE :: v_dummy
        INTEGER, DIMENSION(:), ALLOCATABLE :: per
        INTEGER, DIMENSION(:,:),ALLOCATABLE:: voisin
        REAL(DP) :: ver, dummy
        INTEGER :: UNIT, status, Form, ds, Nphys, Ndim, ii, k, ncount, nTriangles, nEdges, nNames
        INTEGER :: j1, j2, l11,l12, l21, l22, i1, i2, l
        LOGICAL  :: exist, period
        character(LEN=6), dimension(:), allocatable:: PhysName

        !------------------------------------------------
        ! INQUIRE(FILE = 'submesh_'//leftString(com%me+1), EXIST = exist)
        ! IF( .NOT. exist) THEN
        !    WRITE(*,*) 'ERROR: file submesh_'//leftString(com%me+1), ' not found'
        !    STOP
        ! ENDIF
        N_E_max = 0
        N_B_max = 0

        unit = 1
        !fileString = 'submesh_'//leftString(com%me+1)
        fileString = 'submesh_'//leftString(com%me+1)

        open(unit, file=fileString, iostat=status)
        !!!! Skip to Nodes !!!
        read(unit,*)
        read(unit,*)mesh%subMeshId

        read(unit,*) !! Names
        read(unit,*) nNames
        allocate (PhysName(nNames))
        do i = 1,nNames
          read(unit,*)idummy,PhysName(idummy)
        enddo

        read(unit,*)
        read(unit,*)nTriangles
        do i = 1,nTriangles
            read(unit,*)
        end do

        read(unit,*)
        read(unit,*)nEdges
        do i = 1,nEdges
            read(unit,*)
        end do
        read(unit,*)
        read(unit, *) N_Nodes

        !!! Read Nodes !!!
        allocate( Coords(3, N_Nodes) )
        allocate( mesh%coordsIds(2,N_Nodes) )

        do i = 1, N_nodes
           read(UNIT, *) mesh%coordsIds(1,i), Coords(1, i), Coords(2, i)
           mesh%coordsIds(2,i) = i
           Coords(3, i) = 0
        enddo
        !!! Back to the beginning !!! 
        close(unit)
        open(unit, file=fileString, iostat=status)

        N_ele = nTriangles + nEdges
        N_E_max = 0
        N_B_max = 0

        allocate(eleIds(nTriangles))

        read(unit,*) !! Submesh
        read(unit,*)

        read(unit,*) !! Names
        read(unit,*) nNames

        do i = 1,nNames
          read(unit,*)
        enddo

        read(unit,*)
        read(unit,*)nTriangles
        allocate( ele(N_ele) )

        do i = 1,nTriangles
            read(unit,*)ele(i)%id, ele_type, ele(i)%subMeshId, ele(i)%isOnBoundary
            ele(i)%ele_type = ele_type

            ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
            ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

            BACKSPACE(Unit)
            ele(i)%log_type = 0

            read(unit,*)dummy, dummy, dummy, dummy, ele(i)%nu

            do j=1,size(ele(i)%NU)
                ele(i)%NU(j) = findIndexInArray(mesh%coordsIds(1,:),ele(i)%Nu(j))
            enddo

            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(ele_type, 2) )

            ele(i)%indexInArray = i
            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(INT(ele_type), 2) )
        end do
        read(unit,*)
        read(unit,*)

        do i = nTriangles + 1, N_ele
            read(unit,*)ele(i)%id, ele_type, ele(i)%log_type
            ele(i)%ele_type = ele_type

            ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
            ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

            BACKSPACE(unit)

            read(unit,*)dummy, dummy, ele(i)%log_type, ele(i)%nu

            do j=1,size(ele(i)%NU)
                ele(i)%NU(j) = findIndexInArray(mesh%coordsIds(1,:),ele(i)%Nu(j))
            enddo

            call boundary_log(ele(i)%log_type, ele(i)%bc_tag, Nphys, PhysName)

            ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(INT(ele_type), 2))

        end do

        close(unit)

        !! Default behaviour after this. 

        Mesh%Ns = N_Nodes
        Mesh%Ndofs=N_nodes

        Ndim=2
        N_b = COUNT(ele%log_type /= 0)  !extract boundary elements, physical surface must be marked with 0 in .geo file
        PRINT*, 'N_b = ', N_b
        Mesh%Nt = N_ele - N_b

        allocate (Mesh%e(Mesh%nt))
        PRINT*, "Gmsh ns,Nt", Mesh%ns, Mesh%nt

        !!! Load Elements into Mesh !!!

        ii = 0
        j1 = 1

        DO i = 1, nTriangles
           ii=ii+1

           ALLOCATE(Mesh%e(ii)%nu(SIZE(ele(i)%nu,1)))
           ALLOCATE(Mesh%e(ii)%coor(ndim,SIZE(ele(i)%nu,1)))

           Mesh%e(ii)%localNu = ele(i)%NU
           Mesh%e(ii)%itype=DATA%itype
           Mesh%e(ii)%nvertex=vertex_ele(ele(i)%ele_type)
           Mesh%e(ii)%nsommets= SIZE( ele(i)%NU )
           Mesh%e(ii)%Nu = ele(i)%NU
           Mesh%e(ii)%coor(1,:)=coords(1,ele(i)%nu(:) )
           Mesh%e(ii)%coor(2,:)=coords(2,ele(i)%nu(:) )
           Mesh%e(ii)%subMeshId = ele(i)%subMeshId

           allocate( Mesh%e(ii)%coorL(Mesh%e(ii)%nvertex,SIZE(ele(i)%nu,1)) )
           call Mesh%e(ii)%getCoorL()

           ALLOCATE(Mesh%e(ii)%n(2,Mesh%e(ii)%nvertex))
           mesh%e(ii)%volume=Mesh%e(ii)%aire()

           Mesh%e(ii)%n=Mesh%e(ii)%normale()
           CALL mesh%e(ii)%quadrature()
           ALLOCATE(Mesh%e(ii)%base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))
           ALLOCATE(Mesh%e(ii)%inv_base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))

           CALL Mesh%e(ii)%base_ref()

           Mesh%e(ii)%id = ele(i)%id
           mesh%e(ii)%flag=.TRUE.

           mesh%e(ii)%isOnBoundary = .FALSE.

           if (ele(i)%isOnBoundary == 1) then
                Mesh%e(ii)%isOnBoundary = .TRUE.
                j1 = j1 + 1
           endif
           CALL mesh%e(ii)%eval_coeff()
        ENDDO

        Mesh%NsegFr = N_b

        ALLOCATE(Mesh%fr(N_b), Mesh%nsfacfr(2,N_b), Mesh%LogFac(n_b))

        !!! Load Triangles into Mesh !!!

        j = 0
        DO i =nTriangles + 1, N_ele
           j = j+1
           Mesh%fr(j)%Nsommets= SIZE(ele(i)%NU)
           Mesh%fr(j)%Nvertex= vertex_ele(ele(i)%ele_type)
           ALLOCATE(Mesh%fr(j)%nu( SIZE(ele(i)%NU) ))

           Mesh%fr(j)%nu  = ele(i)%nu
           Mesh%fr(j)%log = ele(i)%log_type
           Mesh%fr(j)%bc_tag = ele(i)%bc_tag

           Mesh%nsFacFr(:,j)=ele(i)%nu(1:2)
           Mesh%LogFac(j)=ele(i)%log_type

           ALLOCATE(Mesh%fr(j)%coor(ndim,SIZE(ele(i)%nu,1)))
           mesh%fr(j)%coor(1,:)=coords(1,Mesh%fr(j)%nu(:))
           mesh%fr(j)%coor(2,:)=coords(2,Mesh%fr(j)%nu(:))
           Mesh%fr(j)%itype_b=DATA%itype_b
           ALLOCATE(Mesh%fr(j)%n(2))
           mesh%fr(j)%volume=mesh%fr(j)%aire()
           mesh%fr(j)%n=mesh%fr(j)%normale()
           CALL mesh%fr(j)%quadrature()
        ENDDO

        DEALLOCATE(ele, coords)
    end subroutine readMeshParallel

    subroutine decParallel(Var,debug,DATA,Mesh, dt, n_theta, alpha, beta, gamma, theta, com, mainParameters)
        implicit none
        TYPE(maillage), INTENT(in):: mesh
        TYPE(variables), INTENT(inout):: var,debug
        TYPE(donnees), INTENT(in):: DATA
        real(DP), intent(in) :: dt
        Type(meshCom) :: Com

        REAL(DP),DIMENSION(3,2:4), INTENT(in):: alpha
        REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

        INTEGER, DIMENSION(4), INTENT(in):: n_theta
        REAL(DP),DIMENSION(0:3,3,2:4), INTENT(in):: theta

        TYPE(element):: e, e1, e2
        TYPE(arete):: ed
        TYPE(frontiere):: efr

        TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
        TYPE(Pvar),DIMENSION(2):: f1,f2,f3, f4,f5
        TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: res,residu, uu, difference
        TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
        TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
        TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: flux, flux_c
        INTEGER:: nt,itype, jt, i, j, kt, is, l, k_inter, p1, p2, k, iseg, jt1, jt2, ll, kt0, lp, ierror
        type(mainNodeParameters), intent(inout) :: mainParameters
        character(10) :: time

        do k_inter=1,DATA%iter !loop for the corrections r=1,...,R
            do is=1, Mesh%ndofs
                Var%up(:,is)=Var%ua(:,is)
            enddo

            do k=2, DATA%iordret !loop for the subtimesteps m=1,.......M within [t_{n}, t_{n+1}]
                do is=1, Mesh%Ndofs
                    Var%un(is)%u=0.0
                enddo
                
                do jt=1, Mesh%nt
                    call main_update(k,dt,Mesh%e(jt),Var,DATA,alpha,beta,gamma,n_theta,theta)
                enddo
                !--------------------------------------------------------------------------------------
                ! EDGE UPDATE DONE ONLY IN CASE WE DO NOT HAVE LXF scheme
                if (DATA%ischema==4 .OR.DATA%ischema==5 ) then !(with jumps--> Burman'stuff)
                    call edge_main_update_parallel(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta,com)
                endif

                call BC(Mesh, dt, Data%temps, Var%ua(k-1,:),Var%un, DATA)
                !---------------------------------------------------------------------------------------
                ! Update Un on Boundaries
                if (com%me == com%main) then
                    call mainReceiveUn(com,mesh,mainParameters, var)
                    call mpi_barrier(FluidBox_Comm, ierror)

                    call updateUnMatrix(mainParameters)

                    call mainDistributeUn(com, mesh, mainParameters, var)
                    call mpi_barrier(FluidBox_Comm, ierror)
                else
                    call childSendUn(mesh, com, Var)
                    call mpi_barrier(FluidBox_Comm, ierror)

                    call childReceiveUn(mesh, com, var)
                    call mpi_barrier(FluidBox_Comm, ierror)
                endif
                !--------- Update of the solution -----------------------------------------------

                do is=1, Mesh%ndofs
                    var%ua(k-1,is)=Var%up(k-1,is)-Var%un(is)*Mesh%aires(is)
                enddo

                do is=1, Mesh%Ndofs
                    Var%un(is)%u=0.0
                enddo
            enddo
        enddo
    end subroutine decParallel

    subroutine mainReceiveUn(com, mesh, mainParameters, var)
        implicit none
        TYPE(variables), INTENT(inout) :: var
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        type(mainNodeParameters), intent(inout) :: mainParameters
        integer :: maximum, StatInfo, i, j, ierror
        integer, dimension(mpi_status_size) :: status
        real(dp), dimension(:), allocatable :: realvalues

        maximum = maxval(mesh%subMeshsNdofs)
        allocate(realValues(maximum))

        do i = 1,mesh%nDofs
            do j = 1,4
                mainParameters%unMatrix(1,j,i) = var%un(i)%u(j)                
            enddo
        enddo

        do i = 1,com%NTasks-1
            do j = 1,4
                call MPI_Recv(realValues,maximum,mpi_real8,i,j,FluidBox_Comm,status,StatInfo)
                mainParameters%unMatrix(i+1,j,:) = realValues
            enddo
        enddo
    end subroutine mainReceiveUn

    subroutine childSendUn(mesh, com, var)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        TYPE(variables), INTENT(in) :: var
        integer :: ierror, currentSize, i, j
        real(dp), dimension(:), allocatable :: sendReals

        allocate(sendReals(mesh%ndofs))
        
        do i=1,4
            do j=1,mesh%ndofs
                sendReals(j) = var%un(j)%u(i)
            enddo
            call mpi_send(sendReals,mesh%ndofs,mpi_real8,com%main,i,FluidBox_Comm,ierror)
        enddo
    end subroutine childSendUn

    subroutine createNuFile(Com, Mesh)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        character(len=100) :: fileName, tempString
        integer :: myUnit, addedElements, nDofs, i, j
        integer, dimension(:), allocatable :: addedIds
        type(elementSend) :: boundaryTriangleValues
        type(element) :: e1,e2,tempElement
        type(elementSend), dimension(:), allocatable :: addedNuElements
        logical :: exist

        allocate(addedNuElements(mesh%ndofs))
        allocate(addedIds(mesh%nsegmt))
        addedIds = -1
        addedElements = 0

        do i = 1,mesh%nsegmt
            tempElement%id = -1
            if (mesh%edge(i)%touchingElements(1) <= 0 .or. mesh%edge(i)%touchingElements(2) <= 0) cycle
            
            e1 = mesh%boundaryE(mesh%edge(i)%touchingElements(1))
            e2 = mesh%boundaryE(mesh%edge(i)%touchingElements(2))

            if (e1%submeshId /= com%me+1) then
                tempElement = e2
            else if (e2%subMeshId /= com%me+1) then
                tempElement = e1
            else 
                cycle
            endif

            if (inIntArray(addedIds,tempElement%id)) cycle

            boundaryTriangleValues%id = com%me

            do j = 1,size(tempElement%nu)
                boundaryTriangleValues%indexInNu(j) = tempElement%nu(j)
                boundaryTriangleValues%coords(2*j-1) = tempElement%coor(1,j)
                boundaryTriangleValues%coords(2*j) = tempElement%coor(2,j)   
            enddo

            addedElements = addedElements + 1
            addedIds(addedElements) = tempElement%id
            addedNuElements(addedElements) = boundaryTriangleValues
        enddo

        write(tempString,*)com%me

        filename = 'nuFile_'//adjustl(trim(tempString))
        myUnit = 11+com%me
        inquire(file=filename, exist=exist)
        open(myUnit, file=filename)
        write(myUnit,*)'Neighbours'
        write(myUnit,*)size(mesh%neighbourIds)
        do i = 1,size(mesh%neighbourIds)
            write(myUnit,*)mesh%neighbourIds(i)
        enddo

        write(myUnit,*)'Elements'
        write(myUnit,*)addedElements
        do i = 1,addedElements
          write(myUnit,*)addedNuElements(i)
        enddo
        call mpi_barrier(com%comWorld,j)

        close(myUnit)
        deallocate(addedNuElements, addedIds)
    end subroutine createNuFile

    subroutine createDependencyMap(mesh, com, mainParameters)
        implicit none
        integer :: size, j, i, addedNdofs, nSubDomains, myUnit, nDofs, k, l, currentId, x, y, addedCorrespondencies, n, calcSize
        integer :: nNeighbours, tempInt, nAddedCoords
        Type(meshCom), intent(in) :: Com
        character(len=100) :: fileName, tempString
        TYPE(maillage), INTENT(in) :: mesh
        type(mainNodeParameters), intent(inout) :: mainParameters
        type(elementSend), dimension(:), allocatable :: boundaryTriangleValues
        logical :: exist, currentIsInner, addCurrent
        real(DP), dimension(2) :: coords,secondCoords
        integer, dimension(:,:), allocatable :: tempMatrix, correspondencies
        real(DP), dimension(:,:), allocatable :: addedCoords, tempaddedCoords
        integer, dimension(:,:), allocatable :: neighbourMatrix
        integer, dimension(:), allocatable :: tempIntVector
        real(DP), dimension(:), allocatable :: tempRealVector
      

        nDofs = size(boundaryTriangleValues(1)%indexInNu)
        allocate(neighbourMatrix(com%nTasks,com%nTasks))
        
        neighbourMatrix = 0
        nAddedCoords = 0
        myUnit = 90
        addedNdofs = 0
        calcSize = 0

        allocate(tempIntVector(nDofs))
        allocate(tempRealVector(2*nDofs))

        do j=0,com%NTasks -1
            write(*,*)'Reading Nu-File of process', j
            write(tempString,*)j
            filename = 'nuFile_'//adjustl(trim(tempString))
            open(myUnit, file=filename)
            read(myUnit,*)
            read(myUnit,*)nNeighbours
            do i = 1,nNeighbours
                write(*,*)'Reading neighbour',i
                read(myUnit,*)tempInt
                neighbourMatrix(j+1,tempInt) = 1
            enddo
            read(myUnit,*)
            read(myUnit,*)tempInt
            calcSize = calcSize + tempInt
            close(myUnit)
        enddo

        allocate(boundaryTriangleValues(calcSize))

        do j=0,com%NTasks -1
            write(*,*)'Reading Nu-File of process', j
            write(tempString,*)j
            filename = 'nuFile_'//adjustl(trim(tempString))
            open(myUnit, file=filename)
            read(myUnit,*)
            read(myUnit,*)nNeighbours
            do i = 1,nNeighbours
                write(*,*)'Reading neighbour',i
                read(myUnit,*)tempInt
                neighbourMatrix(j+1,tempInt) = 1
            enddo
            read(myUnit,*)
            read(myUnit,*)tempInt
            
            write(*,*)'found ndofs:',tempInt
            do i = 1,tempInt       
                addedNdofs = addedNdofs + 1
                read(myUnit,*)boundaryTriangleValues(addedNdofs)
            enddo
            close(myUnit)
        enddo

        write(*,*)'Read Nu-Files'

        write(*,*)'allocating addedCoords: 2,', addedNdofs,'*',ndofs,addedNdofs*ndofs
        tempint = addedNdofs*ndofs
        write(*,*)tempInt
        allocate(addedCoords(2,tempInt))
        write(*,*)'nulling addedCorrespondencies'
        addedCorrespondencies = 0
        write(*,*)'Allocating correspondencies: ',com%NTasks,',',addedNdofs
        allocate(correspondencies(com%NTasks,addedNdofs))

        do j=1,addedNdofs
            write(*,*)'Going through all dofs:',j,' out of ',addedNdofs            
            do k=1,nDofs
                currentIsInner = .false.    
                coords(1) = boundaryTriangleValues(j)%coords(2*k-1)
                coords(2) = boundaryTriangleValues(j)%coords(2*k)
                currentId = boundaryTriangleValues(j)%id
                do i=j+1,addedNdofs
                    do l=1,nDofs
                        secondCoords(1) = boundaryTriangleValues(i)%coords(2*l-1)
                        secondCoords(2) = boundaryTriangleValues(i)%coords(2*l)
                        if (.not. coordsAreAdded(coords(1),coords(2),addedCoords)) then
                            if (coords(1) == secondCoords(1) &
                            .and. coords(2) == secondCoords(2) &
                            .and. currentId /= boundaryTriangleValues(i)%id) then
                                if (.not. currentIsInner) then
                                    currentIsInner = .true.
                                    addedCorrespondencies = addedCorrespondencies + 1

                                    do n = 1,com%NTasks
                                        correspondencies(n,addedCorrespondencies) = -1
                                    enddo
                                endif

                                correspondencies(currentId+1,addedCorrespondencies) &
                                    = boundaryTriangleValues(j)%indexInNu(k)
                                correspondencies(boundaryTriangleValues(i)%id+1,addedCorrespondencies) &
                                    = boundaryTriangleValues(i)%indexInNu(l)
                            endif
                        endif
                    enddo
                enddo
                nAddedCoords = nAddedCoords + 1
                addedCoords(1,nAddedCoords) = coords(1)
                addedCoords(2,nAddedCoords) = coords(2)
            enddo
        enddo

        allocate(mainParameters%nDofsCorrespondencies(size(correspondencies,1),addedCorrespondencies))
        mainParameters%nDofsCorrespondencies = correspondencies(:,1:addedCorrespondencies)

        deallocate(correspondencies, addedCoords, boundaryTriangleValues, neighbourMatrix)
    end subroutine createDependencyMap

    subroutine mainDistributeUn(com, mesh, mainParameters, var)
        implicit none
        TYPE(variables), INTENT(inout) :: var
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        type(mainNodeParameters), intent(inout) :: mainParameters
        integer i,j,k, ierror
        real(dp), dimension(:), allocatable :: sendReals

        allocate(sendReals(maxval(mesh%subMeshsNdofs)))

        do i = 1,com%NTasks
            do k = 1,4
                if (i == 1) then
                    do j = 1,mesh%subMeshsNdofs(i)
                        var%un(j)%u(k) = mainParameters%unMatrix(i,k,j) 
                    enddo
                else
                    do j = 1,mesh%subMeshsNdofs(i)
                        sendReals(j) = mainParameters%unMatrix(i,k,j) 
                    enddo
                    call mpi_send(sendReals,mesh%subMeshsNdofs(i),mpi_real8,i-1,k,FluidBox_Comm,ierror)
                endif
            enddo            
        enddo
    end subroutine mainDistributeUn

    subroutine childReceiveUn(mesh, com, var)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        TYPE(variables), INTENT(inout) :: var
        integer :: ierror, currentSize, i, j
        integer, dimension(mpi_status_size) :: status
        real(dp), dimension(:), allocatable :: receiveReals

        allocate(receiveReals(mesh%ndofs))
        
        do i=1,4
            call MPI_Recv(receiveReals,mesh%ndofs,mpi_real8,com%main,i,FluidBox_Comm,status,ierror)
            do j=1,mesh%ndofs
                var%un(j)%u(i) = receiveReals(j)
            enddo
        enddo
    end subroutine childReceiveUn

    subroutine updateUnMatrix(mainParameters)
        implicit none
        type(mainNodeParameters), intent(inout) :: mainParameters
        real(dp) :: sum
        integer :: i,j,k

        do i = 1,4
            do j = 1,size(mainParameters%nDofsCorrespondencies,2)
                sum = 0
                do k = 1, size(mainParameters%nDofsCorrespondencies,1)
                    if (mainParameters%nDofsCorrespondencies(k,j) /= -1) then
                        sum = sum + mainParameters%unMatrix(k,i,mainParameters%nDofsCorrespondencies(k,j))
                    endif
                enddo
                do k=1, size(mainParameters%nDofsCorrespondencies,1)
                    if (mainParameters%nDofsCorrespondencies(k,j) /= -1) then
                        mainParameters%unMatrix(k,i,mainParameters%nDofsCorrespondencies(k,j)) = sum
                    endif
                enddo
            enddo
        enddo
    end subroutine updateUnMatrix

    subroutine updateMassMatrix(com, mesh, mainParameters)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(inout) :: mesh
        type(mainNodeParameters), intent(in) :: mainParameters
        real(dp), dimension(:,:), allocatable :: mainAiresMatrix
        real(dp) :: sum
        integer :: maximum,i, ierror, j, k
        integer, dimension(mpi_status_size) :: status
        real(dp), dimension(:), allocatable :: tempReals
        
        allocate(tempReals(mesh%ndofs))

        if (com%me == com%main) then
            maximum = maxval(mesh%subMeshsNdofs)    
            allocate(mainAiresMatrix(com%nTasks,maximum))
            mainAiresMatrix = 0
            mainAiresMatrix(1,1:mesh%nDofs) = mesh%aires

            do i = 1,com%NTasks-1
                call MPI_Recv(mainAiresMatrix(i+1,:),maximum,mpi_real8,i,1,FluidBox_Comm,status,StatInfo)
            enddo

            call mpi_barrier(FluidBox_Comm, ierror)

            do j = 1,size(mainParameters%nDofsCorrespondencies,2)
                sum = 0
                do k = 1, size(mainParameters%nDofsCorrespondencies,1)
                    if (mainParameters%nDofsCorrespondencies(k,j) /= -1) then
                        sum = sum + mainAiresMatrix(k,mainParameters%nDofsCorrespondencies(k,j))
                    endif
                enddo
                do k=1, com%nTasks
                    mainAiresMatrix(k,mainParameters%nDofsCorrespondencies(k,j)) = sum
                enddo
            enddo 
            
            do j = 1, size(mesh%aires)
                mesh%aires(j) = mainAiresMatrix(1,j)
            enddo

            do j = 1, com%nTasks -1 
                call mpi_send(mainAiresMatrix(j+1,:),mesh%subMeshsNdofs(j+1),mpi_real8,j,1,FluidBox_Comm,ierror)
            enddo
            call mpi_barrier(FluidBox_Comm, ierror)
        else
            call mpi_send(mesh%aires,mesh%ndofs,mpi_real8,com%main,1,FluidBox_Comm,ierror)
            call mpi_barrier(FluidBox_Comm, ierror)

            call MPI_Recv(mesh%aires,mesh%nDofs,mpi_real8,com%main,1,FluidBox_Comm,status,StatInfo)
            call mpi_barrier(FluidBox_Comm, ierror)
        endif

        Mesh%aires=1.0_dp/Mesh%aires

        deallocate(tempReals)

        if (com%me == com%main) then
            deallocate(mainAiresMatrix)
        endif
    end subroutine updateMassMatrix

    subroutine createNuMap(com,mesh,mainParameters)
        Type(meshCom), intent(in) :: Com
        integer :: maximum,i, ierror, j, k, unit, nVertices, nTriangles, nEdges, id, status, nNames
        TYPE(maillage), INTENT(in) :: mesh
        type(mainNodeParameters), intent(inout) :: mainParameters
        character(len=30) :: fileString

        unit = 10
        allocate(mainParameters%nuMap(com%nTasks,mesh%ns))
        mainParameters%nuMap = -1;

        do j=1,com%nTasks
            fileString = 'submesh_'//leftString(j)
            open(unit, file=fileString, iostat=status)

            read(unit,*)
            read(unit,*)

            read(unit,*) !! Names
            read(unit,*) nNames

            do i = 1,nNames
                read(unit,*)
            enddo

            read(unit,*)
            read(unit,*)nTriangles

            do k = 1,nTriangles
                read(unit,*)
            enddo

            read(unit,*)
            read(unit,*)nEdges

            do k = 1,nEdges
                read(unit,*)
            enddo

            read(unit,*)
            read(unit,*)nVertices

            do k = 1,nVertices
                read(unit,*)id
                mainParameters%nuMap(j,id) = k
            enddo
        enddo
    end subroutine createNuMap

    subroutine updateMainUa(com, var, globalVar, mainParameters, data, mesh)
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(in) :: mesh
        TYPE(variables), INTENT(inout):: globalVar
        TYPE(variables), INTENT(in):: var
        type(mainNodeParameters), intent(in) :: mainParameters
        TYPE(donnees), intent(in) :: data
        real(dp), dimension(:,:,:), allocatable :: tempUa
        integer :: uaSize, StatInfo, j, k, l, ierror, index
        integer, dimension(mpi_status_size) :: status
        real(dp), dimension(:), allocatable :: sendReal, receiveReal

        if (com%me == com%main) then     
            uaSize = size(globalVar%ua,2)
            allocate(tempUa(com%nTasks,4,uaSize))
            allocate(receiveReal(uaSize))
            tempUa = -99999

            do j=1,4
                do l = 1, mesh%nDofs
                    tempUa(1,j,l) = var%ua(0,l)%u(j)  
                enddo
                do i = 2, com%nTasks
                    call MPI_Recv(tempUa(i,j,:),uaSize,mpi_real8,i-1,j,FluidBox_Comm,status,StatInfo)
                enddo
                call mpi_barrier(FluidBox_Comm, ierror)
            enddo

            do j = 1,4 
                do l = 1,uaSize
                    do i = 1, com%nTasks
                        index = mainParameters%nuMap(i,l)
                        if (index /= -1) then
                            globalVar%ua(0,l)%u(j) = tempUa(i,j,index)
                        endif
                    enddo
                enddo
            enddo
            deallocate(tempua)
            deallocate(receiveReal)
        else
            allocate(sendReal(mesh%nDofs))
            sendReal = -1;
            do j=1,4
                do l = 1, mesh%ndOfs
                    sendReal(l) = var%ua(0,l)%u(j)
                enddo
                call mpi_send(sendReal,mesh%nDofs,mpi_real8,com%main,j,FluidBox_Comm,ierror)
                call mpi_barrier(FluidBox_Comm, ierror)
            enddo
            deallocate(sendreal)
        endif
    end subroutine updateMainUa
    
    subroutine updateSubMeshsNdofs(com,mesh)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(inout) :: mesh
        integer :: ierror, i
        integer, dimension(mpi_status_size) :: status

        allocate(mesh%subMeshsNdofs(com%nTasks))

        if (com%me == com%main) then
            mesh%subMeshsNdofs(1) = mesh%ndofs
            do i = 1,com%nTasks-1
                call mpi_recv(mesh%subMeshsNdofs(i+1),1,MPI_INT,i,1,FluidBox_Comm,status,StatInfo)
            enddo
            call mpi_barrier(com%comWorld, ierror)
        else    
            call mpi_send(mesh%ndofs,1,MPI_INT,com%main,1,FluidBox_Comm,ierror)
            call mpi_barrier(com%comWorld, ierror)
        endif

        call mpi_bcast(mesh%subMeshsNdofs,com%nTasks,mpi_int,com%main,com%comWorld,ierror)
    end subroutine updateSubMeshsNdofs

    subroutine createTransferPlan(mesh,com)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(inout) :: mesh
        integer :: ierror, i, j, currentSize, endsize
        integer, dimension(mpi_status_size) :: status
        integer, dimension(:,:), allocatable :: neighbours, sendNeighbours, tempTransferPlan
        if (com%me == com%main) then
            allocate(neighbours(com%ntasks,com%ntasks))
            allocate(mesh%transferPlan(2,0))
            neighbours = -1
            neighbours(1,1:size(mesh%neighbourIds)) = mesh%neighbourIds
            do i = 1, com%nTasks-1
                call mpi_recv(neighbours(i+1,:),com%ntasks,MPI_INT,i,1,FluidBox_Comm,status,StatInfo)
            enddo
        else
            call mpi_send(mesh%neighbourIds,size(mesh%neighbourIds),MPI_INT,com%main,1,FluidBox_Comm,ierror)
        endif
        call mpi_barrier(com%comWorld, ierror)
        
        if (com%me == com%main) then
            do i = 1, com%nTasks
                do j = 1,com%nTasks
                    if (.not. neighbours(i,j) < i) then
                        currentSize = size(mesh%transferPlan,2)
                        allocate(tempTransferPlan(2,currentSize))
                        tempTransferPlan = mesh%transferPlan

                        deallocate(mesh%transferPlan)
                        allocate(mesh%transferPlan(2,currentSize+2))

                        mesh%transferplan(:,1:currentSize) = tempTransferPlan(:,:)
                        mesh%transferplan(1,currentSize+1) = i
                        mesh%transferplan(2,currentsize+1) = neighbours(i,j)
                        mesh%transferplan(2,currentSize+2) = i
                        mesh%transferplan(1,currentsize+2) = neighbours(i,j)
                        deallocate(tempTransferPlan)
                    endif
                enddo
            enddo
        endif

        endsize = size(mesh%transferplan,2);

        call mpi_bcast(endsize,1,mpi_int,com%main,com%comWorld,ierror)
        call mpi_barrier(com%comWorld, ierror)

        if (com%me /= com%main) then
            allocate(mesh%transferplan(2,endsize))
        endif
        call mpi_bcast(mesh%transferplan(1,:),endsize,mpi_int,com%main,com%comWorld,ierror)
        call mpi_bcast(mesh%transferplan(2,:),endsize,mpi_int,com%main,com%comWorld,ierror)      
        call mpi_barrier(com%comWorld, ierror)
    end subroutine createTransferPlan

    subroutine updateNeighboursNu(mesh,com, globalmesh)
        implicit none
        Type(meshCom), intent(in) :: Com
        TYPE(maillage), INTENT(inout) :: mesh, globalmesh
        integer, dimension(mpi_status_size) :: status
        integer :: i,j,elementIndex, ierror, sendId, k
        TYPE(element):: e, e1, e2
        integer, dimension(:), allocatable :: receiveInt, sendInt
        
        e%volume = -1

        allocate(receiveInt(mesh%e(1)%nsommets))
        allocate(sendInt(mesh%e(1)%nsommets))

        do i = 1,size(mesh%transferPlan,2)
            if (com%me + 1 == mesh%transferPlan(1,i)) then
                do j = 1,mesh%nsegmt
                    if (mesh%edge(j)%touchingElements(1) > 0) then
                        e1 = mesh%boundaryE(mesh%edge(j)%touchingElements(1))
                    else
                        e1%subMeshId = -1
                    endif
                    if (mesh%edge(j)%touchingElements(2) > 0) then
                        e2 = mesh%boundaryE(mesh%edge(j)%touchingElements(2))
                    else
                        e2%subMeshId = -1
                    endif

                    if (e1%subMeshId == mesh%transferPlan(2,i) &
                        &.or. e2%subMeshId == mesh%transferPlan(2,i)) then
                        if (e1%subMeshId == com%me+1) then
                            sendId = e2%subMeshId
                            e = e1
                        else if (e2%subMeshId == com%me + 1) then
                            sendId = e1%subMeshId
                            e = e2
                        endif
                        if (e%volume >= 0) then
                            do k = 1,mesh%nT
                                if (mesh%e(k)%id == e%id) then
                                    e%nu = mesh%e(k)%nu
                                    e%nsommets = mesh%e(k)%nsommets
                                    e%id = mesh%e(k)%id
                                    exit
                                endif
                            enddo
                            call mpi_send(e%nu,e%nsommets,mpi_int,sendId-1,e%id,FluidBox_Comm,ierror)
                        endif
                        e%volume = -1
                    endif
                enddo
            else if (com%me + 1 == mesh%transferPlan(2,i)) then
                do j = 1,mesh%nsegmt

                    if (mesh%edge(j)%touchingElements(1) > 0) then
                        e1 = mesh%boundaryE(mesh%edge(j)%touchingElements(1))
                    else
                        e1%subMeshId = -1
                    endif
                    if (mesh%edge(j)%touchingElements(2) > 0) then
                        e2 = mesh%boundaryE(mesh%edge(j)%touchingElements(2))
                    else
                        e2%subMeshId = -1
                    endif
                    
                    if (e1%subMeshId == mesh%transferPlan(1,i)) then
                        e = e1
                        elementIndex = 1
                    else if (e2%subMeshId == mesh%transferPlan(1,i)) then
                        e = e2
                        elementIndex = 2
                    endif
                    if (e%volume >= 0) then
                        call mpi_recv(receiveInt,&
                            &e%nsommets,mpi_int,e%submeshId-1,e%id,FluidBox_Comm,status,ierror)
                            mesh%boundaryE(mesh%edge(j)%touchingElements(elementIndex))%localNu = receiveInt
                    endif
                    e%volume = -1                
                enddo
            endif
            call mpi_barrier(FluidBox_Comm, ierror)
        enddo
    end subroutine updateNeighboursNu

    subroutine stop_run(ident)
        implicit none
        CHARACTER(LEN = *), PARAMETER :: mod_name = "stop_run"
        INTEGER, INTENT(IN), OPTIONAL :: ident

        PRINT *, mod_name, "MPI_Abort va demarrer"
        IF (Present(ident)) THEN
            !CALL delegate_stop(ident)
        ELSE
            !CALL delegate_stop( -1)
        END IF
    end subroutine stop_run

    subroutine IniCom(Com)
        implicit none
        CHARACTER(LEN = *), PARAMETER       :: mod_name = "IniCom"
        TYPE(MeshCom), INTENT(INOUT)        :: Com
        INTEGER                             :: Me, MyTid, BarrierOpt, NTasks
        INTEGER                             :: itask

        BarrierOpt               = 0
        CALL mpi_init(StatInfo)

        IF (StatInfo /= 0) THEN
        PRINT *, mod_name, " : ERREUR : StatInfo(Mpi_init)==", StatInfo
        CALL stop_run()
        END IF

        ! P. JACQ : Création d'un communicateur privé (important pour couplage)
        FluidBox_Comm = mpi_comm_world

        CALL mpi_comm_size(FluidBox_Comm, NTasks, StatInfo)
        CALL mpi_comm_rank(FluidBox_Comm, Me, StatInfo)
        MyTid = Me
        PRINT *, mod_name, " Me==", Me, " NTasks==", NTasks

        Com%comWorld     = mpi_comm_world
        Com%NTasks      = NTasks
        Com%Me          = Me
        Com%main        = 0
    end subroutine IniCom

    subroutine EndCom(Com)
        implicit none
        CHARACTER(LEN = *), PARAMETER   :: mod_name = "EndCom"
        TYPE(MeshCom), INTENT(INOUT)    :: Com

        IF (Com%NTasks == 1) THEN
            RETURN
        END IF

        

        IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_BARRIER: ', StatInfo
        END IF

        CALL mpi_finalize(StatInfo)
        IF (StatInfo < 0) THEN
            PRINT *, mod_name, ' ERREUR dans MPI_FINALIZE: ', StatInfo
        END IF
    end subroutine EndCom

    function coordsAreAdded(i,j,addedCoords) result(added)
        real(dp) :: i,j
        real(dp), dimension(:,:) :: addedCoords
        integer :: k
        logical :: added
        real :: limit = 0

        added = .false.

        do k=1,size(addedCoords,2)
            if ((abs(addedCoords(1,k) - i) .le. limit) .and. (abs(addedCoords(2,k) - j) .le. limit)) then
                added = .true.
                exit
            endif
        enddo
    end function coordsAreAdded

    function inIntArray(haystack,needle) result(found)
        implicit none
        Logical                 :: found
        Integer, dimension(:)   :: haystack
        integer                 :: needle,i

        found = .FALSE.

        do i=1,size(haystack)
            if (haystack(i) == needle) then
                found = .TRUE.
            endif
        enddo
    end function inIntArray

    function findIndexInArray(arrayArg,needle) result(index)
       implicit none
       integer :: index, needle,j
       integer, dimension(:) :: arrayArg
       index = -1

       do j = 1,size(arrayArg)
          if (arrayArg(j) == needle) then
            index = j
          endif
        enddo
    end function findIndexInArray

    function leftString(inInt) result(outAns)
        implicit none
        character(len=100)  :: outAns
        INTEGER             :: inInt
        write(outAns,*)inInt
        outAns = trim(adjustl(outAns))
    end function leftString

    function checkExitCondition(Com, boundary) result(exitCondition)
        logical :: exitCondition
        Type(meshCom) :: Com
        integer :: send, receive
        integer, dimension(mpi_status_size) :: status
        real(dp) :: boundary

        exitCondition = .TRUE.

        if (com%me /= com%main) then
            if (boundary.LE.1.d-6) then
                send = 1
            else
                send = 0
            endif

            call mpi_send(send,1,MPI_INT,com%main,1,mpi_comm_world,ierror)
        else
            do i=1,com%NTasks-1
                call MPI_Recv(receive,1,MPI_INT,i,1,mpi_comm_world,status,StatInfo)
                if (receive == 0) then
                    exitCondition = .false.
                endif
            enddo

            if (.NOT. (boundary .LE. 1.d-6)) then
                exitCondition = .false.
            endif

            send = 0

            if (exitCondition) then
                send = 1
            endif
        endif

        call mpi_barrier(mpi_comm_world,ierror)
        call mpi_bcast(send,1,mpi_int,com%main,mpi_comm_world,ierror)

        if (send /= 1) then
            exitCondition = .false.
        endif
    end function checkExitCondition

end module ParallelFunctions
