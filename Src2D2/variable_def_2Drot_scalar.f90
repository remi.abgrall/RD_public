MODULE variable_def

  IMPLICIT NONE
  INTEGER,PUBLIC, PARAMETER:: iflux=2 ! 0: burger, 1 rotation, 2: kpp
  INTEGER, PUBLIC,PARAMETER:: n_vars = 1   ! number of primitive variables
  INTEGER, PUBLIC,PARAMETER:: n_dim  = 2   ! number of physical dimensions
  REAL,    PARAMETER:: pi=ACOS(-1.) ! pi

  !===============================================================
  ! Type for the vector of primitive variables
  TYPE, PUBLIC :: PVar
     INTEGER                              :: NVars = n_vars
     REAL, DIMENSION(n_vars)              :: U

   CONTAINS 
     PROCEDURE, PUBLIC:: flux            => flux_scal
     PROCEDURE, PUBLIC:: spectral_radius => spectral_radius_scal
     PROCEDURE, PUBLIC:: Jacobian        => Jacobian_scal
     PROCEDURE, PUBLIC:: Nmat            => Nmat_scal
  END TYPE PVar

  PRIVATE


CONTAINS


!!!!-----------------------


  FUNCTION roe_scal(e,u,x) RESULT (J)
    ! evaluate a roe average to estimate a rough speed for 
    ! Burman jump operator
    CLASS(Pvar), INTENT(in):: e

    REAL,DIMENSION(N_Vars), INTENT(in):: u
    REAL, DIMENSION(n_dim), INTENT(in):: x
    REAL, DIMENSION(n_dim):: J
    REAL:: v
    v=0.5*(e%u(1)+u(1))
    SELECT CASE(iflux)
    CASE(0)
       ! For burger
       j(1)=v; J(2)=1
       !   j(1)=-v; J(2)=0.
    CASE(1)
       ! For rotation
       J(1) = -x(2) *2.*pi !1.0
       J(2) = (x(1))*2.*pi !0.0
    CASE(2)
       ! kpp
       J(1) = COS(v)
       J(2) = -SIN(v)
    END SELECT
  END FUNCTION roe_scal

  FUNCTION flux_scal(e, x) RESULT(f)
    CLASS(PVar),                  INTENT(in) :: e
    REAL,       DIMENSION(n_dim), INTENT(in) :: x
    TYPE(PVar), DIMENSION(n_dim)             :: f
    SELECT CASE(iflux)
    CASE(0)
       ! burger
       f(1)%U(1) = 0.5*( e%u(1))**2
       f(2)%U(1) = e%u(1)
    CASE(1)
       f(1)%U(1) = -x(2)*e%U(1)*2.*pi  !e%U(1)
       f(2)%U(1) =  x(1)*e%U(1)*2.*pi  !1.0
    CASE(2) ! kpp
       f(1)%U(1) = SIN( e%u(1))
       f(2)%U(1) = COS(e%u(1) )
    CASE(4)
       ! advectio
       f(1)%u(1)=-e%u(1)
       f(2)%u(1)=0.
       ! rotation
    END SELECT
    ! kpp

  END FUNCTION flux_scal

  FUNCTION Jacobian_Scal(e,x) RESULT(J)
    CLASS(Pvar),              INTENT(in) :: e
    REAL, DIMENSION(n_dim),   INTENT(in) :: x
    REAL, DIMENSION(n_Vars,n_Vars,n_dim) :: J
    SELECT  CASE(iflux)
    CASE(0)
       !burgers
       J(1,1,1) = e%u(1)
       J(1,1,2) = 1.0
    CASE(1) ! rotation
       !rotation
       J(1,1,1) = -x(2) *2.*pi !1.0
       J(1,1,2) =  x(1) *2.*pi !0.0
    CASE(2)
       !kpp
       J(1,1,1) =  COS(e%u(1))
       J(1,1,2) = -SIN(e%u(1))
    CASE(3)
       ! advection
       J(1,1,1) = -1.0
       J(1,1,2) = 0.0
    END SELECT

  END FUNCTION Jacobian_scal

  FUNCTION evalues_scal(e,x,n) RESULT(lambda)
    ! eigenvalues: diagonal matrix. It is written as a matrix for ease of calculations
    CLASS(PVar),            INTENT(in) :: e
    REAL, DIMENSION(n_dim), INTENT(in) :: x
    REAL, DIMENSION(n_dim)             :: n
    REAL, DIMENSION(n_Vars,n_Vars)     :: lambda

    REAL, DIMENSION(n_vars, n_vars, n_dim) :: J
    INTEGER:: i

    J= Jacobian_scal(e,x)
    lambda=0.
    DO i=1, n_Vars
       lambda(i,i) = J(1,1,1)*n(1) + J(1,1,2)*n(2) !n(1)
    ENDDO

  END FUNCTION evalues_scal

  REAL FUNCTION spectral_radius_scal(e,x,n)
    ! compute the maximum value of eigenvalues:
    ! max_i {lambda_ii}
    CLASS(PVar),            INTENT(in) :: e
    REAL, DIMENSION(n_dim), INTENT(in) :: x
    REAL, DIMENSION(n_dim), INTENT(in) :: n
    REAL, DIMENSION(n_Vars,n_Vars)     :: lambda1,lambda2
    REAL, DIMENSION(n_dim):: m
    m=0.
    m(1)=1
    lambda1 = evalues_scal(e,x,m)
    m=0
    m(2)=1
    lambda2 = evalues_scal(e,x,m)
    spectral_radius_scal =SQRT(lambda1(1,1)**2+lambda2(1,1)**2) * SQRT(n(1)**2 + n(2)**2 )
    !print*,"spectral", 2*pi, x
    !spectral_radius_scal=2*pi*sqrt(x(1)**2+x(2)**2)!*sqrt(n(1)**2 + n(2)**2 )!*e%volum

  END  FUNCTION spectral_radius_scal

  FUNCTION rvectors_scal(e,n) RESULT(R)
    ! right e-vectors
    CLASS(PVar),        INTENT(in) :: e
    REAL, DIMENSION(n_dim)         :: n
    REAL, DIMENSION(n_Vars,n_Vars) :: R

    R(:,:) = 1.0

  END FUNCTION rvectors_scal

  FUNCTION lvectors_scal(e,n) RESULT(L)
    ! left e-vectors
    CLASS(PVar),         INTENT(in) :: e
    REAL, DIMENSION(n_dim)          :: n
    REAL, DIMENSION(n_Vars,n_Vars)  :: L

    L(:,:) = 1.0

  END FUNCTION lvectors_scal



  FUNCTION Nmat_scal(e, n_ord, grad, x) RESULT (Nmat)
    CLASS(Pvar),                  INTENT(in) :: e
    INTEGER,                      INTENT(in) :: n_ord
    REAL, DIMENSION(n_dim),       INTENT(in) :: x
    REAL, DIMENSION(n_dim,n_ord), INTENT(in) :: grad
    REAL, DIMENSION(n_vars,n_vars)           :: Nmat
    REAL, DIMENSION(n_vars, n_vars, n_dim)   :: J
    INTEGER:: l

    J= Jacobian_scal(e,x)
    Nmat=0
    DO l=1, n_ord
       Nmat = Nmat + ABS(grad(1,l)*J(1,1,1)+grad(2,l)*J(1,1,2) )
    ENDDO
    Nmat =1./(nmat+1.e-6)

  END FUNCTION Nmat_scal

  FUNCTION min_mat_scal(e, x, n, alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL, DIMENSION(n_dim), INTENT(in) :: x
    REAL,                   INTENT(in) :: alpha
    REAL, DIMENSION(n_dim), INTENT(IN) :: n
    REAL, DIMENSION(N_vars, N_vars)    :: Ap
    REAL, DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL, DIMENSION(n_Vars,n_Vars)     :: R
    REAL, DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_scal(e,x,n)
    R      = rvectors_scal(e,n)
    L      = lvectors_scal(e,n)
    Ap     = MATMUL( R, MATMUL( MIN(lambda(:,:),alpha), L) )

  END FUNCTION min_mat_scal


  FUNCTION max_mat_scal(e,x,n,alpha) RESULT (Ap)
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL, DIMENSION(n_dim), INTENT(in) :: x
    REAL,                   INTENT(in) :: alpha
    REAL, DIMENSION(n_dim), INTENT(in) :: n
    REAL, DIMENSION(N_vars, N_vars)    :: Ap
    REAL, DIMENSION(n_Vars,n_Vars)     :: lambda
    REAL, DIMENSION(n_Vars,n_Vars)     :: R
    REAL, DIMENSION(n_Vars,n_Vars)     :: L

    lambda = evalues_scal(e,x,n)
    R      = rvectors_scal(e,n)
    L      = lvectors_scal(e,n)
    Ap     = MATMUL( R, MATMUL( MAX(lambda(:,:),alpha), L) )

  END FUNCTION max_mat_scal

END MODULE variable_def
