!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE  time_stepping
  USE param2d
  USE scheme
  USE overloading
  USE Model
  USE update
  USE PRECISION
  USE Boundary
  IMPLICIT NONE
CONTAINS

  SUBROUTINE dec( Var,DATA,Mesh, dt, n_theta, alpha, beta, gamma, theta)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="dec"

    TYPE(maillage), INTENT(inout):: mesh
    TYPE(variables), INTENT(inout):: var
    TYPE(donnees), INTENT(in):: DATA
    REAL(DP), INTENT(in):: dt


    REAL(DP),DIMENSION(3,2:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma

    !    INTEGER, DIMENSION(4), INTENT(in):: n_theta
    !    REAL(DP),DIMENSION(0:3,3,2:4), INTENT(in):: theta

    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta

    TYPE(element):: e, e1, e2
    TYPE(arete):: ed
    TYPE(frontiere):: efr

    TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: res,residu, uu, difference
    TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
    TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
    TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: flux, flux_c
    TYPE(pvar),DIMENSION(:), ALLOCATABLE:: wn

    INTEGER:: nt,itype, jt, i, kt, is, l, k_inter, p1, p2, k, iseg, jt1, jt2, ll, kt0, lp


    DO is=1, Mesh%ndofs
       Var%up(:,is)=Var%ua(:,is)
    ENDDO


    loop: DO k=2, DATA%iordret !loop for the subtimesteps m=1,.......M within [t_{n}, t_{n+1}]

       DO jt=1, Mesh%nt
          Var%un(:,jt)=0.0_dp
       ENDDO


       DO jt=1, Mesh%nt
          !          var%un(:,jt)=0._dp
          CALL main_update(k,jt,dt,Mesh%e(jt),Var,DATA,alpha,beta,gamma,n_theta,theta	)
        ENDDO

       !--------------------------------------------------------------------------------------
       ! EDGE UPDATE DONE ONLY IN CASE WE DO NOT HAVE LXF scheme

       IF (DATA%ischema==4 .OR.DATA%ischema==5.or.Data%ischema==8  ) THEN !(with jumps--> Burman'stuff)
          CALL edge_main_update(k,DATA,Mesh,Var,dt,alpha,beta,gamma,n_theta,theta)
       ENDIF

       !--------------------------------------------------------------------------------------
       DO jt=1, Mesh%Nsegfr
          var%un_b(:, jt)=0._dp
       ENDDO
       CALL BC(Mesh, k, dt, DATA%temps, Var%ua, Var%un_b, DATA, alpha,beta,gamma,n_theta,theta)
       !
       ! if correction kinetic momentum, then do it
       !
       IF (DATA%cor) THEN
          CALL with_correction( )
       ELSE

          CALL without_correction ()

       END IF

       !      Var%un=0.0_dp
       !      var%un_b=0._dp



       !***************************************

    ENDDO loop

  CONTAINS

    SUBROUTINE with_correction( )
      ! correction for the kinetic momentum
      DO jt=1, Mesh%nt
         CALL main_update_corrected(k,jt,dt,Mesh%e(jt),Var,DATA,alpha,beta,gamma,n_theta,theta)
      ENDDO

      CALL BC_corrected(Mesh, k, dt, DATA%temps, Var%ua, Var%un_b, DATA, alpha,beta,gamma,n_theta,theta)
      !--------- Update of the solution -----------------------------------------------

      DO jt=1, Mesh%nt
         e=Mesh%e(jt)
         DO l=1, e%nsommets
            var%ua(k-1,e%nu(l))=Var%ua(k-1,e%nu(l))-Var%un(l,jt)*Mesh%aires(e%nu(l))
         ENDDO
      ENDDO

      DO jt=1, Mesh%Nsegfr
         efr=Mesh%fr(jt)
         DO l=1, efr%nsommets
            var%ua(k-1,efr%nu(l)) =Var%ua(k-1,efr%nu(l))-Var%un_b(l,jt)*Mesh%aires(efr%nu(l))
         ENDDO
      ENDDO
    END SUBROUTINE with_correction



    SUBROUTINE without_correction ()  ! no modification
      type(Pvar):: prim
      DO jt=1, Mesh%nt
         e=Mesh%e(jt)
         !         print*, mod_name, "int", jt, k
         DO l=1, e%nsommets
            !            prim=var%ua(k-1,e%nu(l))%cons2prim()
            !            print*,'a', Var%un(l,jt)%u
            var%ua(k-1,e%nu(l))=Var%ua(k-1,e%nu(l))-Var%un(l,jt)*Mesh%aires(e%nu(l))
            !            prim=var%ua(k-1,e%nu(l))%cons2prim()
            !            print*,'p', prim%u
         ENDDO
      ENDDO

      DO jt=1, Mesh%Nsegfr
         efr=Mesh%fr(jt)
         !         print*, mod_name, efr%bc_tag, jt, k-1
         DO l=1, efr%nsommets
            prim=Var%ua(k-1,efr%nu(l))%cons2prim()
            !            print*,'a',  prim%u
            var%ua(k-1,efr%nu(l)) =Var%ua(k-1,efr%nu(l))-Var%un_b(l,jt)*Mesh%aires(efr%nu(l))
            !          prim=Var%ua(k-1,efr%nu(l))%cons2prim()
            !            print*,'p' , prim%u

         ENDDO
      ENDDO
      !      stop
    END SUBROUTINE without_correction

  END SUBROUTINE dec

END MODULE time_stepping

