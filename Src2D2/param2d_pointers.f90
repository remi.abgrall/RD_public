!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE param2d
  USE element_class
  USE variable_def
  USE arete_class
  USE frontiere_class
  !  USE neighbor_class
  USE PRECISION

  IMPLICIT NONE
  INTEGER, PARAMETER:: nmax=16

  TYPE, PUBLIC:: neighbor
     INTEGER                           :: nbre
     INTEGER, DIMENSION(:), ALLOCATABLE:: nvois ! list of neigboring elements
     INTEGER, DIMENSION(:), ALLOCATABLE:: loc  ! position if is in vois(k)
  END TYPE neighbor

  TYPE, PUBLIC:: gradient
     REAL(dp), DIMENSION(:,:), ALLOCATABLE:: grad
  END TYPE gradient

  TYPE, PUBLIC:: var_edge
     TYPE(Pvar), DIMENSION(:), pointer:: var
  END TYPE var_edge


  TYPE maillage
     INTEGER:: ndofs
     INTEGER:: Ndim=2
     INTEGER:: ns, nt, Nsegfr, Nsegmt
     INTEGER, DIMENSION(:,:),   POINTER    :: NsfacFr=>Null()
     INTEGER, DIMENSION(:)  ,   POINTER    :: LogFac=>Null()!, logt
     INTEGER, DIMENSION(:,:),   POINTER    :: nubo=>Null()
     INTEGER, DIMENSION(:,:),   POINTER    :: nutv=>Null()
     INTEGER, DIMENSION(:,:),   POINTER    :: nuseg=>Null()
     INTEGER, DIMENSION(:,:),   POINTER    :: nusv=>Null()
     TYPE(element),DIMENSION(:),POINTER:: e=>Null()
     TYPE(frontiere),DIMENSION(:),POINTER:: fr=>Null()
     TYPE(arete)  ,DIMENSION(:),POINTER:: edge=>Null()
     INTEGER, DIMENSION(:),     POINTER:: per=>Null()
     LOGICAL                           :: period
     REAL(DP),DIMENSION(:),POINTER     :: aires=>Null()

     TYPE(neighbor), DIMENSION(:), POINTER    :: vois =>Null()       ! list of elements and position if is in these elements



#ifdef parallel
     TYPE(element), DIMENSION(:), ALLOCATABLE:: boundaryE
     TYPE(element), DIMENSION(:), ALLOCATABLE :: innerBoundaryE
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: coordsIds
     INTEGER, DIMENSION(:), ALLOCATABLE :: subMeshsNdofs ! This stores the amount of dofs of each submesh
     INTEGER :: submeshId ! If Parallel is active we need to store the submesh id
     INTEGER, DIMENSION(:), ALLOCATABLE :: neighbourIds ! this will be a vector of all the connected submeshs
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: transferPlan
#endif

  END TYPE maillage

  TYPE variables
     REAL(DP):: dt
     INTEGER:: Ncells
     TYPE(PVar), DIMENSION(:,:), POINTER:: ua=>Null(), up=>Null()
     TYPE(Pvar), DIMENSION(:,:),   POINTER:: un=>Null(), un_b=>Null()
  END TYPE variables

  TYPE donnees
     INTEGER:: iordret,iter ! also defines the number of levels in the Dec
     REAL(DP):: cfl
     INTEGER:: itype, itype_b
     INTEGER:: ktmax
     REAL(DP):: tmax
     REAL(DP):: temps=0._dp
     INTEGER:: ifre
     INTEGER:: ischema
     REAL(DP):: theta_jump
     REAL(DP) :: theta_jump2
     CHARACTER(Len=80)     :: maillage
     LOGICAL:: restart, loc_dec
     INTEGER:: limit
     INTEGER:: Icas ! case : initialisation
     LOGICAL:: cor  ! 1: kinetic momentum, 2: vorticity, other: no correction
     LOGICAL:: mood
  END TYPE donnees

#ifdef parallel
  TYPE meshCom
     INTEGER                          :: NTasks !< NTasks = nombre total de taches
     INTEGER                          :: Me  !< Me = numbro du processeur courant (processeur ou teche ?)
     INTEGER                          :: main
     INTEGER                          :: comWorld
     INTEGER                          :: triangleVectorMpiType
     INTEGER                          :: triangleMpiType
  END TYPE meshCom

  TYPE mainNodeParameters
     INTEGER, DIMENSION(:), ALLOCATABLE :: maxBoundaryTrianglesVector
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: nDofsCorrespondencies ! Index 1: nTasks, index 2: correspondencies
     REAL(dp), DIMENSION(:,:,:), ALLOCATABLE :: unMatrix ! Index 1: nTasks, Index 2: Variables, Index 3: Values
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: nuMap
  END TYPE mainNodeParameters
#endif
END MODULE param2d
