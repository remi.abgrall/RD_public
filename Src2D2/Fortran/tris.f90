program test
  implicit none
  integer:: m
  real, dimension(:,:), allocatable:: points,z
  real, dimension(2):: x
  integer:: i, l

   read(1,*) m
  allocate(points(2,m),z(2,m))
  do i=1,m
     read(1,*) x
     l=l+1
     points(:,i)=x
     print*, points(:,i)
  enddo
  call tri(points, z)
  print*, "apres"
  do i=1, m
     write(*,*) z(:,i)
  enddo

contains
  subroutine tri(x,y)
    implicit none
    real, dimension(:,:), intent(in):: x
    real, dimension(:,:), intent(out):: y
    real, dimension(2, size(x, dim=2)):: buff
    logical, dimension(size(x,dim=2)):: ind
    integer:: m, j, k

    m=size(x,dim=2)
    ind=.true.
    k=0
    do i=1, m
       j=minloc(points(1,:),1, ind)
       k=k+1
       buff(:, k)=points(:,j)
       ind(j)=.false.
    enddo
    y=buff
  end subroutine tri
  end program test
