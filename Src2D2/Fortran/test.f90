! test.f90
program call_python
!  use, intrinsic :: iso_c_binding

  implicit none


  integer:: n=1
  real:: tested(2),cloud(2,15), zcloud(2*15), bord(2*15)
  real:: x(2)
  integer:: m(1),flag,f
  integer:: i,l
  m=15
  l=0
  do i=1, 15
     read(1,*) x
     l=l+1
     cloud(:,i)=x
     zcloud(l)=x(1)
     l=l+1
     zcloud(l)=x(2)
  enddo

  read(2,*) x
  tested(1)=sum(cloud(1,:))/15.
  tested(2)=sum(cloud(2,:))/15.
  print*, 'hello'
  print*, 'zcloud'
  do l=1, 30
     print*, l,zcloud(l)
  enddo
  print*, 'test'
  do i=1,2
     print*, i, tested(i)
  enddo
  
  call make_quick(cloud,m, bord)
print*, 'output=',bord
end program call_python
