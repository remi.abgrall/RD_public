module quick_f
      USE, INTRINSIC :: iso_c_binding
  IMPLICIT NONE
  PUBLIC::
contains
  subroutine quick_h(cloud, m, bord) bind(c)
    use iso_c_binding
    integer(c_int), dimension(1), intent(in):: m
    real(c_double), DIMENSION(2*m(1)), INTENT(in)::cloud
    real(c_double), DIMENSION(2*m(1)), INTENT(out)::bord
  end subroutine quick_h

     INTEGER(C_INT), DIMENSION(1), INTENT(in):: m
    REAL(dp), INTENT(in):: cloud(m(1)*2)
    REAL(dp), INTENT(out):: bord(m(1)*2)
    call make_quick(cloud,m,bord)
    
  end subroutine quick_h
end module quiack_f

