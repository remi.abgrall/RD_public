!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE postprocessing

  ! This module is intended to output the postprocessing files with the data to generate plots.
  ! The data can in general easily read via visit (as example)
  ! In the data files are printed ALL the DOFs of the domain 

  USE variable_def
  USE param2d
  USE utils
  USE Model
  USE PRECISION

  IMPLICIT NONE

CONTAINS
  SUBROUTINE visu(DATA,Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of triangles only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var

    SELECT CASE (Mesh%e(1)%nvertex)
    CASE(3)
       CALL visu_tri(DATA, Lnombre, Mesh, Var)
    CASE(4)
       CALL visu_quad(DATA, Lnombre, Mesh, Var)
    CASE default
       PRINT*, mod_name
       PRINT*, "wrong case. We assume a full triangle or a full quad mesh"
    END SELECT
  END SUBROUTINE visu



  SUBROUTINE visu_tri(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_tri"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of triangles only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss


    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max
    real(dp), dimension(Mesh%ndofs):: vor
    TYPE(PVar) :: sol

    TYPE(PVar) :: W ! solution in primitive variables

    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    SELECT CASE(itype)
    CASE(1)
       IF(.NOT.ALLOCATED(nu2)) THEN

          ALLOCATE(nu2(3,Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF

    CASE(2,3)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(3,4*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF
    CASE(4,5)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(3,9*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L 114 ordre non pris en compte, type d'element: ", itype
       STOP
    END SELECT




    ip1(1)=2; ip1(2)=3; ip1(3)=1
    SELECT CASE(itype)
    CASE(1)
       DO jt=1, Mesh%nt
          nu2(1:3,jt)=Mesh%e(jt)%nu(1:3)
          coor(1,nu2(1:3,jt))=mesh%e(jt)%coor(1,:)
          coor(2,nu2(1:3,jt))=mesh%e(jt)%coor(2,:)
          nt2=Mesh%nt
       ENDDO
    CASE(2,3)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)

          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          jtt=jtt+1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(6)


          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(5)

          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(6)


          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(6)

       ENDDO
       nt2=jtt

    CASE(4,5)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          jtt=jtt+1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(9)

          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(10)
          nu2(3,jtt)=iss(9)

          jtt=jtt+1
          nu2(1,jtt)=iss(4)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(10)

          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(6)
          nu2(3,jtt)=iss(10)

          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(6)

          jtt=jtt+1
          nu2(1,jtt)=iss(6)
          nu2(2,jtt)=iss(7)
          nu2(3,jtt)=iss(10)

          jtt=jtt+1
          nu2(1,jtt)=iss(10)
          nu2(2,jtt)=iss(7)
          nu2(3,jtt)=iss(8)

          jtt=jtt+1
          nu2(1,jtt)=iss(10)
          nu2(2,jtt)=iss(8)
          nu2(3,jtt)=iss(9)

          jtt=jtt+1
          nu2(1,jtt)=iss(7)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(8)


       ENDDO
       nt2=jtt
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L215 pas encore implemente"
    END SELECT
    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution

    ! Recalculate solution at vertices for B2 case

    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)

       DO i = 1,e%nsommets


          DO j=1, n_vars
             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j)*e%base_at_dofs(:,i))
          ENDDO
       END DO ! i

    END DO ! jt






    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )


    IF (ios /= 0) THEN
       PRINT*, mod_name
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'

    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,*)'VARIABLES = "x","y",'//TRIM(ADJUSTL(vars3))
    WRITE(10,*)'ZONE T="P1", F=FEPOINT, N='//TRIM(ADJUSTL(nombre1))//&
         &     ", E="//TRIM(ADJUSTL(nombre2))//", ET=TRIANGLE"



    ! file output format
    WRITE(nvar_str,*) n_vars+5
    WRITE(format_str1,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    WRITE(nvar_str,*) n_vars
    WRITE(format_str,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    ! write data in Tecplot format

    DO is = 1, Var%ncells


       W = va2(is)%cons2prim()

       WRITE (10, format_str1) Coor(:, is), W%U(1:n_vars)




    END DO

100 FORMAT(5(e16.8))

    !     ecriture connectivites
    DO jt = 1, nt2
       WRITE(10, *) ( nu2(k, jt), k = 1, 3)
    END DO



    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_tri


  SUBROUTINE visu_quad_sub(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_quad_sub"
    !
    ! write a mesh, tecplot format.
    ! it is assumed it is made of quad2 only
    !
    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss


    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max

    TYPE(PVar) :: sol

    TYPE(PVar) :: W ! solution in primitive variables

    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    SELECT CASE(itype)
    CASE(1)
       IF(.NOT.ALLOCATED(nu2)) THEN

          ALLOCATE(nu2(4,Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF

    CASE(2,3)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(4,4*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF
    CASE(4,5)
       IF(.NOT.ALLOCATED(nu2)) THEN
          ALLOCATE(nu2(9,9*Mesh%nt))
          ALLOCATE(va2(Var%ncells))
          ALLOCATE(va (Var%ncells))
       ENDIF
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L 114 ordre non pris en compte, type d'element: ", itype
       STOP
    END SELECT



    SELECT CASE(itype)
    CASE(1)
       DO jt=1, Mesh%nt
          nu2(:,jt)=Mesh%e(jt)%nu(:)
          coor(1,nu2(:,jt))=mesh%e(jt)%coor(1,:)
          coor(2,nu2(:,jt))=mesh%e(jt)%coor(2,:)
          nt2=Mesh%nt
       ENDDO
    CASE(2,3) !Q2
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)

          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)

          jtt=jtt+1
          nu2(1,jtt)=iss(1)
          nu2(2,jtt)=iss(5)
          nu2(3,jtt)=iss(9)
          nu2(4,jtt)=iss(8)


          jtt=jtt+1
          nu2(1,jtt)=iss(5)
          nu2(2,jtt)=iss(2)
          nu2(3,jtt)=iss(6)
          nu2(4,jtt)=iss(9)


          jtt=jtt+1
          nu2(1,jtt)=iss(6)
          nu2(2,jtt)=iss(3)
          nu2(3,jtt)=iss(7)
          nu2(4,jtt)=iss(9)

          jtt=jtt+1
          nu2(1,jtt)=iss(7)
          nu2(2,jtt)=iss(4)
          nu2(3,jtt)=iss(8)
          nu2(4,jtt)=iss(9)

       ENDDO
       nt2=jtt

    CASE(4,5)
       jtt=0
       DO jt=1,Mesh%nt
          n_ord=Mesh%e(jt)%nsommets
          iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
          coor(1,iss(1:n_ord))=mesh%e(jt)%coor(1,:)
          coor(2,iss(1:n_ord))=mesh%e(jt)%coor(2,:)
          !1          
          jtt=jtt+1
          nu2(1,jtt)=iss(1 )
          nu2(2,jtt)=iss(5 )
          nu2(3,jtt)=iss(13)
          nu2(4,jtt)=iss(12)
          !2
          jtt=jtt+1
          nu2(1,jtt)=iss(5 )
          nu2(2,jtt)=iss(6 )
          nu2(3,jtt)=iss(14)
          nu2(4,jtt)=iss(13)
          !3
          jtt=jtt+1
          nu2(1,jtt)=iss(6 )
          nu2(2,jtt)=iss(2 )
          nu2(3,jtt)=iss(7 )
          nu2(4,jtt)=iss(14)
          !4
          jtt=jtt+1
          nu2(1,jtt)=iss(12)
          nu2(2,jtt)=iss(13)
          nu2(3,jtt)=iss(16)
          nu2(4,jtt)=iss(11)
          !5
          jtt=jtt+1
          nu2(1,jtt)=iss(13)
          nu2(2,jtt)=iss(14)
          nu2(3,jtt)=iss(15)
          nu2(4,jtt)=iss(16)
          !6
          jtt=jtt+1
          nu2(1,jtt)=iss(14)
          nu2(2,jtt)=iss(7 )
          nu2(3,jtt)=iss(8 )
          nu2(4,jtt)=iss(15)
          !7
          jtt=jtt+1
          nu2(1,jtt)=iss(11)
          nu2(2,jtt)=iss(16)
          nu2(3,jtt)=iss(10)
          nu2(4,jtt)=iss(14)
          !8
          jtt=jtt+1
          nu2(1,jtt)=iss(16)
          nu2(2,jtt)=iss(15)
          nu2(3,jtt)=iss(9 )
          nu2(4,jtt)=iss(10)
          !9
          jtt=jtt+1
          nu2(1,jtt)=iss(15)
          nu2(2,jtt)=iss(8 )
          nu2(3,jtt)=iss(3 )
          nu2(4,jtt)=iss(9 )



       ENDDO
       nt2=jtt
    CASE default
       PRINT*, mod_name
       PRINT*, "initialisation L215 pas encore implemente"
    END SELECT

    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution


    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)

       DO i = 1,e%nsommets



          DO j=1, n_vars
             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j) * e%base_at_dofs(:,4))
          ENDDO
       END DO ! i

    END DO ! jt





    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )

    IF (ios /= 0) THEN
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,*)'VARIABLES = "x","y",'//TRIM(ADJUSTL(vars3))
    WRITE(10,*)'ZONE NODES='//TRIM(ADJUSTL(nombre1))//&
         &     ", ELEMENTS="//TRIM(ADJUSTL(nombre2))//", DATAPACKING=POINT, ZONETYPE=FEQUADRILATERAL"


    ! file output format
    WRITE(nvar_str,*) n_vars+4
    WRITE(format_str1,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    WRITE(nvar_str,*) n_vars
    WRITE(format_str,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    ! write data in Tecplot format

    DO is = 1, Var%ncells

       W = va2(is)%cons2prim()

       WRITE (10, format_str1) Coor(:, is), W%U(1:n_vars)

    END DO

100 FORMAT(5(e16.8))

    !     ecriture connectivites
    DO jt = 1, nt2
       WRITE(10, *) ( nu2(k, jt), k = 1, 4)
    END DO


    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_quad_sub

  SUBROUTINE visu_quad(DATA, Lnombre, Mesh, Var)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "visu_quad"

    INTEGER, INTENT(in):: lnombre
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(in):: Var
    INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE:: nu2
    TYPE(PVar),DIMENSION(:),ALLOCATABLE,SAVE:: va2,va
    REAL(DP), DIMENSION(:,:),ALLOCATABLE:: coor
    REAL(DP), DIMENSION(2,nmax):: cx
    REAL(DP),DIMENSION(2,3):: n
    INTEGER, DIMENSION(nmax):: iss


    INTEGER:: itype, model_interp
    INTEGER:: is,jt,k,l,i,is1,is2,jt1,jt2,jt3,jtt, iter
    INTEGER:: is3,is4,is5,is6,iseg1,iseg2,iseg3,nt2
    REAL(DP):: err, err2,  L1, L2, L3
    REAL(DP):: x,y, x1, x2, x3, y1, y2, y3, aire, t
    INTEGER:: myunit , ifac, ndim,  j
    INTEGER:: ip1(3), kc, k1, k2,  ios, n_ord
    INTEGER:: nvar
    CHARACTER(LEN = 75) :: nombre, nombre1, nombre2, viscosite, vars, vars_2, nvar_str, format_str, vars3, format_str1
    TYPE(element):: e
    TYPE(frontiere):: efr
    REAL(DP), DIMENSION(Mesh%e(1)%nsommets):: base
    REAL(DP), DIMENSION(Mesh%fr(1)%nsommets):: base_efr
    TYPE(PVar) :: uloc, vloc
    REAL(DP) :: enrg
    TYPE(PVar), DIMENSION(Mesh%fr(1)%nsommets) :: u
    REAL(DP), DIMENSION(n_dim) :: xx
    REAL(DP),DIMENSION(2):: yy
    REAL(DP), DIMENSION(Mesh%ndofs):: v_min, v_max

    TYPE(PVar) :: sol

    TYPE(PVar) :: W ! solution in primitive variables

    WRITE(nombre, *) lnombre

    itype=DATA%itype

    ALLOCATE(coor(2,Var%ncells))

    IF(.NOT.ALLOCATED(nu2)) THEN
      ALLOCATE(nu2(Mesh%e(1)%nsommets,Mesh%nt))
      ALLOCATE(va2(Var%ncells))
      ALLOCATE(va (Var%ncells))
    ENDIF

    DO jt=1, Mesh%nt
       nu2(:,jt)=Mesh%e(jt)%nu(:)
       coor(1,nu2(:,jt))=mesh%e(jt)%coor(1,:)
       coor(2,nu2(:,jt))=mesh%e(jt)%coor(2,:)
       nt2=Mesh%nt
    ENDDO

    l=0


    !
    !     ... Format Declarations ...
    !
    !     Calcul de la solution


    DO jt = 1,Mesh%nt
       e = Mesh%e(jt)

       DO i = 1,e%nsommets

          DO l=1, e%nsommets
             base(l) = e%base(l, e%coorL(:,i) )
          END DO

          DO j=1, n_vars
             !va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j) * e%base_at_dofs(:,i))
             va2(e%nu(i))%u(j) = SUM(Var%ua(0,e%nu(:))%u(j) * base )
          ENDDO
       END DO ! i

    END DO ! jt

    MyUnit                         = 8

    myUnit=10

    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//"_"//TRIM(ADJUSTL(nombre))//".plt", IOSTAT = ios )

    IF (ios /= 0) THEN
       PRINT *," ERREUR : impossible d'ouvrir ", "sol.plt", ios
       STOP
    END IF
    REWIND(10)


    WRITE(nombre1,*) Var%ncells
    WRITE(nombre2,*) nt2


    ! write the Tecplot header for n_vars variables
    vars_2=""
    DO nvar = 1, n_vars
       WRITE(nvar_str,*) nvar
       vars=vars_2
       WRITE(vars_2,*) TRIM(ADJUSTL(vars))//'"U'//TRIM(ADJUSTL(nvar_str))//'",'
    END DO
    vars=vars_2
    vars3=TRIM(ADJUSTL(vars))

    WRITE(10,*)'TITLE = "Fbx T=6.5980E+03, Kt=6600"'
    WRITE(10,*)'VARIABLES = "x","y",'//TRIM(ADJUSTL(vars3))
    WRITE(10,*)'ZONE NODES='//TRIM(ADJUSTL(nombre1))//&
         &     ", ELEMENTS="//TRIM(ADJUSTL(nombre2))//", DATAPACKING=POINT, ZONETYPE=FEQUADRILATERAL"


    ! file output format
    WRITE(nvar_str,*) n_vars+4
    WRITE(format_str1,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    WRITE(nvar_str,*) n_vars
    WRITE(format_str,*) "("//TRIM(ADJUSTL(nvar_str))//"(e16.8))"

    ! write data in Tecplot format

    DO is = 1, Var%ncells

       W = va2(is)%cons2prim()

       WRITE (10, format_str1) Coor(:, is), W%U(1:n_vars)

    END DO

100 FORMAT(5(e16.8))

    !     ecriture connectivites
    DO jt = 1, nt2
       WRITE(10, *) ( nu2(k, jt), k = 1, 4)
    END DO


    CLOSE(10)


    DEALLOCATE(coor)

10  FORMAT(2(1x,e10.4))
11  FORMAT(3(1x,i5))
12  FORMAT(e10.4)

  END SUBROUTINE visu_quad



  SUBROUTINE errors(DATA, Mesh, Var, error_L1, error_L2, error_linf)
    TYPE(donnees),  INTENT(in)  :: DATA
    TYPE(maillage), INTENT(in)  :: Mesh
    TYPE(variables), INTENT(in)  :: Var
    TYPE(PVar),     INTENT(out) :: error_L1, error_L2, error_Linf

    TYPE(element)              :: e
    REAL(DP),    ALLOCATABLE       :: base(:)
    INTEGER, ALLOCATABLE       :: iss(:)
    TYPE(PVar),ALLOCATABLE,DIMENSION(:)                 :: error_L1_loc, error_L2_loc
    INTEGER                    :: jt, n_ord, iq, l, ios
    REAL(DP)                       :: xx, yy, h
    TYPE(PVar)                 :: sol_appr, sol_ex

!!$    error_L1%u = 0.0_dp
!!$    error_L2%u = 0.0_dp
!!$    error_linf%u=0.0_dp
!!$    ALLOCATE(error_L1_loc(Mesh%nt),error_L2_loc(Mesh%nt) )
!!$
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       error_L1_Loc%u(l)=0.0_dp
!!$       error_L2_Loc%u(l)=0.0_dp
!!$    ENDDO
!!$
!!$    ! Loop through mesh elements
!!$    DO jt=1, Mesh%nt
!!$
!!$
!!$       ! get element
!!$       e=Mesh%e(jt)
!!$
!!$       n_ord=e%nsommets
!!$
!!$       ALLOCATE(base(n_ord))
!!$       ALLOCATE(iss(n_ord))
!!$
!!$       iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
!!$
!!$       ! Loop through the quadrature points on the element e
!!$       DO iq=1, e%nquad
!!$
!!$          ! get the shape function
!!$          DO l=1,n_ord
!!$             base(l) = e%base(l,e%quad(:,iq))
!!$          ENDDO ! l
!!$
!!$          xx=SUM(e%coor(1,1:3)*e%quad(:,iq)); yy=SUM(e%coor(2,1:3)*e%quad(:,iq))
!!$
!!$          DO l=1, SIZE(sol_appr%u)
!!$             sol_appr%u(l) = SUM(Var%ua(0,e%nu(:))%u(l)*base)
!!$          ENDDO
!!$          sol_appr = convert_cons2prim(sol_appr)
!!$          sol_ex   = exact_vortex(xx,yy, DATA%temps)
!!$          DO l=1, Var%ua(0,1)%nvars
!!$             error_L1_loc(jt)%U(l)   = error_L1_loc(jt)%U(l) + ( ABS(sol_appr%U(l)-sol_ex%U(l))    )*e%weight(iq)
!!$             error_L2_loc(jt)%U(l)   = error_L2_loc(jt)%U(l) + ( ABS(sol_appr%U(l)-sol_ex%U(l))**2 )*e%weight(iq)
!!$             error_Linf%u(l)     = MAX(error_linf%u(l),ABS(sol_appr%U(l)-sol_ex%U(l)))
!!$          ENDDO
!!$       END DO ! iq
!!$
!!$       DEALLOCATE(base)
!!$       DEALLOCATE(iss)
!!$       DO l=1, Var%ua(0,1)%nvars
!!$          error_L1_loc(jt)%U(l)= error_L1_loc(jt)%U(l)*e%volume
!!$          error_L2_loc(jt)%U(l)= error_L2_loc(jt)%U(l)*e%volume
!!$       ENDDO
!!$
!!$    END DO ! jt
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       error_L2%U(l) = SQRT(SUM(error_L2_loc(:)%U(l)))
!!$       error_L1%U(l) =      SUM(error_L1_loc(:)%U(l))
!!$    ENDDO
!!$
!!$    OPEN( UNIT = 14, FILE = TRIM(ADJUSTL(DATA%maillage))//".err", IOSTAT = ios )
!!$
!!$    h= SQRT(MAX( SUM(Mesh%e(jt)%n(:,1)**2),SUM(Mesh%e(jt)%n(:,2)**2),SUM(Mesh%e(jt)%n(:,3)**2)))
!!$
!!$    WRITE (14,fmt=50) error_L1%U, Mesh%ns, h
!!$    WRITE (14,fmt=50) error_L2%U, Mesh%ns, h
!!$    WRITE (14,fmt=50) error_Linf%U, Mesh%ns, h
!!$    CLOSE(14)
!!$
!!$50  FORMAT((f15.10,1x,f15.10,1x,f15.10,1x,f15.10,1x,I6,1x,f15.10))
!!$
!!$    DO l=1, Var%ua(0,1)%nvars
!!$       PRINT*, 'variable= ', l, ' error_L1 = ', error_L1%U(l)
!!$       PRINT*, 'variable= ', l, ' error_L2 = ', error_L2%U(l)
!!$       PRINT*, 'variable = ',l, ' error Linf=', error_Linf%u(l)
!!$    ENDDO
!!$    DEALLOCATE(error_L1_loc,error_L2_loc )
!!$    CALL coupesol(Mesh,Var)
  END SUBROUTINE errors


  SUBROUTINE errors_old(DATA, Mesh, Var, error_L1, error_L2, error_linf)
    TYPE(donnees),  INTENT(in)  :: DATA
    TYPE(maillage), INTENT(in)  :: Mesh
    TYPE(variables), INTENT(in)  :: Var
    TYPE(PVar),     INTENT(out) :: error_L1, error_L2, error_Linf

    TYPE(element)              :: e
    REAL(DP),    ALLOCATABLE       :: base(:)
    INTEGER, ALLOCATABLE       :: iss(:)

    INTEGER                    :: jt, n_ord, iq, l, ios
    REAL(DP)                       :: xx, yy, h
    TYPE(PVar)                 :: sol_appr, sol_ex

    error_L1%u = 0.0_dp
    error_L2%u = 0.0_dp
    error_linf%u=0._dp
!!$
!!$    ! Loop through mesh elements
!!$    DO jt=1, Mesh%nt
!!$
!!$       ! get element
!!$       e=Mesh%e(jt)
!!$
!!$       n_ord=e%nsommets
!!$
!!$       ALLOCATE(base(n_ord))
!!$       ALLOCATE(iss(n_ord))
!!$
!!$       iss(1:n_ord)=Mesh%e(jt)%nu(1:n_ord)
!!$
!!$       ! Loop through the quadrature points on the element e
!!$       DO iq=1, e%nquad
!!$
!!$          ! get the shape function
!!$          DO l=1,n_ord
!!$             base(l) = e%base(l,e%quad(:,iq))
!!$          ENDDO ! l
!!$
!!$          xx=SUM(e%coor(1,1:3)*e%quad(:,iq)); yy=SUM(e%coor(2,1:3)*e%quad(:,iq))
!!$
!!$          DO l=1, SIZE(sol_appr%u)
!!$             sol_appr%u(l) = SUM(Var%ua(0,e%nu(:))%u(l)*base)
!!$          ENDDO
!!$  !        sol_ex   = solution(xx,yy, DATA%temps)
!!$
!!$          error_L1%U   = error_L1%U + ( ABS(sol_appr%U-sol_ex%U)    )*e%weight(iq)*e%volume
!!$          error_L2%U   = error_L2%U + ( ABS(sol_appr%U-sol_ex%U)**2 )*e%weight(iq)*e%volume
!!$          error_Linf%u = MAX(error_linf%u,ABS(sol_appr%U-sol_ex%U))
!!$
!!$       END DO ! iq
!!$
!!$       DEALLOCATE(base)
!!$       DEALLOCATE(iss)
!!$
!!$
!!$    END DO ! jt
!!$
!!$    error_L2%U = SQRT(error_L2%U)
!!$
!!$    OPEN( UNIT = 10, FILE = TRIM(ADJUSTL(DATA%maillage))//".err", IOSTAT = ios )
!!$    h= MAXVAL(Mesh%e(:)%h)
!!$
!!$    WRITE (10, *) error_L1%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    WRITE (10, *) error_L2%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    WRITE (10, *) error_Linf%U, Mesh%ns, h!SQRT(MAXVAL(Mesh%e(:)%volume))
!!$    CLOSE(10)
!!$
!!$    PRINT*, 'error_L1 = ', error_L1%U
!!$    PRINT*, 'error_L2 = ', error_L2%U
!!$    PRINT*, "error Linf", error_Linf%u
!!$
!!$    CALL coupesol(Mesh,Var)
  END SUBROUTINE errors_old

  SUBROUTINE writesol(DATA,Var,kt)
    TYPE(variables), INTENT(in):: Var
    INTEGER, INTENT(in):: kt
    TYPE(donnees), INTENT(in)::DATA
    OPEN(unit=10, file="output.sol", form="unformatted")
    REWIND(10)
    WRITE(10) SIZE(Var%ua,dim=2)
    WRITE(10) DATA%temps,kt
    WRITE(10) Var%ua(0,:)
    CLOSE(10)
  END SUBROUTINE writesol

  SUBROUTINE readsol(DATA,ndofs,kt,Var)
    TYPE(variables), INTENT(inout):: Var
    TYPE(donnees), INTENT(inout)::DATA
    INTEGER, INTENT(out):: kt
    INTEGER, INTENT(in):: ndofs
    INTEGER:: n, j
    PRINT*, "hello"
    OPEN(unit=10, file="output.sol", form="unformatted")
    READ(10) n
    IF (n.NE.ndofs) THEN
       WRITE(*,*) "The restart files and the mesh are not compatible"
       WRITE(*,*) "The file contains ", n, "dofs"
       WRITE(*,*) "The mesh has ", ndofs, "dofs"
       STOP
    ENDIF
    READ(10) DATA%temps,kt
    READ(10) Var%ua(0,:)
    CLOSE(10)
  END SUBROUTINE readsol

  SUBROUTINE coupesol(Mesh,Var)
    TYPE(variables), INTENT(in):: Var
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(element):: e
    INTEGER:: is, jt, l
    TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: u
    REAL(DP):: r

    DO l=1, n_vars
       REWIND(l)
    ENDDO

    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u(e%nsommets))
       u=Control_to_Cons(Var%ua(0,e%nu(:)),e)
       DO is=1, e%nsommets
          DO l=1, Var%ua(0,1)%nvars
             r=SQRT(SUM(e%coor(:,is)**2))
             WRITE(l,*) r,u(is)%u(l)
          ENDDO
       ENDDO
       DEALLOCATE(u)
    ENDDO

    DO l=1, n_vars
       CLOSE(l)
    ENDDO

  END SUBROUTINE coupesol

END MODULE postprocessing
