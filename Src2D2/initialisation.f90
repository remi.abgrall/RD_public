!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE initialisation
  USE param2d
  USE utils
  USE Model
  USE PRECISION
  IMPLICIT NONE

CONTAINS

  SUBROUTINE don(DATA)
    !
    IMPLICIT NONE
    TYPE(donnees),INTENT(inout):: DATA
    INTEGER:: l
    INTEGER:: model
    CHARACTER(len=80):: maillage

    OPEN(1,file=TRIM(ADJUSTL(DATA%rootname)))
    READ(1,*)DATA%nunit
    !    READ(1,*) data%maillage
    READ(1,*)DATA%itype
    READ(1,*)DATA%itype_b
    READ(1,*)DATA%defcorr
    READ(1,*)DATA%ordre_temps
    READ(1,*)DATA%model
    !    READ(1,*)Data%model_interpol
    READ(1,*)DATA%ncont
    !   READ(1,*)Data%iloc
    READ(1,*)DATA%imeth
    READ(1,*)DATA%cfl
    READ(1,*)DATA%tmax
    READ(1,*)DATA%ktmax
    READ(1,*)DATA%ifre
    READ(1,*)DATA%ifre1
    READ(1,*) DATA%period
    READ(1,*) DATA%explicite
    READ(1,*) DATA%djump
    READ(1,*) DATA%Jan
    READ(1,*) DATA%global_mat
    READ(1,*) DATA%global_scheme
    READ(1,*) DATA%test



    !


    CLOSE(1)

  END SUBROUTINE don

  SUBROUTINE init(DATA,Mesh, Var)
    ! initialisation of the problem. Here for scalars
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variable), INTENT(out):: Var
    INTEGER:: ns, nsommets, l
    INTEGER, DIMENSION(:),POINTER:: nu
    REAL(DP),DIMENSION(:,:), POINTER:: coor
    INTEGER:: jt, k
    ns=Mesh%ns
    Var%Ncells=Mesh%ns
    k=mesh%e(1)%nsommets

    ALLOCATE(Var%ua(ns),var%un(ns),Var%ub(ns), var%res(k, mesh%nt))
    ALLOCATE(var%umin(ns), var%umax(ns))

    CALL Model_Init(DATA, Mesh, Var)

  END SUBROUTINE init


  SUBROUTINE mem_cleanup(DATA,Mesh, Var)
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variable), INTENT(out):: Var

    DEALLOCATE(Var%ua,var%un,Var%ub,var%res)
    DEALLOCATE(var%umin, var%umax)
  END SUBROUTINE mem_cleanup

END MODULE initialisation
