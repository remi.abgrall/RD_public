MODULE neighbor_class
  IMPLICIT NONE
   TYPE, PUBLIC:: neighbor
     INTEGER, DIMENSION(:), ALLOCATABLE:: vois ! list of neigboring elements
     INTEGER, DIMENSION(:), ALLOCATABLE:: loc  ! position if is in vois(k)
  END TYPE neighbor

END MODULE neighbor_class
