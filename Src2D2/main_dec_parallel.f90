!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
PROGRAM main
  USE param2d
  USE scheme
  USE overloading
  USE Model
  USE geom
  USE GeomGraph
  USE postprocessing
  USE time_stepping
  USE update
  USE PRECISION
  use parallelFunctions

  IMPLICIT NONE
  CHARACTER(LEN = *), PARAMETER :: mode_name = "main"

  INTEGER, DIMENSION(5)::loc_ndofs=(/2,3,3,4,4/)

  REAL(DP),DIMENSION(3,2:4):: alpha
  REAL(DP),DIMENSION(3,3,3:4):: beta, gamma

  INTEGER, DIMENSION(4):: n_theta
  REAL(DP),DIMENSION(0:3,3,2:4):: theta
  REAL(DP), PARAMETER:: s=SQRT(3.)
  TYPE(maillage):: mesh
  TYPE(variables):: var,debug
  TYPE(donnees):: DATA
  TYPE(element):: e, e1, e2
  TYPE(arete):: ed
  TYPE(frontiere):: efr
  TYPE(PVar):: error_L1, error_L2, error_Linf

  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
  TYPE(Pvar),DIMENSION(2):: f1,f2,f3, f4,f5
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: res,residu, uu, differenc
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
  TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: flux, flux_c
  REAL(DP), DIMENSION(3):: x=0.
  INTEGER:: nt,itype, jt, i, kt, is, l, k_inter, k, iseg, jt1, jt2, ll, kt0
  REAL(DP):: dx, dt0, dt, temps
  CHARACTER(LEN = 1024) :: maille
  INTEGER:: nb_args, Impre, n, lp
  TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: u_b, u_c

  ! Parallell
  Type(mainNodeParameters) :: mainParameters
  real(dp), dimension(:), allocatable :: dtVector
  TYPE(maillage):: globalMesh
  TYPE(variables):: globalVar
  character(len=50)   ::  fileNameMesh
  integer :: maxTri, ierror, j, tempUnit
  type(MeshCom)   :: Com
  integer, dimension(mpi_status_size) :: status
  character(10) :: time
  character(30) :: statFile
  logical :: exist
 
!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------
  !--------------------------- READ INITIAL DATA --------------------------------------------------

  nb_args = iargc()

  IF (nb_args < 1) THEN
     PRINT *, " ERREUR : donner le nom du cas en argument (Data%RootName), argc==", nb_args
     STOP
  END IF
  CALL getarg(1,maille)


  OPEN(1, file="Data/don")
  READ(1,*) DATA%itype
  DATA%itype_b=DATA%itype
  READ(1,*)DATA%iordret, DATA%iter
  IF (DATA%iter.LT.DATA%iordret) THEN
     PRINT*, "wrong Data%iter,Data%iordret"
     PRINT*,"Data%iter should be >= than Data%iordret"
     PRINT*, "Data%iordret", DATA%iordret
     PRINT*, "Data%iter", DATA%iter
  ENDIF
  READ(1,*) DATA%ischema
  READ(1,*) DATA%theta_jump
  READ(1,*) DATA%theta_jump2
  READ(1,*) DATA%cfl
  READ(1,*) DATA%ktmax
  READ(1,*) DATA%tmax
  READ(1,*) DATA%ifre
  DATA%maillage=maille
  READ(1,*) DATA%loc_dec ! useless
  READ(1,*) DATA%restart
  READ(1,*) DATA%limit
  READ(1,*) DATA%Icas

  CALL IniCom(Com)
  allocate(dtVector(Com%NTasks))
  WRITE(*,*) 'ME/1 = ', Com%Me,'nTasks',Com%NTasks
  Data%Maillage = trim(Data%Maillage)//'_'//leftString(com%nTasks)

  call initializeData(Com, mainParameters, globalMesh, data)
  write(*,*)'me:',com%me,'initialized Data'
  IF (Com%Me == 0) THEN
      call createNuMap(com, globalMesh, mainParameters)
      globalVar%Ncells = globalMesh%ns
      fileNameMesh = 'globalmesh'
      maxTri = maxval(mainParameters%maxBoundaryTrianglesVector)
  END IF

  CALL theta_alpha_beta()

  call readMeshParallel(Data, Mesh, com)
  print*, com%me,"finished ReadMesh parallel"

  call mpi_barrier(com%comWorld, ierror)
  call mpi_bcast(maxTri,1,mpi_int,com%main,com%comWorld,ierror)
  call mpi_barrier(com%comWorld, ierror)
  call updateSubMeshsNdofs(com,mesh)
  print*, com%me,"updated SubMeshsNodfs"
  
  if (com%me == com%main) then
      allocate(mainParameters%unMatrix(com%NTasks,4,maxval(mesh%submeshsNdofs)))
  endif
  
  Impre=6
  jt=Mesh%nt

  PRINT*, com%me,"exit ReadMesh"

  ! geometrie
  CALL GeomSegments2d(Mesh,Impre, globalMesh)
  PRINT*, com%me,"exit Geomsegments"

  CALL calculateMassMatrLumped(DATA, Mesh)
  PRINT*, com%me,"Exit Mass matrix"

  !initialisation

  Var%Ncells=Mesh%ns

  kt0=0
  DATA%temps=0.0_dp
   ALLOCATE(Var%ua(0:DATA%iordret-1,Mesh%ndofs),&
        & Var%up(0:DATA%iordret-1,Mesh%ndofs)&
        & , var%un(Mesh%ndofs) ,debug%ua(0:DATA%iordret-1,Mesh%ndofs))
   print*, com%me, "End Allocate"

  call createNuFile(Com,Mesh)
  write(*,*)com%me, 'created Nu File'

  call mpi_barrier(com%comWorld,l)

  if (com%me == 0) then
    allocate(globalVar%ua(0:DATA%iordret-1,globalMesh%ndofs))
    call createDependencyMap(mesh,com,mainParameters)
    write(*,*)com%me, 'created Dependency Map'
  endif

  call createTransferPlan(mesh,com)
  write(*,*)com%me, 'created Transfer plan'

  call updateNeighboursNu(mesh,com, globalMesh)
  write(*,*)com%me, 'updated Neighbours-Nu'

  call updateMassMatrix(com, mesh, mainParameters)
  write(*,*)com%me, 'updated Mass-Matrix'

  write(*,*)com%me, 'End preparing parallel computing'

  call mpi_barrier(com%comWorld, ierror)

  !-------------------------------------------------------------------------------------------------------------------------------
  ! ----------Aquire old data if test has been interrupted and does not need to restart from scratch --------
  IF (.NOT.DATA%restart) THEN ! we start with a new solution
     kt=0
     DO jt=1, Mesh%nt
        e=Mesh%e(jt)
        DO l=1, e%nsommets
           Var%ua(0,e%nu(l))= IC(jt,e%coor(:,l),DATA%icas)
        ENDDO

        SELECT CASE(e%itype)
        CASE(1,3,4) !Lagrange-> nothing to do
           Var%un(:)=Var%ua(0,:)
        CASE(2,5) ! Bezier -> modify coeff

           DO k=1, n_vars

              DO l=1, e%nsommets!SIZE(u)
                 Var%un(e%nu(l))%u(k)=SUM(e%inv_base_at_dofs(:,l)*Var%ua(0,e%nu(:))%u(k))
              ENDDO

           ENDDO
           !           DEALLOCATE(u_b,u_c)

        CASE default
           PRINT*, "correct initialistaion not yet implemented for these elements,e%itype=",e%itype
           STOP
        END SELECT

     ENDDO
     DO l=1, n_vars
        PRINT*, "min variable ",l, MINVAL(Var%ua(0,:)%u(l))
        PRINT*, "max variable ",l, MAXVAL(Var%ua(0,:)%u(l))
        PRINT*
     ENDDO
     Var%ua(0,:)=Var%un(:) ! ?
  ELSE
     CALL readsol(DATA,Mesh%ndofs,kt0,Var)
  ENDIF
  kt=kt0

  !-------------------------------------------------------------------------------------------------------------------------------
  call updateMainUa(com, var, globalVar, mainParameters, data, mesh)

  if (com%me == com%main) then
     call visu(data, kt, globalMesh, globalVar)
  endif

  call mpi_barrier(com%comWorld, ierror)
  !-------------------------------------------------------------------------------------------------------------------------------
  ! -------------------- Time- stepping loop -------------------------

  ! Tme logging only for test
  tempUnit = 99
  if (com%me == 0) then
      write(statFile,'(A, I2.2)') 'times_',com%ntasks
      statFile = trim(statFile)
      open(tempUnit, file=statFile)
      write(tempUnit,*)com%nTasks
  endif

  DO kt=kt0+1, DATA%ktmax  ! loop over time steps
    !Logging
    if (com%me == 0) then
        call date_and_time(TIME = time)
        write(*,*)kt,'time ',time
        write(tempUnit,*)time
    endif

    call mpi_barrier(com%comWorld, ierror)
    DO is=1, Mesh%ndofs
        Var%ua(1:,is) = Var%ua(0,is)
        Var%up(:,is)  = Var%ua(0,is)
    ENDDO

     dt0= Pas_de_temps(Mesh,Var, DATA)
     dt=MIN(dt0,DATA%tmax-DATA%temps)
     
      if (com%me == com%main) then
          dtVector(1) = dt
          do j=1,com%Ntasks-1
              call MPI_Recv(dtVector(j+1),1,mpi_real8,j,1,com%comWorld,status,ierror)
          enddo
      else
          call mpi_send(dt,1,MPI_REAL8,com%main,1,com%comWorld,ierror)
      endif

      call mpi_barrier(com%comWorld, ierror)
      dt = MINVAL(dtVector)
      call mpi_bcast(dt,1,mpi_real8,com%main,com%comWorld,ierror)

      call mpi_barrier(com%comWorld, ierror)

     DATA%temps= DATA%temps+dt

     if (checkExitCondition(Com, dt)) exit
     call decParallel(Var, debug, Data, Mesh, dt, n_theta, alpha, beta, gamma, theta, com, mainParameters)

     ! swap solutions
     Var%ua(0,:)=Var%ua(DATA%iordret-1,:)

     IF (MOD(kt,DATA%ifre)==0 .and. .false. ) THEN
        call updateMainUa(com, var, globalVar, mainParameters, data, mesh)
        if (com%me == com%main) then
            call visu(data, kt, globalMesh, globalVar)
            CALL errors(DATA,globalMesh,globalVar,error_L1,error_L2, error_Linf)
            CALL writesol(DATA,globalvar,kt)
        endif
        CALL errors(DATA,Mesh,Var,error_L1,error_L2, error_Linf)
        CALL writesol(DATA,var,kt)
        call mpi_barrier(com%comWorld, ierror)
     ENDIF

  ENDDO ! end  time stepping
  PRINT*, "final time=", DATA%temps
  !post processing

  call mpi_barrier(com%comWorld, ierror)
  call updateMainUa(com, var, globalVar, mainParameters, data, mesh)

  if (com%me == com%main) then
     call visu(data, kt, globalMesh, globalVar)
      CALL errors(DATA,globalMesh,globalVar,error_L1,error_L2, error_Linf)
      CALL writesol(DATA,globalvar,kt)
  endif
  call endcom(Com)

  !-------------------------------------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------------------------------------

CONTAINS
  REAL(DP) FUNCTION pas_de_temps(Mesh,Var,DATA)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Pas_de_Temps"
    TYPE(variables), INTENT(in):: Var
    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    INTEGER:: jt, l, k
    TYPE(element):: e
    TYPE(Pvar), ALLOCATABLE, DIMENSION(:):: u
    REAL(DP):: dt, dx, umax
    REAL(DP), DIMENSION(2):: x=0., n
    dt=HUGE(1.0)

    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u(e%nsommets))
       u(:)=Var%ua(0,e%nu)

       DO l=1, e%nvertex
          dx=SQRT(SUM(e%n(:,l))**2)
          n=e%n(:,l)!/dx
          x=e%coor(:,l)
          ! Remi note: we make a O(h^2) error, not important
          DO k=1, u(1)%nvars
             u(l)%u(k) = e%eval_func(Var%ua(0,e%nu)%u(k),e%coorL(:,l))
          ENDDO


          umax=u(l)%spectral_radius(x,e%n(:,l))
          dt=MIN(dt,e%volume/( ABS(umax)+1.e-6_dp))

       ENDDO
       DEALLOCATE(u)
    ENDDO
    pas_de_temps=dt*DATA%cfl

  END FUNCTION pas_de_temps

  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------



  SUBROUTINE theta_alpha_beta()
    ! loading the weights
    ! quadratic interpolation over [0,1]
    ! alpha: this defines the fractions of Delta t for the Euler method (operator L1)
    ! alpha( subtimestep, order in time)
    ! theta: \int_0^alpha(L,k) f = \sum_{p=0}^{order in time) theta(p,L, order in time)* f(u[p])
    ! beta contains the (time) weight for the Euler step of Version 1
    !
    alpha=0.
    theta=0.
    beta=0.
    gamma=0.
    n_theta=0





    ! order 2:
    n_theta(2)=2
    alpha(1,2)=1._dp
    theta(:,:,2)=0._dp
    theta(0:1,1,2)=0.5_dp

    !
    ! alpha, beta gamma coeffcients for order 3
    !
    alpha(1,3)=0.5_dp
    alpha(2,3)=1.0_dp
    beta(1,2,3)=alpha(1,3)
    beta(2,2,3)=alpha(2,3)-alpha(1,3)
    GAMMA(2,2,3)=beta(2,2,3)
    n_theta(3)=3
    !
    ! integration coefficients for order 3
    theta(0:2,1:2,3)=RESHAPE( (/5._dp/24._dp,1._dp/3._dp,-1._dp/24._dp,1._dp/6._dp,2._dp/3._dp,1._dp/6._dp/),(/3,2/))
!!!!!!!!!!!!!!!!!
    ! cubic interpolation
#if (-2==-2)
    ! Here I choose the points 0.5*(1+cos(k*pi/N)), N=3, k=3,2,1,0
!!!!!!!!!!!!!!
    ! these are

    alpha(1,4)=1._dp/4._dp
    alpha(2,4)=3._dp/4._dp
    alpha(3,4)=1._dp
    n_theta(4)=4

    theta(0:3,1:3,4)=RESHAPE( (/59._dp/576._dp, & !(0,1)
         & 47._dp/288._dp,  &                      !(1,1)
         & -7._dp/288._dp, &                      !(2,1)
         & 5._dp/576._dp,  &                      !(3,1)
         & 3._dp/64._dp,   &                      !(0,2)
         & 15._dp/32._dp,  &                      !(1,2)
         & 9._dp/32._dp,   &                      !(2,2)
         & -3._dp/64._dp,  &                      !(3,2)
         &1._dp/18._dp,    &                      !(0,3)
         &4._dp/9._dp,     &                      !(1,3)
         &4._dp/9._dp,     &                      !(2,3)
         &1._dp/18._dp/)   &                      !(3,3)
         &,(/4,3/))
#endif
#if (-2==0) 
    ! Here I choose the points k/3, k=3,2,1,0
!!!!!!!!!!!!!!
    ! these are

    alpha(1,4)=1_dp./3._dp
    alpha(2,4)=2._dp/3._dp
    alpha(3,4)=1._dp
    n_theta(4)=4

    theta(0:3,1:3,4)=RESHAPE( (/1._dp/8._dp, & !(0,1)
         & 19._dp/72._dp,  &                      !(1,1)
         & -5._dp/72._dp, &                      !(2,1)
         & 1._dp/72._dp,  &                      !(3,1)
         & 1._dp/9._dp,   &                      !(0,2)
         & 4._dp/9._dp,  &                      !(1,2)
         & 1._dp/9._dp,   &                      !(2,2)
         & 0._dp,      &                      !(3,2)
         &1._dp/8._dp,    &                      !(0,3)
         &3._dp/8._dp,     &                      !(1,3)
         &3._dp/8._dp,     &                      !(2,3)
         &1._dp/8._dp/)   &                      !(3,3)
         &,(/4,3/))
#endif
    !! The following lines are good only for the version 1
    beta(1,3,4)=alpha(1,4) ! this gives the fraction of timestep for the Euler method
    beta(2,3,4)=alpha(2,4)-alpha(1,4)
    beta(3,3,4)=alpha(3,4)-alpha(2,4)
    GAMMA(2:3,3,4)=beta(2:3,3,4)
  END SUBROUTINE theta_alpha_beta

END PROGRAM main
