MODULE boundary
  USE param2d
  USE overloading
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  PUBLIC:: BC,boundary_log

CONTAINS
  SUBROUTINE boundary_log(log_type, bc_tag, Nphys, PhysName)
    ! maps the log_fr to the right type of boundary condiction
    CHARACTER(Len=80), PARAMETER:: mod_name="Wave/Boundary_log"
    CHARACTER(Len=6),DIMENSION(Nphys), INTENT(in):: PhysName
    INTEGER, INTENT(in)::log_type, Nphys
    INTEGER, INTENT(out)::bc_tag

    SELECT CASE(TRIM(PhysName(log_type)))
    CASE("Steger","steger")
       bc_tag=2
    CASE("wall","Wall")
       bc_tag=1
    CASE default 
       WRITE(*,*) mod_name, "boundary condition not defined"
       WRITE(*,*) "currently Steger, wall"
       WRITE(*,*) "value=", TRIM(PhysName(log_type)), log_type
       WRITE(*,*) (PhysName)
       STOP
    END SELECT

  END SUBROUTINE boundary_log

  SUBROUTINE BC(Mesh,dt,t,ua,un,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="Wave/Bc"
    REAL(DP), INTENT(in):: t, dt
    TYPE(Pvar), INTENT(in), DIMENSION(:):: ua
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(Pvar), INTENT(inout), DIMENSION(:,:):: un
    TYPE(maillage):: Mesh
    TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: res, u
    TYPE(frontiere):: efr

    INTEGER:: jt, is

    DO jt=1, Mesh%Nsegfr
       efr=mesh%fr(jt)
       ALLOCATE (Res(efr%nsommets))
       ALLOCATE(u(efr%nsommets))
       u(:)=ua(efr%nu(:))


       SELECT CASE(efr%log)!bc_tag)


       CASE(2) ! inflow/outflow
          CALL galerkin_bc2(efr,t,u,res,DATA)
       CASE(1) ! wall
          CALL wall(efr,u,res,DATA)
       CASE default
          PRINT*, mod_name, "wrong BC log fr", efr%log
          STOP
       END SELECT
       DO is=1,efr%nsommets
          un(is,jt)=un(is, jt)+res(is)*dt 
       ENDDO
       DEALLOCATE(Res,u)
    ENDDO
  END SUBROUTINE BC

  FUNCTION BCs(efr,uloc,x,y,temps,Icas) RESULT(ubord)
    CHARACTER(Len=80):: mod_name="Wave/BCs" 
    REAL(DP),            PARAMETER:: pi=ACOS(-1.) ! pi
    REAL(dp), PARAMETER:: a = 1.0
    REAL(dp), PARAMETER:: aa = 100., bb = 100., beta=5._dp/(2._dp*pi)


    TYPE(frontiere), INTENT(in) :: efr
    TYPE(PVar),      INTENT(in) :: uloc
    REAL(DP),            INTENT(in) :: x,y
    REAL(DP),            INTENT(in) :: temps
    INTEGER, INTENT(in)              :: Icas
    TYPE(Pvar)                  :: ubord
    REAL(DP)                        :: mn, r2, dp
    SELECT CASE(Icas)

    CASE(0)
       r2=x*x+y*y
       dp=EXP(-bb*r2)
       ubord%u(1)=u0-y* dp!beta *exp((1._dp-r2)/2._dp) 
       ubord%u(2)=v0+x* dp!beta *exp((1._dp-r2)/2._dp)
       ubord%u(3)=dp+p0

    CASE default
       PRINT*, mod_name,'Wrong BC type in BC_Data, efr%bc_tag = ', efr%bc_tag
       STOP
    END SELECT


  END FUNCTION BCs

  SUBROUTINE  galerkin_bc2(e,t,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="Galerkin_bc2"
    ! Stegger warming modifie
    ! boundary conditions
    !***********assumes inward normals********
    TYPE(frontiere),                   INTENT(in) :: e
    REAL(DP),                              INTENT(in) :: t
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(donnees),   INTENT(in)   :: DATA
    REAL(DP), DIMENSION(2,e%nsommets)                 :: grad
    REAL(DP), DIMENSION(2)                            :: gr, x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base
    TYPE(PVar)                                    :: uloc, ubord ,umoy, comp!(n_dim)
    REAL(DP), DIMENSION(1,1,2)                        :: vit
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim)          :: Jac
    INTEGER                                       :: i, iq, l, k
    REAL(DP)                                          :: alpha
    REAL(DP), PARAMETER                               :: sigma = 1.0 ! SBP constant
    REAL(DP), PARAMETER                               :: eps=0._dp
    TYPE(Pvar), DIMENSION(n_dim)                      :: flux
    res=0._dp

    DO k=1, e%nsommets
       DO i=1, e%nquad
          y=e%quad(:,i)
          x(1)=SUM(y*e%coor(1,1:2))
          x(2)=SUM(y*e%coor(2,1:2))
          DO l=1, e%nsommets
             base(l)=e%base(l,y)
          ENDDO
          DO l=1, n_vars
             umoy%u(l)=SUM(u(:)%u(l))/REAL(e%nsommets,dp)
             uloc%u(l)  = SUM(base*u(:)%u(l))
          ENDDO
          ubord = BCs(e,uloc,x(1),x(2),t, DATA%icas)
          !Jac=uloc%Jacobian(x)
          !          DO l=1,4! n_vars
          !             flux=ubord%flux(x)-uloc%flux(x)
          !             comp%u(l)= SUM( flux(:)%u(l)*e%n(:) )
          !          ENDDO
          Res(k)%u(:)=Res(k)%u(:)&
               &+MATMUL( ubord%min_mat(x,eps,-e%n),(ubord%u(:)-uloc%u(:)) )*e%weight(i)*base(k)


       ENDDO ! i
    ENDDO ! k
  END SUBROUTINE galerkin_bc2


  SUBROUTINE wall (e,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(PVar)                                    :: uloc,ubar
    REAL(dp):: un, h, p, ro, eps, q2,hbar,pbar,epsbar,unn,fluxE,correction,uu,vv,vbar,c,ut,unnn
    REAL(dp),DIMENSION(2):: n,nn
    INTEGER:: i,k,l
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base

    res=0._dp

!!$    DO i=1, e%nsommets
!!$
!!$       DO k=1, e%nquad
!!$          y=e%quad(:,i)
!!$          x(1)=SUM(y*e%coor(1,1:2))
!!$          x(2)=SUM(y*e%coor(2,1:2))
!!$          DO l=1, e%nsommets
!!$             base(l)=e%base(l,y)
!!$          ENDDO
!!$
!!$
!!$          DO l=1, n_vars
!!$             uloc%u(l)  = SUM(base*u(:)%u(l))
!!$          ENDDO
!!$
!!$          ro=uloc%u(1)
!!$          q2=SUM(uloc%u(2:3)**2)/(ro*ro)
!!$          eps=(uloc%u(4)-0.5_dp*ro*q2)/ro
!!$          p=pEOS(ro,eps)
!!$          h=(ro*eps+p)
!!$          n=-e%n
!!$
!!$
!!$          un=SUM(uloc%u(2:3)*n)/ro
!!$          nn=n/SQRT(n(1)**2+n(2)**2)
!!$          unnn=SUM(uloc%u(2:3)*nn(1:2) )/ro
!!$          ut=(-uloc%u(2)*nn(2)+uloc%u(3)*nn(1))/ro
!!$
!!$          unn=SQRT(SUM(uloc%u(2:3)**2)/ro)
!!$
!!$
!!$
!!$
!!$          res(i)%u(1)=res(i)%u(1)+2._dp*un*uloc%u(1)*e%weight(k)*base(i)
!!$          res(i)%u(2)=res(i)%u(2)+(uloc%u(2)-ro*unnn*nn(1)-ro*ut*nn(2))*un*e%weight(k)*base(i)
!!$          res(i)%u(3)=res(i)%u(3)+(uloc%u(3)-ro*unnn*nn(2)+ro*ut*nn(1))*un*e%weight(k)*base(i)
!!$          res(i)%u(4)=res(i)%u(4)
!!$
!!$
!!$       ENDDO ! k

!!$
!!$ ENDDO
  END SUBROUTINE wall






END MODULE boundary
