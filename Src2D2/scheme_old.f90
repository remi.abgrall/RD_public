!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE scheme !2D
  ! In this module all scheme  for the flux are collected.
  ! Structure of this module:
  ! - schema -> allows to choose between different scheme for the flux approximation,
  !                 which are coded in this same module and are the following
  !                 a) galerkin
  !                 b) galerkin + psi limiting
  !                 c) local Lax-Friedrichs (=Rusanov)
  ! - stabilization: Burman's jump
  ! - limiting: generic function to activate the limiting,
  !              which can be done either via conservative variables, or characteristic ones
  ! The boundary conditions
  !  inflow/outflow: log=1
  !  wall:           log=2
  !
  USE param2d
  USE overloading
  USE init_bc
  USE Model
  USE PRECISION
  USE element_class
  IMPLICIT NONE
  PRIVATE
  INTERFACE jump
     MODULE PROCEDURE jump_burman 
  END INTERFACE jump
  INTERFACE stream
     MODULE PROCEDURE stream_new
  END INTERFACE stream

  PUBLIC:: schema, jump


CONTAINS
  FUNCTION schema(limit, ischema, e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "scheme"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! residual (supg or psi)
    !
    INTEGER                          , INTENT(in):: limit
    INTEGER,                           INTENT(in):: ischema
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:),          INTENT(in):: u
    TYPE(PVar), DIMENSION(:),          INTENT(in):: du,dJ
    TYPE(PVar), DIMENSION(:,:),        INTENT(in):: flux,fluxg
    REAL(DP)   ,                       INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res

    SELECT CASE (ischema)
    CASE(3)
       res= galerkin(e, u, du, flux, dt)
    CASE(4)
       res= galerkin_new(e, u, du,dJ, flux,fluxg, dt)
    CASE(5) ! with Burman filtering
       res= psi_jump(limit, e, u, du, flux, dt)
    CASE(6) ! with stream filtering
       res= psi_new_stream(limit, e, u, du, flux, dt)
    CASE(7) ! with stream filtering
       res= supg_new(limit, e, u, du,dJ, flux,fluxg, dt)
    CASE(-1)
       res=lxf_new(e, u, du,dJ, flux,fluxg, dt)
    CASE default
       PRINT*, "scheme not defined"
       STOP
    END SELECT
  END FUNCTION schema


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ SCHEME FOR FLUX ---------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------

  FUNCTION galerkin(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "galerkin"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                           INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)    :: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim):: vit, n
    !    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, flux_loc(n_dim), u_loc

    INTEGER:: i, iq, l, k, ll
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii

    DO l=1, e%nsommets
       res(l)%u=0._dp
    ENDDO
    DO i=1, e%nsommets


       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO


          u_loc=SUM(base*u)
          du_loc=SUM(base*du)
#if (1==1)
          flux_loc=u_loc%flux()!xx)
#else
          flux_loc(1)=SUM(base*flux(1,:))
          flux_loc(2)=SUM(base*flux(2,:))
#endif
          ! 

          res(i) = res(i) +(base(i)* du_loc-&
               &dt*(Grad(1,i)*flux_loc(1)+Grad(2,i)*flux_loc(2)) )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i

    ! integral on the boundary
    DO k=1,e%nvertex ! here a triangle

       n=-e%n(:,k)

       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO

          DO ll=1, n_vars
             u_loc%u(ll)=SUM(base*u%u(ll))
          ENDDO

          flux_loc=u_loc%flux()
          !          flux_loc(1)=SUM(base*flux(1,:))
          !          flux_loc(2)=SUM(base*flux(2,:))

          DO l=1,e%nsommets
             res(l)=res(l)+dt* e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*base(l)
          ENDDO
       ENDDO !iq
    ENDDO !edge

  END FUNCTION galerkin

  FUNCTION phij(e, u, du,dJ, flux,fluxg, dt) RESULT(resj)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "phij"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                           INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: dJ,du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)    :: fluxg,flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar) , DIMENSION(1)       :: resj

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim):: vit, n
    !    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, fluxg_loc(n_dim), u_loc

    INTEGER:: i, iq, l, k, ll
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii

    
    resj(1)%u=0._dp
    
    


    DO iq=1, e%nquad

       DO l=1, e%nsommets
          base(l  )=e%base    (l, e%quad(:,iq))
          grad(:,l)=e%gradient(l, e%quad(:,iq))
       ENDDO


       u_loc=SUM(base*u)
       du_loc=SUM(base*dJ)
!#if (1==1)
!       flux_loc=u_loc%flux()!xx)
!#else
       fluxg_loc(1)=SUM(base*fluxg(1,:))
       fluxg_loc(2)=SUM(base*fluxg(2,:))
!#endif
          ! 

       resj = resj +( du_loc)*e%weight(iq)

    ENDDO ! iq

    resj= resj* e%volume
    

    ! integral on the boundary
    DO k=1,e%nvertex ! here a triangle

       n=-e%n(:,k)

       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO

          DO ll=1, n_vars
             u_loc%u(ll)=SUM(base*u%u(ll))
          ENDDO

          !flux_loc=u_loc%flux()
          fluxg_loc(1)=SUM(base*fluxg(1,:))
          fluxg_loc(2)=SUM(base*fluxg(2,:))

          
          resj=resj+dt* e%weight_edge(k,iq)*SUM(fluxg_loc(:)*n(:))
          
       ENDDO !iq
    ENDDO !edge

  END FUNCTION phij

  FUNCTION galerkin_new(e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Galerkin_new"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du,dJ
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux,fluxg
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res
    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim):: vit, n
    !    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, flux_loc(n_dim), u_loc

    INTEGER:: i, iq, l, k, ll
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii


    DO l=1, n_vars
       res(:)%u(l)=MATMUL(e%masse(:,:),du%u(l)) &
            & +dt* ( MATMUL(e%coeff(:,:,1),flux(1,:)%u(l)) &
            &      +  MATMUL(e%coeff(:,:,2),flux(2,:)%u(l)) )
    ENDDO
    ! integral on the boundary
    DO k=1,e%nvertex ! here a triangle

       n=-e%n(:,k)

       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO

          DO ll=1, n_vars
             u_loc%u(ll)=SUM(base*u%u(ll))
          ENDDO

          !flux_loc=u_loc%flux()
          flux_loc(1)=SUM(base*flux(1,:))
          flux_loc(2)=SUM(base*flux(2,:))

          DO l=1,e%nsommets
             res(l)=res(l)+dt* e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*base(l)
          ENDDO
       ENDDO !iq
    ENDDO !edge

  END FUNCTION galerkin_new

  FUNCTION psi_jump(limit, e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Psi_jump"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du,dJ
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux,fluxg
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)             :: res, v,u_cons
    TYPE(Pvar), DIMENSION(e%nsommets)             :: phi,  phi_LF
    LOGICAL:: out
    phi_LF=LxF_new(e, u, du,dJ, flux,fluxg, dt)
    !        phi_LF=LxF(e, u, du, flux, dt)

    SELECT CASE(limit)
    CASE(3) ! hybrid char & var (the last applies in case of NAN)
       CALL lim_psi_jump_char(u,phi_LF,res,out)
       ! if NaN detected, then do limitation by variables
       IF (out) CALL lim_psi_jump_var(u,phi_LF,res)
    CASE(2)
       CALL lim_psi_jump_char(u,phi_LF, res,out)
    CASE(1)
       CALL lim_psi_jump_var(u,phi_LF, res)
    CASE default
       PRINT*, "wrong limiter"
    END SELECT


  END FUNCTION psi_jump


  FUNCTION supg_new(limit, e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "supg_new"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du,dJ
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux,fluxg
    TYPE(PVar), DIMENSION(1)                      ::resj
    REAL(DP)   ,                        INTENT(in):: dt
    !REAL(DP) , DIMENSION(1)::ke 
    REAL(DP)                                      :: we,psi,ke
    TYPE(PVar), DIMENSION(e%nsommets)             :: res
    INTEGER                                       :: i
    REAL(DP)                                      :: aire,r
    INTEGER                                       :: next,k
    
    res=galerkin_new(e, u, du,dJ, flux,fluxg, dt)

    res=res+stream(e, u, du, flux, dt)
    
!!$    we=0
!!$    DO i=1, e%nsommets
!!$       we=we+res(i)%u(3)*e%coor(1,i)-res(i)%u(2)*e%coor(2,i)
!!$    END DO
!!$    resj=phij(e, u, du,dJ, flux,fluxg, dt)
!!$    psi=resj(1)%u(1)-we
!!$    !SELECT CASE(e%nvertex)
!!$
!!$       !-----------------------------
!!$       ! for triangles
!!$    !CASE(3)
!!$       !SELECT CASE(e%itype)
!!$       !CASE(1) ! P1
!!$    !a= (e%coor(1,2)-e%coor(1,1))*(e%coor(:,3)-e%coor(:,1))
!!$    r=0._dp!psi/(4._dp*e%volume)
!!$
!!$!    DO k=1,e%nsommets
!!$!       res(k)%u(2)=res(k)%u(2)+r*(e%coor(1,e%next(k))-e%coor(1,e%next(e%next(k))))
!!$!       res(k)%u(3)=res(k)%u(3)+r*(e%coor(2,e%next(k))-e%coor(2,e%next(e%next(k))))
!!$!    END DO
!!$
!!$    !res=res+cor(e, u, du, flux, dt)
  END FUNCTION supg_new



  FUNCTION psi_new_stream(limit, e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Psi_new_stream"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du,dJ
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux,fluxg
    REAL(DP)   ,                        INTENT(in):: dt
    TYPE(donnees)                                 :: DATA

    TYPE(PVar), DIMENSION(e%nsommets)             :: res, v,u_cons
    TYPE(Pvar), DIMENSION(e%nsommets)             :: phi,  phi_LF, diss
    LOGICAL:: out
    phi_LF=LxF_new(e, u, du,dJ, flux,fluxg, dt)

    SELECT CASE(limit)
    CASE(3) ! hybrid char & var (the last applies in case of NAN)
       CALL lim_psi_jump_char(u,phi_LF,res,out)
       ! if NaN detected, then do limitation by variables
       IF (out) CALL lim_psi_jump_var(u,phi_LF,res)
    CASE(2)
       CALL lim_psi_jump_char(u,phi_LF, res,out)
    CASE(1)
       CALL lim_psi_jump_var(u,phi_LF, res)
    CASE default
       PRINT*, "wrong limiter"
    END SELECT

    res=res+stream(e, u, du, flux, dt)


  END FUNCTION psi_new_stream

  FUNCTION lxf( e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF"
    ! this version is conservative by taking properly into account the Gauss formula
    ! what ever the quadrature points chosen
    ! the residuals are defined using the galerkin residuals+LxF dissipation.
    ! The first ordre residual satisfies an energy (entropy) inequality
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res,v

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp, n
    TYPE(Pvar), DIMENSION(e%nsommets):: phi

    TYPE(PVar) :: du_loc, flux_loc(n_dim), u_loc, ubar, vbar
    REAL(DP):: alpha,h
    INTEGER:: i, iq, l, k
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii
    !Complicated stuff to evaluate the dissipation coefficient
    !h=maxval(sqrt(e%n(1,:)**2+e%n(2,:)**2))
    ubar= SUM(u)/REAL(e%nsommets,DP)
    ! it seems to be necessary to go into physical state
    v=Control_to_Cons(u,e)
    vbar=SUM(v)/REAL(e%nsommets,DP)
    !
    alpha=0._dp
    DO l=1, e%nvertex
       alpha=MAX(alpha,vbar%spectral_radius(xx,e%n(:,l)))
    ENDDO
    !alpha=alpha
    !
    res=0._dp
    phi=0._dp

    DO i=1, e%nsommets

       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO


          u_loc=SUM(base*u)
          du_loc=SUM(base*du)
#if (1==0)
          flux_loc=u_loc%flux()
#else
          flux_loc(1)=SUM(base*flux(1,:))
          flux_loc(2)=SUM(base*flux(2,:))
#endif
          phi(i) = phi(i) +(base(i)* du_loc-&
               &dt*(Grad(1,i)*flux_loc(1)+Grad(2,i)*flux_loc(2)) )*e%weight(iq)

       ENDDO ! iq
    ENDDO ! i
    phi=e%volume*phi

    ! integral on the boundary
    DO k=1,e%nvertex

       n=-e%n(:,k)
       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO
          u_loc=SUM(base*u)
          flux_loc=u_loc%flux()
          !          flux_loc(1)=SUM(base*flux(1,:))
          !          flux_loc(2)=SUM(base*flux(2,:))
          DO l=1,e%nsommets
             phi(l)   = phi(l)   + dt * e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*base(l)

          ENDDO
       ENDDO !iq
    ENDDO !edge

    res =   phi+dt*alpha*(u-ubar)

  END FUNCTION lxf

  FUNCTION lxf_new( e, u, du,dJ, flux,fluxg, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF_new"
    ! this version is conservative by taking properly into account the Gauss formula
    ! what ever the quadrature points chosen
    ! the residuals are defined using the galerkin residuals+LxF dissipation.
    ! The first ordre residual satisfies an energy (entropy) inequality
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du,dJ
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux,fluxg
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res,v
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp

    TYPE(Pvar), DIMENSION(e%nsommets):: phi

    TYPE(PVar) ::  ubar, vbar
    REAL(DP):: alpha
    INTEGER:: l

    ubar= SUM(u)/REAL(e%nsommets,DP)
    ! it seems to be necessary to go into physical state
    v=Control_to_Cons(u,e)
    vbar=SUM(v)/REAL(e%nsommets,DP)
    !
    alpha=0._dp
    DO l=1, e%nvertex
       alpha=MAX(alpha,vbar%spectral_radius(xx,e%n(:,l)))
    ENDDO
    !alpha=alpha
    !
    phi=galerkin_new(e, u, du,dJ, flux,fluxg, dt)

    res =   phi+dt*alpha*(u-ubar)

    !    res =   dt*alpha*(u-ubar)

  END FUNCTION lxf_new


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ LIMITING ---------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------

  SUBROUTINE lim_psi_jump_var(u,phi,res) 
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "lim_psi_jump_var"
    ! blendig between limited  and LxF in case of NAN
    ! used for Jump
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(in):: phi
    TYPE(Pvar), DIMENSION(:), INTENT(INout):: res
    REAL(DP)                                :: Phi_tot
    REAL(DP), DIMENSION(SIZE(res)):: x
    REAL(DP):: den, l, truc
    INTEGER :: k,i

    l=100._dp

    DO k=1,n_vars

       phi_tot=SUM(phi(:)%u(k))

       IF (ABS (phi_tot)>tiny(1.0)) THEN
          x=MAX(phi(:)%u(k)/phi_tot,0._dp)
          den=SUM(x)+TINY(1.0_dp)
          truc= SUM(ABS(phi%u(k))) + TINY(1.0)
          !         l=MIN(l,(ABS(phi_tot)/truc))
          l=ABS(phi_tot)/truc
          res(:)%u(k)=(1._dp-l)*phi_tot*x/den+l*phi(:)%u(k)
       ELSE
          res(:)%u(k)=0._dp
       END IF

    END DO

    !    DO i = 1,SIZE(phi)
    !       res(i)%u= (1.0-l)*res(i)%u + l*phi(i)%u
    !    END DO

  END SUBROUTINE lim_psi_jump_var

  SUBROUTINE lim_psi_jump_char(u,phi,res,out)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "lim_psi_jump_char"
    !  blendig between limited  and LxF
    ! used for Jump
    !. R. Abgrall 10/1/16
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(in):: phi
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    LOGICAL, INTENT(out):: out

    REAL(DP)                                :: Phi_tot
    TYPE(Pvar), DIMENSION(SIZE(phi))    :: phi_ch
    REAL(DP), DIMENSION(SIZE(phi))          :: x
    REAL(DP)    :: den, l, truc
    INTEGER :: k, i
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: EigR, EigL
    REAL(DP), DIMENSION(n_dim)         :: v_nn
    TYPE(Pvar) :: ubar

    res = 0.0_dp

    DO k = 1,n_vars
       ubar%u(k)= SUM(u%u(k))/REAL(SIZE(u),DP)
    END DO

    v_nn = ubar%u(2:3)/ubar%u(1)
    IF ( SUM(v_nn**2) < 1.e-16_dp ) v_nn = (/ 0.5_dp, 0.5_dp /)
    EigR = ubar%rvectors(v_nn)
    IF (SUM(ABS(EigR)).NE.SUM(ABS(Eigr)) ) THEN
       out=.TRUE.

       RETURN
    ENDIF
    EigL = ubar%lvectors(v_nn)

    DO i = 1,SIZE(phi)
       phi_ch(i)%u = MATMUL(EigL,phi(i)%u)
    END DO

    l=100.
    DO k=1,n_vars

       phi_tot=SUM(phi_ch(:)%u(k))

       IF (ABS (phi_tot)>tiny(1.0_dp)) THEN
          x=MAX(phi_ch(:)%u(k)/phi_tot,0._dp)
          den=SUM(x)+TINY(1.0_dp)
          truc= SUM(ABS(phi_ch%u(k)))+TINY(1.0)
          !         l=MIN(l,ABS(phi_tot)/truc)
          l=ABS(phi_tot)/truc
          res(:)%u(k)=(1._dp-l)*phi_tot*x/den+l*phi_ch(:)%u(k)
       ELSE
          res(:)%u(k)=0.0_dp
       ENDIF
    END DO  ! k

    DO i = 1,SIZE(phi)
       !       res(i)%u =(1.0-l)* MATMUL(EigR,res(i)%u)+l*phi(i)%u
       res(i)%u =  MATMUL(EigR,res(i)%u)
    END DO

  END SUBROUTINE lim_psi_jump_char


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ STABILIZATION ------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
!!!!!!!!!! Jump

  SUBROUTINE jump_burman(  ed, e1, e2, u1, u2, resJ, theta, theta2)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Jump_burman"
    ! burman operator
    REAL(DP), INTENT(IN):: theta, theta2
    REAL(DP), PARAMETER :: pi=ACOS(-1._dp)
    !
    ! barycentric coordinates of vertices
    !
    REAL(DP), DIMENSION(3,3), PARAMETER:: a3=RESHAPE( (/1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0/), (/3,3/))
    REAL(DP), DIMENSION(4,4), PARAMETER:: a4=RESHAPE( (/1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0/), (/4,4/))
    !
    TYPE(element), INTENT(in):: e1, e2
    TYPE(arete), INTENT(in):: ed
    TYPE(element):: ee
    TYPE(PVar), DIMENSION(:), INTENT(in):: u1, u2
    TYPE(PVar), DIMENSION(:,:), INTENT(out):: resJ
    TYPE(PVar), DIMENSION(e1%nsommets,2) :: resH
    REAL(DP), DIMENSION(e1%nvertex,2):: x

    REAL(DP),DIMENSION(2,e1%nsommets,2):: grad
    REAL(DP), DIMENSION(2,2,e1%nsommets, 2) :: hess
    TYPE(PVar),DIMENSION(e1%nsommets):: u
    TYPE(Pvar):: u_loc
    REAL(DP), DIMENSION(n_dim,N_vars,2):: gr
    REAL(DP), DIMENSION(n_dim,n_dim, N_vars,2):: gr2
    REAL(DP), DIMENSION(2):: xx
    INTEGER:: i,j,l, k
    REAL(DP):: diff1, diff2, diff1H,diff2H, h, beta
    REAL(DP), DIMENSION(2)::  n
    REAL(DP):: beta1, beta2
    TYPE(PVar) :: uloc, ubar1, ubar2
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    REAL(DP):: grr,grl
    TYPE(Pvar), DIMENSION(e1%nsommets):: v1,w1,z1
    TYPE(Pvar), DIMENSION(e2%nsommets)::  v2,w2,z2

    grr=0.; grl=0. 
    DO l=1, n_vars
       resJ(:,:)%u(l)=0.
       resH(:,:)%u(l)=0.
    ENDDO
    h=ed%volume
    beta=0.

    beta1 = 0.
    beta2 = 0.

#if (1==0)
    w1=Control_to_cons(u1,e1)
    DO l=1, e1%nsommets
       z1(l)%u=w1(l)%entropy_var()
    ENDDO
    v1=Control_to_Cons(z1,e1)
    w2=Control_to_cons(u2,e2)
    DO l=1, e2%nsommets
       z2(l)%u=w2(l)%entropy_var()
    ENDDO
    v2=Control_to_Cons(z2,e2)
#else
    v1=u1
    v2=u2
    w1=Control_to_Cons(u1,e1)
    w2=Control_to_Cons(u2,e2)
#endif
    DO l=1, N_vars
       ubar1%u(l)= SUM(u1%u(l))/REAL(e1%nsommets,dp)
       ubar2%u(l)= SUM(u2%u(l))/REAL(e2%nsommets,dp)
    ENDDO
    xx(1)=0.5*(SUM(e1%coor(1,:))+SUM(e2%coor(1,:)))/REAL(e1%nsommets,dp)
    xx(2)=0.5*(SUM(e1%coor(2,:))+SUM(e2%coor(2,:)))/REAL(e1%nsommets,dp)

    n=ed%n/ed%volume


    DO i=1,ed%nquad
       ! coordonnees barycentriques sur les faces
       ! these two sets must correspond to the SAME geometrical point, but seen from
       !different elements. For now it is dealt like this, maybe it is simpler to compute
       ! the geometrical point and then to evaluate the barycentric ccordinate. The
       ! jonglerie with indices is done in GeoGraph.f90
       SELECT CASE(e1%nvertex)
       CASE(3) ! triangle
          x(:,1)=ed%quad(1,i)*a3(:,ed%nu(1,1))+ed%quad(2,i)*a3(:,ed%nu(2,1))
          x(:,2)=ed%quad(1,i)*a3(:,ed%nu(1,2))+ed%quad(2,i)*a3(:,ed%nu(2,2))
       CASE(4) ! quadrangle
          x(:,1)=ed%quad(1,i)*a4(:,ed%nu(1,1))+ed%quad(2,i)*a4(:,ed%nu(2,1))
          x(:,2)=ed%quad(1,i)*a4(:,ed%nu(1,2))+ed%quad(2,i)*a4(:,ed%nu(2,2))
       CASE default
          PRINT*, mod_name
          PRINT*, 'wrong case'
       END SELECT



       ! on a les points de quadrature in the barycentric coordinates of the 2 elements sharin ed
       DO j=1,2 ! we compute grad u and the grad phi at the quadrature points for each element sharing the edge ed
          IF (j==1) THEN
             ee=e1
             u=v1
          ELSE
             ee=e2
             u=v2
          ENDIF
          DO l=1, ee%nsommets
             grad(:,l,j)   = ee%gradient(l, x(:,j) )
!!$             hess(:,:,l,j) = ee%hessian (l, x(:,j) )

          ENDDO
          DO l=1, n_vars
             gr(1,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *grad(1,:,j) )
             gr(2,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *grad(2,:,j) )
!!$             gr2(1,1,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *hess(1,1,:,j) )
!!$             gr2(1,2,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *hess(1,2,:,j) )
!!$             gr2(2,1,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *hess(2,1,:,j) )
!!$             gr2(2,2,l,j)=SUM( (u(:)%u(l)-u(1)%u(l)) *hess(2,2,:,j) )

          ENDDO
       ENDDO
       !       grr=grr+SQRT(SUM(gr(1,:)**2))*ed%weight(i)
       !       grl=grl+SQRT(SUM(gr(2,:)**2))*ed%weight(i)

       gr(:,:,1)=gr(:,:,1)-gr(:,:,2) ! jump of gradientns
       gr2(:,:,:,1)= gr2(:,:,:,1)-gr2(:,:,:,2) !jump of the hessian

       DO k=1, n_vars
          DO l=1,e1%nsommets
             diff1          =  (gr(1,k,1)*n(1)+gr(2,k,1)*n(2))*(grad(1,l,1)*n(1)+grad(2,l,1)*n(2))
             resJ(l,1)%u(k) =  resJ(l,1)%u(k)+ diff1 * ed%weight (i) !? + -
!!$             diff1H         =  (gr2(1,1,k,1)*n(1)*n(1)+&
!!$                  & gr2(1,2,k,1)*n(1)*n(2)+&
!!$                  & gr2(2,1,k,1)*n(2)*n(1)+&
!!$                  &	gr2(2,2,k,1)*n(2)*n(2))* &
!!$                  & (hess(1,1,l,1)*n(1)*n(1)+ &
!!$                  &	hess(1,2,l,1)*n(1)*n(2)+ &
!!$                  &	hess(2,1,l,1)*n(1)*n(2)+ &
!!$                  &	hess(2,2,l,1)*n(2)*n(2) )

!!$             resH(l,1)%u(k) =  resH(l,1)%u(k)+ diff1H * ed%weight(i) !? + -
          ENDDO

          DO l=1,e2%nsommets
             diff2          = (gr(1,k,1)*n(1)+gr(2,k,1)*n(2)) *(grad(1,l,2)*n(1)+grad(2,l,2)*n(2))
             resJ(l,2)%u(k) = resJ(l,2)%u(k)  - diff2 * ed%weight (i) !? + -

!!$             diff2H         =  (gr2(1,1,k,1)*n(1)*n(1)+&
!!$                  &	gr2(1,2,k,1)*n(1)*n(2)+&
!!$                  &	gr2(2,1,k,1)*n(2)*n(1)+&
!!$                  &	gr2(2,2,k,1)*n(2)*n(2))* &
!!$                  &	 (hess(1,1,l,2)*n(1)*n(1)+ &
!!$                  &		hess(1,2,l,2)*n(1)*n(2)+ &
!!$                  &		hess(2,1,l,2)*n(1)*n(2)+ &
!!$                  &		hess(2,2,l,2)*n(2)*n(2) )
!!$
!!$             resH(l,2)%u(k) =  resH(l,2)%u(k)- diff2H * ed%weight(i)

          ENDDO
       ENDDO
    ENDDO

    ! homogeneous to speed * h**2  *h (last h for integration formula)
    beta =  theta * ed%volume * h*h *  ed%jump_flag !!* alpha        !!thet(xx,grr,grl)
    beta2 =  theta2 * ed%volume * h**4  * ed%jump_flag !!* alpha 
    !    beta=beta*thet2(e1,e2,u1,u2)

    resJ(:,1)= ResJ(:,1)* beta !!+ ResH(:,1)* beta2 
    resJ(:,2)= ResJ(:,2)* beta !!+ ResH(:,2)* beta2 



  END SUBROUTINE jump_burman

  FUNCTION stream_old(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "stream"
    REAL(dp), DIMENSION(n_dim), PARAMETER:: xx=0._dp, n=(/1._dp,0._dp/)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e Jac*graphi * tau * ( du + dt* div flux) dx
    !

    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad

    REAL(dp), DIMENSION(n_vars,n_vars) :: nmat
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, divflux_loc, u_loc, gru_loc(2)

    INTEGER:: i, iq, l, k, ll




    res=0._dp


    u_loc=0._dp

    DO l=1, e%nvertex
       u_loc=u_loc+u(l)
    ENDDO

    u_loc=u_loc/REAL(e%nvertex,dp)
    Nmat=u_loc%Nmat(e%nvertex, e%n/e%volume ,xx, dt)

    DO i=1, e%nsommets
       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO



          u_loc=SUM(base*u)
          du_loc=SUM(base*du)
          gru_loc(1)=SUM( grad(1,:)*u(:))
          gru_loc(2)=SUM( grad(2,:)*u(:))
          Jac=u_loc%Jacobian(xx)

          divflux_loc=0._dp
          DO l=1, n_dim
             divflux_loc = divflux_loc + SUM(grad(l,:)*flux(l,:))
             !divflux_loc%u=divflux_loc%u+ matmul(Jac(:,:,l), gru_loc(l)%u )
          ENDDO


          divflux_loc%u=MATMUL(nmat, du_loc%u(:)+ dt* divflux_loc%u(:) )


          !          res(i)%u = res(i)%u + MATMUL(TRANSPOSE( Jac(:,:,1)*grad(1,i)+Jac(:,:,2)*grad(2,i) ), divflux_loc%u  )*e%weight(iq)
          res(i)%u = res(i)%u + MATMUL( Jac(:,:,1)*grad(1,i)+Jac(:,:,2)*grad(2,i) , divflux_loc%u  )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i



  END FUNCTION stream_old

  FUNCTION stream_new(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *)        , PARAMETER :: mod_name = "stream_new"
    REAL(dp), DIMENSION(n_dim), PARAMETER :: xx=0._dp, n=(/1._dp,0._dp/)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e Jac*graphi * tau * ( du + dt* div flux) dx
    !

    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    !    REAL(DP), DIMENSION(e%nsommets,e%nquad),save:: base
    !    REAL(DP), DIMENSION(n_dim,e%nsommets,e%nquad),save:: grad
!    REAL(DP), DIMENSION(3,7),SAVE:: base
!    REAL(DP), DIMENSION(n_dim,3,7),SAVE:: grad
!    LOGICAL,SAVE:: ine=.FALSE.
    REAL(dp), DIMENSION(n_vars,n_vars) :: nmat
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, divflux_loc, u_loc, gru_loc(2)

    INTEGER:: i, iq, l, k, ll

!!$    IF (.NOT. ine) THEN
!!$       DO iq=1,e%nquad
!!$          DO l=1,e%nsommets
!!$             base(l  ,iq)=e%base    (l, e%quad(:,iq))
!!$             grad(:,l,iq)=e%gradient(l, e%quad(:,iq))
!!$          ENDDO
!!$       ENDDO
!!$       ine=.TRUE.
!!$    ENDIF


    res=0._dp


    u_loc=0._dp

    DO l=1, e%nvertex
       u_loc=u_loc+u(l)
    ENDDO

    u_loc=u_loc/REAL(e%nvertex,dp)
    Nmat=u_loc%Nmat(e%nvertex, e%n/e%volume ,xx, dt)

    DO i=1, e%nsommets
       DO iq=1, e%nquad

          u_loc = SUM(e%base_at_quad(:, iq)*u)
          du_loc= SUM(e%base_at_quad(:, iq)*du)
          gru_loc(1)=SUM( e%grad_at_quad(1,2:,iq)*( u(:)-u(1)) )
          gru_loc(2)=SUM( e%grad_at_quad(2,2:,iq)*( u(:)-u(1)) )
          Jac=u_loc%Jacobian(xx)

          divflux_loc=0._dp
          DO l=1, n_dim
             divflux_loc%u=divflux_loc%u+ MATMUL(Jac(:,:,l), gru_loc(l)%u )
          ENDDO


          divflux_loc%u=MATMUL(nmat, du_loc%u(:)+  divflux_loc%u(:) * dt )


          !          res(i)%u = res(i)%u + MATMUL(TRANSPOSE( Jac(:,:,1)*grad(1,i)+Jac(:,:,2)*grad(2,i) ), divflux_loc%u  )*e%weight(iq)
          res(i)%u = res(i)%u + &
               & MATMUL( Jac(:,:,1)*e%grad_at_quad(1,i,iq)+Jac(:,:,2)*e%grad_at_quad(2,i,iq) ,&
               & divflux_loc%u  )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i



  END FUNCTION stream_new




END MODULE scheme
