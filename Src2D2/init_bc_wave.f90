!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE init_bc
  USE param2d
  USE overloading
  USE variable_def
  USE PRECISION
  IMPLICIT NONE

  PRIVATE
  PUBLIC:: IC
CONTAINS

  !---------------------------------------
  ! Setup domain - comes from external mesh, so ignore for now
  !---------------------------------------

  !---------------------------------------
  ! Set initial and boundary conditions
  !---------------------------------------
  TYPE(Pvar) FUNCTION IC(jt,x,initial_cond) RESULT(Var)
    CHARACTER(Len=80):: mod_name="Wave/IC"    
    INTEGER, INTENT(in)                :: initial_cond, jt
    REAL(DP), DIMENSION(:), INTENT(in) :: x
    REAL(DP)                               :: alpha
    REAL(DP), DIMENSION(n_dim)             :: y, centre
 
 
    REAL(dp):: ro, u, v, Ma,p,r2,delp
    REAL(DP),            PARAMETER:: dpi=ACOS(-1._dp) ! pi
    REAL(dp), PARAMETER:: a = 1.0
    REAL(dp), PARAMETER:: aa = 100., bb = 100.

    !---------------
    ! for Euler
    SELECT CASE(initial_cond)
    CASE(0) ! perturbation of ro=1,u=0,p=1
       r2=(sum(x**2))
       delp=exp(-bb*r2)
       ! First example
       Var%u(1) = (p0+delp)**(-gam)/s0-ro0
       Var%u(2) = u0
       Var%u(3) = v0
       Var%u(4) = delp

    CASE default
       PRINT*, "Wrong test number for wave_IC"
       STOP
    END SELECT

  END FUNCTION IC


END MODULE init_bc
