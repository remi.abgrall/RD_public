!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE utils
  USE param2d
  USE variable_def
  USE PRECISION

  IMPLICIT NONE
  REAL(DP), PARAMETER:: Lenght=1._dp
  REAL(DP), PARAMETER:: pi=ACOS(-1._dp)


!!$
CONTAINS
  SUBROUTINE vorticity(Mesh, ua, vor)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "vorticity"
    TYPE(maillage), INTENT(in):: mesh
    TYPE(Pvar), DIMENSION(:), INTENT(in):: ua
    REAL(dp), DIMENSION(:), INTENT(out):: vor
    REAL(dp), DIMENSION(n_dim):: vit, l_v
    REAL(dp), DIMENSION(n_dim,3):: grad
    REAL(dp):: grux,gruy,grvx,grvy
    REAL(dp),DIMENSION(2)::x=0._dp
    INTEGER:: jt,  l, jseg
    TYPE(element):: e
    TYPE(frontiere):: efr
    IF (MINVAL(Mesh%e(:)%nsommets).NE.3 .OR. MAXVAL(Mesh%e(:)%nsommets).NE.3) THEN
       PRINT*, mod_name, "only for P1"
       STOP
    ENDIF
    vor=0._dp
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       grad(:,1)=e%gradient(1,x)
       grad(:,2)=e%gradient(2,x)
       grad(:,3)=e%gradient(3,x)
       vit(1)=sum(ua(e%nu)%u(1))/3._dp
       vit(2)=sum(ua(e%nu)%u(2))/3._dp
       grux=SUM(grad(1,:)*ua(e%nu)%u(1))
       gruy=SUM(grad(2,:)*ua(e%nu)%u(1))
       grvx=SUM(grad(1,:)*ua(e%nu)%u(2))
       grvy=SUM(grad(2,:)*ua(e%nu)%u(2))
       DO l=1, e%nsommets
          vor(e%nu(l))=vor(e%nu(l))+(vit(2)*grad(2,l)-vit(1)*grad(1,l))*e%volume!(-gruy+grvx)*e%volume
       ENDDO
    ENDDO
    DO jseg=1, Mesh%nsegfr
       efr=mesh%fr(jseg)
!       print*, "jseg=", jseg
!       print*, vor(efr%nu(:))
       l_v=( e%coor(:,2)-e%coor(:,1) )
       vit=0.5_dp*( ua(efr%nu(1))%u(1:2)+ua(efr%nu(2))%u(1:2) )
       vor(efr%nu(1))=vor(efr%nu(1))+SUM(vit*l_v)
       vor(efr%nu(2))=vor(efr%nu(2))-SUM(vit*l_v)

    ENDDO

    vor=vor*Mesh%aires
  END SUBROUTINE vorticity

!!$TYPE(Pvar) FUNCTION exact_vortex(x,y,temps)
!!$    REAL(DP), INTENT(in):: x,y
!!$    REAL(DP), OPTIONAL, INTENT(in)::temps
!!$    REAL(DP),DIMENSION(n_dim):: xx
!!$    REAL(DP):: t,p,r2,beta,uinf,vinf
!!$    
!!$    IF (PRESENT(temps)) THEN
!!$       t=temps
!!$    ELSE
!!$       t=0._dp
!!$    ENDIF
!!$    xx=(/x,y/)
!!$
!!$    r2=xx(1)**2+xx(2)**2
!!$    beta=5._dp
!!$    uinf=0._dp
!!$    vinf=0._dp
!!$
!!$    exact_vortex%u(1)=(1._dp-((gmm-1._dp)*beta*beta)/(8._dp*gmm*pi**2)*exp(1._dp-r2))**(1._dp/(gmm-1._dp))
!!$
!!$    exact_vortex%u(2)= uinf- xx(2)*(beta/(2._dp*pi))*exp((1._dp-r2)/2._dp)
!!$
!!$    exact_vortex%u(3)= vinf+ xx(1)*(beta/(2._dp*pi))*exp((1._dp-r2)/2._dp)
!!$
!!$    p=exact_vortex%u(1)**gmm
!!$    
!!$    exact_vortex%u(4)= p
!!$  
!!$  END FUNCTION exact_vortex
!!$
!!$  TYPE(Pvar) FUNCTION solution(x,y,temps)
!!$    REAL(DP), INTENT(in):: x,y
!!$    REAL(DP), OPTIONAL, INTENT(in)::temps
!!$    REAL(DP),DIMENSION(n_dim):: xx
!!$    REAL(DP):: t
!!$    IF (PRESENT(temps)) THEN
!!$       t=temps
!!$    ELSE
!!$       t=0._dp
!!$    ENDIF
!!$    xx=(/x,y/)
!!$    solution=fonc(xx)
!!$
!!$  END FUNCTION solution
!!$
!!$  TYPE(Pvar) FUNCTION solution0(x,y,temps)
!!$    REAL(DP), INTENT(in):: x,y
!!$    REAL(DP), OPTIONAL, INTENT(in)::temps
!!$    REAL(DP),DIMENSION(n_dim):: xx
!!$    REAL(DP):: t
!!$    IF (PRESENT(temps)) THEN
!!$       t=temps
!!$    ELSE
!!$       t=0._dp
!!$    ENDIF
!!$    xx=(/x,y/)
!!$    ! rotation
!!$    xx(1)=COS(2._dp*pi*t)*x-SIN(2._dp*pi*t)*y
!!$    xx(2)=SIN(2._dp*pi*t)*x+COS(2._dp*pi*t)*y
!!$    IF (ABS(x*y).LE.1.e-3_dp) THEN
!!$       solution0=fonc(xx)
!!$    ELSE
!!$       solution0%u(:)=0._dp
!!$    ENDIF
!!$  END FUNCTION solution0
!!$
!!$  REAL(DP) FUNCTION kpp(x)
!!$    REAL(DP), DIMENSION(n_dim), INTENT(in)::x
!!$    REAL(DP):: r
!!$    r=SUM(x**2)
!!$    kpp=pi/4._dp
!!$    IF (r<1._dp) kpp=3.5_dp*pi
!!$  END FUNCTION kpp
!!$
!!$  Type(Pvar) FUNCTION fonc(x) RESULT(Var)
!!$    REAL(DP), DIMENSION(n_dim), INTENT(in) :: x
!!$    REAL(DP), DIMENSION(n_dim) :: y, centre
!!$    REAL(DP) :: r
!!$       r = SQRT(SUM(x**2))
!!$
!!$       IF (r <= 0.5_dp) THEN
!!$          Var%u(1) = 1._dp ! [kg/m^3]
!!$          Var%u(2) = 0._dp ! [m/s]
!!$          Var%u(3) = 0._dp ! [m/s]
!!$          Var%u(4) = Var%u(1)*epsEOS(Var%u(1),1._dp) ! [J]
!!$       ELSE
!!$          Var%u(1) = 0.125_dp
!!$          Var%u(2) = 0._dp
!!$          Var%u(3) = 0._dp
!!$          Var%u(4) = Var%u(1)*epsEOS(Var%u(1),0.1_dp)
!!$       END IF
!!$  END FUNCTION fonc
!!$
!!$  REAL(DP) FUNCTION burger(x)
!!$    REAL(DP), DIMENSION(n_dim), INTENT(in):: x
!!$    IF (x(2).GE.0.5) THEN
!!$       IF (-2._dp*(x(1)-3._dp/4._dp)+(x(2)-0.5_dp).GE.0_dp) THEN
!!$          burger=1.5_dp!-0.5
!!$       ELSE
!!$          burger=-0.5_dp!1.5
!!$       ENDIF
!!$    ELSE
!!$       burger=MAX(-.5_dp,MIN(1.5_dp,(x(1)-0.75_dp)/(x(2)-0.5_dp)))
!!$    ENDIF
!!$
!!$  END FUNCTION burger
!!$
!!$
!!$  REAL(DP) FUNCTION krivodonova(x)
!!$    ! L Krivodonova
!!$    ! JCP 226, pp 879-896, 2007
!!$    ! SHu and Hu
!!$    ! JCP 126, pp 202-228, 1996
!!$    !
!!$    REAL(DP), PARAMETER:: a=0.5_dp, z=-0.7_dp, delta=0.05_dp, alpha=10._dp,&
!!$         & beta=log(2._dp)/(36._dp*delta*delta)
!!$    REAL(DP), INTENT(in):: x
!!$    REAL(DP):: r,  phi0
!!$    IF (x.GE.0_dp) THEN
!!$       r=INT(x)
!!$    ELSE
!!$       r=FLOOR(x)
!!$    ENDIF
!!$    r=2._dp*(x-r)-1._dp
!!$    phi0=0._dp
!!$    IF (r.GE.-0.8_dp.AND.r.LE.-0.6_dp) THEN
!!$       phi0=( G(r,beta,z-delta)+G(r,beta,z+delta)+4*G(r,beta,z))/6._dp
!!$    ENDIF
!!$    IF (r.GE.0._dp.AND.r.LE.0.2_dp) THEN
!!$       phi0=1._dp-ABS( 10._dp*(r-0.1_dp))
!!$    ENDIF
!!$    IF (r.GE.-0.4_dp.AND.r.LE.-0.2_dp) THEN
!!$       phi0=1._dp
!!$    ENDIF
!!$    IF (r.GE.0.4_dp.AND.r.LE.0.6_dp) THEN
!!$       phi0=(Ff(r,alpha,a-delta)+Ff(r,alpha,a+delta)+4._dp*Ff(r,alpha,z))/6._dp
!!$    ENDIF
!!$    krivodonova=phi0
!!$
!!$  END FUNCTION krivodonova
!!$
!!$  REAL(DP) FUNCTION G(x,beta,z)
!!$    REAL(DP), INTENT(in):: x,beta,z
!!$    G=EXP(-beta*(x-z)**2)
!!$  END FUNCTION G
!!$
!!$  REAL(DP) FUNCTION Ff(x,alpha,a)
!!$    REAL(DP), INTENT(in):: x,alpha,a
!!$    Ff=SQRT( MAX( 1._dp-alpha*alpha*(x-a)*(x-a),0._dp))
!!$  END  FUNCTION Ff
!!$
!!$
!!$  REAL(DP) FUNCTION zalesak(x,y)
!!$    REAL(DP), INTENT(in):: x,y
!!$    REAL(DP):: r, yy, phi0
!!$
!!$
!!$    IF (SQRT((x-0.5)**2+(y-0.75_dp)**2)/0.15_dp .LE.1._dp) THEN
!!$       IF (ABS(x-0.5_dp).GE.0.25_dp .OR. y.GE.0.85_dp-1.e-8_dp) THEN
!!$
!!$          phi0=1._dp     !%%% Zalesak
!!$
!!$       END IF
!!$    ELSE IF (SQRT((x-0.5_dp)**2+(y-0.25_dp)**2)/0.15_dp.LE.1._dp) THEN
!!$
!!$       phi0=1._dp-SQRT((x-0.5_dp)**2+(y-0.25_dp)**2)/0.15_dp !  %%%  Cône
!!$
!!$    ELSE IF (SQRT((x-0.25_dp)**2+(y-0.5_dp)**2)/0.15_dp.LE.1._dp) THEN
!!$
!!$       phi0=0.25_dp*(1._dp+COS(PI*MIN(SQRT((x-0.25_dp)**2+(y-0.5_dp)**2)/0.15_dp,1._dp))) ! %%% smooth bump
!!$
!!$    END IF
!!$    zalesak=phi0
!!$  END FUNCTION zalesak
!!$
!!$  FUNCTION BC_data(efr,uloc,x,y,temps, DATA) RESULT(ubord)
!!$    TYPE(frontiere), INTENT(in) :: efr
!!$    TYPE(PVar),      INTENT(in) :: uloc
!!$    REAL(DP),            INTENT(in) :: x,y
!!$    REAL(DP),            INTENT(in) :: temps
!!$    TYPE(Pvar)                  :: ubord
!!$    TYPE(donnees),   INTENT(in)   :: DATA
!!$    REAL(DP),            PARAMETER:: pi=ACOS(-1._dp) ! pi
!!$    REAL(DP)::vel,ro,u,v,p
!!$    REAL(DP),DIMENSION(2)           ::  nor
!!$    REAL(DP)                        :: mn 
!!$
!!$    SELECT CASE(DATA%Icas)
!!$    CASE(1,8)
!!$       ! Sod
!!$       ubord = uloc
!!$
!!$       !--------------------------------------------------------------------------------------
!!$
!!$    CASE(11)
!!$       ! BC for Step
!!$       SELECT CASE(efr%bc_tag)
!!$       CASE(4,6)
!!$          ! outflow (right horizontal wall)
!!$          ubord = uloc
!!$
!!$       CASE(1,2,3,5)
!!$          ! wall (horizontal). COMPUTE THE state with symmetry
!!$          nor=efr%n/SQRT(SUM(efr%n**2))
!!$          mn= sum(uloc%u(2:3)*nor)
!!$
!!$          ubord%u(1) = uloc%u(1)
!!$          ubord%u(2:3) = uloc%u(2:3)- mn*nor
!!$          ubord%u(4) = uloc%u(4)
!!$
!!$
!!$       CASE default
!!$          print*, 'Wrong BC type in BC_Data, efr%bc_tag = ', efr%bc_tag
!!$          stop
!!$       END SELECT
!!$
!!$       !--------------------------------------------------------------------------------------
!!$    CASE(10)
!!$       !! BC for DMR
!!$       SELECT CASE(efr%bc_tag)
!!$       CASE(5)
!!$          ro=8._dp; u=8.25_dp; v=0._dp; p=116.5_dp
!!$          ! inflow (left horizontal wall)
!!$          ubord%u(1) = ro
!!$          ubord%u(2) = ro*u
!!$          ubord%u(3) = ro*v
!!$          ubord%u(4) = ro * epsEOS(ro,p)+0.5_dp*ro*(u*u+v*v)
!!$
!!$       CASE(3)
!!$          ! outflow (right horizontal wall)
!!$          ubord = uloc
!!$
!!$       CASE(1,2,4)
!!$          ! wall (horizontal). COMPUTE THE state with symmetry
!!$          nor=efr%n/SQRT(SUM(efr%n**2))
!!$          mn= sum(uloc%u(2:3)*nor)
!!$
!!$          ubord%u(1) = uloc%u(1)
!!$          ubord%u(2:3) = uloc%u(2:3)- mn*nor
!!$          ubord%u(4) = uloc%u(4)
!!$
!!$
!!$       CASE default
!!$          print*, 'Wrong BC type in BC_Data, efr%bc_tag = ', efr%bc_tag
!!$          stop
!!$       END SELECT
!!$
!!$
!!$    CASE default
!!$       PRINT*, "Wrong test number for BC_data(), test = ", DATA%Icas
!!$       STOP
!!$    END SELECT
!!$  END FUNCTION BC_data

END MODULE utils
