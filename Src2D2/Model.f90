!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE model

  ! This module allows to pass from control to physical variables and viceversa
  ! it also contains the initialization of the bnchmark problem

  USE param2d
  USE init_bc
  USE PRECISION

CONTAINS
  SUBROUTINE Model_init(DATA,Mesh, Var)
    ! initialisation of the problem. Here for scalars
    TYPE(donnees), INTENT(in):: DATA
    TYPE(maillage), INTENT(in):: Mesh
    TYPE(variables), INTENT(inout):: Var
    INTEGER:: ns, nsommets, l
    INTEGER, DIMENSION(:),POINTER:: nu
    REAL(DP),DIMENSION(:,:),  POINTER:: coor
    REAL(DP), DIMENSION(:), POINTER:: sol
    INTEGER:: jt, k
    ns=Mesh%ns
    k=mesh%e(1)%nsommets

    DO jt=1, Mesh%nt
       Nsommets=Mesh%e(jt)%Nsommets
       NULLIFY(nu); NULLIFY(coor); NULLIFY(sol)
       nu=>Mesh%e(jt)%nu(:)
       coor=>Mesh%e(jt)%coor

       DO l=1,Nsommets
          Var%ua(0,nu(l)) = IC(jt,coor(:,l),DATA%Icas)
       ENDDO
    ENDDO
  END SUBROUTINE Model_init

  FUNCTION Control_to_Cons(u,e) RESULT (u_cons)
    ! crom control point, compute the values a physical dofs
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(element), INTENT(in):: e
    TYPE(Pvar),DIMENSION(SIZE(u,dim=1)):: u_cons
    INTEGER:: k
    u_cons(1:e%nvertex)=u(1:e%nvertex)
    SELECT CASE(e%itype)
    CASE(1) ! Lagrange
    CASE(3,4) ! Lagrange
       u_cons(e%nvertex+1:)=u(e%nvertex+1:)
    CASE(2,5) ! Bezier
       DO k=1, n_vars
          DO l=e%nvertex+1, e%nsommets!SIZE(u)
             u_cons(l)%u(k)=SUM(e%base_at_dofs(:,l)*u(:)%u(k))
          ENDDO
       ENDDO
    CASE default
       PRINT*, "erreur dans Model/Control_to_Cons"
       STOP
    END SELECT
  END FUNCTION Control_to_cons

  FUNCTION Cons_to_Control(u_cons,e) RESULT (u)
    ! from values at dofs, compute control pounts
    REAL(DP), PARAMETER:: untier=1./3.
    !integer, INTENT(in):: n_vars
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u_cons
    TYPE(element), INTENT(in):: e
    TYPE(Pvar),DIMENSION(SIZE(u_cons,dim=1)):: u
    TYPE(Pvar):: S1,S2
    INTEGER:: l
    u=u_cons
    SELECT CASE(e%itype)
    CASE(1,3,4) ! Lagrange
    CASE(2,5) ! Bezier
       DO k=1, n_vars
          DO l=e%nvertex+1, e%nsommets!SIZE(u)
             u(l)%u(k)=SUM(e%inv_base_at_dofs(:,l)*u_cons(:)%u(k))
          ENDDO
       ENDDO
    CASE default
       PRINT*, "erreur dans Model/Control_to_Cons"
       STOP
    END SELECT
    !enddo
  END FUNCTION Cons_to_Control

END MODULE Model

