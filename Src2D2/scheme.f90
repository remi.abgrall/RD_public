!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE scheme !2D
  ! In this module all scheme  for the flux are collected.
  ! Structure of this module:
  ! - schema -> allows to choose between different scheme for the flux approximation,
  !                 which are coded in this same module and are the following
  !                 a) galerkin
  !                 b) galerkin + psi limiting
  !                 c) local Lax-Friedrichs (=Rusanov)
  ! - stabilization: Burman's jump
  ! - limiting: generic function to activate the limiting,
  !              which can be done either via conservative variables, or characteristic ones
  ! The boundary conditions
  !  inflow/outflow: log=1
  !  wall:           log=2
  !
  USE param2d
  USE overloading
  USE init_bc
  USE Model
  USE PRECISION
  IMPLICIT NONE
  PRIVATE
  INTERFACE jump
     MODULE PROCEDURE jump_burman
  END INTERFACE jump
  INTERFACE stream
     MODULE PROCEDURE stream_new
  END INTERFACE stream
  INTERFACE galerkin
     MODULE PROCEDURE galerkin_new
  END INTERFACE galerkin
  INTERFACE lxf
     MODULE PROCEDURE lxf_new
  END INTERFACE lxf
  INTERFACE phij
     MODULE PROCEDURE phij_kineticmomentum
  END INTERFACE phij

!!$    INTERFACE correction
!!$       MODULE PROCEDURE correction_kinetic
!!$       Module procedure correction_entropy
!!$    END INTERFACE correction

  PUBLIC:: schema, jump, phij, phi_e, correction_kinetic, correction_entropy


CONTAINS
  !  FUNCTION schema(limit, ischema, e, u, du,dJ,  flux, fluxg,  dt) RESULT(res)
  FUNCTION schema(limit, e, u, du,  flux,   dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "scheme"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! residual (supg or psi)
    !
    INTEGER                          , INTENT(in):: limit
    TYPE(element),                     INTENT(inout):: e
    TYPE(PVar), DIMENSION(:),          INTENT(in):: u
    TYPE(PVar), DIMENSION(:),          INTENT(in):: du!, dJ
    TYPE(PVar), DIMENSION(:,:),        INTENT(in):: flux!, fluxg
    REAL(DP)   ,                       INTENT(in):: dt
    TYPE(PVar), DIMENSION(e%nsommets)            :: res


    SELECT CASE (e%type_flux)
    CASE(3)
       res= galerkin(e, u, du,   flux,   dt)
    CASE(4)
       res= galerkin(e, u, du, flux, dt)
    CASE(5) ! with Burman filtering
       res= psi_jump(limit, e, u, du, flux,  dt)
    CASE(6) ! with stream filtering
       res= psi_new_stream(limit, e, u, du, flux, dt)
    CASE(7) ! with stream filtering
       res= supg_new(limit, e, u, du,  flux, dt)
    CASE(8)
       res=bidouille( e, u, du, flux, dt)
    CASE(-1)
       res=lxf(e, u, du,  flux,  dt)
    CASE default
       PRINT*, "scheme not defined"
       STOP
    END SELECT


  END FUNCTION schema


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ SCHEME FOR FLUX ---------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------


  FUNCTION galerkin_new(e, u, du, flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Galerkin_new"
    ! input
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    INTEGER::  l

    IF (e%type_flux==-1.OR.e%type_flux==5.OR.e%type_flux==6 ) THEN
       DO l=1, n_vars
          res(:)%u(l)=  dt* ( MATMUL(e%coeff(:,:,1),flux(1,:)%u(l)-flux(1,1)%u(l)) &
               &      +  MATMUL(e%coeff(:,:,2),flux(2,:)%u(l)-flux(2,1)%u(l)) )
       ENDDO
       DO l=1, e%nsommets
          res(l)= res(l)+ e%volume/REAL(e%nsommets)*du(l)
       ENDDO
    ELSE
       DO l=1, n_vars
          res(:)%u(l)=MATMUL(e%masse(:,:),du%u(l)) &
               & +dt* ( MATMUL(e%coeff(:,:,1),flux(1,:)%u(l)-flux(1,1)%u(l)) &
               &      +  MATMUL(e%coeff(:,:,2),flux(2,:)%u(l) -flux(2,1)%u(l)) )
       ENDDO
    ENDIF


  END FUNCTION galerkin_new



  FUNCTION phij_kineticmomentum(e, u, du, flux, dt) RESULT(resj)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "phij"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                           INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)    :: flux
    REAL(DP)      ,                 INTENT(in):: dt

    REAL(qp)       :: resj

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim):: vit, n, gg, xx
    !    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, u_loc, flux_loc(n_dim)

    INTEGER:: i, iq, l, k, ll, k1, k2
    REAL(DP), DIMENSION(:),ALLOCATABLE:: yy


    resj=SUM( du(:)%u(3)*e%yy(1,:)-du(:)%u(2)*e%yy(2,:)) * e%volume

    ALLOCATE(yy(SIZE(e%quad_edge,dim=2)))
    SELECT CASE(e%nvertex)
    CASE(3) ! triangle
       DO k=1,e%nvertex ! here a triangle

          k1=e%next(k)
          k2=e%next(k1)
          ! normale exterieure
          n(1)= ( e%coor(2,k2)-e%coor(2,k1)) !?-
          n(2)=-(e%coor(1,k2)-e%coor(1,k1)) !?+

          DO iq=1,e%nquad_edge
             yy(:)=e%quad_edge(k,:,iq)
             xx(1)=SUM( yy*e%coor(1,1:e%nvertex) )  ! physical location of the quadrature point
             xx(2)=SUM( yy*e%coor(2,1:e%nvertex) )  ! physical location of the quadrature point
             DO l=1,e%nsommets
                base(l)=e%base(l,yy)
             ENDDO

             DO ll=1, n_vars
                u_loc%u(ll)=SUM(base*u%u(ll))
             ENDDO

             ! flux_loc=u_loc%flux()
             flux_loc(1)=SUM(base*flux(1,:))
             flux_loc(2)=SUM(base*flux(2,:))

             gg(1)=flux_loc(1)%u(3)*xx(1)-flux_loc(1)%u(2)*xx(2)
             gg(2)=flux_loc(2)%u(3)*xx(1)-flux_loc(2)%u(2)*xx(2)
             resj=resj+dt* e%weight_edge(k,iq)*SUM( gg*n(:) )
          ENDDO !iq
       ENDDO !edge
    CASE(4) ! quadrangle

       DO k=1, e%nvertex
          k1=e%edge(1,k)
          k2=e%edge(2,k)
          n(1)=-(e%coor(2,k2)-e%coor(2,k1))
          n(2)= (e%coor(1,k2)-e%coor(1,k1))
          DO iq=1,e%nquad_edge
             yy(:)=e%quad_edge(k,:,iq)
             xx=e%iso(yy)   !physical location of the quadrature point

             DO l=1,e%nsommets
                base(l)=e%base(l,yy)
             ENDDO

             DO ll=1, n_vars
                u_loc%u(ll)=SUM(base*u%u(ll))
             ENDDO

             ! flux_loc=u_loc%flux()
             flux_loc(1)=SUM(base*flux(1,:))
             flux_loc(2)=SUM(base*flux(2,:))

             gg(1)=flux_loc(1)%u(3)*xx(1)-flux_loc(1)%u(2)*xx(2)
             gg(2)=flux_loc(2)%u(3)*xx(1)-flux_loc(2)%u(2)*xx(2)
             resj=resj+dt* e%weight_edge(k,iq)*SUM( gg*n(:) )
          ENDDO !iq
       ENDDO !edge
    CASE default
       PRINT*, mod_name
       PRINT*, 'erreur edge'
       STOP
    END SELECT

    DEALLOCATE(yy)

  END FUNCTION phij_kineticmomentum



  REAL(dp) FUNCTION Phi_e(e,   du, flux, dt)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "phi_e"
    ! input:
    ! e: element
    ! du: temporal increment up-u0
    !
    ! flux: values of the entropy flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *(entropy_p-Entropy_u) - dt* \int_e nabla varphi_i . entropyflux
    !
    TYPE(element),                         INTENT(in):: e
    REAL(dp), DIMENSION(e%nsommets)      , INTENT(in):: du
    REAL(dp), DIMENSION(n_dim,e%nsommets), INTENT(in):: flux
    REAL(dp)   ,                        INTENT(in)   :: dt
    REAL(dp)                                         :: entropy
    REAL(dp), DIMENSION(e%nsommets)                  :: base
    INTEGER                                          :: iq, l, k
    REAL(dp), DIMENSION(3)                           :: yy
    REAL(dp), DIMENSION(n_dim)                       :: flux_loc
    REAL(dp)                                         :: ds_loc
    REAL(DP), DIMENSION(n_dim)                       ::  n

    entropy=0._dp
    DO iq=1, e%nquad
       ds_loc=SUM(e%base_at_quad(:,iq)*du)
       entropy=entropy+ ds_loc*e%weight(iq)
    ENDDO
    entropy=entropy*e%volume
    DO k=1, e%nvertex
       n=-e%n(:,k)

       DO iq=1,e%nquad_edge


          flux_loc(1)=SUM(e%base_at_quad_edge(k,:,iq)*flux(1,:))
          flux_loc(2)=SUM(e%base_at_quad_edge(k,:,iq)*flux(2,:))

          DO l=1,e%nsommets
             entropy=entropy+dt* e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*e%base_at_quad_edge(k,l,iq)
          ENDDO
       ENDDO !iq
    ENDDO !edge

  END FUNCTION Phi_e


  !===========================================================================
  SUBROUTINE Correction_kinetic( e ,psi, du, u,flux,dt)
    ! ----------------------------------------------------------------------
    ! Correction function scaling through cartesian scalar product

    ! ----------------------------------------------------------------------
    IMPLICIT NONE 

    ! ---------------------- Variables declaration -------------------------
    ! Variables of interest
    TYPE(element), INTENT(in) 				:: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(inout)	:: psi
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in)	:: u, du
    TYPE(Pvar),DIMENSION(n_dim,e%nsommets), INTENT(in)  :: flux
    REAL(dp),                               INTENT(in)  :: dt

    REAL(dp) :: we, phi, resk, alphaa, cor
    REAL(dp), DIMENSION(n_dim):: xbar
    INTEGER:: l
    we=0._dp

    DO l=1, e%nsommets
       we=we+(psi(l)%u(3)*e%y(1,l)-psi(l)%u(2)*e%y(2,l) )
    END DO
    resk=phij(e, u,du,flux,dt)
    phi=resk-we
    xbar(1)=SUM(e%y(1,:))/e%nsommets
    xbar(2)=SUM(e%y(2,:))/e%nsommets
    cor=SUM(  (e%y(1,:)-xbar(1) )**2+( e%y(2,:)-xbar(2) )**2 )
    alphaa=phi/(cor)


    DO l=1,e%nsommets
       psi(l)%u(2)=psi(l)%u(2)-alphaa*(e%y(2,l)-xbar(2) )
       psi(l)%u(3)=psi(l)%u(3)+alphaa*(e%y(1,l)-xbar(1) )
    END DO

  END SUBROUTINE correction_kinetic

  SUBROUTINE Correction_entropy( e ,psi, entropy, u,dt)
    ! ----------------------------------------------------------------------
    ! Correction function scaling through cartesian scalar product

    ! ----------------------------------------------------------------------
    IMPLICIT NONE

    ! ---------------------- Variables declaration -------------------------
    ! Variables of interest
    TYPE(element), INTENT(in) 				:: e
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(inout)	:: psi
    TYPE(PVar), DIMENSION(e%nsommets), INTENT(in)	:: u
    REAL(dp), INTENT(in):: entropy

    ! Buffer variables to clearify the readibility motiviated through Elise's code
    TYPE(PVar):: vbar
    REAL(dp):: buff, buff2
    TYPE(Pvar):: restot
    TYPE(Pvar), DIMENSION(e%nsommets):: W, V
    TYPE(Pvar), DIMENSION(e%nsommets) 	::  Cpsi
    REAL(dp), DIMENSION(u(1)%nvars,u(1)%nvars):: A0
    REAL(dp), DIMENSION(e%nsommets):: Q
    REAL(dp) 	:: err,alpha,dt
    INTEGER :: s,l

!!! ---------------------- Variables initialisations
    buff  = 0.0_dp
    buff2 = 0.0_dp
    W = 0.0_dp
    V = 0.0_dp
    restot=0._dp

!!! --------------- Computation of the correction term


    ! Convert the conservative variables to the entropy ones defined in variable_def
    DO s = 1, e%nsommets
       V(s) = u(s)%cons2entropy( )
    END DO
    vbar=0._dp
    DO s=1, e%nsommets
       vbar=vbar+u(s)
       restot=restot+psi(s)
    ENDDO
    vbar=vbar/REAL(e%nsommets,dp)
    A0=vbar%Hessian_entropy()
    vbar=0._dp
    DO s=1, e%nsommets
       vbar=vbar+v(s)
    ENDDO
    vbar=vbar/REAL(e%nsommets,dp)
!!! Loop on the Dofs
!!! Buff is for the term sum_K(v-vbar)**2
!!! buff2 is sum_K <V,Psi>
    DO s = 1, e%nsommets
       buff  = buff+  SUM( (V(s)%u(:)-vbar%u(:) ) * MATMUL(A0, V(s)%u(:)-vbar%u(:) ) )
       buff2 = buff2+ SUM( (V(s)%u(:)-vbar%u(:) ) * ( psi(s)%u(:) -restot%u(:) ) )
    END DO
    IF (buff<0._dp) PRINT*,"buf", buff
!!! Compute alpha and E !
    !! +Tiny to avoid the inverstion by zero
!!! err is the entropy error done in each timestep
    alpha = 1.0_dp/(buff+1.e-10_dp)!+TINY(1.0_dp)) 
    err = (entropy-buff2 )/(buff+1.e-10_dp)!*alpha  !?????

    ! Add the correction term r= E*alpha*(v-vbar)

    DO l=1,e%nsommets
       psi(l)%u(:)=psi(l)%u(:) + err*MATMUL(A0,v(l)%u(:)-Vbar%u(:))
    END DO


  END SUBROUTINE correction_entropy

  FUNCTION psi_jump(limit, e, u, du,  flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Psi_jump"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)             :: res, v,u_cons
    TYPE(Pvar), DIMENSION(e%nsommets)             :: phi,  phi_LF
    LOGICAL:: out
    phi_LF=LxF(e, u, du, flux,  dt)

    SELECT CASE(limit)
    CASE(3) ! hybrid char & var (the last applies in case of NAN)
       CALL lim_psi_jump_char(u,phi_LF,res,out)
       ! if NaN detected, then do limitation by variables
       IF (out) CALL lim_psi_jump_var(u,phi_LF,res)
    CASE(2)
       CALL lim_psi_jump_char(u,phi_LF, res,out)
    CASE(1)
       CALL lim_psi_jump_var(u,phi_LF, res)
    CASE default
       PRINT*, "wrong limiter"
    END SELECT


  END FUNCTION psi_jump

  FUNCTION bidouille( e, u, du, flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF_new"
    ! this version is conservative by taking properly into account the Gauss formula
    ! what ever the quadrature points chosen
    ! the residuals are defined using the galerkin residuals+LxF dissipation.
    ! The first ordre residual satisfies an energy (entropy) inequality
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    TYPE(element),                     INTENT(inout):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res,v, LxF
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp

    TYPE(Pvar), DIMENSION(e%nsommets):: phi

    TYPE(PVar) ::  ubar, vbar
    REAL(DP):: alpha
    INTEGER:: l

    ubar= SUM(u)/REAL(e%nsommets,DP)
    ! it seems to be necessary to go into physical state
    v=Control_to_Cons(u,e)
    vbar=SUM(v)/REAL(e%nsommets,DP)
    !
    alpha=0._dp
    DO l=1, e%nvertex
       alpha=MAX(alpha,vbar%spectral_radius(xx,e%n(:,l)))
    ENDDO

    !
    phi=galerkin(e,u,du,  flux, dt)
    DO l=1, e%nsommets
       LxF(l)=phi(l)+alpha*dt*(u(l)-ubar)
    ENDDO

    CALL lim(e%l, dt*alpha, ubar, u, phi,LxF,res)

  END FUNCTION bidouille

  FUNCTION supg_new(limit, e, u, du,  flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "supg_new"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux
    REAL(DP)   ,                        INTENT(in):: dt
    TYPE(PVar), DIMENSION(e%nsommets)             :: res


    INTEGER                                       :: next,k, i


    res=galerkin(e, u, du, flux,  dt)

    res=res+stream(e, u, du, flux, dt)


  END FUNCTION supg_new



  FUNCTION psi_new_stream(limit, e, u, du,   flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Psi_new_stream"
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi with streamline filtering
    ! additional test to reduce the order if the results are weird
    !
    INTEGER                          , INTENT(in) :: limit
    TYPE(element),                     INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: u
    TYPE(PVar), DIMENSION(:), INTENT(in)          :: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)        :: flux
    REAL(DP)   ,                        INTENT(in):: dt
    TYPE(donnees)                                 :: DATA

    TYPE(PVar), DIMENSION(e%nsommets)             :: res, v,u_cons
    TYPE(Pvar), DIMENSION(e%nsommets)             :: phi,  phi_LF, diss
    LOGICAL:: out
    INTEGER:: i, l
    phi_LF=LxF(e, u, du,  flux, dt)

    SELECT CASE(limit)
    CASE(3) ! hybrid char & var (the last applies in case of NAN)
       CALL lim_psi_jump_char(u,phi_LF,res,out)
       ! if NaN detected, then do limitation by variables
       IF (out) CALL lim_psi_jump_var(u,phi_LF,res)
    CASE(2)
       CALL lim_psi_jump_char(u,phi_LF, res,out)
    CASE(1)
       CALL lim_psi_jump_var(u,phi_LF, res)
    CASE default
       PRINT*, "wrong limiter"
    END SELECT

    res=res+stream(e, u, du, flux, dt)
    out=.FALSE.
    DO i=1, e%nsommets
       DO l=1, n_vars
          IF (res(i)%u(l).NE.res(i)%u(l)) THEN
             out=.TRUE.
             EXIT
          ENDIF
       ENDDO
    ENDDO
    IF (out) res=phi_LF

  END FUNCTION psi_new_stream


  FUNCTION lxf_new( e, u, du, flux,  dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF_new"
    ! this version is conservative by taking properly into account the Gauss formula
    ! what ever the quadrature points chosen
    ! the residuals are defined using the galerkin residuals+LxF dissipation.
    ! The first ordre residual satisfies an energy (entropy) inequality
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res,v
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp

    TYPE(Pvar), DIMENSION(e%nsommets):: phi

    TYPE(PVar) ::  ubar, vbar
    REAL(DP):: alpha
    INTEGER:: l

    ubar= SUM(u)/REAL(e%nsommets,DP)
    ! it seems to be necessary to go into physical state
    v=Control_to_Cons(u,e)
    vbar=SUM(v)/REAL(e%nsommets,DP)
    !
    alpha=0._dp
    DO l=1, e%nvertex
       alpha=MAX(alpha,vbar%spectral_radius(xx,e%n(:,l)))
    ENDDO

    !
    phi=galerkin(e,u,du,  flux, dt)

    res =   phi+dt*alpha*(u-ubar)


  END FUNCTION lxf_new



  FUNCTION lxf_P1_ref( e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF_P1"
    ! this version use the fact that the total residual, in the quadrature free approach, is a weighted
    ! sum of P1 resudials
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER, PARAMETER, DIMENSION(3,1):: nuloc1=RESHAPE((/1,2,3/),(/3,1/)) ! Tri, B1
    INTEGER, PARAMETER                :: ntri1=1                           ! Tri, B1
    INTEGER, PARAMETER, DIMENSION(3,3):: nuloc2=RESHAPE( (/1,4,6,4,2,5,6,5,3/),(/3,3/)) !tri B2
    INTEGER, PARAMETER                :: ntri2=3                                        !tri B2
    INTEGER, PARAMETER, DIMENSION(3,6):: nuloc3=RESHAPE( (/1,4,9,8,7,3,5,2,6,4,5,10,10,6,3,9,10,8/),(/3,6/) )
    INTEGER, PARAMETER                :: ntri3=6                                   !tri B3
    LOGICAL:: LL

    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp

    TYPE(Pvar):: phi

    TYPE(PVar) ::  ubar, vbar
    REAL(DP):: alpha
    INTEGER:: l,l_tri, is(3), ntri, indic
    INTEGER, DIMENSION(:,:),ALLOCATABLE:: nuloc
    REAL(dp), DIMENSION(:), ALLOCATABLE:: coeff
    LL=.FALSE.

    SELECT CASE(e%nsommets)
    CASE(3) ! P1
       ntri=ntri1
       ALLOCATE(nuloc(3,1),coeff(1))
       nuloc=nuloc1
       coeff=0.5_dp !e%n is twice \int\nabla \lambda
    CASE(6)
       ntri=ntri2
       ALLOCATE(nuloc(3,ntri),coeff(ntri))
       nuloc=nuloc2
       coeff=1._dp/3._dp
    CASE(10)
       ntri=ntri3
       ALLOCATE(nuloc(3,ntri),coeff(ntri))
       nuloc=nuloc3
       coeff(1:3)=0.25_dp
       coeff(4:6)=0.125_dp
    CASE default
       PRINT*, mod_name, "error elements"
       STOP
    END SELECT



    res=(e%volume/REAL(e%nsommets,dp))*du 

    DO l_tri=1,ntri
       phi=0._dp
       DO l=1,e%nvertex
          phi=phi+SUM(flux(:,nuloc(l,l_tri))*e%n(:,l))
       ENDDO
       ubar=SUM(u(nuloc(:,l_tri)))/REAL(e%nvertex,dp)
       alpha=0._dp
       DO l=1,e%nvertex
          alpha=MAX(alpha,u(nuloc(l,l_tri))%spectral_radius(xx,e%n(:,l)))
       ENDDO
       !       alpha=alpha*0.5_dp  !e%n is twice \int\nabla \lambda
       res(nuloc(:,l_tri))=res(nuloc(:,l_tri))+ &
            & coeff(l_tri)*dt*( phi/REAL(e%nvertex,dp)+alpha*(u(nuloc(:,l_tri))-ubar) )
    ENDDO
    IF (dt.NE.dt) THEN
       LL=.TRUE.
       indic=1
    ENDIF
    DO l=1, n_vars
       IF (ubar%u(l).NE.ubar%u(l)) THEN
          LL=.TRUE.
          indic=3
       ENDIF
    ENDDO
    IF (alpha.NE.alpha) THEN
       LL=.TRUE.
       indic=4
    ENDIF
    IF (phi%u(l).NE.phi%u(l)) THEN
       LL=.TRUE.
       indic=2
    ENDIF


    IF (LL) THEN
       PRINT*, mod_name, " NaN", indic
       DO l=1, u(1)%nvars
          PRINT*, u(l)%u
       ENDDO
       STOP
    ENDIF

    DEALLOCATE(nuloc,coeff)

  END FUNCTION lxf_P1_ref

  FUNCTION lxf_P1( e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF_P1"
    ! this version use the fact that the total residual, in the quadrature free approach, is a weighted
    ! sum of P1 resudials
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    INTEGER, PARAMETER, DIMENSION(3,1):: nuloc1=RESHAPE((/1,2,3/),(/3,1/)) ! Tri, B1
    INTEGER, PARAMETER                :: ntri1=1                           ! Tri, B1
    INTEGER, PARAMETER, DIMENSION(3,4):: nuloc2=RESHAPE( (/1,4,6,4,2,5,6,5,3,5,6,4/),(/3,4/)) !tri B2
    INTEGER, PARAMETER                :: ntri2=4                                       !tri B2
    INTEGER, PARAMETER, DIMENSION(3,6):: nuloc3=RESHAPE( (/1,4,9,8,7,3,5,2,6,4,5,10,10,6,3,9,10,8/),(/3,6/) )
    INTEGER, PARAMETER                :: ntri3=6                                   !tri B3
    LOGICAL:: LL

    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp

    TYPE(Pvar):: phi
    TYPE(Pvar),DIMENSION(e%nsommets):: v

    TYPE(PVar) ::  ubar, vbar
    REAL(DP):: alpha
    INTEGER:: l,l_tri, is(3), ntri, indic
    INTEGER, DIMENSION(:,:),ALLOCATABLE:: nuloc
    REAL(dp), DIMENSION(:), ALLOCATABLE:: coeff
    LL=.FALSE.



    SELECT CASE(e%nsommets)
    CASE(3) ! P1
       ntri=ntri1
       ALLOCATE(nuloc(3,1),coeff(1))
       nuloc=nuloc1
       coeff=0.5_dp !e%n is twice \int\nabla \lambda
    CASE(6)
       ntri=ntri2
       ALLOCATE(nuloc(3,ntri),coeff(ntri))
       nuloc=nuloc2
       coeff(1:3)=1._dp/6._dp; coeff(4)=-1._dp/6._dp
    CASE(10)
       ntri=ntri3
       ALLOCATE(nuloc(3,ntri),coeff(ntri))
       nuloc=nuloc3
       coeff(1:3)=0.25_dp
       coeff(4:6)=0.125_dp
    CASE default
       PRINT*, mod_name, "error elements"
       STOP
    END SELECT




    res=(e%volume/REAL(e%nsommets,dp))* du 
    v=Control_to_Cons(u,e)
    DO l_tri=1,ntri
       phi=0._dp
       DO l=1,e%nvertex
          phi=phi+SUM(flux(:,nuloc(l,l_tri))*e%n(:,l))
       ENDDO
       ubar=SUM(u(nuloc(:,l_tri)))/REAL(e%nvertex,dp)  ! u, v !?
       alpha=0._dp
       DO l=1,e%nvertex
          alpha=MAX(alpha,v(nuloc(l,l_tri))%spectral_radius(xx,e%n(:,l)))
       ENDDO
       !       alpha=alpha*0.5_dp  !e%n is twice \int\nabla \lambda
       res(nuloc(:,l_tri))=res(nuloc(:,l_tri))+ &
            & coeff(l_tri)*dt* phi/REAL(e%nvertex,dp)+ABS(coeff(l_tri))*dt*alpha*(u(nuloc(:,l_tri))-ubar) ! u, v !?
    ENDDO
    IF (dt.NE.dt) THEN
       LL=.TRUE.
       indic=1
    ENDIF
    DO l=1, n_vars
       IF (ubar%u(l).NE.ubar%u(l)) THEN
          LL=.TRUE.
          indic=3
       ENDIF
    ENDDO
    IF (alpha.NE.alpha) THEN
       LL=.TRUE.
       indic=4
    ENDIF
    IF (phi%u(l).NE.phi%u(l)) THEN
       LL=.TRUE.
       indic=2
    ENDIF


    IF (LL) THEN
       PRINT*, mod_name, " NaN", indic
       PRINT*, 'u'
       DO l=1, u(1)%nvars
          PRINT*, u(l)%u
       ENDDO
       PRINT*, 'v'
       DO l=1, v(1)%nvars
          PRINT*, v(l)%u
       ENDDO

       STOP
    ENDIF

    DEALLOCATE(nuloc,coeff)

  END FUNCTION lxf_P1


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ LIMITING ---------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------

  SUBROUTINE lim_psi_jump_var(u,phi,res) 
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "lim_psi_jump_var"
    ! blendig between limited  and LxF in case of NAN
    ! used for Jump
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(in):: phi
    TYPE(Pvar), DIMENSION(:), INTENT(INout):: res
    REAL(DP)                                :: Phi_tot
    REAL(DP), DIMENSION(SIZE(res)):: x
    REAL(DP):: den, l, truc
    INTEGER :: k,i

    l=100._dp

    DO k=1,n_vars

       phi_tot=SUM(phi(:)%u(k))

       IF (ABS (phi_tot)>tiny(1.0)) THEN
          x=MAX(phi(:)%u(k)/phi_tot,0._dp)
          den=SUM(x)+TINY(1.0_dp)
          truc= SUM(ABS(phi%u(k))) + TINY(1.0_dp)
          l=ABS(phi_tot)/truc
          res(:)%u(k)=(1._dp-l)*phi_tot*x/den+l*phi(:)%u(k)
       ELSE
          res(:)%u(k)=0._dp
       END IF

    END DO

    !    DO i = 1,SIZE(phi)
    !       res(i)%u= (1.0-l)*res(i)%u + l*phi(i)%u
    !    END DO

  END SUBROUTINE lim_psi_jump_var

  SUBROUTINE lim_psi_jump_char(u,phi,res,out)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "lim_psi_jump_char"
    !  blendig between limited  and LxF
    ! used for Jump
    !. R. Abgrall 10/1/16
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u
    TYPE(Pvar), DIMENSION(:), INTENT(in):: phi
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    LOGICAL, INTENT(out):: out

    REAL(DP)                                :: Phi_tot
    TYPE(Pvar), DIMENSION(SIZE(phi))    :: phi_ch
    REAL(DP), DIMENSION(SIZE(phi))          :: x
    REAL(DP)    :: den, l, truc
    INTEGER :: k, i
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: EigR, EigL
    REAL(DP), DIMENSION(n_dim)         :: v_nn
    TYPE(Pvar) :: ubar
    out=.FALSE.
    res = 0.0_dp

    DO k = 1,n_vars
       ubar%u(k)= SUM(u%u(k))/REAL(SIZE(u),DP)
    END DO

    v_nn = ubar%u(2:3)/ubar%u(1)
    IF ( SUM(v_nn**2) < 1.e-16_dp ) v_nn = (/ 0.5_dp, 0.5_dp /)
    EigR = ubar%rvectors(v_nn)
    IF (SUM(ABS(EigR)).NE.SUM(ABS(Eigr)) ) THEN
       out=.TRUE.

       RETURN
    ENDIF
    EigL = ubar%lvectors(v_nn)

    DO i = 1,SIZE(phi)
       phi_ch(i)%u = MATMUL(EigL,phi(i)%u)
    END DO


    DO k=1,n_vars

       phi_tot=SUM(phi_ch(:)%u(k))

       IF (ABS (phi_tot)>tiny(1.0_dp)) THEN
          x=MAX(phi_ch(:)%u(k)/phi_tot,0._dp)
          den=SUM(x)+TINY(1.0_dp)
          truc= SUM(ABS(phi_ch%u(k)))+TINY(1.0_dp)
          l=ABS(phi_tot)/truc
          res(:)%u(k)=(1._dp-l)*phi_tot*x/den+l*phi_ch(:)%u(k)
       ELSE
          res(:)%u(k)=0.0_dp
       ENDIF
    END DO  ! k

    DO i = 1,SIZE(phi)
       res(i)%u =  MATMUL(EigR,res(i)%u)
    END DO

  END SUBROUTINE lim_psi_jump_char

  SUBROUTINE lim(l,alpha,ubar, u,phi, LxF, res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "routine lim"
    !  blendig between limited  and LxF
    ! used for Jump
    !. R. Abgrall 10/1/16
    REAL(dp), INTENT(in):: alpha
    TYPE(Pvar), INTENT(in) :: ubar
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u, phi, LxF
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    REAL(dp), INTENT(out):: l

    REAL(DP)                                :: Phi_tot
    TYPE(Pvar), DIMENSION(SIZE(phi))    :: phi_ch
    REAL(dp), DIMENSION(n_Vars,n_vars):: beta
    REAL(dp), DIMENSION(n_Vars,n_vars):: gama
    INTEGER :: k, i
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: EigR, EigL
    REAL(DP), DIMENSION(n_dim)         :: v_nn

    res = 0.0_dp
    gama=0._dp

    v_nn = ubar%u(2:3)/ubar%u(1)
    IF ( SUM(v_nn**2) < 1.e-16_dp ) v_nn = (/ 0.5_dp, 0.5_dp /)
    EigR = ubar%rvectors(v_nn)
    IF (SUM(ABS(EigR)).NE.SUM(ABS(Eigr)) ) THEN
       CALL lim_var(l,alpha,ubar, u,phi, LxF, res)
       !res=LxF
       RETURN
    ENDIF
    EigL = ubar%lvectors(v_nn)

    DO i = 1,SIZE(phi)
       phi_ch(i)%u = MATMUL(EigL,LxF(i)%u)
    END DO


    DO k=1,n_vars
       phi_tot=SUM(phi_ch(:)%u(k))
       IF (ABS(phi_tot)>0.) THEN
          gama(k,k)=ABS(phi_tot)/(SUM(ABS(phi_ch(:)%u(k)))+TINY(1.0_dp) )
       ELSE
          gama(k,k)=0._dp
       ENDIF
    ENDDO

    l=1._dp!-maxval(gama)

    beta= MATMUL(EigR,MATMUL(gama,EigL))
    DO i = 1,SIZE(phi)
       res(i)%u = phi(i)%u+ MATMUL(beta,( u(i)%u-ubar%u)) *alpha
    END DO
  CONTAINS
    REAL(dp) FUNCTION ff(a,b)
      IMPLICIT NONE
      !      real(dp), parameter:: alpha=3._dp,theta=3._dp
      REAL(dp), PARAMETER:: alpha=1._dp,theta=10._dp
      REAL(dp), DIMENSION(:), INTENT(in):: a
      REAL(dp), INTENT(in):: b
      REAL(dp), DIMENSION(SIZE(a)):: c
      c=0.5_dp*( TANH(alpha*(ABS(a/b)-theta))+1._dp)
      !      c=0.5_dp*( tanh( a/b+10)-1)
      ff=0._dp!MAXVAL(c)
    END FUNCTION ff
  END SUBROUTINE lim


  SUBROUTINE lim_var(l,alpha,ubar, u,phi, LxF, res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "routine lim_var"
    !  blendig between limited  and LxF
    ! used for Jump
    !. R. Abgrall 10/1/16
    REAL(dp), INTENT(in):: alpha
    TYPE(Pvar), INTENT(in) :: ubar
    TYPE(Pvar), DIMENSION(:), INTENT(in):: u, phi, LxF
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    REAL(dp), INTENT(out):: l

    REAL(DP)                                :: Phi_tot
    REAL(dp), DIMENSION(n_Vars):: beta
    INTEGER :: k, i

    res = 0.0_dp

    DO k=1,n_vars
       phi_tot=SUM(phi(:)%u(k))
       IF (ABS(phi_tot)>0._dp) THEN
          beta(k)=ABS(phi_tot)/(SUM(ABS(phi(:)%u(k)))+TINY(1.0_dp) )*alpha
       ELSE
          beta(k)=0._dp
       ENDIF
    ENDDO

    l=1._dp!-maxval(beta)


    DO i = 1,SIZE(phi)
       res(i)%u = phi(i)%u+beta*( u(i)%u-ubar%u)
    END DO
  END SUBROUTINE lim_var


  !----------------------------------------------------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
  !------------------------ STABILIZATION ------------------------------------------------------------------
  !----------------------------------------------------------------------------------------------------------------
!!!!!!!!!! Jump

  SUBROUTINE jump_burman(  ed, e1, e2, u1, u2, resJ1, ResJ2, theta, theta2)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Jump_burman"
    REAL(DP), PARAMETER :: pi=ACOS(-1._dp)
    !
    ! barycentric coordinates of vertices (tri) or coordinates used for the evaluation of
    ! basis functions & gradients (quad). In reference element.
    !
    REAL(DP), DIMENSION(3,3), PARAMETER:: a3=RESHAPE( (/1._dp,0._dp,0._dp,0._dp,1._dp,&
         & 0._dp,0._dp,0._dp,1._dp/), (/3,3/))
    REAL(dp), DIMENSION(2,4), PARAMETER:: a4=RESHAPE( (/0._dp,0._dp,1._dp,0._dp,1._dp,1._dp,0._dp,1._dp/),(/2,4/))
    ! burman operator
    REAL(DP), INTENT(IN):: theta, theta2
    !
    TYPE(element), INTENT(in):: e1, e2
    TYPE(arete), INTENT(in):: ed

    TYPE(PVar), DIMENSION(:), INTENT(in):: u1, u2
    TYPE(PVar), DIMENSION(:), INTENT(out):: resJ1, ResJ2
    REAL(DP), DIMENSION(e1%nvertex):: x1
    REAL(DP), DIMENSION(e2%nvertex):: x2

    TYPE(gradient), DIMENSION(2):: grad
    TYPE(var_edge), DIMENSION(2):: va

    REAL(DP), DIMENSION(n_dim,N_vars):: gr
    REAL(DP), DIMENSION(2):: xx=0._dp
    INTEGER:: i,j,l, k, n1, n2
    REAL(DP):: diff1, diff2, beta
    REAL(DP), DIMENSION(2)::  n
    TYPE(PVar) ::  ubar1, ubar2
    REAL(DP)::  alpha, dgr, h

    ALLOCATE(grad(1)%grad(2,e1%nsommets), grad(2)%grad(2,e2%nsommets))
    ALLOCATE(va(1)%var(e1%nsommets), va(2)%var(e2%nsommets) )
    alpha=0._dp

    DO l=1, n_vars
       resJ1(:)%u(l)=0._dp
       resJ2(:)%u(l)=0._dp
    ENDDO

    h=ed%volume
    beta=0._dp

    !    print*, mod_name, size(ed%nu,dim=2),e1%nvertex,e2%nvertex
    !    print*, ed%nu(:,1)
    !    print*, ed%nu(:,2)
    !    print*

    va(1)%var=u1
    va(2)%var=u2

    DO l=1, N_vars
       ubar1%u(l)= SUM(u1%u(l))/REAL(e1%nsommets,dp)
       ubar2%u(l)= SUM(u2%u(l))/REAL(e2%nsommets,dp)
    ENDDO
    !    xx(1)=0.5*(SUM(e1%coor(1,:))/REAL(e1%nsommets,dp)+SUM(e2%coor(1,:))/REAL(e2%nsommets,dp))
    !    xx(2)=0.5*(SUM(e1%coor(2,:))/REAL(e1%nsommets,dp)+SUM(e2%coor(2,:))/REAL(e2%nsommets,dp))

    n=ed%n/ed%volume

    alpha=MAX( ubar1%spectral_radius(xx, n), ubar2%spectral_radius(xx, n) ) 



    DO i=1,ed%nquad
       ! coordonnees barycentriques sur les faces
       ! these two sets must correspond to the SAME geometrical point, but seen from
       ! different elements. For now it is dealt like this, maybe it is simpler to compute
       ! the geometrical point and then to evaluate the barycentric ccordinate. The
       ! jonglerie with indices is done in GeoGraph.f90
       ! This is seen in the reference element.

       SELECT CASE(e1%nvertex)
       CASE(3) ! triangle
          x1(:)=ed%quad(1,i)*a3(:,ed%nu(1,1))+ed%quad(2,i)*a3(:,ed%nu(2,1))
          n1=3
       CASE(4) ! quadrangle
          x1(1:2)=ed%quad(1,i)*a4(1:2,ed%nu(1,1))+ed%quad(2,i)*a4(1:2,ed%nu(2,1))
          n1=2
       CASE default
          PRINT*, mod_name
          PRINT*, 'wrong case e1 '
       END SELECT
       SELECT CASE(e2%nvertex)
       CASE(3) ! triangle
          x2(:)=ed%quad(1,i)*a3(:,ed%nu(1,2))+ed%quad(2,i)*a3(:,ed%nu(2,2))
          n2=3
       CASE(4) ! quadrangle
          x2(1:2)=ed%quad(1,i)*a4(1:2,ed%nu(1,2))+ed%quad(2,i)*a4(1:2,ed%nu(2,2))
          n2=3
       CASE default
          PRINT*, mod_name
          PRINT*, 'wrong case e2 '
       END SELECT

       ! on a les points de quadrature in the barycentric coordinates of the 2 elements sharin ed

       DO l=1, e1%nsommets
          grad(1)%grad(:,l)=e1%gradient(l, x1(1:n1) )
       ENDDO


       DO l=1, e2%nsommets
          grad(2)%grad(:,l)=e2%gradient(l, x2(1:n2) )
       ENDDO


       DO l=1, n_vars
          gr(1,l)=SUM( ( va(1)%var(:)%u(l)-va(1)%var(1)%u(l) ) *grad(1)%grad(1,:) )-&
               &  SUM( ( va(2)%var(:)%u(l)-va(2)%var(1)%u(l) ) *grad(2)%grad(1,:) )
          gr(2,l)=SUM( ( va(1)%var(:)%u(l)-va(1)%var(1)%u(l) ) *grad(1)%grad(2,:) )-&
               &  SUM( ( va(2)%var(:)%u(l)-va(2)%var(1)%u(l) ) *grad(2)%grad(2,:) )
       ENDDO



       DO l=1,e1%nsommets
          !                    dgr= grad(1)%grad(1,l)*n(1)+grad(1)%grad(2,l)*n(2)
          DO k=1, n_vars
             diff1=SUM(gr(:,k)*grad(1)%grad(:,l))
             !                          diff1         =  ( gr(1,k)*n(1)+gr(2,k)*n(2) ) * dgr
             resJ1(l)%u(k) =  resJ1(l)%u(k)+ diff1 * ed%weight (i) 
          ENDDO
       ENDDO

       DO l=1,e2%nsommets
          !                   dgr= grad(2)%grad(1,l)*n(1)+grad(2)%grad(2,l)*n(2) 
          DO k=1, n_vars
             diff2=SUM(gr(:,k)*grad(2)%grad(:,l))
             !                         diff2         = ( gr(1,k)*n(1)+gr(2,k)*n(2) ) * dgr
             resJ2(l)%u(k) = resJ2(l)%u(k)  - diff2 * ed%weight (i) 
          ENDDO
       ENDDO
    ENDDO

    ! homogeneous to speed * h**2  *h (last h for integration formula)
    beta =  theta   * ed%jump_flag * alpha * h* h* ed%volume



    resJ1(:)= ResJ1(:)* beta !* e1%l
    resJ2(:)= ResJ2(:)* beta !* e2%l

    DEALLOCATE(grad(1)%grad, grad(2)%grad,va(1)%var,va(2)%var)

  END SUBROUTINE jump_burman



  SUBROUTINE jump_burman_scaled(  ed, e1, e2, u1, u2, resJ1, ResJ2, theta, theta2)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Jump_burman_scaled"
    ! burman operator
    REAL(DP), INTENT(IN):: theta, theta2
    REAL(DP), PARAMETER :: pi=ACOS(-1._dp)
    !
    ! barycentric coordinates of vertices
    !
    REAL(DP), DIMENSION(3,3), PARAMETER:: a3=RESHAPE( (/1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0/), (/3,3/))
    REAL(DP), DIMENSION(4,4), PARAMETER:: a4=RESHAPE( (/1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,1.0/), (/4,4/))
    !
    TYPE(element), INTENT(in):: e1, e2
    TYPE(arete), INTENT(in):: ed

    TYPE(PVar), DIMENSION(:), INTENT(in):: u1, u2
    TYPE(PVar), DIMENSION(:), INTENT(out):: resJ1, ResJ2

    REAL(DP), DIMENSION(e1%nvertex):: x1
    REAL(DP), DIMENSION(e2%nvertex):: x2


    TYPE(gradient), DIMENSION(2):: grad
    TYPE(var_edge), DIMENSION(2):: va, vac, so, soeq, soeqc
    TYPE(var_edge),DIMENSION(2,2):: flux,fluxc, fluxeq, fluxeqc
    TYPE(Pvar),DIMENSION(2):: fl
    TYPE(PVar),DIMENSION(e1%nsommets):: u
    TYPE(Pvar):: u_loc
    REAL(DP), DIMENSION(n_dim,N_vars,2):: gr
    REAL(DP), DIMENSION(n_dim,n_dim, N_vars,2):: gr2
    REAL(dp), DIMENSION(2,N_vars):: div
    REAL(DP), DIMENSION(2):: xx
    INTEGER:: i,j,l, k
    REAL(DP):: diff1, diff2, diff1H,diff2H, h, beta
    REAL(DP), DIMENSION(2)::  n
    REAL(DP):: beta1, beta2
    TYPE(PVar) :: uloc, ubar1, ubar2
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    REAL(DP):: grr,grl
    TYPE(Pvar), DIMENSION(e1%nsommets):: v1,w1,z1, prim1
    TYPE(Pvar), DIMENSION(e2%nsommets)::  v2,w2,z2, prim2

    ALLOCATE(grad(1)%grad(2,e1%nsommets), grad(2)%grad(2,e2%nsommets))
    ALLOCATE(va(1)%var(e1%nsommets) , va(2)%var(e2%nsommets) )
    ALLOCATE(vac(1)%var(e1%nsommets), vac(2)%var(e2%nsommets) )

    ALLOCATE(flux(1,1)%var(e1%nsommets),flux(1,2)%var(e1%nsommets))
    ALLOCATE(flux(2,1)%var(e2%nsommets),flux(2,2)%var(e2%nsommets))
    ALLOCATE(fluxc(1,1)%var(e1%nsommets),fluxc(1,2)%var(e1%nsommets))
    ALLOCATE(fluxc(2,1)%var(e2%nsommets),fluxc(2,2)%var(e2%nsommets)) 

    grr=0._dp; grl=0._dp

    DO l=1, n_vars
       resJ1(:)%u(l)=0.
       resJ2(:)%u(l)=0.
    ENDDO
    w1=Control_to_cons(u1,e1)
    w2=Control_to_cons(u2,e2)
    DO l=1, e1%nsommets
       prim1(l)=w1(l)%cons2prim()
    ENDDO
    DO l=1, e2%nsommets
       prim2(l)=w2(l)%cons2prim()
    ENDDO
    DO l=1, e1%nsommets
       fl(:)=w1(l)%flux()
       DO k=1, 2
          fluxc(1,k)%var(l)=fl(k)
       ENDDO
    ENDDO

    DO l=1, e2%nsommets
       fl(:)=w2(l)%flux()
       DO k=1, 2
          fluxc(2,k)%var(l)=fl(k)
       ENDDO

    ENDDO

    flux(1,1)%var=cons_to_control(fluxc(1,1)%var,e1)
    flux(1,2)%var=cons_to_control(fluxc(1,2)%var,e1)
    flux(2,1)%var=cons_to_control(fluxc(2,1)%var,e2)
    flux(2,2)%var=cons_to_control(fluxc(2,2)%var,e2)


    h=ed%volume
    beta=0.

    beta1 = 0.
    beta2 = 0.

#if (1==0)
    w1=Control_to_cons(u1,e1)
    DO l=1, e1%nsommets
       z1(l)%u=w1(l)%entropy_var()
    ENDDO
    v1=Control_to_Cons(z1,e1)
    w2=Control_to_cons(u2,e2)
    DO l=1, e2%nsommets
       z2(l)%u=w2(l)%entropy_var()
    ENDDO
    v2=Control_to_Cons(z2,e2)
#else
    va(1)%var=u1
    va(2)%var=u2
    w1=Control_to_Cons(u1,e1)
    w2=Control_to_Cons(u2,e2)
#endif
    DO l=1, N_vars
       ubar1%u(l)= SUM(u1%u(l))/REAL(e1%nsommets,dp)
       ubar2%u(l)= SUM(u2%u(l))/REAL(e2%nsommets,dp)
    ENDDO
    xx(1)=0.5*(SUM(e1%coor(1,:))+SUM(e2%coor(1,:)))/REAL(e1%nsommets,dp)
    xx(2)=0.5*(SUM(e1%coor(2,:))+SUM(e2%coor(2,:)))/REAL(e1%nsommets,dp)

    n=ed%n!/ed%volume


    DO i=1,ed%nquad
       ! coordonnees barycentriques sur les faces
       ! these two sets must correspond to the SAME geometrical point, but seen from
       !different elements. For now it is dealt like this, maybe it is simpler to compute
       ! the geometrical point and then to evaluate the barycentric ccordinate. The
       ! jonglerie with indices is done in GeoGraph.f90

       SELECT CASE(e1%nvertex)
       CASE(3) ! triangle
          x1(:)=ed%quad(1,i)*a3(:,ed%nu(1,1))+ed%quad(2,i)*a3(:,ed%nu(2,1))
       CASE(4) ! quadrangle
          x1(:)=ed%quad(1,i)*a4(:,ed%nu(1,1))+ed%quad(2,i)*a4(:,ed%nu(2,1))
       CASE default
          PRINT*, mod_name
          PRINT*, 'wrong case e1 '
       END SELECT
       SELECT CASE(e2%nvertex)
       CASE(3) ! triangle
          x2(:)=ed%quad(1,i)*a3(:,ed%nu(1,2))+ed%quad(2,i)*a3(:,ed%nu(2,2))
       CASE(4) ! quadrangle
          x2(:)=ed%quad(1,i)*a4(:,ed%nu(1,2))+ed%quad(2,i)*a4(:,ed%nu(2,2))
       CASE default
          PRINT*, mod_name
          PRINT*, 'wrong case e2 '
       END SELECT

       ! on a les points de quadrature in the barycentric coordinates of the 2 elements sharin ed
       DO j=1,2 ! we compute grad u and the grad phi at the quadrature points for each element sharing the edge ed
          IF (j==1) THEN
             DO l=1, e1%nsommets
                grad(j)%grad(:,l)=e1%gradient(l, x1(:) )
             ENDDO
          ELSE
             DO l=1, e2%nsommets
                grad(j)%grad(:,l)=e2%gradient(l, x2(:) )
             ENDDO
          ENDIF

          DO l=1, n_vars
             gr(1,l,j)=SUM( (va(j)%var(:)%u(l)-va(j)%var(1)%u(l)) *grad(j)%grad(1,:) )
             gr(2,l,j)=SUM( (va(j)%var(:)%u(l)-va(j)%var(1)%u(l)) *grad(j)%grad(2,:) )
             div(j,l)=SUM( ( flux(j,1)%var(:)%u(l)-flux(j,1)%var(1)%u(l)  )*grad(j)%grad(1,:) )&
                  &  +SUM( ( flux(j,2)%var(:)%u(l)-flux(j,2)%var(1)%u(l)  )*grad(j)%grad(2,:) )
          ENDDO
       ENDDO

       gr(:,:,1)=gr(:,:,1)-gr(:,:,2) ! jump of gradientns
       div(1,:) = div(1,:)-div(2,:)  ! jump of divergence
       DO k=1, n_vars
          DO l=1,e1%nsommets
             diff1          =  div(1,k)*(grad(1)%grad(1,l)*n(1)+grad(1)%grad(2,l)*n(2))
             resJ1(l)%u(k) =  resJ1(l)%u(k)+ diff1 * ed%weight (i) !? + -

          ENDDO

          DO l=1,e2%nsommets
             diff2          = div(1,k) *(grad(2)%grad(1,l)*n(1)+grad(2)%grad(2,l)*n(2))
             resJ2(l)%u(k) = resJ2(l)%u(k)  - diff2 * ed%weight (i) !? + -



          ENDDO
       ENDDO
    ENDDO

    !
    beta =  theta * h*h *  ed%jump_flag 

    resJ1(:)= ResJ1(:)* beta 
    resJ2(:)= ResJ2(:)* beta 

    DEALLOCATE(grad(1)%grad, grad(2)%grad,va(1)%var,va(2)%var)
    DEALLOCATE(flux(1,1)%var,flux(1,2)%var,flux(2,1)%var,flux(2,2)%var)
    DEALLOCATE(fluxc(1,1)%var,fluxc(1,2)%var,fluxc(2,1)%var,fluxc(2,2)%var)

  END SUBROUTINE jump_burman_scaled

  FUNCTION stream_new(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *)        , PARAMETER :: mod_name = "stream_new"
    REAL(dp), DIMENSION(n_dim), PARAMETER :: xx=0._dp, n=(/1._dp,0._dp/)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e Jac*graphi * tau * ( du + dt* div flux) dx
    !

    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res, s


    REAL(dp), DIMENSION(n_vars,n_vars) :: nmat
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, divflux_loc, u_loc, gru_loc(n_dim), s_loc

    INTEGER:: i, iq, l, k, ll



    res=0._dp

    u_loc=0._dp

    DO l=1, e%nvertex
       u_loc=u_loc+u(l)%cons2prim()
    ENDDO

    u_loc=u_loc/REAL(e%nvertex,dp)

    u_loc=u_loc%prim2cons()

    Nmat=u_loc%Nmat(e%nvertex, e%n ,xx, dt/e%volume)*e%volume

    DO i=1, e%nsommets
       DO iq=1, e%nquad

          u_loc = SUM(e%base_at_quad(:, iq)*u )
          du_loc= SUM(e%base_at_quad(:, iq)*du)
          gru_loc(1)=SUM( e%grad_at_quad(1,:,iq)*( u(:)-u(1)) )
          gru_loc(2)=SUM( e%grad_at_quad(2,:,iq)*( u(:)-u(1)) )
          Jac=u_loc%Jacobian(xx)

          divflux_loc=0._dp
          DO l=1, n_dim
             divflux_loc%u=divflux_loc%u+ MATMUL(Jac(:,:,l), gru_loc(l)%u )
          ENDDO


          divflux_loc%u=MATMUL(nmat, du_loc%u(:)+dt*(   divflux_loc%u(:) ) )


          res(i)%u = res(i)%u + &
               & MATMUL( Jac(:,:,1)*e%grad_at_quad(1,i,iq)+Jac(:,:,2)*e%grad_at_quad(2,i,iq) ,&
               & divflux_loc%u  )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i
    DO i=1, e%nsommets
       DO l=1, n_vars
          IF (res(i)%u(l).NE.res(i)%u(l)) THEN
             res=0._dp
             EXIT
          ENDIF
       ENDDO
    ENDDO



  END FUNCTION stream_new



  !************* Old routines ***** Junk

  FUNCTION galerkin_old(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "galerkin"
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e varphi_i *du - dt* \int_e nabla varphi_i . flux
    !
    ! A boundary term is added to match the boundary conditions
    TYPE(element),                           INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in)    :: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim):: vit, n
    !    TYPE(Pvar),DIMENSION(n_dim):: div_flux

    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, flux_loc(n_dim), u_loc
    TYPE(Pvar), DIMENSION(e%nsommets)::s

    INTEGER:: i, iq, l, k, ll
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii
    REAL(dp), DIMENSION(n_dim):: xx=0._dp

    DO l=1, e%nsommets
       res(l)%u=0._dp
    ENDDO



    DO i=1, e%nsommets


       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO


          u_loc=SUM(base*u)
          du_loc=SUM(base*du)


#if (1==1)
          flux_loc=u_loc%flux()!xx)
#else
          flux_loc(1)=SUM(base*flux(1,:))
          flux_loc(2)=SUM(base*flux(2,:))
#endif
          ! 

          res(i) = res(i) +(base(i)* (du_loc)-&
               &dt*(Grad(1,i)*flux_loc(1)+Grad(2,i)*flux_loc(2)) )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i

    ! integral on the boundary
    DO k=1,e%nvertex ! here a triangle

       n=-e%n(:,k)

       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO

          DO ll=1, n_vars
             u_loc%u(ll)=SUM(base*u%u(ll))
          ENDDO

          flux_loc=u_loc%flux()
          !          flux_loc(1)=SUM(base*flux(1,:))
          !          flux_loc(2)=SUM(base*flux(2,:))

          DO l=1,e%nsommets
             res(l)=res(l)+dt* e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*base(l)
          ENDDO
       ENDDO !iq
    ENDDO !edge

  END FUNCTION galerkin_old



  FUNCTION lxf_old( e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "LxF"
    ! this version is conservative by taking properly into account the Gauss formula
    ! what ever the quadrature points chosen
    ! the residuals are defined using the galerkin residuals+LxF dissipation.
    ! The first ordre residual satisfies an energy (entropy) inequality
    !
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! psi RDS without filtering. Can be added with jump filtering
    !
    TYPE(element),                     INTENT(in):: e
    TYPE(PVar), DIMENSION(:), INTENT(in):: u
    TYPE(PVar), DIMENSION(:), INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:), INTENT(in):: flux
    REAL(DP)   ,                        INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)            :: res,v

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad
    REAL(DP), DIMENSION(n_dim)::  xx=0._dp, n
    TYPE(Pvar), DIMENSION(e%nsommets):: phi

    TYPE(PVar) :: du_loc, flux_loc(n_dim), u_loc, ubar, vbar, s_loc
    REAL(DP):: alpha,h
    INTEGER:: i, iq, l, k
    REAL(DP), DIMENSION(3):: yy
    INTEGER,PARAMETER, DIMENSION(3):: ip=(/2,3,1/),ip1=(/3,1,2/)
    INTEGER,DIMENSION(3):: ii
    !Complicated stuff to evaluate the dissipation coefficient
    !h=maxval(sqrt(e%n(1,:)**2+e%n(2,:)**2))

    ubar= SUM(u)/REAL(e%nsommets,DP)
    ! it seems to be necessary to go into physical state
    v=Control_to_Cons(u,e)
    vbar=SUM(v)/REAL(e%nsommets,DP)
    !
    alpha=0._dp
    DO l=1, e%nvertex
       alpha=MAX(alpha,vbar%spectral_radius(xx,e%n(:,l)))
    ENDDO
    !alpha=alpha
    !
    res=0._dp
    phi=0._dp

    DO i=1, e%nsommets

       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO


          u_loc=SUM(base*u)
          du_loc=SUM(base*du)
#if (1==0)
          flux_loc=u_loc%flux()
#else
          flux_loc(1)=SUM(base*flux(1,:))
          flux_loc(2)=SUM(base*flux(2,:))
#endif
          phi(i) = phi(i) +(base(i)* du_loc-&
               &dt*(Grad(1,i)*flux_loc(1)+Grad(2,i)*flux_loc(2)) )*e%weight(iq)

       ENDDO ! iq
    ENDDO ! i
    phi=e%volume*phi

    ! integral on the boundary
    DO k=1,e%nvertex

       n=-e%n(:,k)
       DO iq=1,e%nquad_edge
          yy(:)=e%quad_edge(k,:,iq)
          DO l=1,e%nsommets
             base(l)=e%base(l,yy)
          ENDDO
          u_loc=SUM(base*u)
          flux_loc=u_loc%flux()
          !          flux_loc(1)=SUM(base*flux(1,:))
          !          flux_loc(2)=SUM(base*flux(2,:))
          DO l=1,e%nsommets
             phi(l)   = phi(l)   + dt * e%weight_edge(k,iq)*SUM(flux_loc(:)*n(:))*base(l)

          ENDDO
       ENDDO !iq
    ENDDO !edge

    res =   phi+dt*alpha*(u-ubar)

  END FUNCTION lxf_old

  FUNCTION stream_old(e, u, du, flux, dt) RESULT(res)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "stream"
    REAL(dp), DIMENSION(n_dim), PARAMETER:: xx=0._dp, n=(/1._dp,0._dp/)
    ! input:
    ! e: element
    ! du: temporal increment
    ! flux: values of the flux at the dofs
    ! dt: time step
    ! output:
    ! \int_e Jac*graphi * tau * ( du + dt* div flux) dx
    !

    TYPE(element),                  INTENT(in):: e
    TYPE(PVar), DIMENSION(:),       INTENT(in):: u
    TYPE(PVar), DIMENSION(:),       INTENT(in):: du
    TYPE(PVar), DIMENSION(:,:),     INTENT(in):: flux
    REAL(DP)      ,                 INTENT(in):: dt

    TYPE(PVar), DIMENSION(e%nsommets)         :: res

    REAL(DP), DIMENSION(e%nsommets):: base
    REAL(DP), DIMENSION(n_dim,e%nsommets):: grad

    REAL(dp), DIMENSION(n_vars,n_vars) :: nmat
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: Jac
    TYPE(PVar) :: du_loc, divflux_loc, u_loc, gru_loc(2)

    INTEGER:: i, iq, l, k, ll




    res=0._dp


    u_loc=0._dp

    DO l=1, e%nvertex
       u_loc=u_loc+u(l)
    ENDDO

    u_loc=u_loc/REAL(e%nvertex,dp)
    Nmat=u_loc%Nmat(e%nvertex, e%n/e%volume ,xx, dt)

    DO i=1, e%nsommets
       DO iq=1, e%nquad

          DO l=1, e%nsommets
             base(l  )=e%base    (l, e%quad(:,iq))
             grad(:,l)=e%gradient(l, e%quad(:,iq))
          ENDDO



          u_loc=SUM(base*u)
          du_loc=SUM(base*du)
          gru_loc(1)=SUM( grad(1,:)*u(:))
          gru_loc(2)=SUM( grad(2,:)*u(:))
          Jac=u_loc%Jacobian(xx)

          divflux_loc=0._dp
          DO l=1, n_dim
             divflux_loc = divflux_loc + SUM(grad(l,:)*flux(l,:))
          ENDDO


          divflux_loc%u=MATMUL(nmat, du_loc%u(:)+ dt* divflux_loc%u(:) )


          res(i)%u = res(i)%u + MATMUL( Jac(:,:,1)*grad(1,i)+Jac(:,:,2)*grad(2,i) , divflux_loc%u  )*e%weight(iq)

       ENDDO ! iq

       res(i)= res(i)* e%volume
    ENDDO ! i



  END FUNCTION stream_old



END MODULE scheme
