!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE geom
  USE PRECISION
  USE param2d
  USE Boundary
  IMPLICIT NONE
  PRIVATE

  ! GMSH_ELE(i, j): i = #Ndofs, j = # Verts
  INTEGER, DIMENSION(31, 2), PARAMETER :: GMSH_ELE = &
                                !    1  2  3  4  5  6  7  8  9  0  1   2   3    4  5  6  7   8   9   9  
       RESHAPE( (/ 2, 3, 4, 4, 8, 6, 5, 3, 6, 9, 10, 27, 18, 14, 1, 8, 20, 15, 13, 9, 10, 12, 15, 15, 21, 4, 5, 6, 20, 35, 56, &
       2, 3, 4, 4, 8, 6, 5, 2, 3, 4,  4,  8,  6,  5, 1, 4,  8,  6,  5, 3,  3,  3,  3,  3,  3, 2, 2, 2,  4,  4,  4 /), &
       (/31, 2/) )
  INTEGER, DIMENSION(31),PARAMETER::vertex_ele=&
                                !1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       (/2,3,4,4,8,6,5,2,3,4,4,8,6,5,1,4,8,6,5,3,3,3,3,3,3,2,2,2,4,4,4/)

  INTERFACE coord_kinetic
     MODULE PROCEDURE coord_kinetic_new
  END INTERFACE coord_kinetic

  INTERFACE calculateMassMatrLumped
     MODULE PROCEDURE calculateMassMatrLumped_new
  END INTERFACE calculateMassMatrLumped

  INTERFACE voisinage
     MODULE PROCEDURE voisinage_new
  END INTERFACE voisinage

  PUBLIC:: ReadMeshGMSH2, coord_kinetic, calculateMassMatrLumped

CONTAINS



  !=======================================
  SUBROUTINE ReadMeshGMSH2(DATA,Mesh)
    !=======================================

    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "ReadMeshGMSH2"
    TYPE(donnees),INTENT(in):: DATA
    TYPE(maillage),INTENT(inout):: mesh

    !---------------------------------------------
    REAL(DP), DIMENSION(:,:), ALLOCATABLE :: Coords

    TYPE :: ele_str
       INTEGER, DIMENSION(:), ALLOCATABLE :: NU
       INTEGER, DIMENSION(:), ALLOCATABLE :: VV
       INTEGER :: log_type, bc_tag, ele_type
    END TYPE ele_str
    TYPE(ele_str), DIMENSION(:), ALLOCATABLE :: ele

    INTEGER :: N_nodes, N_ele, N_b, i, j, &
         N_E_max, N_B_max, idummy, &
         ele_type, n_tag

    INTEGER, DIMENSION(:), ALLOCATABLE :: v_dummy
    INTEGER, DIMENSION(:), ALLOCATABLE :: per
    INTEGER, DIMENSION(:,:),ALLOCATABLE:: voisin
    CHARACTER(LEN=80), DIMENSION(:), ALLOCATABLE:: PhysName

    REAL(DP) :: ver, dummy

    INTEGER :: UNIT, status, Form, ds, Nphys, Ndim, ii, k, ncount
    INTEGER :: j1, j2, l11,l12, l21, l22, i1, i2, l

    REAL(DP):: moyenne

    LOGICAL  :: exist, period
    !---------------------------------------------

    INQUIRE(FILE = TRIM(ADJUSTL(DATA%maillage))//".msh", EXIST = exist)
    IF( .NOT. exist) THEN
       WRITE(*,*) 'ERROR: file ', TRIM(ADJUSTL(DATA%maillage)), ' not found'
       STOP
    ENDIF

    UNIT = 1

    OPEN(UNIT, FILE = TRIM(ADJUSTL(DATA%maillage))//".msh", IOSTAT = status)
    READ(UNIT, *) !! $MeshFormat !!
    READ(UNIT, *) Ver, Form, ds
    IF( ver /= 2.2_dp ) THEN
       PRINT*, ver
       WRITE(*,*) 'ERROR: unknown GMSH version'
       STOP
    ENDIF
    IF( Form /= 0 ) THEN
       WRITE(*,*) 'ERROR: no ASCII file'
       STOP
    ENDIF
    READ(UNIT, *) !!$EndMeshFormat !!

    READ(UNIT, *) !!$PhysicalNames
    READ(UNIT,*) Nphys
    ALLOCATE (Physname(Nphys))
    DO i=1,Nphys
       READ(UNIT,*) dummy,idummy,PhysName(idummy)
       PRINT*, TRIM(ADJUSTL(physName(idummy)))
    ENDDO

    READ(UNIT,*) !!$EndPhysicalNames

    READ(UNIT,*) !!$Nodes
    READ(UNIT, *) N_Nodes

    ALLOCATE( Coords(3, N_Nodes) )
    DO i = 1, N_nodes
       READ(UNIT, *) dummy, Coords(:, i)
    ENDDO
    READ(UNIT, *) !!$EndNodes !!

    READ(UNIT, *) !!$Elements !!
    READ(UNIT, *) N_ele
    ALLOCATE( ele(N_ele) )

    N_E_max = 0
    N_B_max = 0

    DO i = 1, N_ele

       READ(UNIT, *) idummy, ele_type, n_tag
       ele(i)%ele_type=ele_type

       ALLOCATE( ele(i)%NU(GMSH_ELE(ele_type, 1)) )
       ALLOCATE( ele(i)%VV(GMSH_ELE(ele_type, 2)) )

       !       if (ele_type == 26) then
       !       print*, 'ele_type = ', ele_type
       !       print*, 'GMSH_ELE(ele_type, 1) = ', GMSH_ELE(ele_type,1), ', GMSH_ELE(ele_type, 2) = ', GMSH_ELE(ele_type, 2)
       !       end if

       ALLOCATE( v_dummy(n_tag-1) )

       BACKSPACE(UNIT)

       READ(UNIT, *) idummy, idummy, idummy,  &
            ele(i)%log_type, v_dummy, &
            ele(i)%NU


       ele(i)%VV = ele(i)%NU( 1:GMSH_ELE(ele_type, 2) )

       !       ele(i)%bc_tag = v_dummy(1)

       DEALLOCATE(v_dummy)

    ENDDO

!!$    READ(UNIT, *) !!$EndElements !!
!!$    Mesh%period=DATA%period
!!$    IF (Mesh%period) THEN
!!$       READ(UNIT,*) !! periodi s'il y en a
!!$       ALLOCATE(Mesh%per(N_nodes))
!!$       DO i=1,N_nodes
!!$          Mesh%per(i)=i ! a priori pas de periodicite
!!$       ENDDO
!!$       READ(UNIT,*)  !! 5
!!$       ! d'abord les 4 coins
!!$       READ(UNIT,*) idummy, i,j !
!!$       READ(UNIT,*) 
!!$       READ(UNIT,*) 
!!$       Mesh%per(i)=j
!!$       READ(UNIT,*) idummy, i,j! 2
!!$       READ(UNIT,*) 
!!$       READ(UNIT,*) 
!!$       Mesh%per(i)=j
!!$       READ(UNIT,*) idummy, i,j! 3
!!$       READ(UNIT,*) 
!!$       READ(UNIT,*) 
!!$       Mesh%per(i)=j
!!$
!!$
!!$
!!$
!!$
!!$       READ(UNIT,*)
!!$       READ(UNIT,*) idummy
!!$       DO k=1, idummy
!!$          READ(UNIT,*) i,j
!!$          Mesh%per(i)=j
!!$       ENDDO
!!$
!!$       READ(UNIT,*)
!!$       READ(UNIT,*) idummy
!!$       DO k=1, idummy
!!$          READ(UNIT,*) i,j
!!$          Mesh%per(i)=j
!!$       ENDDO
!!$
!!$    ENDIF
    CLOSE(UNIT)

    Mesh%Ns = N_Nodes
    Mesh%Ndofs=N_nodes

    Ndim=2


    N_b = COUNT(ele%log_type /= 0)  ! extract boundary elements, physical surface must be marked with 0 in .geo file
    PRINT*, 'N_b = ', N_b

    Mesh%Nt = N_ele - N_b
    !    Nullify(Mesh%e)
    ALLOCATE (Mesh%e(Mesh%nt))


    PRINT*, "Gmsh ns,Nt", Mesh%ns, Mesh%nt

    ii=0
    DO i = N_b+1, N_ele
       ii=ii+1

       ALLOCATE(Mesh%e(ii)%nu(SIZE(ele(i)%nu,1)))
       ALLOCATE(Mesh%e(ii)%coor(ndim,SIZE(ele(i)%nu,1)))

       Mesh%e(ii)%itype=DATA%itype
       Mesh%e(ii)%nvertex=vertex_ele(ele(i)%ele_type)
       Mesh%e(ii)%nsommets= SIZE( ele(i)%NU )
       Mesh%e(ii)%Nu = ele(i)%NU
       Mesh%e(ii)%coor(1,:)=coords(1,ele(i)%nu(:) )
       Mesh%e(ii)%coor(2,:)=coords(2,ele(i)%nu(:) )

       ALLOCATE( Mesh%e(ii)%coorL(Mesh%e(ii)%nvertex,SIZE(ele(i)%nu,1)) )
       CALL Mesh%e(ii)%getCoorL()

       ALLOCATE(Mesh%e(ii)%n(2,Mesh%e(ii)%nvertex))


       mesh%e(ii)%volume=Mesh%e(ii)%aire()

       Mesh%e(ii)%n=Mesh%e(ii)%normale()


       CALL mesh%e(ii)%quadrature()

       ALLOCATE(Mesh%e(ii)%base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))
       ALLOCATE(Mesh%e(ii)%inv_base_at_dofs( SIZE(ele(i)%nu), SIZE(ele(i)%nu)))
       ALLOCATE(Mesh%e(ii)%grad_at_dofs(n_dim,SIZE(ele(i)%nu), SIZE(ele(i)%nu)))

       CALL Mesh%e(ii)%base_ref()
       CALL Mesh%e(ii)%grad_ref()

       mesh%e(ii)%flag=.TRUE.

       CALL mesh%e(ii)%eval_coeff()

       ! check
#if(1==0)
       DO l=1, mesh%e(ii)%nsommets
          PRINT*, l,mesh%e(ii)%coor(:,l)
       ENDDO
       PRINT*
       PRINT*,4, (2*mesh%e(ii)%coor(:,1)+mesh%e(ii)%coor(:,2))/3.-mesh%e(ii)%coor(:,4)
       PRINT*,5, (2*mesh%e(ii)%coor(:,2)+mesh%e(ii)%coor(:,1))/3.-mesh%e(ii)%coor(:,5)
       PRINT*,6, (2*mesh%e(ii)%coor(:,2)+mesh%e(ii)%coor(:,3))/3.-mesh%e(ii)%coor(:,6)
       PRINT*,7, (2*mesh%e(ii)%coor(:,3)+mesh%e(ii)%coor(:,2))/3.-mesh%e(ii)%coor(:,7)
       PRINT*,8, (2*mesh%e(ii)%coor(:,3)+mesh%e(ii)%coor(:,1))/3.-mesh%e(ii)%coor(:,8)
       PRINT*, 9,(2*mesh%e(ii)%coor(:,1)+mesh%e(ii)%coor(:,3))/3.-mesh%e(ii)%coor(:,9)
       PRINT*, 10,(mesh%e(ii)%coor(:,1)+mesh%e(ii)%coor(:,2)+mesh%e(ii)%coor(:,3))/3.-mesh%e(ii)%coor(:,10)
       STOP
#endif 
    ENDDO

    Mesh%NsegFr = N_b

    ALLOCATE(Mesh%fr(N_b), Mesh%nsfacfr(2,N_b), Mesh%LogFac(n_b))
    j = 0
    DO i =1, N_b

       CALL boundary_log(ele(i)%log_type, ele(i)%bc_tag, Nphys, PhysName)


       j = j+1
       Mesh%fr(j)%Nsommets= SIZE(ele(i)%NU)
       Mesh%fr(j)%Nvertex= vertex_ele(ele(i)%ele_type)
       ALLOCATE(Mesh%fr(j)%nu( SIZE(ele(i)%NU) ))

       Mesh%fr(j)%nu  = ele(i)%nu
       Mesh%fr(j)%log = ele(i)%bc_tag!  see boundary_log
       Mesh%fr(j)%bc_tag = ele(i)%bc_tag

       Mesh%nsFacFr(:,j)=ele(i)%nu(1:2)! i avant au lieu de j
       Mesh%LogFac(j)=ele(i)%log_type   ! i avant au lieu de j
       ALLOCATE(Mesh%fr(j)%coor(ndim,SIZE(ele(i)%nu,1)))
       mesh%fr(j)%coor(1,:)=coords(1,Mesh%fr(j)%nu(:))
       mesh%fr(j)%coor(2,:)=coords(2,Mesh%fr(j)%nu(:))
       Mesh%fr(j)%itype_b=DATA%itype_b
       ALLOCATE(Mesh%fr(j)%n(2))
       mesh%fr(j)%volume=mesh%fr(j)%aire()
       !      mesh%fr(j)%n=mesh%fr(j)%normale() Revaluated later in Geomgraph to make sure it fits with the
       ! orientation
       CALL mesh%fr(j)%quadrature()

    ENDDO


    ! dans le cas ordre 3 on cherche 
    IF (Mesh%period) THEN
       IF (SIZE(ele(1)%nu).GT.2) THEN !assume carre
          ALLOCATE(voisin(Mesh%ns,2))
          voisin=-1
          DO i=1, N_b
             DO j=1,2
                ii=ele(i)%nu(j)
                IF (Voisin(ii,1)==-1) THEN
                   Voisin(ii,1)=i
                ELSE IF (Voisin(ii,2)==-1) THEN
                   Voisin(ii,2)=i
                ELSE
                   PRINT*, 'erreur voisins L 700 ', i
                ENDIF
             ENDDO
          ENDDO

          DO i=1,N_b
             i1=ele(i)%nu(1)
             i2=ele(i)%nu(2)
             j1=Mesh%per(i1)
             j2=Mesh%per(i2)

             IF ((i1==j1.AND.i2==j2).OR.(i1==j2.AND.i2==j1)) THEN
                Mesh%per(ele(i)%nu(3))=ele(i)%nu(3)
                EXIT ! on est sur un cote qui se mape lui meme
             ENDIF

             l11=voisin(j1,1)
             l12=voisin(j1,2)
             l21=voisin(j2,1)
             l22=voisin(j2,2)

             IF (l11==l21) k=ele(l11)%nu(3)
             IF (l12==l21) k=ele(l12)%nu(3)
             IF (l11==l22) k=ele(l11)%nu(3)
             IF (l12==l22) k=ele(l12)%nu(3)

             Mesh%per(ele(i)%nu(3)) =k
          ENDDO

          DEALLOCATE(voisin)
       ENDIF
    ENDIF





    DEALLOCATE(ele, coords)




    IF (DATA%mood) CALL voisinage(Mesh)



  END SUBROUTINE ReadMeshGMSH2
  !===========================

  !==========================================================================
  ! Subroutine to calculate the global lumped mass matrix
  ! MassMatrLumped    is the global lumped mass matrix (PETSc)
  ! MatLumped         is the global lumped mass matrix (Fortran)
  ! MatLumpedInv      is its inverse (using Inverse function from algebra.f90)
  SUBROUTINE calculateMassMatrLumped_old(DATA, Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "calculateMassMatrLumped_old"
    TYPE(donnees),            INTENT(in) :: DATA
    TYPE(maillage),           INTENT(inout) :: Mesh
    TYPE(element)              :: e

    INTEGER, ALLOCATABLE       :: iss(:)
    INTEGER:: jt, i



    ALLOCATE(Mesh%aires(1:Mesh%ns))
    Mesh%aires = 0.0_dp

    ! Loop through mesh elements
    DO jt=1, Mesh%nt

       ! get element
       e=Mesh%e(jt)



       ALLOCATE(iss(e%nsommets))

       ! mapping between the local ang global DOFs
       iss(1:e%nsommets)=e%nu(1:e%nsommets)


       ! Loop through the DOFs of the element e
       DO i = 1,e%nsommets

          ! update the entry of the lumped mass matrix (Fortran)
          Mesh%aires(iss(i)) = Mesh%aires(iss(i)) + e%volume/REAL(e%nsommets,dp)

       END DO ! i

       DEALLOCATE(iss)

    END DO ! jt
#ifndef parallel
    Mesh%aires=1.0_dp/Mesh%aires
#endif
  END SUBROUTINE calculateMassMatrLumped_old


  SUBROUTINE calculateMassMatrLumped_new(DATA, Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="calculateMassMatrLumped_new"
    TYPE(donnees),            INTENT(in) :: DATA
    TYPE(maillage),           INTENT(inout) :: Mesh
    TYPE(element)              :: e

    REAL(dp):: vol
    INTEGER:: jt, i



    ALLOCATE(Mesh%aires(1:Mesh%ns))
    Mesh%aires = 0.0_dp

    ! Loop through mesh elements
    DO jt=1, Mesh%nt

       ! get element
       e=Mesh%e(jt)


       SELECT CASE(e%nvertex)
       CASE(3)! tri

          Mesh%aires(e%nu) = Mesh%aires(e%nu) + e%volume/REAL(e%nsommets,dp)
       CASE (4) ! quad

          ! Loop through the DOFs of the element e
          DO i = 1,e%nsommets
             CALL volume(i,e, vol) ! \int_K basis_i dx
             ! update the entry of the lumped mass matrix (Fortran)

             Mesh%aires(e%nu(i)) = Mesh%aires(e%nu(i)) + vol!e%volume/REAL(e%nsommets,dp)

          END DO ! i

       CASE default
          PRINT*, mod_name, " cas non prevu"
          STOP
       END SELECT

    END DO ! jt

#ifndef parallel
    Mesh%aires=1.0_dp/Mesh%aires
#endif
  CONTAINS
    SUBROUTINE volume(i,e,vol)
      IMPLICIT NONE
      TYPE(element), INTENT(in):: e
      INTEGER, INTENT(in):: i
      REAL(dp), INTENT(out):: vol

      INTEGER                  :: iq
      REAL(dp)                 :: det, phi
      REAL(dp), DIMENSION(2)   :: x
      REAL(dp), DIMENSION(2,2) :: jac
      vol=0._dp
      DO iq=1, e%nquad
         x=e%quad(1:2,iq)
         Jac(1,1) = e%coor(1,2) - e%coor(1,1) + x(2)*(e%coor(1,1) - e%coor(1,2) + e%coor(1,3) - e%coor(1,4))
         Jac(1,2) = e%coor(1,4) - e%coor(1,1) + x(1)*(e%coor(1,1) - e%coor(1,2) + e%coor(1,3) - e%coor(1,4))
         Jac(2,1) = e%coor(2,2) - e%coor(2,1) + x(2)*(e%coor(2,1) - e%coor(2,2) + e%coor(2,3) - e%coor(2,4))
         Jac(2,2) = e%coor(2,4) - e%coor(2,1) + x(1)*(e%coor(2,1) - e%coor(2,2) + e%coor(2,3) - e%coor(2,4)) 

         det=ABS( Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1) )
         phi=e%base(i,x)
         vol=vol+phi*det*e%weight(iq)

      ENDDO


    END SUBROUTINE volume
  END SUBROUTINE calculateMassMatrLumped_new

  SUBROUTINE coord_kinetic_old(Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="coord_kinetic_old"
    TYPE(maillage), INTENT(inout):: Mesh
    REAL(dp), DIMENSION(:,:), ALLOCATABLE:: vals
    REAL(dp), DIMENSION(:,:), ALLOCATABLE:: coords
    REAL(dp):: airs
    TYPE(element):: e
    TYPE(frontiere):: efr
    INTEGER:: jt, l
    INTEGER:: ndim=2

    SELECT CASE (Mesh%e(1)%itype)
    CASE(1)
       ALLOCATE( vals(3,3) )
       VALS(1  ,1)=1._qp/6._qp
       VALS(2:3,1)=1._qp/12._qp
       VALS(2  ,2)=VALS(1,1)
       VALS(1  ,2)=VALS(2,1)
       VALS(3  ,2)=VALS(2,1)
       VALS(1:2,3)=VALS(2,1)
       VALS(3  ,3)=VALS(1,1)
    CASE(2)
       ALLOCATE(vals(3,6) )
       VALS(1,1)=1._qp/10._qp
       VALS(2:3,1)=1._qp/30._qp
       VALS(2,2)=VALS(1,1)
       VALS(1,2)=VALS(2,1)
       VALS(3,2)=VALS(2,1)
       VALS(1:2,3)=VALS(2,1)
       VALS(3,3)=VALS(1,1)
       VALS(3,4)=1._qp/30._qp
       VALS(2,5)=VALS(3,4)
       VALS(1,6)=VALS(3,4)
       VALS(1:2,4)=1._qp/15._qp
       VALS(2:3,6)=1._qp/15._qp
       VALS(1,5)=1._qp/15._qp
       VALS(3,5)=1._qp/15._qp
    CASE default
       PRINT*, mod_name
       PRINT*, "Kinetic: this case does not exist"
       PRINT*, "type=", Mesh%e(1)%itype
    END SELECT
    ALLOCATE(coords(ndim, Mesh%ndofs))

    coords=0._dp
    DO jt=1, Mesh%nt
       ALLOCATE(Mesh%e(jt)%yy(ndim,e%nsommets))
       e=Mesh%e(jt)
       DO l=1,e%nsommets
          e%yy(1,l)=SUM(VALS(1:3,l)*e%coor(1,1:3) )
          e%yy(2,l)=SUM(VALS(1:3,l)*e%coor(2,1:3) )
          coords(:,e%nu(l))=coords(:,e%nu(l))+ e%yy(:,l) *e%volume
       ENDDO
    ENDDO

    coords(1,:)=coords(1,:) * mesh%aires
    coords(2,:)=coords(2,:) * mesh%aires

    DO jt=1, Mesh%nt
       ALLOCATE(Mesh%e(jt)%y(ndim,e%nsommets))
       e=Mesh%e(jt)
       DO l=1, e%nsommets
          e%y(:,l)=coords(:,e%nu(l))
       ENDDO
    ENDDO

    ! for the boundary conditions

    DO jt=1, Mesh%Nsegfr
       ALLOCATE(Mesh%fr(jt)%y(ndim, Mesh%fr(jt)%nsommets))
       efr=Mesh%fr(jt)
       DO l=1, efr%nsommets
          efr%y(:,l)=coords(:,efr%nu(l))
       ENDDO
    ENDDO
    DEALLOCATE(coords)

  END SUBROUTINE coord_kinetic_old


  SUBROUTINE coord_kinetic_new(Mesh)
    ! valid for quad & triangles
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER        :: mod_name ="coord_kinetic_new"
    TYPE(maillage), INTENT(inout)        :: Mesh
    REAL(dp), DIMENSION(6,6,6)             :: vals
    REAL(dp), DIMENSION(:,:), ALLOCATABLE:: coords
    REAL(dp)                             :: airs, z(2)
    TYPE(element)                        :: e
    TYPE(frontiere)                      :: efr
    INTEGER                              :: jt, l, n, iq
    INTEGER                              :: ndim=2
    vals=0._dp

    ! e%nsommet=3,triangle
    VALS(1  ,1,3)=1._qp/6._qp
    VALS(2:3,1,3)=1._qp/12._qp
    VALS(2  ,2,3)=VALS(1,1,3)
    VALS(1  ,2,3)=VALS(2,1,3)
    VALS(3  ,2,3)=VALS(2,1,3)
    VALS(1:2,3,3)=VALS(2,1,3)
    VALS(3  ,3,3)=VALS(1,1,3)
    ! e%nsommet=6,triangle
    VALS(1,1,6)=1._qp/10._qp
    VALS(2:3,1,6)=1._qp/30._qp
    VALS(2,2,6)=VALS(1,1,6)
    VALS(1,2,6)=VALS(2,1,6)
    VALS(3,2,6)=VALS(2,1,6)
    VALS(1:2,3,6)=VALS(2,1,6)
    VALS(3,3,6)=VALS(1,1,6)
    VALS(3,4,6)=1._qp/30._qp
    VALS(2,5,6)=VALS(3,4,6)
    VALS(1,6,6)=VALS(3,4,6)
    VALS(1:2,4,6)=1._qp/15._qp
    VALS(2:3,6,6)=1._qp/15._qp
    VALS(1,5,6)=1._qp/15._qp
    VALS(3,5,6)=1._qp/15._qp
!!!
    ALLOCATE(coords(ndim, Mesh%ndofs))

    coords=0._dp
    DO jt=1, Mesh%nt
       n=Mesh%e(jt)%nsommets
       ALLOCATE(Mesh%e(jt)%yy(ndim, Mesh%e(jt)%nsommets))
       e=Mesh%e(jt)


       SELECT CASE(e%nvertex)
       CASE(3)! triangle
          DO l=1,e%nsommets
             e%yy(1,l)=SUM( VALS(1:3,l,e%nsommets)*e%coor(1,1:3) )
             e%yy(2,l)=SUM( VALS(1:3,l,e%nsommets)*e%coor(2,1:3) )
             coords(:,e%nu(l))=coords(:,e%nu(l))+ e%yy(:,l) *e%volume
          ENDDO
       CASE(4) ! quadrangle
          DO l=1, e%nsommets
             CALL machin(l,e,z)
             e%yy(:,l)=z/e%volume
             coords(:,e%nu(l))=coords(:,e%nu(l))+ z
          ENDDO

       END SELECT
    ENDDO

    coords(1,:)=coords(1,:) * mesh%aires
    coords(2,:)=coords(2,:) * mesh%aires

    DO jt=1, Mesh%nt
       ALLOCATE(Mesh%e(jt)%y(ndim,Mesh%e(jt)%nsommets))
       e=Mesh%e(jt)
       DO l=1, e%nsommets
          e%y(:,l)=coords(:,e%nu(l))
       ENDDO
    ENDDO

    ! for the boundary conditions

    DO jt=1, Mesh%Nsegfr
       ALLOCATE(Mesh%fr(jt)%y(ndim, Mesh%fr(jt)%nsommets))
       efr=Mesh%fr(jt)
       DO l=1, efr%nsommets
          efr%y(:,l)=coords(:,efr%nu(l))
       ENDDO
    ENDDO
    DEALLOCATE(coords)
  CONTAINS
    SUBROUTINE machin(l,e,y)
      IMPLICIT NONE
      INTEGER,                INTENT(in) :: l
      TYPE(element),          INTENT(in) :: e
      REAL(dp), DIMENSION(2), INTENT(out):: y
      INTEGER                  :: iq
      REAL(dp)                 :: det, phi, vol
      REAL(dp), DIMENSION(2)   :: x
      REAL(dp), DIMENSION(2)   :: z
      REAL(dp), DIMENSION(2,2) :: jac
      REAL(dp), DIMENSION(2)   :: a,b,c

      y(:)=0._dp; vol=0._dp
      DO iq=1, e%nquad

         x=e%quad(1:2,iq)
         z=e%iso(x)
         !         a=e%coor(:,2)-e%coor(:,1)
         !         b=e%coor(:,4)-e%coor(:,1)
         !         c=e%coor(:,3)-e%coor(:,2)+e%coor(:,1)-e%coor(:,4)
         !         z=e%coor(:,1)+ ( a+c*x(2) )*x(1) + b*x(2) ! location of the point thanks to iso parametric transformation

         !         jac(1,1)=a(1)+x(2)*c(1)
         !         Jac(1,2)=b(1)+x(1)*c(1)
         !         Jac(2,1)=a(2)+x(2)*c(2)
         !         Jac(2,2)=b(2)+x(1)*c(2)
         Jac=e%d_iso(x)

         det=ABS( Jac(1,1)*Jac(2,2) - Jac(1,2)*Jac(2,1) )
         phi=e%base(l,x(1:2))
         y(:)=y(:)+z*phi*det*e%weight(iq)

      ENDDO

    END SUBROUTINE machin


  END SUBROUTINE coord_kinetic_new




  SUBROUTINE coord_kinetic_ent(Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="coord_kinetic entropy"
    TYPE(maillage), INTENT(inout):: Mesh
    RETURN
  END SUBROUTINE coord_kinetic_ent


  SUBROUTINE voisinage_old(Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "voisinage_old"

    TYPE(Maillage), INTENT(inout):: mesh
    TYPE(element):: e
    INTEGER:: jt, k, is
    INTEGER, DIMENSION(:), ALLOCATABLE:: is2jt, compte
    ALLOCATE(is2jt(Mesh%ns), compte(Mesh%ns) )
    is2jt=0
    compte=0
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       DO k=1, e%nvertex!sommets
          is2jt(e%nu(k))=is2jt(e%nu(k))+1 ! number of element that contain the vertex is
       ENDDO
    ENDDO
    ALLOCATE (Mesh%vois(Mesh%ns))!dofs) )
    DO is=1, Mesh%ns!dofs
       ALLOCATE(Mesh%vois(is)%nvois(is2jt(is)), Mesh%vois(is)%loc(is2jt(is)) )
       Mesh%vois(is)%nvois=0
    ENDDO


    DO jt=1, Mesh%nt
       e=Mesh%e(jt)

       DO k=1, e%nvertex!sommets
          compte(e%nu(k))=compte(e%nu(k))+1
          mesh%vois( e%nu(k) )%nvois( compte(e%nu(k)) )=jt ! index of the element that contains e%nu(k)<->is
          mesh%vois( e%nu(k) )%loc( compte(e%nu(k)) )=k    ! in that element is=e%nu(k) is number k
       ENDDO
    ENDDO
    DO is=1, Mesh%ns!dofs
       mesh%vois(is)%nbre=SIZE(Mesh%vois(is)%nvois) ! how many element contain is
    ENDDO


    DEALLOCATE(compte, is2jt)
  END SUBROUTINE voisinage_old

  SUBROUTINE voisinage_new(Mesh)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "voisinage_new"

    TYPE(Maillage), INTENT(inout):: mesh
    TYPE(element):: e
    INTEGER:: jt, k, is
    INTEGER, DIMENSION(:), ALLOCATABLE:: is2jt, compte
    ALLOCATE(is2jt(Mesh%ns), compte(Mesh%ns) )
    is2jt=0
    compte=0
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       DO k=1, e%nvertex!sommets
          is2jt(e%nu(k))=is2jt(e%nu(k))+1 ! number of element that contain the vertex is
       ENDDO
    ENDDO
    ALLOCATE (Mesh%vois(Mesh%ns))!dofs) )
    DO is=1, Mesh%ns!dofs
       ALLOCATE(Mesh%vois(is)%nvois(is2jt(is)), Mesh%vois(is)%loc(is2jt(is)) )
       Mesh%vois(is)%nvois=0
    ENDDO


    DO jt=1, Mesh%nt
       e=Mesh%e(jt)

       DO k=1, e%nvertex!sommets
          compte(e%nu(k))=compte(e%nu(k))+1
          mesh%vois( e%nu(k) )%nvois( compte(e%nu(k)) )=jt ! index of the element that contains e%nu(k)<->is
          mesh%vois( e%nu(k) )%loc( compte(e%nu(k)) )=k    ! in that element is=e%nu(k) is number k
       ENDDO
    ENDDO
    DO is=1, Mesh%ns!dofs
       mesh%vois(is)%nbre=SIZE(Mesh%vois(is)%nvois) ! how many element contain is
    ENDDO


    DEALLOCATE(compte, is2jt)
  END SUBROUTINE voisinage_new

END MODULE geom
