MODULE boundary
  USE param2d
  USE overloading
  USE PRECISION
  use init_bc
  IMPLICIT NONE
  PRIVATE
  INTERFACE BC_corrected
     MODULE PROCEDURE  BC_kinetic
  END INTERFACE BC_corrected
  INTERFACE wall
     MODULE PROCEDURE  wall_2
  END INTERFACE wall

  PUBLIC:: BC ,boundary_log, BC_corrected

CONTAINS
  SUBROUTINE boundary_log(log_type, bc_tag, Nphys, PhysName)
    ! maps the log_fr to the right type of boundary condiction
    CHARACTER(Len=80), PARAMETER:: mod_name="Euler/Boundary_log"
    CHARACTER(Len=80),DIMENSION(Nphys), INTENT(in):: PhysName
    INTEGER, INTENT(in)::log_type, Nphys
    INTEGER, INTENT(out)::bc_tag
    INTEGER:: l

    SELECT CASE(TRIM(ADJUSTL(PhysName(log_type))))
    CASE("Steger","steger","Inflow","inflow","Outflow","outflow")
       bc_tag=2
    CASE("wall","Wall")
       bc_tag=1
!!$       case("moving")
!!$           ele(i)%bc_tag=3
    CASE default 
       WRITE(*,*) mod_name, "boundary condition not defined"
       WRITE(*,*) "currently Steger, wall, Inflow, OutFlow"
       WRITE(*,*)"nbre of boundary", Nphys
       WRITE(*,*) "value=", TRIM(PhysName(log_type)), log_type
       DO l=1, Nphys
          WRITE(*,*) TRIM(PhysName(l))
       ENDDO

       STOP
    END SELECT

  END SUBROUTINE boundary_log

  SUBROUTINE BC(Mesh,k,dt,t,ua,un,DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(Len=80), PARAMETER             :: mod_name="Euler/Bc"
    REAL(DP),                    INTENT(in)  :: t, dt
    INTEGER, INTENT(in):: k
    TYPE(maillage),              INTENT(in)  :: Mesh
    TYPE(donnees),            INTENT(in)     :: DATA
    TYPE(Pvar),    INTENT(in), DIMENSION(0:DATA%iordret-1,Mesh%ndofs)  :: ua
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma
    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta
    TYPE(Pvar), INTENT(inout), DIMENSION(:,:):: un
    TYPE(Pvar), DIMENSION(:),ALLOCATABLE    :: res, u
    TYPE(frontiere)                          :: efr

    INTEGER                                  :: jt, is, lp

    DO jt=1, Mesh%Nsegfr
       efr=mesh%fr(jt)

       ALLOCATE (Res(efr%nsommets))
       ALLOCATE(u(efr%nsommets))
#if (1==0)
       DO lp=n_theta(DATA%iordret)-1,n_theta(DATA%iordret)-1
#else
       DO lp=0,n_theta(DATA%iordret)-1
#endif
          u(:)=ua(lp,efr%nu(:))



          SELECT CASE(efr%bc_tag)!bc_tag)
          CASE(3) ! moving_wall

             CALL moving_wall(efr,u,res,DATA)
          CASE(2) ! inflow/outflow
             CALL galerkin_bc2(efr,t,u,res,DATA)
          CASE(1) ! wall
             CALL wall(efr,u,res,DATA)
          CASE default
             PRINT*, mod_name, "wrong BC log fr", efr%bc_tag
             STOP
          END SELECT
          DO is=1,efr%nsommets
#if (1==0)
            un(is,jt)=un(is, jt)+res(is)*dt
#else
            un(is,jt)=un(is, jt)+theta(lp, k-1, DATA%iordret)*res(is)*dt
#endif
          ENDDO
       ENDDO
       DEALLOCATE(Res,u)
    ENDDO
  END SUBROUTINE BC

  SUBROUTINE BC_kinetic(Mesh,k,dt,t,ua,un,DATA,alpha,beta,gamma,n_theta,theta)
    IMPLICIT NONE
    CHARACTER(Len=80), PARAMETER           :: mod_name="Euler/Bc_kinetic"
    INTEGER, INTENT(in):: k
    REAL(DP),                    INTENT(in):: t, dt
    TYPE(maillage),              INTENT(in):: Mesh
    TYPE(donnees),            INTENT(in)   :: DATA

    TYPE(Pvar),    INTENT(in), DIMENSION(0:DATA%iordret-1,Mesh%ndofs):: ua
    TYPE(Pvar), INTENT(inout), DIMENSION(:,:):: un
    REAL(DP),DIMENSION(3,3:4), INTENT(in):: alpha
    REAL(DP),DIMENSION(3,3,3:4), INTENT(in):: beta, gamma
    INTEGER, DIMENSION(-4:4), INTENT(in):: n_theta
    REAL(DP),DIMENSION(0:3,3,-4:4), INTENT(in):: theta

    TYPE(Pvar), DIMENSION(:),ALLOCATABLE   ::  u
    REAL(dp):: resj, resJJ,  we, psi, cor, alphaa, xbar(2)
    TYPE(frontiere)                        :: efr

    INTEGER                                :: jt, is, lp

    DO jt=1, Mesh%Nsegfr
       efr=mesh%fr(jt)
       ALLOCATE(u(efr%nsommets))
       resJJ=0._dp
       DO lp=0, n_theta(DATA%iordret)-1
          u(:)=ua(lp,efr%nu(:))

          SELECT CASE(efr%bc_tag)!bc_tag)
          CASE(3) ! moving_wall
             PRINT*, mod_name,  "moving wall not yet done"
             STOP
             !          CALL moving_wall(efr,u,res,DATA)
          CASE(2) ! inflow/outflow
             !             PRINT*, mod_name,  "inflow/outflow not yet done"
             resj=0._dp
             !          CALL galerkin_bc2(efr,t,u,res,DATA)
          CASE(1) ! wall

             CALL wall_kinetic(efr,u,resj,DATA)
          CASE default
             PRINT*, mod_name, "wrong BC log fr", efr%bc_tag
             STOP
          END SELECT
          resJJ=resJJ+theta(lp, k-1, DATA%iordret)*resJ
       ENDDO
       we=0._dp
       DO is=1, efr%nsommets
          we=we+un(is,jt)%u(3)*efr%y(1,is)-un(is,jt)%u(2)*efr%y(2,is)
       ENDDO
       psi=dt*resJJ-we
       xbar(1)=SUM(efr%y(1,:))/REAL(efr%nsommets,dp)
       xbar(2)=SUM(efr%y(2,:))/REAL(efr%nsommets,dp)
       cor=SUM( ( efr%y(1,:)-xbar(1) )**2+( efr%y(2,:)-xbar(2) )**2)
       alphaa=psi/(cor)

       DO is=1,efr%nsommets

          un(is,jt)%u(2)=un(is, jt)%u(2)- alphaa *( efr%y(2,is)-xbar(2) )
          un(is,jt)%u(3)=un(is, jt)%u(3)+ alphaa *( efr%y(1,is)-xbar(1) )
       ENDDO
       DEALLOCATE(u)
    ENDDO

  END SUBROUTINE BC_kinetic

    FUNCTION BCs(efr,uloc,x,y,temps,Icas) RESULT(ubord)
    TYPE(frontiere), INTENT(in) :: efr
    TYPE(PVar),      INTENT(in) :: uloc
    REAL(DP),            INTENT(in) :: x,y
    REAL(DP),            INTENT(in) :: temps
    INTEGER, INTENT(in)              :: Icas
    TYPE(Pvar)                  :: ubord
     real(dp),dimension(2):: z

    z(1)=x; z(2)=y
    ubord=IC(0,z,Icas,temps)
    end function BCs



  SUBROUTINE  galerkin_bc2(e,t,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="Galerkin_bc2"
    ! Stegger warming modifie
    ! boundary conditions
    !***********assumes inward normals********
    TYPE(frontiere),                   INTENT(in) :: e
    REAL(DP),                              INTENT(in) :: t
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(donnees),   INTENT(in)   :: DATA
    REAL(DP), DIMENSION(2,e%nsommets)                 :: grad
    REAL(DP), DIMENSION(n_dim)                        :: n, x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base
    TYPE(PVar)                                        :: uloc, ubord 
    REAL(DP), DIMENSION(1,1,2)                        :: vit
    REAL(DP), DIMENSION(n_vars,n_vars)                :: Jac
    INTEGER                                           :: i, iq, l, k
    REAL(DP)                                          :: alpha
    REAL(DP), PARAMETER                               :: sigma = 1.0 ! SBP constant
    REAL(DP), PARAMETER                               :: eps=0._dp
    TYPE(Pvar), DIMENSION(n_dim)                      :: flux
    TYPE(Pvar)                                        :: dphi, prim
    real(dp), dimension(4,4):: lambda

    res=0._dp
    n=-e%n ! interior normal in principle
    DO k=1, e%nsommets
       DO i=1, e%nquad
          y=e%quad(:,i)

          x(1)=SUM(y*e%coor(1,1:2))
          x(2)=SUM(y*e%coor(2,1:2))
          DO l=1, e%nsommets
             base(l)=e%base(l,y)
          ENDDO
          DO l=1, n_vars
             uloc%u(l)  = SUM(base*u(:)%u(l))
          ENDDO
          ubord = BCs(e,uloc,x(1),x(2),t, DATA%icas)

          dphi%u(:)=-MATMUL( uloc%max_mat(x,eps,n)         , ubord%u(:)-uloc%u(:) )
 

          Res(k)%u(:)=Res(k)%u(:) +  dphi%u(:) *e%weight(i)*base(k)

       ENDDO ! i
    ENDDO ! k


  END SUBROUTINE galerkin_bc2



  SUBROUTINE wall_old (e,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(PVar)                                    :: uloc,ubar
    REAL(dp):: un, h, p, ro, eps, q2,hbar,pbar,epsbar,unn,fluxE,correction,uu,vv,vbar,c,ut,unnn
    REAL(dp),DIMENSION(2):: n,nn
    INTEGER:: i,k,l
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base

    res=0._dp
    n=-e%n
    DO i=1, e%nsommets

       DO k=1, e%nquad
          y=e%quad(:,i)

          x(1)=SUM(y*e%coor(1,1:2))
          x(2)=SUM(y*e%coor(2,1:2))

          DO l=1, e%nsommets
             base(l)=e%base(l,y)
          ENDDO


          DO l=1, n_vars
             uloc%u(l)  = SUM(base*u(:)%u(l))
          ENDDO

          ro=uloc%u(1)
          q2=SUM(uloc%u(2:3)**2)/(ro*ro)
          p=uloc%pEOS()
          h=(uloc%u(4)+p)



          un=SUM(uloc%u(2:3)*n)/ro
          nn=n/SQRT(n(1)**2+n(2)**2)
          unnn=SUM(uloc%u(2:3)*nn(1:2) )/ro
          ut=(-uloc%u(2)*nn(2)+uloc%u(3)*nn(1))/ro

          unn=SQRT(SUM(uloc%u(2:3)**2)/ro)




          res(i)%u(1)=res(i)%u(1)+2._dp*un*uloc%u(1)*e%weight(k)*base(i)
          res(i)%u(2)=res(i)%u(2)+(uloc%u(2)-ro*unnn*nn(1)-ro*ut*nn(2))*un*e%weight(k)*base(i)
          res(i)%u(3)=res(i)%u(3)+(uloc%u(3)-ro*unnn*nn(2)+ro*ut*nn(1))*un*e%weight(k)*base(i)
          res(i)%u(4)=res(i)%u(4)


       ENDDO ! k


    ENDDO
  END SUBROUTINE wall_old

  SUBROUTINE wall_new (e,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(PVar)                                    :: uloc,ubar
    REAL(dp):: ro, ener, p, h, un
    REAL(dp),DIMENSION(2):: n,nn
    INTEGER:: i,k,l, kl
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base
    TYPE(Pvar), DIMENSION(2,e%nsommets):: flux
    TYPE(Pvar), DIMENSION(2):: fl
    REAL(dp), DIMENSION(e%nsommets):: pression
    ! Evaluation of the flux (only for P1/Q1)
    IF (e%nsommets.NE.e%nvertex) THEN
       PRINT*, mod_name
       PRINT*, "for now only for linear elements"
       STOP
    ENDIF
    DO l=1, e%nsommets
       flux(:,l)=u(l)%flux()
       pression(l)=u(l)%pEOS()
    ENDDO


    res=0._dp
    n=-e%n  ! interior normal in principle

    DO i=1, e%nsommets

       DO k=1, e%nquad
          y=e%quad(:,k)

          DO l=1, e%nsommets
             base(l)=e%base(l,y)
          ENDDO
          DO kl=1, 4
             fl(1)%u(kl)=SUM(base(:)*flux(1,:)%u(kl))
             fl(2)%u(kl)=SUM(base(:)*flux(2,:)%u(kl))
          ENDDO
          p=SUM(base*pression)
          res(i)%u(1  )=res(i)%u(1  )+(fl(1)%u(1  )*n(1)+fl(2)%u(1  )*n(2)         )*e%weight(k)*base(i)
          res(i)%u(2:3)=res(i)%u(2:3)+(fl(1)%u(2:3)*n(1)+fl(2)%u(2:3)*n(2)-p*n(1:2))*e%weight(k)*base(i)
          res(i)%u(4  )=res(i)%u(4  )+(fl(1)%u(4  )*n(1)+fl(2)%u(4  )*n(2)         )*e%weight(k)*base(i)

       ENDDO ! k


    ENDDO

  END SUBROUTINE wall_new

  SUBROUTINE wall_2 (e,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    TYPE(PVar)                                    :: uloc,v, w, prim
    REAL(dp):: ro, ener, p, h, un
    REAL(dp),DIMENSION(2):: n
    INTEGER:: i,k,l,ll
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base
    REAL (dp), DIMENSION(e%nsommets,e%nsommets):: buse

    res=0._dp
    ! interior normal in principle
    n=-e%n  ! - pour triangle.....????
    
    DO i=1, e%nsommets

       DO k=1, e%nquad
          y=e%quad(:,k)
          x(1)=SUM( y*e%coor(1,1:2) ) ! this assumes that the boundary is straight (and gmsh provides curved boundary)
          x(2)=SUM( y*e%coor(2,1:2) )
          DO l=1, e%nsommets
             base(l)=e%base(l,y)
          ENDDO

          DO l=1, n_vars
             uloc%u(l)  = SUM(base*u(:)%u(l))
          ENDDO

          ro=uloc%u(1)
          ener=uloc%u(4)
          p=uloc%pEOS()
          h=(ener+p)

          un=SUM(uloc%u(2:3)*n)/ro

          res(i)%u(1)  =res(i)%u(1)  +ro*un              *e%weight(k)*base(i)
          res(i)%u(2:3)=res(i)%u(2:3)+uloc%u(2:3)*un     *e%weight(k)*base(i)
          res(i)%u(4)  =res(i)%u(4)  + un* h             *e%weight(k)*base(i)

       ENDDO ! k


    ENDDO

  END SUBROUTINE wall_2


  SUBROUTINE wall_kinetic (e,u, resj,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall_kinetic"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    REAL(dp), INTENT(out):: resj
    TYPE(PVar)                                    :: uloc
    REAL(dp):: un
    REAL(dp):: rot
    REAL(dp),DIMENSION(2):: n,nn
    INTEGER:: k,l
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(e%nsommets)                   :: base
    REAL(dp):: a(2)

    resj=0._dp
    n=-e%n ! interior normal in principle
    DO k=1, e%nquad
       y=e%quad(:,k)
       x(1)=SUM(y*e%coor(1,1:2))
       x(2)=SUM(y*e%coor(2,1:2))
       DO l=1, e%nsommets
          base(l)=e%base(l,y)
       ENDDO


       DO l=1, n_vars
          uloc%u(l)  = SUM(base*u(:)%u(l))
       ENDDO



       un=SUM(uloc%u(2:3)*n)/uloc%u(1)

       rot=x(1)*uloc%u(3)-x(2)*uloc%u(2)


       resj=resj + rot* un *e%weight(k)



    ENDDO ! k



  END SUBROUTINE wall_kinetic




  SUBROUTINE moving_wall (e,u,res,DATA)
    IMPLICIT NONE
    CHARACTER(Len=80):: mod_name="wall"
    TYPE(frontiere),                   INTENT(in) :: e
    TYPE(PVar), DIMENSION(:), INTENT(in) :: u
    TYPE(donnees),   INTENT(in)   :: DATA
    TYPE(PVar), DIMENSION(:), INTENT(out):: res
    REAL(dp):: un, h, p, ro, eps, q2,V,Vn
    REAL(dp),DIMENSION(2):: n
    INTEGER:: i
    DO i=1, e%nsommets
       V=100._dp
       ro=u(i)%u(1)
       q2=SUM(u(i)%u(2:3)**2)/(ro*ro)
       eps=(u(i)%u(4)-ro*q2*0.5_dp)/ro
       p=u(i)%pEOS()
       h=(eps+p)/ro
       n=e%n
       Vn=SUM(V*n)
       un=SUM(u(i)%u(2:3)*n)/ro

       res(i)%u(1)=(un-Vn)*u(i)%u(1)
       res(i)%u(2:3)=u(i)%u(2:3)*un-ro*V*V*n(1)
       res(i)%u(4)=(un-Vn)*h
    ENDDO
  END SUBROUTINE moving_wall




END MODULE boundary
