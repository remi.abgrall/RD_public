MODULE dans_mon_convexe
  USE PRECISION
    USE, INTRINSIC :: iso_c_binding
  IMPLICIT NONE
  PUBLIC:: convexe
CONTAINS
  SUBROUTINE convexe(tested,cloud,m,f)

    INTERFACE
       SUBROUTINE is_in_hull_plot(tested,cloud, m) BIND (c)
                  USE iso_c_binding
         USE PRECISION
         INTEGER(C_INT),DIMENSION(1), INTENT(in):: m
         REAL(dp),DIMENSION(2), INTENT(in):: tested(2)
         REAL(dp), DIMENSION(2*m(1)), INTENT(in)::cloud
       END SUBROUTINE  is_in_hull_plot

       INTEGER(C_INT) FUNCTION  is_in_hull(tested,cloud, m) BIND (c)
                  USE iso_c_binding
         USE PRECISION
         INTEGER(C_INT),DIMENSION(1), INTENT(in):: m
         REAL(dp),DIMENSION(2), INTENT(in):: tested(2)
         REAL(dp), DIMENSION(2*m(1)), INTENT(in)::cloud
       END FUNCTION  is_in_hull
    END INTERFACE
!!!!!!!!!!!!!!!!!!!
    INTEGER(C_INT), DIMENSION(1), INTENT(in):: m
    REAL(dp), INTENT(in):: tested(2),cloud(m(1)*2)
    INTEGER(C_INT), INTENT(out):: f

    REAL(c_double):: x(2)
    INTEGER(C_INT):: n(1)
    INTEGER(C_INT):: i,l
    INTEGER(C_INT),DIMENSION(1):: flag
    n(1)=2*m(1)
!!$    PRINT*, 'cloud',m(1)
!!$    DO l=1, n(1)
!!$       PRINT*, l,cloud(l)
!!$    ENDDO
!!$    PRINT*, 'tested' 
!!$    PRINT*,tested
!!$    CALL  is_in_hull_plot( tested, cloud, n)
!!$    print*, 'onarrive ds hull'
    flag= is_in_hull( tested, cloud,n)
!!$        print*, "flag", flag(1)
    f=flag(1)

  END SUBROUTINE convexe
END MODULE dans_mon_convexe
