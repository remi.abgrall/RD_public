import cffi
ffibuilder = cffi.FFI()

header = """
extern void  is_in_hull_plot(double*, double*, int8_t*);
extern int8_t  is_in_hull(double*, double*, int8_t*);
"""


module = """
#import sys
from my_plugin import ffi
import numpy as np
# This has to be adapted everytime...
#sys.path.append('/Users/rabgra/Nextcloud.tmp/contraintes/Src2D2.2/Python/')
import my_module

def in_hull(p, hull):
   
    from scipy.spatial import Delaunay
    if not isinstance(hull,Delaunay):
        hul = Delaunay(hull,'Qt')

    return hul.find_simplex(p)>=0
def plot_in_hull(p, hull):

    import matplotlib.pyplot as plt
    from matplotlib.collections import PolyCollection, LineCollection

    from scipy.spatial import Delaunay
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)

    # plot triangulation
    poly = PolyCollection(hull.points[hull.vertices], facecolors='w', edgecolors='b')
    plt.clf()
    plt.title('in hull')
    plt.gca().add_collection(poly)
    plt.plot(hull.points[:,0], hull.points[:,1], 'o')
    #plt.plot(p[:,0],p[:,1],"r*")
    inside = in_hull(p,hull)
    ind=np.where(inside)[0]
    plt.plot(p[ind,0],p[ ind,1],'*k')
    print(inside)
    ind=np.where(~ inside)[0]
    plt.plot(p[ind,0],p[ind,1],'*r')
#    plt.show()
    plt.savefig('test.png')

    # plot the convex hull
    edges = set()
    edge_points = []
    def add_edge(i, j):
        if (i, j) in edges or (j, i) in edges:
            # already added
            return
        edges.add( (i, j) )
        edge_points.append(hull.points[ [i, j] ])

        for ia, ib in hull.convex_hull:
            add_edge(ia, ib)

        lines = LineCollection(edge_points, color='g')
        plt.gca().add_collection(lines)
           

    # plot tested points `p` - black are inside hull, red outside
        inside = in_hull(p,hull)
        plt.plot(p[ inside,0],p[ inside,1],'*k')
        plt.plot(p[-inside,0],p[-inside,1],'*r')
        plt.show()

@ffi.def_extern()

def is_in_hull_plot(tested_ptr, cloud_ptr, m_ptr ):
#tested: vectors (n,2)
# cloud : vector (m,2)
# one test if tested is in convenient hull(clod)


    m=my_module.asarray(ffi,m_ptr,shape=(1,))
    tested=my_module.asarray2d(ffi,tested_ptr,shape=(2,))
    cloud=my_module.asarray2d(ffi,cloud_ptr,shape=(m[0],))
    print(in_hull(tested,cloud))

    plot_in_hull(tested,cloud)

@ffi.def_extern()

def is_in_hull(tested_ptr, cloud_ptr, m_ptr ):
#tested: vectors (n,2)
# cloud : vector (m,2)
# one test if tested is in convenient hull(clod)

    m=my_module.asarray(ffi,m_ptr,shape=(1,))
    tested=my_module.asarray2d(ffi,tested_ptr,shape=(2,))
    cloud=my_module.asarray2d(ffi,cloud_ptr,shape=(m[0],))
#    print "is_in_hull"
#    print cloud
#    print "tested"
#    print tested
    i=0 
    if in_hull(tested,cloud):
        i=1
    return i 

"""

with open("plugin.h", "w") as f:
    f.write(header)

ffibuilder.embedding_api(header)
ffibuilder.set_source("my_plugin", r'''
    #include "plugin.h"
''')

ffibuilder.embedding_init_code(module)
ffibuilder.compile(target="libplugin.dylib", verbose=True)



