!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------

PROGRAM main
  USE param2d
  USE scheme
  USE overloading
  USE Model
  USE geom
  USE GeomGraph
  USE postprocessing
  USE time_stepping
  USE update
  USE PRECISION

  IMPLICIT NONE
  CHARACTER(LEN = *), PARAMETER :: mod_name = "main"

  INTEGER, DIMENSION(5)::loc_ndofs=(/2,3,3,4,4/)

  REAL(DP),DIMENSION(3,2:4):: alpha
  REAL(DP),DIMENSION(3,3,3:4):: beta, gamma

  INTEGER, DIMENSION(-4:4):: n_theta
  REAL(DP),DIMENSION(0:3,3,-4:4):: theta
  TYPE(maillage):: mesh
  TYPE(variables):: var,debug
  TYPE(donnees):: DATA
  TYPE(element):: e, e1, e2
  TYPE(arete):: ed
  TYPE(frontiere):: efr
  TYPE(PVar):: error_L1, error_L2, error_Linf
  INTEGER, DIMENSION(:),ALLOCATABLE :: fluxes_mood
  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: temp_debug, temp_var, temp

  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: u, up,ua1,ua2,up1,up2, up_p, u_p
  TYPE(Pvar),DIMENSION(2):: f1,f2,f3, f4,f5
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: res,residu, uu, difference
  TYPE(Pvar), DIMENSION(:), ALLOCATABLE :: u1, u2
  TYPE(Pvar), DIMENSION(:,:), ALLOCATABLE:: resJ
  TYPE(Pvar),DIMENSION(:,:), ALLOCATABLE:: flux, flux_c
  REAL(DP), DIMENSION(3):: x=0.
  INTEGER:: nt,itype, jt, i, kt, is, l, k_inter, k, iseg, jt1, jt2, ll, kt0
  REAL(DP):: dx, dt0, dt, temps
  CHARACTER(LEN = 1024) :: maille
  INTEGER:: nb_args, Impre, n, lp
  TYPE(Pvar), DIMENSION(:),ALLOCATABLE:: u_b, u_c
  CHARACTER(10) :: time
  CHARACTER(25) :: statFile
  LOGICAL :: exist
  REAL(dp):: kinetic, kine, mass,masse, masse0, kinetic0
  INTEGER :: nflux, nnmax, nfrmax


!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------
!!$-------------------------------------------------------------------




  !--------------------------- READ INITIAL DATA --------------------------------------------------

  nb_args = iargc()

  IF (nb_args < 1) THEN
     PRINT *, " ERREUR : donner le nom du cas en argument (Data%RootName), argc==", nb_args
     STOP
  END IF
  CALL getarg(1,maille)


  OPEN(1, file="Data/don")
  READ(1,*) DATA%itype
  DATA%itype_b=DATA%itype
  READ(1,*)DATA%iordret, DATA%iter
  IF (DATA%iter.LT.DATA%iordret) THEN
     PRINT*, "wrong Data%iter,Data%iordret"
     PRINT*,"Data%iter should be >= than Data%iordret"
     PRINT*, "Data%iordret", DATA%iordret
     PRINT*, "Data%iter", DATA%iter
  ENDIF
  READ(1,*) DATA%ischema
  READ(1,*) DATA%theta_jump
  READ(1,*) DATA%theta_jump2
  READ(1,*) DATA%cfl
  READ(1,*) DATA%ktmax
  READ(1,*) DATA%tmax
  READ(1,*) DATA%ifre
  DATA%maillage=maille
  READ(1,*) DATA%loc_dec ! useless
  READ(1,*) DATA%restart
  READ(1,*) DATA%limit
  READ(1,*) DATA%Icas
  READ(1,*) DATA%cor ! 1: kinetic momentum, 2: vorticity, other: no correction
  READ(1,*) DATA%mood
  IF(DATA%mood) THEN
     READ(1,*)jt
     ALLOCATE(fluxes_mood(jt))
     DO k=1, jt
        READ(1,*) fluxes_mood(k)
     ENDDO
  ENDIF

  !----------- SET geometry of the problem test --------------------------
  PRINT*, "before ReadMesh"
  CALL theta_alpha_beta()

  CALL ReadMeshGMSH2(DATA,Mesh)
  Impre=6
  jt=Mesh%nt

  PRINT*, "exit ReadMesh"

  ! geometrie
  CALL GeomSegments2d(Mesh,Impre)

  PRINT*, "exit Geomsegments"

  CALL calculateMassMatrLumped(DATA, Mesh)
  PRINT*, "Exit Mass matrix"
  !  IF (DATA%cor)
  CALL coord_kinetic(Mesh)
  !initialisation

  !  CALL test_geom()

  Var%Ncells=Mesh%ns
  !  CALL PRINT()
  kt0=0
  DATA%temps=0.0_dp
  nnmax=MAXVAL(Mesh%e(:)%nsommets)
  nfrmax=MAXVAL(Mesh%fr(:)%nsommets)
  ! initialisation
  ALLOCATE(Var%ua(0:DATA%iordret-1,Mesh%ndofs),&
       & Var%up(0:DATA%iordret-1,Mesh%ndofs)&
       & , var%un(nnmax, Mesh%nt)&
       &, var%un_b(nfrmax, Mesh%Nsegfr))
  IF (DATA%mood) THEN
     ALLOCATE( debug%ua(0:DATA%iordret-1,Mesh%ndofs) &
          ,debug%up(0:DATA%iordret-1,Mesh%ndofs)&
          & , debug%un(nnmax,Mesh%nt), &
          & debug%un_b(nfrmax, Mesh%Nsegfr), &
          & temp_var(0:DATA%iordret-1,Mesh%ndofs), &
          & temp(0:DATA%iordret-1,Mesh%ndofs), &
          & temp_debug(0:DATA%iordret-1,Mesh%ndofs) )
     !     allocate(debug%ua_e(nnmax,mesh%nt), debug%up_e(nnmax,mesh%nt) )
  ENDIF
  PRINT*, "End Allocate"

  ! Testing times
#if (-1 == 1)
  statFile = 'times_01.csv'
  INQUIRE(file=statFile, exist=exist)
  OPEN(99, file=statFile)
  WRITE(99,*)1
#endif

  !-------------------------------------------------------------------------------------------------------------------------------
  ! ----------Aquire old data if test has been interrupted and does not need to restart from scratch --------
  IF (.NOT.DATA%restart) THEN ! we start with a new solution
     kt=0
     DO jt=1, Mesh%nt
        e=Mesh%e(jt)
        DO l=1, e%nsommets
           Var%ua(0,e%nu(l))= IC(jt,e%coor(:,l),DATA%icas)
        ENDDO

        SELECT CASE(e%itype)
        CASE(1,3,4) !Lagrange-> nothing to do
           Var%up(0,:)=Var%ua(0,:)
        CASE(2,5) ! Bezier -> modify coeff

           DO k=1, n_vars

              DO l=1, e%nsommets!SIZE(u)
                 Var%up(0,e%nu(l))%u(k)=SUM(e%inv_base_at_dofs(:,l)*Var%ua(0,e%nu(:))%u(k))
              ENDDO

           ENDDO

           !           DEALLOCATE(u_b,u_c)


        CASE default
           PRINT*, "correct initialistaion not yet implemented for these elements,e%itype=",e%itype
           STOP
        END SELECT

     ENDDO
     CALL output()

     Var%ua(0,:)=Var%up(0,:) ! ?


  ELSE
     CALL readsol(DATA,Mesh%ndofs,kt0,Var)
  ENDIF
  kt=kt0

  !-------------------------------------------------------------------------------------------------------------------------------
  OPEN(12, file='kineticdifference.dat')
  OPEN(13, file='kinetic.dat')
  PRINT*, "Initialisation"
  CALL output()


  CALL visu(DATA, kt, mesh, Var)


  !-------------------------------------------------------------------------------------------------------------------------------
  ! -------------------- Time- stepping loop -------------------------

  DO kt=kt0+1, DATA%ktmax
#if (1==1)
     IF (DATA%cor) THEN
        CALL kinetic_momentum()

     ELSE

        kinetic=0._dp
        masse=0._dp

        CALL kinetic_momentum()

     ENDIF


#endif
     CALL date_and_TIME(TIME = time)
     WRITE(99,*)time

     IF (DATA%mood) THEN
        DO is=1, Mesh%ndofs
           temp(:,is)=Var%ua(0,is)
           Var%ua(1:,is)=Var%ua(0,is)
           debug%ua(:,is)=Var%ua(0,is)
           temp_debug(:,is)=Debug%ua(:,is)
           temp_var(:,is)=Var%ua(:,is)
           !          Var%un(:,is)=0.0_dp
           Var%up(:,is)=Var%ua(0,is)

        ENDDO
     ELSE
        DO is=1, Mesh%ndofs
           Var%ua(1:,is) = Var%ua(0,is)
           Var%up(:,is)  = Var%ua(0,is)
        ENDDO

     ENDIF


     dt0= Pas_de_temps(Mesh,Var, DATA)

     dt=MIN(dt0,DATA%tmax-DATA%temps)

     IF (dt.LE.1.e-6_dp) EXIT
     DATA%temps= DATA%temps+dt

     Mesh%e(:)%type_flux=DATA%ischema
     Mesh%e(:)%diag     =0
     Mesh%e(:)%diag2=0
     IF (DATA%mood) mesh%e(:)%flag=.TRUE. ! identify the elements where we compute. Initially all of them

     ! Euler time stepping
     ! Here we initialise the run by populating with un
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! start defect
     ! Here we initialise the run by populating with un
     IF (DATA%mood) THEN
        Mesh%e(:)%diag=0
        Mesh%e(:)%diag2=0
        DO nflux = 1,SIZE(fluxes_mood)


           IF (nflux.GT.1) THEN
              DO is=1, Mesh%ndofs
                 Debug%ua(:,is)= temp_debug(:,is)
              END DO
           ENDIF

           DO k_inter=1,DATA%iter !loop for the corrections r=1,...,R

              CALL dec( debug,DATA,Mesh,dt, n_theta, alpha, beta,gamma, theta)


              ! do the test at the end of timestep, not at every iteration!!!!
              ! maybe positivity checks inside every iteration
              ! or do it
              !              CALL test(DATA%iordret-1,Debug,Var, mesh, DATA,fluxes_mood(nflux))

           ENDDO
           ! or do it only after all time cycles
           Mesh%e(:)%diag2=Mesh%e(:)%diag
           Mesh%e(:)%diag=0
           CALL test(DATA%iordret-1,Debug,Var, mesh, DATA,fluxes_mood(nflux))

           !print*
        END DO ! nflux
        Mesh%e(:)%diag=Mesh%e(:)%diag+Mesh%e(:)%diag2
        DO k_inter=1,DATA%iter !loop for the corrections r=1,...,R


           CALL dec(Var,DATA,Mesh,dt, n_theta, alpha, beta,gamma, theta)

        ENDDO


     ELSE
        DO k_inter=1,DATA%iter !loop for the corrections r=1,...,R
           CALL dec(Var,DATA,Mesh,dt, n_theta, alpha, beta,gamma, theta)
        ENDDO
     ENDIF
     ! swap solutions
     Var%ua(0,:)=Var%ua(DATA%iordret-1,:)

     CALL pad()

     IF (MOD(kt,DATA%ifre)==0) THEN

        CALL output()

#if (1==1)        
        PRINT*, "kinetic momentum value ",kinetic
#endif        
        CALL visu(DATA, kt, mesh, Var)
        CALL errors(DATA,Mesh,Var,error_L1,error_L2, error_Linf)
        CALL writesol(DATA,var,kt)
        PRINT*

     ENDIF
  ENDDO ! end  time stepping
  PRINT*, "final time=", DATA%temps
  !post processing
  CALL visu(DATA, kt, mesh, Var)
  CALL writesol(DATA,var,kt)
  CALL errors(DATA,Mesh,Var,error_L1,error_L2, error_Linf)

  !-------------------------------------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------------------------------------
  !-------------------------------------------------------------------------------------------------------------------------------------

CONTAINS
  REAL(DP) FUNCTION pas_de_temps(Mesh,Var,DATA)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "Pas_de_Temps"
    TYPE(variables), INTENT(in):: Var
    TYPE(Maillage), INTENT(in):: Mesh
    TYPE(donnees), INTENT(in):: DATA
    INTEGER:: jt, l, k,j
    TYPE(element):: e
    TYPE(Pvar), ALLOCATABLE, DIMENSION(:):: u
    REAL(DP):: dt, dx, umax
    REAL(DP), DIMENSION(2):: x=0._dp, n
    dt=HUGE(1.0_dp)

    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(u(e%nsommets))
       u(:)=Var%ua(0,e%nu)
       umax=0._dp
       DO l=1, e%nvertex
          n=e%n(:,l)/SQRT(SUM(e%n(:,l)**2))
          umax=MAX(umax,u(l)%spectral_radius(x,n ) )
       ENDDO
       dx=MINVAL(SQRT( e%n(1,:)**2+e%n(2,:)**2))
       dt=MIN(dt, dx/(umax+1.e-10_dp) )

       DEALLOCATE(u)
    ENDDO
    pas_de_temps=dt*DATA%cfl

  END FUNCTION pas_de_temps

  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------
  !-----------------------------------------------------------------------------------------------------------



  SUBROUTINE theta_alpha_beta()
    ! loading the weights
    ! quadratic interpolation over [0,1]
    ! alpha: this defines the fractions of Delta t for the Euler method (operator L1)
    ! alpha( subtimestep, order in time)
    ! theta: \int_0^alpha(L,k) f = \sum_{p=0}^{order in time) theta(p,L, order in time)* f(u[p])
    ! beta contains the (time) weight for the Euler step of Version 1
    !
    INTEGER:: p
    alpha=0.
    theta=0.
    beta=0.
    gamma=0.
    n_theta=0






    ! order 2:
    n_theta(2)=2
    alpha(1,2)=1._dp
    theta(:,:,2)=0._dp
    theta(0:1,1,2)=0.5_dp
    ! order -2
    n_theta(-2)=2
    theta(:,:,-2)=0._dp
    theta(0,1,-2)=SUM(theta(:,1,2))
    !
    ! alpha, beta gamma coeffcients for order 3
    !
    alpha(1,3)=0.5_dp
    alpha(2,3)=1.0_dp
    beta(1,2,3)=alpha(1,3)
    beta(2,2,3)=alpha(2,3)-alpha(1,3)
    GAMMA(2,2,3)=beta(2,2,3)
    n_theta(3)=3
    !
    ! integration coefficients for order 3
    theta(0:2,1:2,3)=RESHAPE( (/5._dp/24._dp,1._dp/3._dp,-1._dp/24._dp,1._dp/6._dp,2._dp/3._dp,1._dp/6._dp/),(/3,2/))
    ! order -3
    n_theta(-3)=3
    theta(:,:,-3)=0._dp
    theta(0,1,-3)=SUM(theta(:,1,3))
    theta(1,2,-3)=SUM(theta(:,2,3))
!!!!!!!!!!!!!!!!!
    ! cubic interpolation
#if (-2==-2)
    ! Here I choose the points 0.5*(1+cos(k*pi/N)), N=3, k=3,2,1,0
!!!!!!!!!!!!!!
    ! these are

    alpha(1,4)=1._dp/4._dp
    alpha(2,4)=3._dp/4._dp
    alpha(3,4)=1._dp
    n_theta(4)=4

    theta(0:3,1:3,4)=RESHAPE( (/59._dp/576._dp, & !(0,1)
         & 47._dp/288._dp,  &                      !(1,1)
         & -7._dp/288._dp, &                      !(2,1)
         & 5._dp/576._dp,  &                      !(3,1)
         & 3._dp/64._dp,   &                      !(0,2)
         & 15._dp/32._dp,  &                      !(1,2)
         & 9._dp/32._dp,   &                      !(2,2)
         & -3._dp/64._dp,  &                      !(3,2)
         &1._dp/18._dp,    &                      !(0,3)
         &4._dp/9._dp,     &                      !(1,3)
         &4._dp/9._dp,     &                      !(2,3)
         &1._dp/18._dp/)   &                      !(3,3)
         &,(/4,3/))
#endif
#if (-2==0) 
    ! Here I choose the points k/3, k=3,2,1,0
!!!!!!!!!!!!!!
    ! these are

    alpha(1,4)=1_dp./3._dp
    alpha(2,4)=2._dp/3._dp
    alpha(3,4)=1._dp
    n_theta(4)=4

    theta(0:3,1:3,4)=RESHAPE( (/1._dp/8._dp, & !(0,1)
         & 19._dp/72._dp,  &                      !(1,1)
         & -5._dp/72._dp, &                      !(2,1)
         & 1._dp/72._dp,  &                      !(3,1)
         & 1._dp/9._dp,   &                      !(0,2)
         & 4._dp/9._dp,  &                      !(1,2)
         & 1._dp/9._dp,   &                      !(2,2)
         & 0._dp,      &                      !(3,2)
         &1._dp/8._dp,    &                      !(0,3)
         &3._dp/8._dp,     &                      !(1,3)
         &3._dp/8._dp,     &                      !(2,3)
         &1._dp/8._dp/)   &                      !(3,3)
         &,(/4,3/))
#endif
    !! The following lines are good only for the version 1
    beta(1,3,4)=alpha(1,4) ! this gives the fraction of timestep for the Euler method
    beta(2,3,4)=alpha(2,4)-alpha(1,4)
    beta(3,3,4)=alpha(3,4)-alpha(2,4)
    GAMMA(2:3,3,4)=beta(2:3,3,4)

    ! order -4
    n_theta(-4)=4
    theta(:,:,-4)=0._dp
    theta(0,1,-4)=SUM(theta(:,1,4))
    theta(1,2,-4)=SUM(theta(:,2,4))
    theta(1,3,-4)=SUM(theta(:,3,4))
#if (1==0)    
    PRINT*, "theta_alpha_beta"
    DO k=1,3
       DO lp=0, 3
          PRINT*, "lp=", lp, " k=", k, " p=",DATA%iordret, theta(lp,k,DATA%iordret)
       ENDDO
    ENDDO

    PRINT*
    DO k=1,3
       DO lp=0, 3
          PRINT*, "lp=", lp, " k=", k, " p=",-DATA%iordret, theta(lp,k,-DATA%iordret)
       ENDDO
    ENDDO
#endif

  END SUBROUTINE theta_alpha_beta


  SUBROUTINE pad()
    IMPLICIT NONE
    TYPE(pvar),DIMENSION(mesh%ndofs):: prim
    TYPE(pvar), DIMENSION(:), ALLOCATABLE:: p
    TYPE(element):: e
    INTEGER:: jt, l

    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       ALLOCATE(p(e%nsommets))
       p=control_to_cons(var%ua(0,e%nu),e)
       DO l=1, e%nsommets
          prim(e%nu(l))=p(l)%cons2prim()
       ENDDO
       DEALLOCATE(p)
    ENDDO
    DO jt=1, mesh%nt
       e=mesh%e(jt)
       IF (MINVAL(prim(e%nu)%u(1)).LE.0._dp .OR.&
            & MINVAL(prim(e%nu)%u(4)).LE.0._dp) THEN
          ! PRINT*, "pad jt=", jt, MINVAL(prim(e%nu)%u(1)),MINVAL(prim(e%nu)%u(4)),e%type_flux
       ENDIF
    ENDDO
  END SUBROUTINE pad

  SUBROUTINE output()
    TYPE(pvar),DIMENSION(mesh%ndofs):: prim
    TYPE(pvar), DIMENSION(mesh%ndofs):: p
    TYPE(element):: e

    PRINT*
    PRINT*, kt, DATA%temps
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       p(e%nu)=control_to_cons(var%ua(0,e%nu),e)
    ENDDO
    DO is=1, Mesh%ndofs
       prim(is)=p(is)%cons2prim()
    ENDDO
    DO l=1, n_vars
       PRINT*, "min prim, variable ",l, MINVAL(prim%u(l))
       PRINT*, "max prim, variable ",l, MAXVAL(prim%u(l))
       PRINT*
    ENDDO
  END SUBROUTINE output


  SUBROUTINE kinetic_momentum()
    IMPLICIT NONE
    TYPE(frontiere)                        :: efr
    REAL(dp):: k_b
    REAL(dp),DIMENSION(2):: n
    INTEGER:: k, kk
    REAL(DP), DIMENSION(2)  ::  x, y
    REAL(DP), DIMENSION(:)  ,ALLOCATABLE  :: base
    TYPE(PVar)                     :: uloc
    TYPE(PVar) ,DIMENSION(:), ALLOCATABLE:: u
    REAL(dp):: un,rot, p
    kinetic=0._dp
    masse=0._dp
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       kine=0._dp
       DO l=1, e%nsommets
          kine=kine-e%yy(2,l)*(var%ua(0,e%nu(l))%u(2))&
               & +e%yy(1,l)*(var%ua(0,e%nu(l))%u(3))

       ENDDO
       kinetic=kinetic+kine * e%volume
       masse=masse+e%volume*SUM( var%ua(0,e%nu)%u(1) )
    ENDDO

    ! evaluate the normal kinetic flux
    k_b=0._dp
    DO jt=1, Mesh%Nsegfr
       efr=Mesh%fr(jt)
       n=-efr%n
       ALLOCATE(u(efr%nsommets))

       ALLOCATE(base(efr%nsommets))
       kine=0._dp
       DO k=1, efr%nquad
          y=efr%quad(:,k)
          x(1)=SUM(y*efr%coor(1,1:2))
          x(2)=SUM(y*efr%coor(2,1:2))
          DO l=1, efr%nsommets
             base(l)=efr%base(l,y)
          ENDDO

          DO kk=1, 2
             u=var%ua(kk-1,efr%nu)
             DO l=1, n_vars
                uloc%u(l)  = SUM(base*u(:)%u(l))
             ENDDO
             p=uloc%pEOS()


             un =SUM(uloc%u(2:3)*n)/uloc%u(1)
             rot=x(1)*uloc%u(3)-x(2)*uloc%u(2)
             p  =p*( x(1)*n(2)-x(2)*n(1))
             kine=kine+ (rot*un +p)*efr%weight(k)
          ENDDO


       ENDDO !
       DEALLOCATE(u,base)
       k_b=k_b+kine*dt*0.5_dp
    ENDDO


    IF (kt==kt0+1) THEN
       kinetic0=kinetic
       masse0=masse
    ELSE
       WRITE(12,*) kt-1, (kinetic-kinetic0),k_b,kinetic-kinetic0+k_b
       WRITE(13,*) kt-1, (kinetic), kinetic0
    ENDIF
  END SUBROUTINE kinetic_momentum

  SUBROUTINE PRINT()
    TYPE(element):: e
    TYPE(frontiere)                          :: efr
    PRINT*, "************************"
    PRINT*, "elements", Mesh%ndofs,Mesh%nt, Mesh%nsegfr
    PRINT*, "************************"

    DO jt=1,mesh%nt
       e=mesh%e(jt)
       PRINT*, "element=", jt, e%nvertex, e%nsommets
       PRINT*, "somments, volume", e%volume
       DO l=1, e%nsommets
          PRINT*, "l=",l,e%nu(l)
       ENDDO
       PRINT*, "coor"
       DO l=1, e%nsommets
          PRINT*, l, e%coor(:,l)
       ENDDO
       PRINT*, "masse"
       DO l=1, e%nsommets
          PRINT*, l, e%masse(l,:)/e%volume
       ENDDO
       PRINT*, "rigidx"
       DO l=1, e%nsommets
          PRINT*, l,e%coeff(l,:,1)/e%volume
       ENDDO
       PRINT*, "rigidy"
       DO l=1, e%nsommets
          PRINT*, l,e%coeff(l,:,2)/e%volume
       ENDDO
       PRINT*, "sommes"
       DO l=1, e%nsommets
          PRINT*, 2.*SUM(e%coeff(:,l,1)),2.*SUM(e%coeff(:,l,2))
       ENDDO

    ENDDO
    PRINT*, "************************"
    PRINT*, "bord"
    PRINT*, "************************"
    DO jt=1, Mesh%Nsegfr
       efr=mesh%fr(jt)
       PRINT*, "vertex, sommets", e%nvertex, e%nsommets
       PRINT*, "nu", jt
       DO l=1, efr%nsommets
          PRINT*, l,efr%nu(l)
       ENDDO

       PRINT*, "coor"
       DO l=1, efr%nsommets
          PRINT*, l,efr%coor(:,l)
       ENDDO
       PRINT*, "normale"
       PRINT*, -efr%n
       PRINT*, "triangle porteur",efr%jt1
       e=mesh%e( efr%jt1)
       PRINT*, "porteur, vertex, sommets",e%nvertex,e%nsommets
       DO l=1, e%nsommets
          PRINT*, l, e%nu(l)
          PRINT*, e%coor(:,l)
       ENDDO
       PRINT*
    ENDDO
    STOP
  END SUBROUTINE PRINT

  SUBROUTINE test_geom()
    IMPLICIT NONE
    TYPE(element):: e
    REAL(dp), DIMENSION(2)::a,b,c
    PRINT*, "test orientation"
    DO jt=1, Mesh%nt
       e=Mesh%e(jt)
       IF (e%nvertex==3)THEN
          a=e%coor(:,2)-e%coor(:,1)
          b=e%coor(:,3)-e%coor(:,1)
          PRINT*, "tri", jt
          PRINT*, (a(1)*b(2)-a(2)*b(1))
          PRINT*
       ELSE
          a=e%coor(:,2)-e%coor(:,1)
          b=e%coor(:,3)-e%coor(:,1)
          PRINT*, "quad", jt
          PRINT*,1,(a(1)*b(2)-a(2)*b(1))
          a=e%coor(:,3)-e%coor(:,1)
          b=e%coor(:,4)-e%coor(:,1)
          PRINT*, 2, (a(1)*b(2)-a(2)*b(1))
          PRINT*
       ENDIF
    ENDDO
    STOP
  END SUBROUTINE test_geom

END PROGRAM main
