!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE frontiere_class
  USE PRECISION
  IMPLICIT NONE
  !assumes 1D frontier elements
  TYPE, PUBLIC:: frontiere
     INTEGER:: nsommets, itype_b, nvertex! nbre de dofs, type element:
     !1->P1
     !2->B2
     !3->P2
     !4->P3
     !5->B3
     ! nombre de sommet dans cet element arete
     LOGICAL:: bord
     INTEGER:: jt1! les deux elements de par et d'autre de l'arete.
     INTEGER:: nvertex_jt1! nbr of vertices of the element jt1 (quad or tri)
     INTEGER:: is1, is2  ! numero (local) dans jt1 de les deux elements de par et d'autre de l'arete.
     INTEGER:: edge(2) ! numero des edges dans les elements jt1,et jt2
     INTEGER, DIMENSION(:),  POINTER :: nu =>NULL()  ! nu( indice des elements, indice des voisins): on cherche a connaitre le numero local des
     ! points communs (puisque le maillage est confome)  aux deux elements sur cette face dans chacun des elements jt1 et jt2
     REAL(DP), DIMENSION(:,:),   POINTER :: coor  =>NULL()  ! il s'agit des coordonnees physique des dofs (communs) sur la face (commune)
     REAL(DP)                            :: volume=0._dp
     REAL(DP), DIMENSION(:),     POINTER :: n   =>NULL()    ! normales exterieures
     REAL(dp), DIMENSION(:,:)  , POINTER :: y => NULL() !(For kinetic momentum preservation)
     INTEGER                         :: log    ! logique
     INTEGER                         :: bc_tag ! boundary marker
!!!!   quadrature de surface !
     REAL(DP),   DIMENSION(:,:), POINTER :: quad  =>NULL()  ! point de quadrature
     REAL(DP),   DIMENSION(:),   POINTER :: weight =>NULL() ! poids
     INTEGER                         :: nquad =0 ! nbre de points de quadrature
!!! quadrature bord (dimension -1)
     REAL(DP),   DIMENSION(:,:), POINTER :: quad_1   =>NULL() ! point de quadrature
     REAL(DP),   DIMENSION(:),   POINTER :: weight_1  =>NULL()! poids
     INTEGER                         :: nquad_1=0  ! nbre de points de quadrature
!!!
   CONTAINS
     PROCEDURE, PUBLIC:: aire=>aire_frontiere
     PROCEDURE, PUBLIC:: quadrature=>quadrature_frontiere
     PROCEDURE, PUBLIC:: normale=>normale_frontiere
     PROCEDURE, PUBLIC:: base=>base_element_frontiere
     PROCEDURE, PUBLIC:: gradient=>gradient_element_frontiere
     FINAL:: clean_frontiere
  END TYPE frontiere

CONTAINS

  REAL(DP) FUNCTION aire_frontiere(e)
    CLASS(frontiere), INTENT(in):: e
    REAL(DP), DIMENSION(2):: a
    a= e%coor(:,2)-e%coor(:,1)

    aire_frontiere=SQRT(a(1)**2+ a(2)**2 )
  END FUNCTION aire_frontiere

  FUNCTION normale_frontiere(e) RESULT(n)
    CLASS(frontiere), INTENT(in)::e
    REAL(DP), DIMENSION(2):: n
    INTEGER:: l, k1, k2

    n(1)=e%coor(2,2)-e%coor(2,1)
    n(2)=e%coor(1,1)-e%coor(1,2)

  END FUNCTION normale_frontiere

  REAL(DP) FUNCTION base_element_frontiere(e,k,t) ! basis functions
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name ="base_element_frontiere"
    CLASS(frontiere), INTENT(in):: e
    INTEGER, INTENT(in):: k ! index of basis function
    REAL(DP), DIMENSION(2):: x ! barycentric coordinate
    REAL(dp), DIMENSION(2), INTENT(in):: t
    !print*,'e%itype_b = ',e%itype_b
    !print*,'k = ', k
    SELECT CASE(e%nvertex_jt1)
    CASE(3,4)
       x=t!???
    CASE(-4)
       x=1._dp-t!????
    CASE default
       PRINT*, mod_name
       PRINT*, 'wrong element'
       STOP
    END SELECT
    SELECT CASE(e%itype_b)
    CASE(1) ! P1
       SELECT CASE(k)
       CASE(1)
          base_element_frontiere=x(1)
       CASE(2)
          base_element_frontiere=x(2)
       CASE default
          PRINT*, "P1 frontiere, numero base ", k
          STOP
       END SELECT
    CASE(2) ! B2 Bezier
       SELECT CASE(k)
       CASE(1)
          base_element_frontiere=x(1)*x(1)
       CASE(2)
          base_element_frontiere=x(2)*x(2)
       CASE(3)
          base_element_frontiere=2._dp*x(1)*x(2)

       CASE default
          PRINT*, "B2 frontiere, numero base ", k
          STOP
       END SELECT
    CASE(3)! P2
       SELECT CASE(k)
       CASE(1)
          base_element_frontiere=(1.0_dp-2._dp*x(1))*x(2)
          !base_element_frontiere=x(1)
       CASE(2)
          base_element_frontiere=(1.0_dp-2._dp*x(2))*x(1)
          !base_element_frontiere=x(2)
       CASE(3)
          base_element_frontiere=4._dp*x(1)*x(2)
          !base_element_frontiere=0.0
       CASE default
          PRINT*, "P2 frontiere, numero base ", k
          STOP
       END SELECT
    CASE(4)! P3
       SELECT CASE(k)
       CASE(1)
          base_element_frontiere=0.5_dp*x(1)*(3.0_dp*x(1)-1.0_dp)*(3.0_dp*x(1)-2.0)
       CASE(2)
          base_element_frontiere=4.5_dp*x(1)*x(2)*(3.0_dp*x(1)-1.0)
       CASE(3)
          base_element_frontiere=4.5_dp*x(1)*x(2)*(3.0_dp*x(2)-1.0)
       CASE(4)
          base_element_frontiere=0.5_dp*x(2)*(3.0_dp*x(2)-1.0_dp)*(3.0_dp*x(2)-2.0_dp)
       CASE default
          PRINT*, "P3 frontiere, numero base ", k
          STOP
       END SELECT
    CASE(5) ! B3
       SELECT CASE(k)
       CASE(1,2)
          base_element_frontiere=x(k)**3
       CASE(3)
          base_element_frontiere=3.0_dp*x(1)*x(1)*x(2)
       CASE(4)
          base_element_frontiere=3.0_dp*x(1)*x(2)*x(2)
       CASE default
          PRINT*, "B3 frontiere, numero base ", k
          STOP
       END SELECT
    CASE default
       PRINT*, "Type non existant", e%itype_b
       STOP
    END SELECT
  END FUNCTION base_element_frontiere

  FUNCTION gradient_element_frontiere(e,k,x) RESULT (grad) ! gradient in reference element
    CLASS(frontiere), INTENT(in):: e
    INTEGER, INTENT(in):: k ! numero de la fonction de base
    REAL(DP), DIMENSION(2), INTENT(in):: x ! coordonnees barycentriques
    REAL(DP),DIMENSION(2):: grad
    REAL(DP):: fx,fy
    SELECT CASE(e%itype_b)
    CASE(1)! P1
       SELECT CASE(k)
       CASE(1)
          fx=1.0_dp; fy=0.0_dp;
       CASE(2)
          fx=0.0_dp; fy=1.0_dp;
       END SELECT
    CASE(2) !B2
       SELECT CASE(k)
       CASE(1)
          fx=2._dp*x(1);fy=0.0_dp    ;
       CASE(2)
          fx=0._dp      ;fy=2._dp*x(2);
       CASE(3)
          fx=2._dp*x(2);fy=2._dp*x(1);
       END SELECT

    CASE(3) ! P2
       SELECT CASE(k)
       CASE(1)
          fx=-2._dp*x(2)  ;fy=1._dp-2._dp*x(1);
       CASE(2)
          fx=1._dp-2._dp*x(2);fy=-2._dp*x(1);
       CASE(3)
          fx=4._dp*x(2);fy=4._dp*x(1);
       END SELECT
    CASE default
       PRINT*, "gradient_element_frontiere: type non existant", e%itype_b
       STOP
    END SELECT

    grad= (fx-fy)/e%volume  ! CHECK !!!
  END FUNCTION gradient_element_frontiere


  SUBROUTINE quadrature_frontiere(e)
    CLASS(frontiere), INTENT(inout):: e
    REAL(DP):: w,zo,xo,s
    INTEGER:: nquad

    !    e%nquad=1
    !    nquad=e%nquad
    !    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
    !
    !    e%quad(1,1)=0.5_dp
    !    e%quad(2,1)=0.5_dp
    !    e%weight(1)=1.0_dp

    !    e%nquad=2
    !    nquad=e%nquad
    !    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
    !
    !    e%quad(1,1)=1.0_dp
    !    e%quad(2,1)=0.0_dp
    !    e%weight(1)=0.25_dp
    !    e%quad(1,2)=0.0_dp
    !    e%quad(2,2)=1.0_dp
    !    e%weight(2)=0.25_dp

!!$        e%nquad=2
!!$        nquad=e%nquad
!!$        ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
!!$    
!!$        s = SQRT(1._dp/3_dp.)
!!$        e%quad(1,1)=0.5_dp*(1.0_dp - s)
!!$        e%quad(2,1)=0.5_dp*(1.0_dp + s)
!!$        e%weight(1)=0.5_dp
!!$        e%quad(1,2)=0.5_dp*(1.0_dp + s)
!!$        e%quad(2,2)=0.5_dp*(1.0_dp - s)
!!$        e%weight(2)=0.5_dp
    NULLIFY(e%quad, e%weight)
    e%nquad=3
    nquad=e%nquad
    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
    s=SQRT(0.6_dp)
    e%quad(1,1)=0.5_dp*(1.0_dp - s)
    e%quad(2,1)=0.5_dp*(1.0_dp + s)
    e%weight(1) = 5.0_dp/18.0_dp
    e%quad(1,2)=0.5_dp*(1.0_dp + s)
    e%quad(2,2)=0.5_dp*(1.0_dp - s)
    e%weight(2) = 5.0_dp/18.0_dp
    e%quad(1,3)=0.5_dp
    e%quad(2,3)=0.5_dp
    e%weight(3)=8.0_dp/18.0_dp

    !    e%nquad=5
    !    nquad=e%nquad
    !    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
    !
    !    s=1._dp/3._dp*SQRT(5._dp-2._dp*SQRT(10._dp/7._dp))
    !    e%quad(1,1)=0.5_dp*(1.0_dp - s)
    !    e%quad(2,1)=0.5_dp*(1.0_dp + s)
    !    e%weight(1) = (322._dp+13._dp*SQRT(70._dp))/900._dp
    !    e%quad(1,2)=0.5_dp*(1.0_dp + s)
    !    e%quad(2,2)=0.5_dp*(1.0_dp - s)
    !    e%weight(2) = (322._dp+13._dp*SQRT(70._dp))/900._dp
    !
    !    s=1._dp/3._dp*SQRT(5._dp+2._dp*SQRT(10._dp/7._dp))
    !    e%quad(1,3)=0.5_dp*(1.0_dp - s)
    !    e%quad(2,3)=0.5_dp*(1.0_dp + s)
    !    e%weight(3) = (322._dp-13._dp*SQRT(70._dp))/900._dp
    !    e%quad(1,4)=0.5_dp*(1.0_dp + s)
    !    e%quad(2,4)=0.5_dp*(1.0_dp - s)
    !    e%weight(4) = (322._dp-13._dp*SQRT(70._dp))/900._dp
    !
    !    e%quad(1,5)=0.5_dp
    !    e%quad(2,5)=0.5_dp
    !    e%weight(5)=128._dp/225._dp
    !
    !    e%weight = 0.5_dp*e%weight

!!$    e%nquad=7
!!$    nquad=e%nquad
!!$    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
!!$
!!$    s=0.4058451513773972_dp
!!$    e%quad(1,1)=0.5_dp*(1.0_dp - s)
!!$    e%quad(2,1)=0.5_dp*(1.0_dp + s)
!!$    e%weight(1) = 0.3818300505051189_dp
!!$    e%quad(1,2)=0.5_dp*(1.0_dp + s)
!!$    e%quad(2,2)=0.5_dp*(1.0_dp - s)
!!$    e%weight(2) = 0.3818300505051189_dp
!!$
!!$    s=0.7415311855993945_dp
!!$    e%quad(1,3)=0.5_dp*(1.0_dp - s)
!!$    e%quad(2,3)=0.5_dp*(1.0_dp + s)
!!$    e%weight(3) = 0.2797053914892766_dp
!!$    e%quad(1,4)=0.5_dp*(1.0_dp + s)
!!$    e%quad(2,4)=0.5_dp*(1.0_dp - s)
!!$    e%weight(4) = 0.2797053914892766_dp
!!$
!!$    s=0.9491079123427585_dp
!!$    e%quad(1,5)=0.5_dp*(1.0_dp - s)
!!$    e%quad(2,5)=0.5_dp*(1.0_dp + s)
!!$    e%weight(5) = 0.1294849661688697_dp
!!$    e%quad(1,6)=0.5_dp*(1.0_dp + s)
!!$    e%quad(2,6)=0.5_dp*(1.0_dp - s)
!!$    e%weight(6) = 0.1294849661688697_dp
!!$
!!$    e%quad(1,7)=0.5_dp
!!$    e%quad(2,7)=0.5_dp
!!$    e%weight(7)=0.4179591836734694_dp




    !    e%nquad=4
    !    nquad=e%nquad
    !    ALLOCATE(e%quad(2,e%nquad),e%weight(e%nquad))
    !
    !    s=sqrt( (15.0_dp+2._dp*sqrt(30._dp))/35.0_dp)
    !    e%quad(1,1)=0.5_dp*(1.0_dp - s)
    !    e%quad(2,1)=0.5_dp*(1.0_dp + s)
    !    e%weight(1) = 0.5_dp-1._dp/6.0_dp*sqrt(5.0_dp/6.0_dp)
    !    e%quad(1,2)=0.5_dp*(1.0_dp + s)
    !    e%quad(2,2)=0.5_dp*(1.0_dp - s)
    !    e%weight(2) = 0.5_dp-1._dp/6.0_dp*sqrt(5.0_dp/6.0_dp)
    !
    !    s=sqrt( (15.0_dp-2._dp*sqrt(30._dp))/35.0_dp)
    !    e%quad(1,3)=0.5_dp*(1.0_dp - s)
    !    e%quad(2,3)=0.5_dp*(1.0_dp + s)
    !    e%weight(3) = 0.5_dp+1._dp/6.0_dp*sqrt(5.0_dp/6.0_dp)
    !    e%quad(1,4)=0.5_dp*(1.0_dp + s)
    !    e%quad(2,4)=0.5_dp*(1.0_dp - s)
    !    e%weight(4) = 0.5_dp+1._dp/6.0_dp*sqrt(5.0_dp/6.0_dp)
    !
    !    e%weight = 0.5_dp*e%weight

    e%weight = e%weight/SUM(e%weight)
  END SUBROUTINE quadrature_frontiere

  SUBROUTINE clean_frontiere(e)
    TYPE(frontiere):: e

    IF (ASSOCIATED(e%nu))      NULLIFY(e%nu)
    IF (ASSOCIATED(e%coor))    NULLIFY(e%coor)
    IF (ASSOCIATED(e%n))       NULLIFY(e%n)
    IF (ASSOCIATED(e%quad))    NULLIFY(e%quad)
    IF (ASSOCIATED(e%weight))  NULLIFY(e%weight)
    IF (ASSOCIATED(e%quad_1))  NULLIFY(e%quad_1)
    IF (ASSOCIATED(e%weight_1))NULLIFY(e%weight_1)

  END SUBROUTINE clean_frontiere
END MODULE frontiere_class
