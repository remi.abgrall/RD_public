!!!  HIGH ORDER IN SPACE AND TIME DEFERRED CORRECTION (EXPLICIT) 
!!!     RESIDUAL DISTRIBUTION METHOD 
!!!  DESIGNED FOR THE SYSTEM GIVEN BY THE EULER EQUATIONS in 1D and 2D
!!!
!!!  Authors:
!!!  Remi Abgrall (University of Zurich),
!!!  Paola Bacigaluppi (University of Zurich),
!!!  Svetlana Tokareva (University of Zurich)
!!!  Institute of Mathematics and Institute of Computational Sciences
!!!  University of Zurich
!!!  July 10, 2018
!!!  Correspondance:	remi.abgrall@math.uzh.ch
!!!  ------------------------------------------
MODULE variable_def
  !------------------------------------------------------------------------------------------------
  ! MODULE SPECIFICALLY DESIGNED FOR 1D SYSTEM OF EULER EQUATIONS
  !------------------------------------------------------------------------------------------------
  ! This module collects all the information related to the following items:
  ! - equations of state: pressure, internal specific energy, speed of sound
  ! - conversion from conservative to primitive and viceversa
  ! - definition of the Jacobian of the considered system
  ! - definition of the Fluxes of the considered system
  ! - definition of the Jacobian in absolute values as: |J|=R* |lambda| *L (cf. AbsJacobian_eul)
  ! - definition of the Eigenvalues (cf. evalues_eul)
  ! - defintion of the Spectral Radius = max ( |lambda|)
  ! - definition of the Right eigenvectors
  ! - definition of the Left eigenvectors
  ! - definition of the positive and negative Jacobian

  USE algebra
  USE PRECISION
  IMPLICIT NONE
  INTEGER, PUBLIC    , PARAMETER:: n_vars = 4   ! number of primitive variables
  INTEGER, PUBLIC    , PARAMETER:: n_vars1 = 1
  INTEGER, PUBLIC    , PARAMETER:: n_dim  = 2   ! number of physical dimensions
  REAL(DP),            PARAMETER:: pi=ACOS(-1._dp) ! pi
  REAL(DP),    PUBLIC, PARAMETER:: gmm=1.4_dp, xka=gmm-1._dp     ! EOS parameters


  ! Type for the vector of primitive variables
  TYPE, PUBLIC :: PVar
     INTEGER                                  :: NVars = n_vars
     REAL(DP), DIMENSION(n_vars)              :: U

   CONTAINS 
     PROCEDURE, PUBLIC:: flux            => flux_eul
     PROCEDURE, PUBLIC:: evalues         => evalues_eul
     PROCEDURE, PUBLIC:: spectral_radius => spectral_radius_eul
     PROCEDURE, PUBLIC:: rvectors        => rvectors_eul
     PROCEDURE, PUBLIC:: lvectors        => lvectors_eul
     PROCEDURE, PUBLIC:: Jacobian        => Jacobian_eul
     PROCEDURE, PUBLIC:: Jacobian_n        => Jacobian_n_eul
     PROCEDURE, PUBLIC:: Nmat            => Nmat_eul
     PROCEDURE, PUBLIC:: min_mat         => min_mat_eul
     PROCEDURE, PUBLIC:: max_mat         => max_mat_eul
     !     PROCEDURE, PUBLIC:: entropy_var     => entropy_var_eul
     PROCEDURE, PUBLIC:: cons2prim        =>convert_cons2prim
     PROCEDURE, PUBLIC:: prim2cons        =>convert_prim2cons
     PROCEDURE, PUBLIC:: peos            => peos_eul
     PROCEDURE, PUBLIC:: eEOS            => eEOS_eul
     procedure, public:: soundSpeed      => soundSpeed_eul
     PROCEDURE, PUBLIC:: entropy2cons        =>convert_entropy2cons
     PROCEDURE, PUBLIC:: cons2entropy        =>convert_cons2entropy
     PROCEDURE, PUBLIC:: entropy_flux     => entropy_flux_eul  !!! New variables in variable deff
     PROCEDURE, PUBLIC:: entropy_function => entropy_function_eul
     PROCEDURE, PUBLIC:: Hessian_Entropy=>Hessian_Entropy_eul
     !     PROCEDURE, PUBLIC:: roe             => roe_eul
  END TYPE PVar


  PRIVATE

CONTAINS

  !--------------------------
  ! p = EOS(rho,eps)
  !--------------------------
  FUNCTION pEOS_eul(e) RESULT (p)
    IMPLICIT NONE
    CLASS(Pvar), INTENT(in):: e
    REAL(dp):: p

    p=xka*( e%u(4)-0.5_dp*SUM(e%u(2:3)**2)/e%u(1) )
  END FUNCTION peos_eul

  !--------------------------
  ! eps = EOS(rho,p)
  !--------------------------
  FUNCTION eEOS_eul(e,ro,p) RESULT (ener)
    IMPLICIT NONE
    CLASS(Pvar), INTENT(in):: e
    REAL(dp), INTENT(in):: ro, p
    REAL(dp):: ener
    ener=p/xka
  END FUNCTION eEOS_eul

  !--------------------------
  ! p = dEOS(rho,eps)/d(rho)
  !--------------------------
  FUNCTION pEOS_rho(rho,eps) RESULT(p)
    IMPLICIT NONE
    REAL(DP), INTENT(in) :: rho, eps
    REAL(DP)             :: p
    p = (gmm-1._dp)*eps
  END FUNCTION pEOS_rho

  !--------------------------
  ! p = dEOS(rho,eps)/d(eps)
  !--------------------------
  FUNCTION pEOS_rhoe(rho,eps) RESULT(p)
    IMPLICIT NONE
    REAL(DP), INTENT(in) :: rho, eps
    REAL(DP)             :: p
    p = (gmm-1._dp)*rho
  END FUNCTION pEOS_rhoe

  !-------------------------
  ! Derivatives from RealFluid
  !-------------------------
  FUNCTION EOS_d_PI_rho(Q) RESULT(dpp)
    IMPLICIT NONE
    TYPE(Pvar), INTENT(in) :: Q
    REAL(DP)                   :: dpp
    dpp = (gmm-1._dp)*0.5_dp*(SUM(Q%U(2:3)**2))/Q%U(1)**2
  END FUNCTION EOS_d_PI_rho

  FUNCTION EOS_d_PI_E(Q) RESULT(dpp)
    IMPLICIT NONE
    TYPE(Pvar), INTENT(in) :: Q
    REAL(DP)                   :: dpp
    dpp = gmm-1._dp
  END FUNCTION EOS_d_PI_E


  !--------------------------
  ! Speed of sound
  !--------------------------
  FUNCTION SoundSpeed_eul(Var) RESULT(a)
    ! Var: conservative
    IMPLICIT NONE
    class(Pvar), intent(in):: Var
    type(Pvar):: Q
    REAL(DP)             :: a
    Q=convert_cons2prim(Var)
    a = SQRT(gmm*Q%u(4)/Q%u(1))
  END FUNCTION SoundSpeed_eul


  !---------------------------------------------
  ! Convert conservative variables to primitive
  !---------------------------------------------
  FUNCTION convert_cons2prim(Q) RESULT(W)
    IMPLICIT NONE
    class(Pvar), INTENT(in) :: Q
    TYPE(Pvar)             :: W
    REAL(DP)                   :: eps
    ! rho
    W%U(1) = Q%U(1)
    ! u
    W%U(2) = Q%U(2)/Q%U(1)
    ! v
    W%U(3) = Q%U(3)/Q%U(1)
    ! p
    eps    = Q%U(4) - 0.5_dp*( Q%U(2)**2 + Q%U(3)**2 )/Q%U(1)
    W%U(4) =   pEOS_eul(Q)
  END FUNCTION convert_cons2prim

  !---------------------------------------------
  ! Convert primitive variables to conservative
  !---------------------------------------------
  FUNCTION convert_prim2cons(W) RESULT(Q)
    IMPLICIT NONE
    class(Pvar), INTENT(in) :: W
    TYPE(Pvar)             :: Q
    REAL(DP)                   :: eps
    ! q1 = rho
    Q%U(1) = W%U(1)
    ! q2 = rho*u
    Q%U(2) = W%U(1)*W%U(2)
    ! q3 = rho*u
    Q%U(3) = W%U(1)*W%U(3)
    ! q4 = E = rho*eps + 0.5*rho*u^2
    eps    = eEOS_eul(W, W%U(1),W%U(4))
    Q%U(4) = W%U(1)*eps + 0.5_dp*W%U(1)*( W%U(2)**2 + W%U(3)**2 )
  END FUNCTION convert_prim2cons


!!!!-----------------------


  FUNCTION roe_eul(e,u,n) RESULT (J)
    IMPLICIT NONE
    ! evaluate a roe average to estimate a rough speed for 
    ! Burman jump operator
    CLASS(Pvar), INTENT(in):: e
    REAL(DP),DIMENSION(:), INTENT(in):: u
    REAL(DP), DIMENSION(:), INTENT(in):: n ! here it will be a normal of norme 1
    REAL(DP), DIMENSION(n_vars,n_vars):: J
    REAL(DP), DIMENSION(n_vars,n_vars,n_dim):: JJ
    REAL(DP),DIMENSION(n_dim):: v=0._dp
    JJ=Jacobian_eul(e,v)
    J(:,:) = JJ(:,:,1)*n(1)+JJ(:,:,2)*n(2)

  END FUNCTION roe_eul

  FUNCTION flux_eul(Var,x) RESULT(f)
    IMPLICIT NONE
    CLASS(PVar),                  INTENT(in) :: Var    ! vector of conservative variables
    REAL(DP),       DIMENSION(:), INTENT(in), OPTIONAL :: x
    type(Pvar), dimension(n_dim):: f
    real(DP), DIMENSION(n_dim)             :: u
    REAL(DP) :: h, p

    !    eps = Var%u(4)/Var%u(1) - 0.5_dp*( Var%u(2)**2 + Var%u(3)**2 )/Var%u(1)**2
    p = pEOS_eul(var)
    h=( Var%u(4) + p )!/Var%u(1)
    u=Var%u(2:1+n_dim)/Var%u(1)

    F(1)%u(1) = Var%u(2)
    F(1)%u(2) = Var%u(2)*u(1) + p
    F(1)%u(3) = Var%u(2)*u(2)
    F(1)%u(4) = u(1)*h

    F(2)%u(1) = Var%u(3)
    F(2)%u(2) = Var%u(3)*u(1)
    F(2)%u(3) = Var%u(3)*u(2) + p
    F(2)%u(4) = u(2)*h

  END FUNCTION flux_eul

  FUNCTION fluxg_eul(Var,x) RESULT(g)
    IMPLICIT NONE
    CLASS(PVar),                  INTENT(in) :: Var    ! vector of conservative variables
    REAL(DP),       DIMENSION(:), INTENT(in) :: x
    TYPE(PVar), DIMENSION(n_dim)             :: g
    REAL(DP) :: eps, p

    eps = Var%u(4)/Var%u(1) - 0.5_dp*( Var%u(2)**2 + Var%u(3)**2 )/Var%u(1)**2
    p = pEOS_eul(Var)
    g(1)%u(1)=Var%u(2)*Var%u(3)*x(1)/Var%u(1)-x(2)*(Var%u(2)**2/Var%u(1) + p)
    g(2)%u(1)=x(1)*(Var%u(3)**2/Var%u(1) + p)-Var%u(2)*Var%u(3)*x(2)/Var%u(1)

  END FUNCTION fluxg_eul

  FUNCTION new_cons(W,x) RESULT(Q)
    IMPLICIT NONE
    CLASS(PVar), INTENT(in) :: W
    REAL(DP),       DIMENSION(:), INTENT(in) :: x
    TYPE(Pvar)            :: Q

    Q%U(1) = W%U(3)*x(1)-W%U(2)*x(2)
  END FUNCTION new_cons

  FUNCTION Jacobian_n_eul(e,x,n) RESULT(Ap)
    IMPLICIT NONE
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(:), INTENT(in) :: x

    REAL(DP), DIMENSION(:), INTENT(in) :: n
    REAL(DP), DIMENSION(N_vars, N_vars):: Ap
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: lambda, diag
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: R
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: L
    Ap=0._dp

    lambda = evalues_eul(e,x,n)


    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( lambda(:,:), L) )


  END FUNCTION Jacobian_n_eul

    FUNCTION Jacobian_eul(Var,x) RESULT(J)
    IMPLICIT NONE
    CLASS(Pvar),              INTENT(in) :: Var
    REAL(DP), DIMENSION(:),   INTENT(in), OPTIONAL :: x
    REAL(DP), DIMENSION(n_Vars,n_Vars,n_dim) :: J
    REAL(DP) :: Vi2, u, v, H, eps, p

    eps = Var%u(4)/Var%u(1) - 0.5_dp*( Var%u(2)**2 + Var%u(3)**2 )/Var%u(1)**2
    p = pEOS_eul(Var)

    u = Var%u(2)/Var%u(1)
    v = Var%u(3)/Var%u(1)

    Vi2 = u**2 + v**2

    H = (Var%u(4) + p)/Var%u(1)

    J=0._dp
    J(1,:,1) = (/ 0._dp, 1._dp, 0._dp, 0._dp /)
    J(2,:,1) = (/ -u**2 + 0.5_dp*(gmm-1._dp)*Vi2, (3._dp-gmm)*u, (1._dp-gmm)*v, gmm-1._dp /)
    J(3,:,1) = (/ -u*v, v, u, 0._dp /)
    J(4,:,1) = (/ u*( 0.5_dp*(gmm-1._dp)*Vi2 - H ), H - (gmm-1._dp)*u**2, (1._dp-gmm)*u*v, gmm*u /)

    J(1,:,2) = (/ 0._dp, 0._dp, 1._dp, 0._dp /)
    J(2,:,2) = (/ -u*v, v, u, 0._dp /)
    J(3,:,2) = (/ -v**2 + 0.5_dp*(gmm-1._dp)*Vi2, (1._dp-gmm)*u, (3._dp-gmm)*v, gmm-1._dp /)
    J(4,:,2) = (/ v*( 0.5_dp*(gmm-1._dp)*Vi2 - H ), (1._dp-gmm)*u*v, H - (gmm-1._dp)*v**2, gmm*v /)


  END FUNCTION Jacobian_eul

  FUNCTION evalues_eul(Var,x,n) RESULT(lambda)
    IMPLICIT NONE
    ! eigenvalues: diagonal matrix. It is written as a matrix for ease of calculations
    CLASS(PVar),            INTENT(in) :: Var
    REAL(DP), DIMENSION(:), INTENT(in), OPTIONAL :: x
    REAL(DP), DIMENSION(n_dim)             :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda

    REAL(DP), DIMENSION(n_vars, n_vars, n_dim) :: J
    REAL(DP)    :: un, c, p, eps, mod_n

    eps = Var%u(4)/Var%u(1) - 0.5_dp*( Var%u(2)**2 + Var%u(3)**2 )/Var%u(1)**2
    p   = pEOS_eul(Var)
    c   = var%SoundSpeed()

    mod_n = SQRT(SUM(n*n))

    lambda = 0._dp
    un = ( Var%u(2)*n(1) + Var%u(3)*n(2) )/Var%u(1)
    lambda(1,1) = un-c*mod_n
    lambda(2,2) = un
    lambda(3,3) = un+c*mod_n
    lambda(4,4) = un

  END FUNCTION evalues_eul

  REAL(DP) FUNCTION spectral_radius_eul(Var,x,n)
    IMPLICIT NONE
    CHARACTER(LEN = *), PARAMETER :: mod_name = "spectral_radius_eul"
    ! compute the maximum value of eigenvalues:
    ! max_i {lambda_ii}
    CLASS(PVar),            INTENT(in) :: Var
    REAL(DP), DIMENSION(:), INTENT(in), OPTIONAL :: x
    REAL(DP), DIMENSION(:), INTENT(in) :: n
    REAL(DP), DIMENSION(n_Vars,n_Vars)     :: lambda1, lambda2
    REAL(DP):: vi, c, p, eps

    eps = Var%u(4)/Var%u(1) - 0.5_dp*( Var%u(2)**2 + Var%u(3)**2 )/Var%u(1)**2
    p   = pEOS_eul(Var)
    c   = var%SoundSpeed()

    vi=SQRT(SUM(Var%u(2:3)**2)/Var%u(1)**2)
    spectral_radius_eul =SQRT(SUM(n(:)**2))*(vi+c)

  END  FUNCTION spectral_radius_eul

  FUNCTION rvectors_eul(Var,v_nn) RESULT(RR)
    IMPLICIT NONE
    ! right e-vectors
    CLASS(PVar),            INTENT(in) :: Var
    REAL(DP), DIMENSION(:), INTENT(in)       :: v_nn
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: RR

    REAL(DP), DIMENSION(SIZE(v_nn)) :: uu
    REAL(DP), DIMENSION(SIZE(v_nn)) :: nn, nn_p, nn_t

    REAL(DP) :: ek, u_n, u_p, ht, cc, pi_rho, pi_E, xi
    REAL(DP) :: rho, E, p, eps, u, v

    nn  = v_nn / SQRT( SUM(v_nn*v_nn) )                             ! > Unit vector
    uu  = Var%U(2:1+n_dim)/Var%U(1)                                       ! > u vector

    u_n = SUM(uu*nn)                                                ! > u.n

    ek = 0.5_dp * SUM(uu**2)                                         ! > Kinetic Energy

    !EOS
    rho = Var%U(1)
    u   = Var%U(2)/Var%U(1)
    v   = Var%U(3)/Var%U(1)
    eps = Var%u(4)/Var%u(1) - 0.5_dp*( SUM(Var%u(2:3)**2) )/Var%u(1)**2
    p   = pEOS_eul(Var)
    cc   = Var%SoundSpeed()
    E   = Var%U(4)
    ht   = (E+p)/rho                                                ! > Total Enthalpy

    nn_p = 0._dp
    nn_p(1) = nn(2); nn_p(2) = -nn(1)                               ! > nn_p.nn = 0
    u_p = SUM(uu*nn_p)                                              ! > u_p = u.nn_p

    pi_rho = EOS_d_PI_rho(Var)                                      ! > Pressure Derivative // rho
    pi_E   = EOS_d_PI_E(Var)                                        ! > Pressure Derivative // rho*E
    xi  = 2._dp*ek - pi_rho/pi_E

    RR = 0._dp

    RR(1,         1) = 1._dp
    RR(2:N_dim+1, 1) = uu - cc*nn
    RR(4,         1) = ht - cc*u_n

    RR(1,         2) = 1._dp
    RR(2:N_dim+1, 2) = uu
    RR(4,         2) = xi

    RR(1,         3) = 1._dp
    RR(2:N_dim+1, 3) = uu + cc*nn
    RR(4,         3) = ht + cc*u_n

    RR(1,         4) = 0._dp
    RR(2:N_dim+1, 4) = nn_p
    RR(4,         4) = u_p

  END FUNCTION rvectors_eul

  FUNCTION lvectors_eul(Var,v_nn) RESULT(LL)
    IMPLICIT NONE
    ! left e-vectors
    CLASS(PVar),         INTENT(in) :: Var
    REAL(DP), DIMENSION(n_dim)          :: v_nn
    REAL(DP), DIMENSION(n_Vars,n_Vars)  :: LL

    REAL(DP), DIMENSION(SIZE(v_nn)) :: uu
    REAL(DP), DIMENSION(SIZE(v_nn)) :: nn, nn_p, nn_t

    REAL(DP) :: ek, u_n, u_p, ht, cc, pi_rho, pi_E, xi
    REAL(DP) :: rho, E, p, eps, u, v

    nn  = v_nn / SQRT( SUM(v_nn*v_nn) )                             ! > Unit vector
    uu  = Var%U(2:3)/Var%U(1)                                       ! > u vector

    u_n = SUM(uu*nn)                                                ! > u.n

    ek = 0.5_dp * SUM(uu**2)                                         ! > Kinetic Energy

    !EOS
    rho = Var%U(1)
    u   = Var%U(2)/Var%U(1)
    v   = Var%U(3)/Var%U(1)
    eps = Var%u(4)/Var%u(1) - 0.5_dp*( SUM(Var%u(2:3)**2) )/Var%u(1)**2
    p   = pEOS_eul(Var)
    cc   = Var%SoundSpeed()
    E   = Var%U(4)
    ht   = (E+p)/rho                                                ! > Total Enthalpy

    nn_p = 0._dp
    nn_p(1) = nn(2); nn_p(2) = -nn(1)                               ! > nn_p.nn = 0
    u_p = SUM(uu*nn_p)                                              ! > u_p = u.nn_p

    pi_rho = EOS_d_PI_rho(Var)                                      ! > Pressure Derivative // rho
    pi_E   = EOS_d_PI_E(Var)                                        ! > Pressure Derivative // rho*E
    xi  = 2._dp*ek - pi_rho/pi_E

    LL = 0._dp

    LL(1, 1)         =  cc*u_n + pi_rho
    LL(1, 2:N_dim+1) = -cc*nn  - pi_E*uu
    LL(1, 4)         =  pi_E

    LL(2, 1)         = -2._dp * (pi_rho - cc*cc)
    LL(2, 2:N_dim+1) =  2._dp *  pi_E*uu
    LL(2, 4)         = -2._dp *  pi_E

    LL(3, 1)         = -cc*u_n + pi_rho
    LL(3, 2:N_dim+1) =  cc*nn  - pi_E*uu
    LL(3, 4)         =  pi_E

    LL(4, 1)         = -2._dp*cc*cc * u_p
    LL(4, 2:N_dim+1) =  2._dp*cc*cc * nn_p
    LL(4, 4)         =  0._dp

    LL = LL / (2._dp * (cc*cc) )

  END FUNCTION lvectors_eul



  FUNCTION Nmat_eul(e, n_ord, grad, x, theta) RESULT (Nmat)
    IMPLICIT NONE
    CHARACTER(LEN = *),                PARAMETER :: mod_name = "Nmat_eul"
    REAL(dp), OPTIONAL,               INTENT(in) :: theta
    CLASS(Pvar),                      INTENT(in) :: e
    INTEGER,                          INTENT(in) :: n_ord
    REAL(DP), DIMENSION(n_dim),       INTENT(in) :: x
    REAL(DP), DIMENSION(n_dim,n_ord), INTENT(in) :: grad
    real(dp):: alpha=0._dp
    REAL(DP), DIMENSION(n_vars,n_vars)           :: Nmat, R, L, lambda
    INTEGER:: ll
    Nmat=0._dp
    IF (PRESENT(theta)) THEN
       DO ll=1, n_ord
          Nmat = Nmat + max_mat_eul(e, x, alpha, grad(:,ll), theta)
       ENDDO
    ELSE
       DO ll=1, n_ord
          Nmat = Nmat + max_mat_eul(e, x, alpha, grad(:,ll))
       ENDDO
    ENDIF

    Nmat =Inverse(Nmat)

  END FUNCTION Nmat_eul

  FUNCTION max_mat_eul(e, x,alpha,  n, dt) RESULT (Ap)
    IMPLICIT NONE
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),            INTENT(in) :: e
    REAL(DP), DIMENSION(:), INTENT(in) :: x

    REAL(dp), OPTIONAL,      INTENT(in):: dt
       REAL(DP),                      INTENT(in) :: alpha
    REAL(DP), DIMENSION(:), INTENT(in) :: n
    REAL(DP), DIMENSION(N_vars, N_vars):: Ap
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: lambda, diag
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: R
    REAL(DP), DIMENSION(n_Vars,n_Vars) :: L
    INTEGER                            :: i
    REAL(dp)                           :: beta
    Ap=0._dp

    lambda = evalues_eul(e,x,n)

    diag=myabs(lambda)
    IF (PRESENT(dt)) THEN
       DO i=1, n_vars
          diag(i,i)=diag(i,i)!+ 1._dp/dt 
       ENDDO
    ENDIF
    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( diag(:,:), L) )


  END FUNCTION max_mat_eul

  FUNCTION myabs(lambda) RESULT (diag)
    REAL, PARAMETER :: eps=0.1
    REAL(dp), DIMENSION(:,:)              , INTENT(in):: lambda
    REAL(dp), DIMENSION(SIZE(lambda,1),SIZE(lambda,2)):: diag
    REAL(dp)                                          :: beta
    INTEGER                                           :: i

    diag=0._dp
    beta=MAXVAL(ABS(lambda))*eps
    DO i=1,SIZE(lambda,1)
       IF (ABS(lambda(i,i)).GE.beta) THEN
          diag(i,i)=ABS(lambda(i,i))
       ELSE
          diag(i,i)=( lambda(i,i)**2+beta**2)/(2._dp*beta)
       ENDIF
    ENDDO

  END FUNCTION myabs

  FUNCTION min_mat_eul(e,x,alpha, n,dt) RESULT (Ap)
    IMPLICIT NONE
    ! in this A must be the result of tensor*vec where tensor are
    ! the jacobian evaluated for class e. It computes the negative part of A
    CLASS(PVar),                   INTENT(in) :: e
    REAL(DP), DIMENSION(:),        INTENT(in) :: x
    REAL(DP),                      INTENT(in) :: alpha
    REAL(dp), OPTIONAL,            INTENT(in) :: dt
    REAL(DP), DIMENSION(:),       INTENT(IN)  :: n
    REAL(DP), DIMENSION(N_vars, N_vars)       :: Ap
    REAL(DP), DIMENSION(n_Vars,n_Vars)        :: lambda
    REAL(DP), DIMENSION(n_Vars,n_Vars)        :: R
    REAL(DP), DIMENSION(n_Vars,n_Vars)        :: L
    INTEGER                                   :: i
    REAL(dp)                                  :: beta

    lambda = evalues_eul(e,x,n)
    beta=alpha*MAXVAL(ABS(lambda))
    DO i=1,n_Vars
       lambda(i,i)=MIN(lambda(i,i),beta)
    ENDDO


    R      = rvectors_eul(e,n)
    L      = lvectors_eul(e,n)
    Ap     = MATMUL( R, MATMUL( lambda(:,:), L) )

    IF (PRESENT(dt)) THEN
       DO i=1, n_vars
          Ap(i,i)=Ap(i,i)+1._dp/dt
       ENDDO
    ENDIF

  END FUNCTION min_mat_eul



  !--------------------------

!!!! Entropy function! We choose the convex entropy function 

  FUNCTION entropy_function_eul(V) RESULT(f)
    IMPLICIT NONE
    CLASS(PVar), INTENT(in)	:: V
    TYPE(PVar)				:: sol
    REAL(dp)					:: f,s
!!! Mathematical entropy  eta= -ro*s/gamma-1 with s= log(p/ro**gamma)

    sol = convert_cons2prim(v)
    s = LOG(sol%u(4))-gmm*LOG(sol%U(1))
    f = -sol%U(1)*s/xka !e%U(1)


  END FUNCTION entropy_function_eul

!!! Entropy flux for the Euler equation

  FUNCTION entropy_flux_eul(Var,x) RESULT(F)
    IMPLICIT NONE
    REAL(dp), DIMENSION(n_dim), INTENT(in),OPTIONAL 	:: x
    CLASS(PVar), INTENT(in) 			:: Var
    TYPE(PVar) 			:: W
    REAL(dp), DIMENSION(n_dim)	:: F
    REAL(dp) 				:: eps, p

    ! ------ Initial conversions and auxiliary variables computations ------
    W = convert_cons2prim(Var)
!!! The next two are only needed when we define the entropy flux in terms of concave functions...
!!!	eps = Var%u(4)/Var%u(1) - 0.5*(Var%u(2)**2 + Var%u(3)**2)/Var%u(1)**2
!!!	p = pEOS(Var%u(1),eps)

!!!  Entropy flux function F_x=eta*u_x 
    F(1) = -W%u(1)*W%u(2)*( LOG(W%u(4))-gmm*log(W%u(1)) )/xka!*(1._dp/(gmm-1._dp))
    F(2) = -W%u(1)*W%u(3)*( LOG(W%u(4))-gmm*log(W%u(1)) )/xka!*(1._dp/(gmm-1._dp))


  END FUNCTION entropy_flux_eul

  FUNCTION Hessian_Entropy_eul(V) RESULT(A0)
    IMPLICIT NONE
    CLASS(Pvar), INTENT(in):: V
    REAL(dp), DIMENSION(2+n_dim,2+n_dim):: A0 !  inverse of the hessian of the entropy
    TYPE(PVar):: sol
    REAL(dp):: r,uu, vv,p
    REAL(dp):: t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, &
         & t13, t14, t15, t16, t17, t18, t19, t20
    sol=convert_cons2prim(V)
    r=sol%u(1)
    uu=sol%u(2)
    vv=sol%u(3)
    p=sol%u(4)



    t1 = (vv ** 2)
    t2 = (t1 ** 2)
    t3 = ((xka) * (t1))
    t4 = ((t3) * (r))
    t5 = 2
    t6 = (uu ** 2)
    t7 = ((r) * (t6))
    t8 = ((t7) * (xka))
    t9 = 1._dp / xka
    t10 = t9 ** 2
    t11 = (r) * t9
    t12 = t11 * (uu)
    t13 = t11 * (vv)
    t14 = t10 / 2._dp
    t15 = t14 * (t5 * (P + t4) + t8)
    t16 = r * t1 + P
    t17 = t13 * (uu)
    t3 = (t14 * (t5 * (xka * t16 + P) + xka * r * (t3 + t6) ))
    t14 = (t3 * uu)
    t3 = t3 * vv
    t18 = xka + 1._dp
    t19 = r ** 2 * xka ** 2
    t20 = 4._dp
    A0(1,1) = t11
    A0(1,2) = t12
    A0(1,3) = t13
    A0(1,4) = t15
    A0(2,1) = t12
    A0(2,2) = ((t5 * (P + t7)) / 2._dp + (t4) / 2._dp) * t9
    A0(2,3) = t17
    A0(2,4) = t14
    A0(3,1) = t13
    A0(3,2) = t17
    A0(3,3) = ((t16 * t5) / 0.2D1 + (t4) / 2._dp) * t9
    A0(3,4) = t3
    A0(4,1) = t15
    A0(4,2) = t14
    A0(4,3) = t3
    A0(4,4) = t9 * t10 / (r) * (t20 * (P ** 2 * t18 + t8 * t18 * P + t19 * t6 * t1&
         &+ t19 * t2) + 8 * t4 * P * t18 + t19 * ((t6 * t5 * t1 + 3 * t2) * xka&
         &+ t6 ** 2)) / 4._dp



    A0=A0/xka

  END FUNCTION Hessian_Entropy_eul



  FUNCTION convert_cons2entropy(e) RESULT(v)
    ! in: conservative
    ! out entropy variable
    IMPLICIT NONE
    class(PVar), INTENT(in):: e
    TYPE(PVar):: v
    REAL(dp):: s, rap
    TYPE(pvar):: prim

    ! ------ Initial conversions and auxiliary variables computations ------
    prim=convert_cons2prim(e)

    ! **********************************************************************
    ! First version that goes with the first entropy flux version
    ! **********************************************************************
    rap=prim%u(1)/prim%u(4)
    s=LOG(prim%u(4))-gmm*LOG(prim%u(1))
    v%u(1)=-(s-gmm)/(xka)-0.5_dp*rap* sum(prim%u(2:3)**2)
    v%u(2:3)=e%u(2:3)/prim%u(4)
    v%u(4)=-rap
    ! **********************************************************************

  END FUNCTION convert_cons2entropy


  FUNCTION convert_entropy2cons(W) RESULT(U)
    IMPLICIT NONE
    class(PVar), INTENT(in):: W
    TYPE(Pvar):: U, V, prim
    INTEGER	:: i
    REAL(dp)	:: s, rap

    ! ------ Initial conversions and auxiliary variables computations ------
    DO i=1,n_vars
       V%u(i) = 0.0_dp
       U%u(i) = 0.0_dp
    END DO

!!!!We calculate first the primitive variables u1 has to be checked againg-> I think V(u(1) is wrong have to check it tomorrow!

    V%u(2:3) = -W%u(2:3)/W%u(4)
    V%u(1) = EXP(W%u(1)-0.5_dp*W%u(4)*(sum(V%u(2:3)**2))-gmm/(gmm-1._dp))*(-W%u(4))**(1._dp/gmm-1._dp) !????
    V%u(4) = -V%u(1)/W%u(4)

!!! -------------------- Final conversion --------------------------------
    U=convert_prim2cons(V)

  END FUNCTION convert_entropy2cons




END MODULE variable_def
