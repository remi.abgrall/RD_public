
!> \brief A geometry and graph manipulation library
!!
!! This library contains all the routines related to geometry and graph manipulation
!! - Reading Meshes
!! - Manipulating graphs
!! - Computing geometry : 
!!   - Computing normals
!!   - Computing volumes
MODULE GeomGraph
  USE PRECISION
  USE param2d

  IMPLICIT NONE
  !==============================================================
  !! Ndegre(jt)    ::  type de l'element
  !! Nu(k, jt)     ::  numero du Keme point de l'element jt
  !! Nubo(k, js)   ::  numero du Keme point du segment js
  !! NsfacFr(k, is)::  numero du Keme point du segment frontiere is
  !! Nuseg(k, jt)  ::  numero du Keme segment de l'element jt
  !! Nusv(k, js)   ::  numero du Keme sommet tel qu'avec le
  !!                  segment js, ils forment un element
  !! Nutv(k, js)   ::  numero du Keme element contenant le segment js
  !! NbVois(k)     ::  numero du voisin du sommet k
  !! LogFac(k)      ::  contrainte de la face fronti�re k
  !!
  !!      MATRICE CSR : Jvcell, Jposi, Vals (, ILUVals), Diag, IvPos
  !! Jvcell(k)           ::  indice de colonne du bloc k dans le stockage CSR
  !! IvPos(k)            ::  acces direct aux colonnes
  !! JPosi(i)            ::  indice dans le stockage CSR du premier bloc non nul de la ieme ligne.
  !! IDiag               ::  indice dans le stockage CSR du bloc diag de la ieme ligne.
  !! (ILU)Vals(:, :, ij) ::  bloc B(i, j) avec ij dans [Jposi(i), Jposi(i+1)-1] et j=Jvcell(ij)
  !!
  !! Remarque 1: Les structures de nos matrices sont SYMETRIQUES.
  !! Remarque 2: Preconditionneur ILU (0) !!!!!!!!!!!!!!!!!!!!!!!
  !==============================================================

  INTEGER, PARAMETER, PRIVATE :: NONE = -100
  LOGICAL, PRIVATE, PARAMETER:: see_alloc=.FALSE.
  TYPE, PRIVATE :: quad
     INTEGER :: kmin, kmed, kmax, ifac
  END TYPE quad
  TYPE cellule
     INTEGER                :: val
     TYPE(cellule), POINTER :: suiv
  END TYPE cellule

  INTEGER, SAVE, PRIVATE :: search_cnt = 0
  INTEGER, SAVE, PRIVATE :: nb_search = 0
  INTEGER, SAVE, PRIVATE :: old_search = 0
  INTEGER, SAVE, PRIVATE :: hdebug = 0

  TYPE(quad), DIMENSION(: ), PRIVATE, SAVE, ALLOCATABLE :: t_ifac

CONTAINS






  !-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !> \brief Recherche des segments du maillage
  !! une methode qui ressemble a SegmentsSinusFr2D de loin; mais la on part des structures (maillage)
  SUBROUTINE GeomSegments2d( Mesh, Impre)
    CHARACTER(LEN = *), PARAMETER :: mod_name = "GeomSegments2d"
    ! ****************
    !   declarations
    ! ***************
    ! variables d'appel
    TYPE(Maillage), INTENT(INOUT)              :: Mesh
    INTEGER,       INTENT(IN)                 :: Impre

    ! variables locales
    TYPE(element)                    :: e
    TYPE(arete)                      :: ed
    INTEGER                          :: Npoint, Nelemt, NFacFr
    INTEGER                          :: Info
    INTEGER                          :: is, js, iv, jt, k, is1, is2, jt1, jt2
    INTEGER                          :: is_min, is_max, jseg, nseg
    INTEGER                          :: next, noth, ntyp

    INTEGER, DIMENSION(: ),   POINTER                :: NewLogfac
    INTEGER, DIMENSION(: ),   POINTER                :: NumFac !-- CCE CCE
    INTEGER, DIMENSION(:, : ), POINTER               :: NewNsfacFr, NsfacFr
    TYPE(frontiere), DIMENSION(:), POINTER           :: NewFr
    INTEGER, DIMENSION(: ),   POINTER                :: Logfac
    TYPE(cellule), POINTER                           :: NewCell, PtCell, PtCellPred
    TYPE(cellule), DIMENSION(: ), POINTER            :: hashtable
    INTEGER, DIMENSION(: ), ALLOCATABLE, SAVE        :: NSupVois
    REAL(DP), DIMENSION(:, : ), ALLOCATABLE, SAVE        :: Coor
    LOGICAL, SAVE                                    :: initialise = .FALSE.

    ! ***************************
    !   initialisations
    ! ***************************

    PRINT*, mod_name
    Npoint = Mesh%Ns

    WRITE(*,*) mod_name, ' Npoint ', Npoint

    IF (.NOT. initialise) THEN


       IF (ALLOCATED(NSupVois)) THEN
          DEALLOCATE(NSupVois)
       END IF
       IF (ALLOCATED(Coor)) THEN
          DEALLOCATE(Coor)
       END IF


       ALLOCATE(NSupVois(Mesh%ns), STAT = Info) 
       IF (Info /= 0) THEN
          WRITE(6, *) mod_name, " ERREUR : Apres Allocation NSupVois, Info =", Info
          STOP
       END IF

       ALLOCATE(Coor(Mesh%Ndim, Mesh%Ns), STAT = Info)
       IF (Info /= 0) THEN
          WRITE(6, *) mod_name, " ERREUR : Apres Allocation Coor, Info =", Info
          STOP
       END IF

    END IF

    Nelemt = Mesh%Nt
    NFacFr = Mesh%Nsegfr

    IF ( NFacFr > 0 ) THEN
       NULLIFY( NsfacFr, Logfac )
       NsfacFr => Mesh%NsfacFr !Mesh%NsfacFr(1: 2, 1: NFacFr)
       Logfac  => Mesh%LogFac   !Mesh%LogFac(1: NFacFr)

    END IF

    WRITE(*,*) " Mesh%Npoint, Mesh%Nelemt, Mesh%NFacFr", Mesh%Ns, Mesh%Nt, Mesh%Nsegfr

    ALLOCATE(hashtable(1: Npoint), STAT = Info)
    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Apres Allocation Hashtable, Info =", Info
    END IF

    IF (Info /= 0) THEN
       WRITE(*,*) mod_name, " ERREUR : Apres Allocation Hashtable, Info =", Info
       STOP
    END IF



    DO is = 1, Npoint
       hashtable(is)%val = is
       NULLIFY(hashtable(is)%suiv)
    END DO

    NSupVois(1:Npoint) = 0
    Mesh%Nsegmt = 0

    ! OK OK OK
    ! *******************************************
    !   construction de hashtable, NSupVois, Nsegmt
    ! *******************************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " size(NSupVois) == ", SIZE(NSupVois)
       WRITE(6, *) mod_name, " Construction de la table des segments hashtable ..."
    END IF

    ! construction de la table des segments

    NULLIFY(PtCell, PtCellPred )

    DO jt = 1, Nelemt

       e=mesh%e(jt)
       ntyp=e%Nvertex
       DO k = 1, e%Nvertex 

          next = MOD(k, ntyp) + 1
          is1  = e%Nu(   k)
          is2  = e%Nu(next)

          is_min = MIN(is1, is2)
          is_max = MAX(is1, is2)

          ! recherche de la position d'insertion de is_max dans
          ! le tableau hashtable(is_min). on utilise une
          ! recherche sequentielle simple.
          ! initialisation des pointeurs
          PtCell     => hashtable(is_min)%suiv
          PtCellPred => hashtable(is_min)

          ! on recherche la place du nouveau voisin
          DO
             IF (.NOT. ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             IF (PtCell%val > is_max) THEN
                EXIT
             END IF
             ! on a trouve la place, on arrete
             PtCellPred => PtCell
             PtCell     => PtCell%suiv
          END DO

          ! on teste si le sommet n'est pas deja la
          IF (PtCellPred%val < is_max) THEN
             ! creation de la nouvelle cellule
             NULLIFY(NewCell)
             ALLOCATE(NewCell)
             NewCell%val  = is_max

             ! insertion de la nouvelle cellule
             NewCell%suiv    => PtCell
             PtCellPred%suiv => NewCell

             ! on a rajoute un voisin, donc on incremente
             ! NSupVois(is_min) et Nsegmt
             NSupVois(is_min) = NSupVois(is_min) + 1
             Mesh%Nsegmt = Mesh%Nsegmt + 1 !-- 2D

          END IF
       END DO

    END DO

    ! le tableau temporaire hashtable est constitue,
    ! on le recopie dans le tableau Nubo
    ! stop

    ! ****************************
    !   creation du tableau Nubo
    ! ****************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Remplissage du tableau Nubo ... Nsegmt = ", Mesh%Nsegmt
    END IF

    ! allocation memoire
    NULLIFY(Mesh%nubo)
    ALLOCATE(Mesh%Nubo(2, Mesh%Nsegmt))

    ! compteur pour les segments frontieres
    next = 0

    ! allocation memoire pour le tableau NewLogfac = LogFac trie
    IF ( NFacFr > 0 ) THEN
       NULLIFY(NewLogfac, NewNsfacFr, NumFac )
       ALLOCATE(NewLogfac(1: NFacFr), NewNsfacFr(1: 2, 1: NFacFr), NumFac(1: NFacFr) )
       NULLIFY(NewFr)
       ALLOCATE(NewFr(NfacFr))
    END IF
    !OK OK OK

    k = 0
    DO is = 1, Npoint
       ! si le sommet is a un(des) voisin(s)
       IF (NSupVois(is) /= 0) THEN
          PtCell => hashtable(is)%suiv
          ! on prend tous les voisins du sommet is
          DO iv = 1, NSupVois(is)
             js            = PtCell%val
             k             = k + 1
             IF ( NFacFr > 0 ) THEN

                IF (SegmentFr(is, js, NFacFr, NsfacFr, jseg)) THEN
                   next                 = next + 1
                   NewNsfacFr(1: 2, next) = NsfacFr(1: 2, jseg)
                   NewFr(next)         = Mesh%fr(jseg)
                   NumFac(next)         = k
                   NewLogfac(next)      = Mesh%LogFac(jseg)
                END IF
             END IF
             Mesh%Nubo(1, k) = is
             Mesh%Nubo(2, k) = PtCell%val
             ! on pointe sur le voisin suivant
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    ! Mesh%LogFac     : OK
    ! NewLogfac       : OK
    ! ici, next doit etre egal a NFacFr
    ! et noth doit etre egal a Nsegmt
    IF ( next /= NFacFr ) THEN
       WRITE(6, *) mod_name, " Probleme !!!!!  !!!!!NFacFr_trouve= ", next
       WRITE(6, *) mod_name, " ERREUR : Probleme !!!!!  !!!!!NFacFr_real(dp)  = ", NFacFr

       STOP

    ELSE
       IF ( Impre > 2 ) THEN
          WRITE(6, *) mod_name, " Ok OK OK !!!!!  NFacFr_trouve= ", next
       END IF
       IF ( Impre > 2 ) THEN
          WRITE(6, *) mod_name, " Ok OK OK !!!!!  NFacFr_real  = ", NFacFr
       END IF
    END IF

    !maintenant que la numerotation dans Nubo est correcte,
    !on construit les tableaux Nuseg, Nusv, Nutv

    ! ***************************************
    !   construction de Nuseg, Nusv et NutvNewLogfac
    ! ***************************************

    IF ( Impre > 2 ) THEN
       WRITE(6, *)
       WRITE(6, *) mod_name, " Stockage des tableaux Nuseg, Nusv et Nutv .."
    END IF

    ! initialisation des tableaux Nuseg, Nusv et Nutv
    ALLOCATE(Mesh%Nuseg(1: 4, 1: Nelemt))
    Mesh%Nuseg(1: 4, 1: Nelemt) = 0
    ALLOCATE(Mesh%Nusv(1: 2, 1: Mesh%Nsegmt))
    Mesh%Nusv(1: 2, 1: Mesh%Nsegmt) = 0
    ALLOCATE(Mesh%Nutv(1: 2, 1: Mesh%Nsegmt))
    Mesh%Nutv(1: 2, 1: Mesh%Nsegmt) = 0
    ! OK OK OK

    DO jt = 1, Nelemt
       e= Mesh%e(jt)
       ntyp = e%Nvertex

       DO k = 1, e%Nvertex
          next = MOD(   k, ntyp) + 1
          noth = MOD(next, ntyp) + 1
          is1 = e%Nu(   k)
          is2 = e%Nu(next)
          jseg = WhichSeg(1, Mesh%Nsegmt, Mesh%Nubo, is1, is2)
          IF ( jseg == 0 ) THEN
             CYCLE
          END IF
          Mesh%Nuseg(k, jt) = jseg
          IF (Mesh%Nutv(1, Mesh%Nuseg(k, jt)) == 0) THEN
             Mesh%Nutv(1, Mesh%Nuseg(k, jt)) = jt
             Mesh%Nusv(1, Mesh%Nuseg(k, jt)) = e%Nu(noth)
          ELSE
             IF (Mesh%Nutv(2, Mesh%Nuseg(k, jt)) == 0) THEN
                Mesh%Nutv(2, Mesh%Nuseg(k, jt)) = jt
                Mesh%Nusv(2, Mesh%Nuseg(k, jt)) = e%Nu(noth)
             ELSE
                WRITE(6, *) mod_name, " ERREUR : Probleme dans la construction des Nutv"
                STOP
             END IF
          END IF
       END DO
    END DO

    ! Nettoyage memoire !!
    DO is = 1, Npoint
       IF (NSupVois(is) /= 0) THEN
          PtCellPred => hashtable(is)%suiv
          PtCell => PtCellPred%suiv
          DO
             DEALLOCATE(PtCellPred)
             IF (.NOT.ASSOCIATED(PtCell)) THEN
                EXIT
             END IF
             PtCellPred => PtCell
             PtCell => PtCell%suiv
          END DO
       END IF
    END DO

    IF ( Impre > 2 ) THEN
       WRITE(6, *) mod_name, " Stockage des tableaux Nuseg, Nusv et Nutv ..."
    END IF

!!!! Construction de mesh%edge
    ALLOCATE(Mesh%edge(Mesh%Nsegmt))
    DO jseg =  1, Mesh%Nsegmt
       jt1= Mesh%nutv(1,jseg)
       jt2= Mesh%nutv(2,jseg)

       ed%jt1=jt1; ed%jt2=jt2

       e=Mesh%e(jt1)
       SELECT CASE( e%nvertex)
       CASE(3,4)! P1,Q1
          ed%nsommets=2
          !         ed%nvertex =2
       CASE(6,9)!P2,Q2,B2
          ed%nsommets=3
          !          ed%nvertex =2
       CASE(10,16)!P3,Q3,B3
          ed%nsommets=4
       END SELECT
       ed%itype=e%itype



       ! maintenant, on traite les eventuels cas particulier
       SELECT CASE (ed%nsommets)
       CASE(2)
          ALLOCATE(ed%nu(2,2))
          ALLOCATE(ed%coor(2,2))
       CASE(3)
          ALLOCATE(ed%nu(3,2))
          ALLOCATE(ed%coor(2,3))
       CASE(4)
          ALLOCATE(ed%nu(4,2))
          ALLOCATE(ed%coor(2,4))
       CASE default
          PRINT*
          PRINT*, mod_name,"L=408, cas non prevu", ed%nsommets

       END SELECT

       IF (jt1>0) CALL ed_ge(ed, mesh%e(jt1), Mesh%nubo(:,jseg),1)
       IF (jt2>0) CALL ed_ge(ed, mesh%e(jt2), mesh%nubo(:,jseg),2)

       ! on compresse un peu:
       ed%bord=.FALSE.

       IF (jt1==0.OR.jt2==0) THEN

          ed%bord=.TRUE.
          IF(jt1==0) THEN
             ed%nu(:,1)=ed%nu(:,2)


          ELSE

             ed%nu(:,2)=ed%nu(:,1)
          ENDIF
          ed%jt1= ed%jt1+ed%jt2

          ed%jt2=ed%jt1

       ENDIF
       ALLOCATE(ed%n(2))
       !       ALLOCATE(ed%c(2))
       ed%n=ed%normale()
       !      ed%c=ed%centre()
       ed%volume= ed%aire()

       CALL ed%quadrature()
       Mesh%edge(jseg)=ed

    ENDDO
!!!! end Construction de mesh%edge

!!$    !! construction of Mesh%neig: for a given element, what are the neigboring elements
!!$    ALLOCATE(Mesh%neig(Nelemt))
!!$    Mesh%nvois_max=0
!!$    DO jt=1, Nelemt
!!$       nseg=COUNT(Mesh%Nuseg(:,jt)/=0) ! how many edges (takes into accout tri, quad)
!!$       Mesh%nvois_max=max(Mesh%nvois_max, nseg )
!!$       ALLOCATE(Mesh%neig(jt)%vois(nseg),Mesh%neig(jt)%edge(nseg) )
!!$       DO jseg=1,nseg
!!$          jt1=Mesh%edge(Mesh%Nuseg(jseg,jt))%jt1
!!$          jt2=Mesh%edge(Mesh%Nuseg(jseg,jt))%jt2
!!$          Mesh%neig(jt)%edge(jseg)=Mesh%Nuseg(jseg,jt) ! which edge are we on ?
!!$          IF(jt1==jt)THEN
!!$             Mesh%neig(jt)%vois(jseg)=jt2  ! previous was opposite
!!$          ELSE
!!$             Mesh%neig(jt)%vois(jseg)=jt1 ! warning: if we are on the boundary, jt2=jt
!!$             ! previous was opposite
!!$
!!$          ENDIF
!!$       ENDDO
!!$    ENDDO
!!$
!!$    !! end construction mesh%neig

!!$    PRINT*, mod_name,Mesh%Nsegmt
!!$    DO jseg=1, Mesh%NsegFR
!!$       PRINT*,"jseg=",jseg, "numfac=",NumFac(jseg)
!!$       PRINT*,"nutv=", Mesh%nutv(:, NumFac(jseg))
!!$       PRINT*,"NsFacFr=", Mesh%NsFacFr(:, jseg), Mesh%fr(jseg)%nu
!!$       PRINT*, "New=", NewNsfacFr(1: 2,jseg)
!!$       !       print*, Mesh%nusv(:,jseg),Mesh%nusv(:, NumFac(jseg))
!!$       jt=Mesh%nutv(1, NumFac(jseg))
!!$       PRINT*, "e%nu=", Mesh%e(jt)%nu
!!$       PRINT*
!!$    ENDDO
    CALL boundary_normals()



    DEALLOCATE(hashtable)
    IF ( NfacFr>0 ) THEN
       DEALLOCATE(NewLogfac)
       DEALLOCATE(NewNsfacFr)
       DEALLOCATE(NumFac, NsFacFr)
    ENDIF
    NULLIFY(Mesh%nubo, Mesh%nutv, Mesh%nuseg, Mesh%nusv)
    WRITE(6, *) mod_name, " Sortie de la procedure"

  CONTAINS

    SUBROUTINE boundary_normals()
      IMPLICIT NONE
      CHARACTER(LEN = *), PARAMETER :: mod_name = "boundary_normals"
      INTEGER:: ifr, iseg, jt, is1, is2, jdeg, k, k1, js1, js2
      REAL(dp):: xija, yija, xGta, yGta
      REAL(dp), DIMENSION(2):: Nij
      
      DO ifr=1, Mesh%NsegFr
         Mesh%fr(ifr)=NewFr(ifr)
      ENDDO
      DEALLOCATE(NewFr)

      DO ifr = 1, Mesh%NsegFr
         iseg   = NumFac(ifr)
         jt     = Mesh%nutv(2, iseg)
         IF (jt /= 0 ) THEN
            CYCLE
         END IF

         is1    = Mesh%fr(ifr)%nu(1)
         is2    = Mesh%fr(ifr)%nu(2)
         !
         xija   = mesh%fr(ifr)%Coor(1, 1)
         yija   = mesh%fr(ifr)%Coor(2, 1)

         xGta   = mesh%fr(ifr)%Coor(1, 2)
         yGta   = mesh%fr(ifr)%Coor(2, 2)


         !

         Nij  =    (/ yGta - yija, - xGta + xija /) 


         jt     =  Mesh%nutv(1, iseg)
         jdeg   =  Mesh%e(jt)%nvertex
         mesh%fr(ifr)%nvertex_jt1= Mesh%e(jt)%nvertex
!
         DO k = 1, jdeg ! we must have the interior normal
            js1 = Mesh%e(jt)%nu(k)
            k1  = MOD(k, jdeg) + 1 !-- %%%% Modulo
            js2 = Mesh%e(jt)%nu(k1)
            IF (js1 == is2 .AND. js2 == is1 ) THEN
               Mesh%fr(ifr)%is1=k1! which local numbering of is1
               Mesh%fr(ifr)%is2=k! and is2
               Nij(1: 2)   = - Nij(1: 2)
               EXIT
            END IF
            IF(js1==is1 .AND. js2==is2) THEN
               Mesh%fr(ifr)%is1=k! which local numbering of is1
               Mesh%fr(ifr)%is2=k1! and is2
                EXIT
            ENDIF
         END DO
         !

         Mesh%fr(ifr)%jt1= Mesh%nutv(1, iseg)! the element on the other par of the frontieer
 
         !
         Mesh%fr(ifr)%n(1: 2)    = Nij(1: 2) !????+ should be interior normal
          !
      END DO
      !      stop
    END SUBROUTINE boundary_normals

    SUBROUTINE ed_ge( ed,e,nubo,nbre)
      CHARACTER(LEN = *), PARAMETER :: mod_name ="ed_ge"
      !> \brief To compute the integrals of the jumps and to assign them properly, we need to
      !! know which local number have the degrees of freedom on an edge, in both neighboring elements
      !! this routine also send the coordinates of the dof on this edge
      !! This routine is specialised to 2D.
      !!
      !! Triangles
      INTEGER, DIMENSION(3), PARAMETER:: ip=(/1,2,3/)
      INTEGER, DIMENSION(3), PARAMETER:: ip1=(/2,3,1/)
      INTEGER, DIMENSION(3), PARAMETER:: ip2=(/4,5,6/)

      INTEGER, DIMENSION(3), PARAMETER:: iq1=(/4,6,8/)
      INTEGER, DIMENSION(3), PARAMETER:: iq2=(/5,7,9/)
      !!
      !! Quadrangle
      !!
      INTEGER, DIMENSION(4), PARAMETER:: ipp=(/1,2,3,4/)
      INTEGER, DIMENSION(4), PARAMETER:: ipp1=(/2,3,4,1/)
      INTEGER, DIMENSION(4), PARAMETER:: ipp2=(/5,6,7,8/)

      INTEGER, DIMENSION(4), PARAMETER:: iqq1=(/5,7,9,11/)
      INTEGER, DIMENSION(4), PARAMETER:: iqq2=(/6,8,10,12/)
!!!!!!
      TYPE(arete), INTENT(inout):: ed
      TYPE(element), INTENT(in):: e
      INTEGER, INTENT(in):: nbre
      INTEGER, DIMENSION(2), INTENT(in):: nubo
!!!!!
      INTEGER:: i1, i2, i, n1, n2, i3, i4
      n1=ed%nsommets; n2=e%nsommets
      SELECT CASE(e%nvertex)
      CASE(3) ! This is a triangle
         !***********************************
         !   This is a triangle
         !***********************************

         SELECT CASE(n1)
         CASE(2)

            DO i=1, 3
               i1=e%nu(ip(i)); i2=e%nu(ip1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%nu(1,nbre)=ip(i)
                  ed%nu(2,nbre)=ip1(i)
                  ed%coor(:,1)=e%coor(:,ip(i))
                  ed%coor(:,2)=e%coor(:,ip1(i))
                  ed%edge(nbre)=i
               ENDIF
            ENDDO

         CASE(3)

            DO i=1,3 ! loop over edges
               i1=e%nu(ip(i)); i2=e%nu(ip1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%coor(:,1)=e%coor(:,ip(i))
                  ed%coor(:,2)=e%coor(:,ip1(i))
                  ed%coor(:,3)=e%coor(:,ip2(i))
                  ed%nu(1,nbre)=ip(i)
                  ed%nu(2,nbre)=ip1(i)
                  ed%nu(3,nbre)=ip2(i)
                  ed%edge(nbre)=i
               ENDIF
            ENDDO

         CASE(4)
            DO i=1,3 ! loop over edges
               i1=e%nu(ip(i)); i2=e%nu(ip1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%coor(:,1)=e%coor(:,ip(i))
                  ed%coor(:,2)=e%coor(:,ip1(i))
                  ed%coor(:,3)=e%coor(:,iq1(i))
                  ed%coor(:,4)=e%coor(:,iq2(i))
                  ed%nu(1,nbre)=ip(i)
                  ed%nu(2,nbre)=ip1(i)
                  ed%nu(3,nbre)=iq1(i)
                  ed%nu(4,nbre)=iq2(i)
                  ed%edge(nbre)=i
               ENDIF
            ENDDO
         CASE default
            PRINT*, mod_name, " wrong element type L432 ", n1
         END SELECT
         IF (nbre==2) THEN ! parcours les edges dans des sens differents
            SELECT CASE(SIZE(ed%nu,1))
            CASE(2)
            CASE(3)
               i1=ed%nu(1,2)
               ed%nu(1,2)=ed%nu(2,2)
               ed%nu(2,2)=i1
            CASE(4)
               !???????
               i1=ed%nu(1,2);i2=ed%nu(2,2);i3=ed%nu(3,2);i4=ed%nu(4,2)
               ed%nu(1,2)=i2
               ed%nu(2,2)=i1
               ed%nu(3,2)=i4
               ed%nu(4,2)=i3
            END SELECT
         ENDIF
      CASE(4) ! This is a quad
         !***********************************
         !   This is a quadrangle
         !***********************************
         SELECT CASE(n1)
         CASE(2)

            DO i=1, 4
               i1=e%nu(ipp(i)); i2=e%nu(ipp1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%nu(1,nbre)=ipp(i)
                  ed%nu(2,nbre)=ipp1(i)
                  ed%coor(:,1)=e%coor(:,ipp(i))
                  ed%coor(:,2)=e%coor(:,ipp1(i))
               ENDIF
            ENDDO

         CASE(3)

            DO i=1,4 ! loop over edges
               i1=e%nu(ipp(i)); i2=e%nu(ipp1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%coor(:,1)=e%coor(:,ipp(i))
                  ed%coor(:,2)=e%coor(:,ipp1(i))
                  ed%coor(:,3)=e%coor(:,ipp2(i))
                  ed%nu(1,nbre)=ipp(i)
                  ed%nu(2,nbre)=ipp1(i)
                  ed%nu(3,nbre)=ipp2(i)
               ENDIF
            ENDDO

         CASE(4)
            DO i=1,4 ! loop over edges
               i1=e%nu(ipp(i)); i2=e%nu(ipp1(i))
               IF( nubo(1)==i1.AND.nubo(2)==i2.OR. nubo(1)==i2.AND.nubo(2)==i1) THEN
                  ed%coor(:,1)=e%coor(:,ipp(i))
                  ed%coor(:,2)=e%coor(:,ipp1(i))
                  ed%coor(:,3)=e%coor(:,iqq1(i))
                  ed%coor(:,4)=e%coor(:,iqq2(i))
                  ed%nu(1,nbre)=ipp(i)
                  ed%nu(2,nbre)=ipp1(i)
                  ed%nu(3,nbre)=iqq1(i)
                  ed%nu(4,nbre)=iqq2(i)
               ENDIF
            ENDDO
         CASE default
            PRINT*, mod_name, " wrong element type L432 ", n1
         END SELECT
         IF (nbre==2) THEN ! parcours les edges dans des sens differents
            SELECT CASE(SIZE(ed%nu,1))
            CASE(2)
            CASE(3)
               i1=ed%nu(1,2)
               ed%nu(1,2)=ed%nu(2,2)
               ed%nu(2,2)=i1
            CASE(4)
               !???????
               i1=ed%nu(1,2);i2=ed%nu(2,2);i3=ed%nu(3,2);i4=ed%nu(4,2)
               ed%nu(1,2)=i2
               ed%nu(2,2)=i1
               ed%nu(3,2)=i4
               ed%nu(4,2)=i3
            END SELECT
         ENDIF
      END SELECT
    END SUBROUTINE ed_ge


    !> \brief Recherche d'un num�ro de segment 2D �tant donn� les num�ros des 2 sommets
    !! Fonction � double r�sultat (donc effet de bord %%%%) :: trouv� et le num�ro de segment
    FUNCTION SegmentFr(is1, is2, NFacFr, NsfacFr, jseg) RESULT(is_seg_fr)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "SegmentFr"
      ! variables d'appel
      INTEGER, INTENT(IN)          :: is1, is2, NFacFr
      INTEGER, INTENT(OUT)         :: jseg !-- %%%%%%%%%%%%%%%%%%%%%%%%%%%% Argument OUT d'une fonction
      INTEGER, DIMENSION(:, : ), POINTER :: NsfacFr
      LOGICAL :: is_seg_fr
      ! variables locales
      INTEGER              :: k, is_min, is_max

      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      jseg = 0

      DO k = 1, NFacFr
         !IF ((NsfacFr(1, k) == is_min) .AND. (NsfacFr(2, k) == is_max)) THEN ! dante ---
         IF( ( NsfacFr(1, k) == is1 .AND.NsfacFr(2, k) == is2 ) .OR. &
              ( NsfacFr(2, k) == is1 .AND.NsfacFr(1, k) == is2 ) ) THEN
            jseg = k
            EXIT
         END IF
      END DO

      is_seg_fr = jseg /=0

    END FUNCTION SegmentFr

    !> \brief Recherche dichotomique d'un segment entre 2 sommets, la recherche ayant lieu dans le tableau nubo
    !! Ensuite recherche dichotomique croissante ou d�croissante du 2�me sommet (on parcourt les segments qui bordent l'�l�ment)
    FUNCTION WhichSeg(idebut, ifin, Nubo, is1, is2) RESULT(i_seg)
      CHARACTER(LEN = *), PARAMETER :: mod_name = "WhichSeg"

      ! variables d'appel
      INTEGER, INTENT(IN)              :: is1, is2, idebut, ifin
      INTEGER, DIMENSION(:, : ), POINTER :: Nubo
      INTEGER :: i_seg

      ! variables locales
      INTEGER              :: jseg2,  debut, Fin, incr, is_min, is_max 

      ! initialisation des variables
      i_seg = -2000000000 !-- pour faire taire l'analyse de flot
      is_min = MIN(is1, is2)
      is_max = MAX(is1, is2)
      debut = idebut
      Fin   = ifin
      jseg  = 0
      !-- WhichSeg = 0 ici ???? %%%%
      IF ( ifin == 0 ) THEN
         RETURN
      END IF

      ! recherche du premier point is1 dans Nubo
      DO
         jseg2 = jseg
         jseg  = (debut + Fin) / 2

         ! fin de la recherche, on sort
         IF ((Nubo(1, jseg) == is_min) .OR. (jseg == jseg2)) THEN
            EXIT
         END IF

         IF (Nubo(1, jseg) < is_min) THEN
            debut = jseg + 1
         ELSE
            Fin = jseg
         END IF

      END DO

      ! si is_min est dans Nubo, on cherche is_max
      IF (Nubo(1, jseg) == is_min) THEN

         ! ruse pour forcer jseg a diminuer ou augmenter
         IF (Nubo(2, jseg) > is_max) THEN
            incr = - 1
         ELSE
            incr = 1
         END IF

         ! recherche de is_max
         DO
            IF (  Nubo(2, jseg) == is_max ) THEN
               i_seg = jseg
               EXIT
            END IF

            IF (  Nubo(1, jseg) /= is_min ) THEN
               i_seg = 0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/1"
               STOP
            END IF

            jseg = jseg + incr
            IF ( jseg < idebut .OR. jseg > ifin ) THEN
               i_seg = 0
               PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/2"
               STOP
            END IF
         END DO
      ELSE
         i_seg = 0
         PRINT *, mod_name, " ERREUR : le [is_min, is_max] n'est pas dans Nubo !!!! ARRET/3"
         STOP
      END IF

    END FUNCTION WhichSeg

  END SUBROUTINE GEOMSEGMENTS2D


END MODULE GeomGraph
